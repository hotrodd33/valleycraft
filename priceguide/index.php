<html>
  <head>
    <title>Valley Craft 2015 Pricing Guide</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width-device-width" />
    <link rel="stylesheet" type="text/css" href="css/style.css" />
	
  </head>
  <body>
    <div id="pricebook">
	<div id="page-header"><a href="index.php?q=2"><img src="/images/valley-craft-logo-transparent.png" alt="Valley Craft Industries" style="width: 200px; float: left;"></a>
    <h1><span class="pageHeader">Valley Craft Industries Online Price Guide</span></h1>
	</div>
	<div id="sidebar-left" class="NoPrint">
    <div class="main_category">
	<p><h2>Select a Category</h2></p>
      
      <div>
       <ul class="button"> 
		<li class="listheader" value="1605" onclick="showCategory(this.value)" style="color: #000;background:#ccc;">NEW! PAL 500 - Aluminum Lift</li>
		<li class="listheader" value="1606" onclick="showCategory(this.value)" style="color: #000;background:yellow;">NEW! PRO VAULT Steel Tool Chest</li>
		<li class="listheader" value="1608" onclick="showCategory(this.value)" style="color: #000;background:#73c267;">NEW! EZ CART Construction Hopper</li>
		<li class="listheader" value="1613" onclick="showCategory(this.value)" style="color: #000;background:#73c267;">NEW! 1,000 lb Capacity Trailer</li>
		</ul>
      </div>
	  
	  <div class="main_heading">
        <a id="myHeader1" href="javascript:showonlyone('sub1');" ><h1>Drum Handling</h1></a>
      </div>
      <div class="newboxes" id="sub1">
        <ul class="button">
          <li class="listsubheader gray" value="15" onclick="showCategory(this.value)">Fork Lift Attachments</li>
		  <li class="listsubheader gray" value="16" onclick="showCategory(this.value)">Portable Lifts</li>
          <li class="listsubheader" value="1526" onclick="showCategory(this.value)">Power Drive</li>
          <li class="listsubheader" value="1527" onclick="showCategory(this.value)">Manual Drive</li>
          <li class="listsubheader" value="1519" onclick="showCategory(this.value)">Level Lift Forward Dump</li>
          <li class="listsubheader" value="1536" onclick="showCategory(this.value)">Drum Positioners</li>
		  <li class="listsubheader gray" value="17" onclick="showCategory(this.value)">Drum Dumpers</li>
          <li class="listsubheader" value="1529" onclick="showCategory(this.value)">Low Level</li>
          <li class="listsubheader" value="1530" onclick="showCategory(this.value)">High Level</li>
          <li class="listsubheader gray" value="18" onclick="showCategory(this.value)">Drum Trucks</li>
          <li class="listsubheader" value="1537" onclick="showCategory(this.value)">Aluminum</li>
          <li class="listsubheader" value="1538" onclick="showCategory(this.value)">Steel</li>
          <li class="sublist" value="1539" onclick="showCategory(this.value)">Chime Hook Options</li>
          <li class="listsubheader" value="1504" onclick="showCategory(this.value)">Drum Dollies</li>
        </ul>
      </div>
    </div>
    <div class="main_category">
      <div class="main_heading">
        <a id="myHeader2" href="javascript:showonlyone('sub2');" ><h1>Material Handling</h1></a>
      </div>
	  <div class="newboxes" id="sub2">
      <ul class="button">
        <li class="listheader">Hand Trucks</li>
        <li class="listsubheader gray" value="73" onclick="showCategory(this.value)">Distribution Trucks</li>
        <li class="listsubheader" value="1540" onclick="showCategory(this.value)">Flat Back</li>
        <li class="listsubheader" value="1541" onclick="showCategory(this.value)">Curved Back</li>
        <li class="listsubheader gray" value="74" onclick="showCategory(this.value)">Pallet Trucks</li>
        <li class="listsubheader" value="1542" onclick="showCategory(this.value)">Aluminum</li>
        <li class="listsubheader" value="1543" onclick="showCategory(this.value)">Steel</li>
        <li class="listsubheader gray" value="75" onclick="showCategory(this.value)">Cylinder Trucks</li>
        <li class="listsubheader gray" value="90" onclick="showCategory(this.value)">Specialty Trucks</li>
        <li class="listheader">Carts &amp; Trucks</li>
        <li class="listsubheader gray" value="91" onclick="showCategory(this.value)">Modular A-Frame</li>
        <li class="listsubheader" value="1550" onclick="showCategory(this.value)">Carts</li>
        <li class="listsubheader" value="1551" onclick="showCategory(this.value)">End Panel/Floor Lock</li>
        <li class="listsubheader" value="1552" onclick="showCategory(this.value)">Other Accessories</li>
        <li class="listsubheader gray" value="92" onclick="showCategory(this.value)">Heavy Duty A-Frame</li>
        <li class="listsubheader gray" value="93" onclick="showCategory(this.value)">Security Trucks</li>
        <li class="listsubheader gray" value="94" onclick="showCategory(this.value)">Platform Trucks</li>
        <li class="listsubheader gray" value="95" onclick="showCategory(this.value)">Waste &amp; Chip</li>
        <li class="listsubheader gray" value="96" onclick="showCategory(this.value)">EZ Dump</li>
		<li class="listheader">Box Dumpers</li>
		<li  class="listsubheader gray"value="22" onclick="showCategory(this.value)">Box Dumpers</li>
		<li class="listheader">Nursery</li>
		<li  class="listsubheader gray"value="1511" onclick="showCategory(this.value)"><span style="color:red">NEW! </span>Nursery Trucks</li>		
		<li class="listheader">Trailers</li>
		<li class="listsubheader gray" value="1613" onclick="showCategory(this.value)"><span style="color:red">NEW! </span>1000 lb capacity</li>
        <li class="listsubheader" value="1614" onclick="showCategory(this.value)">Decks</li>
        <li class="listsubheader" value="1615" onclick="showCategory(this.value)">Wheels</li>
        <li class="listsubheader" value="1616" onclick="showCategory(this.value)">Drawbars and Hitches</li>
        <li class="listsubheader gray" value="1554" onclick="showCategory(this.value)">2000 lb capacity</li>
        <li class="listsubheader" value="1558" onclick="showCategory(this.value)">Decks</li>
        <li class="listsubheader" value="1559" onclick="showCategory(this.value)">Wheels</li>
        <li class="listsubheader" value="1560" onclick="showCategory(this.value)">Drawbars and Hitches</li>
        <li class="listsubheader gray" value="1555" onclick="showCategory(this.value)">4000 lb capacity</li>
        <li class="listsubheader" value="1562" onclick="showCategory(this.value)">Decks</li>
        <li class="listsubheader" value="1563" onclick="showCategory(this.value)">Wheels</li>
        <li class="listsubheader" value="1564" onclick="showCategory(this.value)">Drawbars and Hitches</li>
        <li class="listsubheader gray" value="1556" onclick="showCategory(this.value)">6000 lb capacity</li> 
        <li class="listsubheader" value="1566" onclick="showCategory(this.value)">Decks</li>
        <li class="listsubheader" value="1567" onclick="showCategory(this.value)">Wheels</li>
        <li class="listsubheader" value="1568" onclick="showCategory(this.value)">Drawbars and Hitches</li>       
        <li class="listsubheader gray" value="1557" onclick="showCategory(this.value)">8000 lb capacity</li>
        <li class="listsubheader" value="1570" onclick="showCategory(this.value)">Decks</li>
        <li class="listsubheader" value="1571" onclick="showCategory(this.value)">Wheels</li>
        <li class="listsubheader" value="1572" onclick="showCategory(this.value)">Drawbars and Hitches</li>
      </ul>
	  </div>
    </div>
    <div class="main_category">
      <div class="main_heading">
        <a id="myHeader3" href="javascript:showonlyone('sub3');" ><h1>Storage Solutions</h1></a>
      </div>
      <div class="newboxes" id="sub3">
        <ul class="button">
		  
		  <li class="listheader" value="23" onclick="showCategory(this.value)">High Capacity Cabinets</li>
          <li class="listsubheader gray" value="1574" onclick="showCategory(this.value)">36 in. Wide</li>
          <li class="listsubheader" value="1578" onclick="showCategory(this.value)">Deep Door Cabinets</li>
          <li class="listsubheader" value="1579" onclick="showCategory(this.value)">Flush Door Cabinets</li>
          <li class="listsubheader" value="1580" onclick="showCategory(this.value)">Heavy Duty Cabinets</li>
          <li class="listsubheader" value="1594" onclick="showCategory(this.value)">Hi-Vis Cabinets</li>
          <li class="listsubheader" value="1595" onclick="showCategory(this.value)">Electronic Locking Cabinets</li>
          <li class="listsubheader" value="1581" onclick="showCategory(this.value)">Cabinet Accessories</li>
          <li class="listsubheader gray" value="1575" onclick="showCategory(this.value)">48 in. Wide</li>
          <li class="listsubheader" value="1582" onclick="showCategory(this.value)">Deep Door Cabinets</li>
          <li class="listsubheader" value="1583" onclick="showCategory(this.value)">Flush Door Cabinets</li>
          <li class="listsubheader" value="1584" onclick="showCategory(this.value)">Heavy Duty Cabinets</li>
          <li class="listsubheader" value="1596" onclick="showCategory(this.value)">Electronic Locking Cabinets</li>
          <li class="listsubheader" value="1585" onclick="showCategory(this.value)">Cabinet Accessories</li>
          <li class="listsubheader gray" value="1576" onclick="showCategory(this.value)">60 in. Wide</li>
          <li class="listsubheader" value="1587" onclick="showCategory(this.value)">Flush Door Cabinets</li>
          <li class="listsubheader" value="1589" onclick="showCategory(this.value)">Cabinet Accessories</li>
          <li class="listsubheader gray" value="1577" onclick="showCategory(this.value)">72 in. Wide</li>
          <li class="listsubheader" value="1591" onclick="showCategory(this.value)">Flush Door Cabinets</li>
          <li class="listsubheader" value="1593" onclick="showCategory(this.value)">Cabinet Accessories</li>
          <li class="listsubheader gray" value="1508" onclick="showCategory(this.value)">Heavy Duty Shelving</li>
          <li class="listsubheader" value="1597" onclick="showCategory(this.value)">Corner Posts</li>
          <li class="listsubheader" value="1598" onclick="showCategory(this.value)">Shelf Packs</li>
          <li class="listsubheader" value="1599" onclick="showCategory(this.value)">Panel Packs</li>
          <li class="listsubheader" value="1600" onclick="showCategory(this.value)">Additional Shelves and Accessories</li>	  
		  <li class="listheader" value="61" onclick="showCategory(this.value)">Mobile Cabinets</li>
          <li class="listsubheader gray" value="61" onclick="showCategory(this.value)">Pro Mobile Cabinets</li>
          <li class="listsubheader" value="1601" onclick="showCategory(this.value)">24 in. Wide</li>
          <li class="listsubheader" value="1602" onclick="showCategory(this.value)">48 in. Wide</li>
          <li class="listsubheader" value="1603" onclick="showCategory(this.value)">Mobile Cabinet Accessories</li>
          <li class="listsubheader gray" value="1604" onclick="showCategory(this.value)">Classic Mobile Utility Cabinets</li>
		  <li class="listheader" value="1606" onclick="showCategory(this.value)" style="color: #000;background:yellow;">NEW! PRO VAULT Steel Tool Chest</li>
		  <li class="listheader" value="26" onclick="showCategory(this.value)">Shop Accessories</li>
          <li class="listsubheader" value="64" onclick="showCategory(this.value)">Shop Desks</li>
          <li class="listsubheader" value="65" onclick="showCategory(this.value)">Work Tables</li>
          <li class="listsubheader" value="66" onclick="showCategory(this.value)">Post Protectors</li>
          <li class="listsubheader" value="67" onclick="showCategory(this.value)">Weld Caddy</li>
          <li class="listsubheader" value="68" onclick="showCategory(this.value)">Adjustable Pallet Stand</li>
          <li class="listsubheader" value="69" onclick="showCategory(this.value)">Adjustable Die Cart</li>
          <li class="listsubheader" value="89" onclick="showCategory(this.value)">Plastic Mini Pallets</li>	
          <li class="listheader" value="1509" onclick="showCategory(this.value)" style="color:white; background:red;">NEW! Collectors Edition Garage</li>
		  <li class="listsubheader" value="1520" onclick="showCategory(this.value)">Deep Door Cabinets</li>
		  <li class="listsubheader" value="1521" onclick="showCategory(this.value)">Flush Door Cabinets</li>
		  <li class="listsubheader" value="1522" onclick="showCategory(this.value)">Tool Storage Cabinets</li>
		  <li class="listsubheader" value="1523" onclick="showCategory(this.value)">Adjustable Work Tables</li>
		  <li class="listsubheader" value="1514" onclick="showCategory(this.value)">24'' Wall Cabinets</li>
		  <li class="listsubheader" value="1518" onclick="showCategory(this.value)">Premium Wall Panels</li>
		  <li class="listsubheader" value="1517" onclick="showCategory(this.value)">Triton Hangers</li>
		  <li class="listsubheader" value="1513" onclick="showCategory(this.value)">Storage Bins</li>
		  
        </ul>
      </div>
    </div>
	
	</div>
	<div id="sidebar-right">
    <div id="txtContent" style="width: 100%">
	<h1>Welcome to the Valley Craft Industries Online Price Guide.</h1>
	<p>At Valley Craft Industries we take our customers and dealers seriously. To make being a dealer easier, we are committed to providing the tools necessary to be able to meet the needs of your customers efficiently. We hope you find this online version of our Price Guide useful and efficient.</p>
	<p>If you are looking for a specific feature or function that is currently not available please let us know by contacting us <a href="mailto:isaaccorey@valleycraft.com">here.</a> Although not all requests can or will be honored, it will be taken into consideration for future upgrades!</p>
	<h1>How to Use</h1>
	<p>Select any category or sub category on the left of your screen to view the products listed in that category. The product listing is sortable by clicking on a column header. You can click on a specific image or product code to get to the individual product specifications page. To print or export the current view just select one of the icons that are above the table.</p>
	
	<p>If you experience any issues or need assistance please contact customer service at 651-345-3386</p></div>
	
	</div>
	</div>
	<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
    <script src="scripts.js"></script> 
   	<script type="text/javascript">
	jQuery(document).ready(function () {
            jQuery(".newboxes").hide(); 
			jQuery("li").click(function(){
				// If this isn't already active
			if (!jQuery(this).hasClass("active")) {
				// Remove the class from anything that is active
			jQuery("li.active").removeClass("active");
			// And make this active
			jQuery(this).addClass("active");
  }
});
            
            
        });
		</script>
		
		<script type="text/javascript">
	jQuery(function () {
		jQuery('.click-nav .no-js .have-second-level-menu').click(function(e) {
			jQuery(this).closest('li').find('.sub-menu').slideToggle(200);
			jQuery('.clicker').toggleClass('active');
			e.stopPropagation();
	});
  
		jQuery('.have-third-level-menu').click(function(e) {
			jQuery(this).closest('li').find('ul').slideToggle(200);
			jQuery('.clicker').toggleClass('active');
			e.stopPropagation();
		});
	});
		</script>
    <script type="text/javascript" src="tableExport.js"></script> 
	<script type="text/javascript" src="holder.js"></script> 
	<script type="text/javascript" src="html2canvas.js"></script> 
    <script type="text/javascript" src="jquery.base64.js"></script> 
    <script type="text/javascript" src="jspdf/libs/sprintf.js"></script> 
    <script type="text/javascript" src="jspdf/jspdf.js"></script> 
    <script type="text/javascript" src="jspdf/libs/base64.js"></script>
    <script src="js/sorttable.js" type="text/javascript" language="javascript"></script>
	
	
  </body>
</html>
