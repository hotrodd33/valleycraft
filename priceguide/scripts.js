function showCategory(str) {
    if (str == "") {
        document.getElementById("txtContent").innerHTML = "";
		
        return;
    } else {
	
        if (window.XMLHttpRequest) {
            // code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
			
        } else {
            // code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			    	
                document.getElementById("txtContent").innerHTML = xmlhttp.responseText;
				sorttable.makeSortable(document.getElementById("pricing"));
				
            }
			
        }
		
        xmlhttp.open("GET","getuser.php?q="+str,true);
        
		xmlhttp.send();
		
		
    }

}


function showonlyone(thechosenone) {
     $('.newboxes').each(function(index) {
          if ($(this).attr("id") == thechosenone) {
               $(this).show(300);
          }
          else {
               $(this).hide(300);
          }
     });
}

function seoUrl($string) {
    //Lower case everything
    $string = strtolower($string);
    //Make alphanumeric (removes all other characters)
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    //Clean up multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s_]/", "-", $string);
    return $string;
}