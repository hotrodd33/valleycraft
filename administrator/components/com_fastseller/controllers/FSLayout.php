<?php
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

class FSLayout {

	static public function prepare() {
		echo "<script type=\"text/javascript\">
		window.addEvent('domready', function(){
			if (JoomlaVersion >= 3.1) {
				var header = document.getElementsByClassName('header')[0],
					subhead = document.getElementsByClassName('subhead-collapse')[0];

				if (header)
					header.destroy();
				if (subhead)
					subhead.destroy();
			} else {
				var toolbar,
					elementbox = document.id('element-box');

				toolbar = document.id('toolbar-box') || document.getElementsByClassName('toolbar-box')[0];
				if (toolbar) toolbar.destroy();
				if (elementbox) elementbox.getFirst().className='Hello';
			}
		});
		</script>";
	}

}
