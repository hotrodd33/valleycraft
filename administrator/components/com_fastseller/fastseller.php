<?php
// No direct access
defined( '_JEXEC' ) or die( 'Restricted access' );

//ini_set('display_errors', 1);
//error_reporting(E_ALL);

$base = JURI::base();
$root = JURI::root();

define('FS_AJAX', $base .'components/com_fastseller/ajax/index.php');
define('FS_PATH', JPATH_BASE .'/components/com_fastseller/');

require(FS_PATH .'controllers/FSConf.php');
FSConf::getConfiguration();

require('defines.php');

$doc = JFactory::getDocument();
$doc->setTitle('Fast Seller');
//$mainframe->getCfg('sitename')

$doc->addStyleSheet($base .'components/com_fastseller/static/css/style.css');
if (JVERSION >= 3.1)
	$doc->addStyleSheet($base .'components/com_fastseller/static/css/joomla3.css');
// Editor loads mootools too.
//$doc->addScript($root .'media/system/js/mootools-core.js');
//$doc->addScript($root .'media/system/js/mootools-more.js');
//JHTML::_('behavior.mootools', 'more');
JHTML::_('behavior.framework', 'more');

if (JVERSION < 3)
	$doc->addCustomTag('<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>');
$doc->addCustomTag('<script src="'. $base .'components/com_fastseller/static/jquery/jquery-ui-1.10.4/js/jquery-ui-1.10.4.min.js"></script>');
if (JVERSION < 3)
	$doc->addCustomTag('<script src="'. $base .'components/com_fastseller/static/jquery/jquery-noconflict.js"></script>');
$doc->addStyleSheet($base .'components/com_fastseller/static/jquery/jquery-ui-1.10.4/css/ui-lightness/jquery-ui.min.css');
$doc->addStyleSheet($base .'components/com_fastseller/static/css/jquery-override.css');


require(JPATH_COMPONENT . DIRECTORY_SEPARATOR .'controllers'. DIRECTORY_SEPARATOR .'FSLayout.php');
require_once(FS_PATH .'helpers/hikashop.php');
FSLayout::prepare();
$hikashopIsInstalled = FSHikaShopHelper::isInstalled();

?>
<div id="cmainnav">
	<div class="fs-logo" style="margin:0px 15px 0 0px;vertical-align:middle;display:inline-block"></div>
	<button class="ui-main-nav" data-url="i=HOME"><?php echo JText::_('COM_FS_HOME') ?></button>
	<button class="ui-main-nav"
		data-url="i=ASSIGN"<?php
	if (! $hikashopIsInstalled)
		echo ' disabled="disabled"';
?>><?php echo JText::_('COM_FS_ASSIGN_FILTERS') ?></button>
	<button class="ui-main-nav" data-url="i=CREATE"><?php echo JText::_('COM_FS_CREATE_FILTERS') ?></button>
	<button class="ui-main-nav" data-url="i=CONF"><?php echo JText::_('COM_FS_OPTIONS') ?></button>
	<button class="ui-main-nav" data-url="i=HELP"><?php echo JText::_('COM_FS_HELP') ?></button>
<?php
	if (JVERSION < 3.1)
		echo '<button class="ui-removetop" id="hideTop" title="'.
			JText::_('COM_FS_HIDE_JOOMLA_MENU') .'">&uarr;&darr;</button>';
?>
</div>
<div id="cstatus" class="cstatus hid"><?php echo JText::_('COM_FS_LOADING') ?>
	<br/>
	<img src="<?php echo $base ?>components/com_fastseller/static/img/load.gif" />
</div>
<div id="clayout"></div>
<?php

require_once(JPATH_COMPONENT .'/views/FSAssignFilters/FSAssignFiltersView.php');
FSAssignFiltersView::printParameterHTMLEditorPopup();

?>
<script type="text/javascript">
var JoomlaVersion = parseFloat('<?php echo JVERSION ?>');
var url = '<?php echo FS_AJAX ?>';
<?php
	require(FS_PATH .'static/js/main.js');
?>
</script>
