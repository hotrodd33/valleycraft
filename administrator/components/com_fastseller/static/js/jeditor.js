function JEditorClass(init_options) {
	var options = {
		name: ''
	};
	var editor = null,
		editor_type = '';
	var onChange = null;

	var EDITOR_TYPE_TINYMCE = 'tinyMCE';
	var EDITOR_TYPE_CKEDITOR = 'CKEDITOR';
	var EDITOR_TYPE_CODEMIRROR = 'CodeMirror';

	//this.clear = function() {
	//	if (! editor) init();
	//	switch (editor_type) {
	//		case EDITOR_TYPE_TINYMCE:
	//			editor.setContent('');
	//			break;
	//		case EDITOR_TYPE_CKEDITOR:
	//			editor.setData('');
	//			break;
	//	}
	//}

	this.get = function() {
		// cold load
		if (! editor) init();
		//if (editor) {
			switch (editor_type) {
				case EDITOR_TYPE_TINYMCE:
					if (editor.isHidden())
						return jQuery('#' + options.name).val();
					return editor.getContent();
				case EDITOR_TYPE_CKEDITOR:
					return editor.getData();
				case EDITOR_TYPE_CODEMIRROR:
					return editor.getCode();
				default:
					return jQuery('#' + options.name).val();
			}
		//} else {
			//jQuery('#' + option.name)
	}

	this.set = function(string) {
		if (! editor) init();
		switch (editor_type) {
			case EDITOR_TYPE_TINYMCE:
				if (editor.isHidden())
					jQuery('#' + options.name).val(string);
				else
					editor.setContent(string);
				break;
			case EDITOR_TYPE_CKEDITOR:
				editor.setData(string);
				break;
			case EDITOR_TYPE_CODEMIRROR:
				editor.setCode(string);
				break;
			default:
				jQuery('#' + options.name).val(string);
		}
	}

	this.isDirty = function() {
		if (! editor) init();
		switch (editor_type) {
			// Note, there is a bug with tinyMCE when text is deleted the
			// isDirty() returns false?
			// Space could entered as text to save the change, or toggle the
			// display.
			case EDITOR_TYPE_TINYMCE:
				if (editor.isHidden())
					return true;
				return editor.isDirty();
			case EDITOR_TYPE_CKEDITOR:
				return editor.checkDirty();
			default:
				return true;
		}
	}

	this.resetDirty = function() {
		if (! editor) init();
		switch (editor_type) {
			case EDITOR_TYPE_TINYMCE:
				editor.isNotDirty = true;
				break;
			case EDITOR_TYPE_CKEDITOR:
				editor.resetDirty();
				break;
		}
	}

	this.editor = function() {
		if (! editor) init();
		return editor;
	}

	this.editorType = function() {
		if (! editor) init();
		return editor_type;
	}

	function setOptions() {
		for (option in options) {
			if (init_options[option] != null) {
				options[option] = init_options[option];
			}
		}
	}

	function init() {
		setOptions();

		if (window.tinyMCE) {
			if (tinyMCE.editors.length) {
				editor = tinyMCE.editors[0];
				editor_type = EDITOR_TYPE_TINYMCE;
		//	} else {
		//		editor = jQuery('#' + options.name);
			}
		} else if (window.CKEDITOR) {
			if (options.name == '') {
				console.error('CKEDITOR, must provide its name');
				return;
			}
			editor = CKEDITOR.instances[options.name];
			editor_type = EDITOR_TYPE_CKEDITOR;
		} else if (window.CodeMirror) {
			if (options.name == '') {
				console.error('CodeMirror, must provide its name');
				return;
			}
			editor = Joomla.editors.instances[options.name];
			editor_type = EDITOR_TYPE_CODEMIRROR;
		}
	}

	init();
}
