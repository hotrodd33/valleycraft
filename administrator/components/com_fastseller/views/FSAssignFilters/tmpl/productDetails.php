<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

require_once(FS_PATH .'helpers/hikashop.php');
$hikaConfig = FSHikaShopHelper::getConfig();

$showPDescButton = FSConf::get('show_pdesc_button');
$showProductCode = FSConf::get('show_product_code');
$numberOfProductsOnPage = FSConf::get('products_num_on_page');
$showProductImage = FSConf::get('show_product_image');

$q = JRequest::getVar('q', null);
$skip = JRequest::getVar('skip', 0);
$showonpage = JRequest::getVar('showonpage', $numberOfProductsOnPage);
$urlptid = JRequest::getVar('ptid', null);
$ppid = JRequest::getVar('ppid', null);
$orderby = JRequest::getVar('orderby', 'cat');

?>
<div id="productDetailsList">
<table cellspacing="0" cellpadding="0" width="100%" id="fsProductListTable">
<?php

if ($orderby == 'cat') $current_category = -1;

if ($productsData) {
	foreach ($productsData as $i => $product) {

		if ($orderby == 'cat' && !$ppid && $current_category != $product['cid']) {
			$current_category = $product['cid'];
			echo '<tr><td colspan="3" class="fs-cell-catdelim">';
			echo ($product['category_name']) ?
				$product['category_name'] : JText::_('COM_FS_WITHOUT_CATEGORY');
			echo '</td></tr>';
		}

		$currentRow = 'r-'. $i;
		FSAssignFiltersView::setCurrentRow($currentRow);

?>
	<tr id="<?php echo $currentRow ?>-row" class="fs-row" data-row="<?php echo $currentRow ?>"
		data-ptid="<?php echo (isset($ptid)) ? $ptid : 'none' ?>">
		<td class="fs-cell-tick row-cell-border" align="center" valign="top" data-row="<?php echo $currentRow ?>">
			<div style="color:#AAAAAA;font-size:11px;"><?php echo ($skip + $i + 1) ."." ?></div>
			<div class="row-checkbox-img"> </div>
		</td>
		<td class="fs-cell-name row-cell-border" valign="top" data-row="<?php echo $currentRow ?>">
		<div class="ui-namecell-pid"><?php echo 'id: '. $product['pid'] ?></div>
<?php

	if ($product['published'] == 0) echo '<div class="ui-namecell-unpublish">unpublished</div>';

	if ($showPDescButton) {
?>
		<button type="button" id="<?php echo $currentRow ?>-pdesc" class="ui-namecell-pdesc"
			data-pid="<?php echo $product['pid'] ?>">i</button>
<?php
	}

	if ($ppid) {
		if ($ppid == $product['pid'])
			echo '<div class="ui-namecell-showchildren" data-pid="'. $product['pid'] .
			'" data-skip="'. $skip .'">&larr; '. JText::_('COM_FS_BACK') .'</div>';
	} else if (FSAssignFiltersModel::product_has_children($product['pid']))
		echo '<div class="ui-namecell-showchildren" data-pid="'. $product['pid'] .
			'" data-skip="'. $skip .'">'. JText::_('COM_FS_PARENT') .'</div>';

?>
	<div class="clear"></div>
<?php

if ($showProductImage) {
	$image_path = $hikaConfig->get('uploadfolder');
	$x = $hikaConfig->get('thumbnail_x', 100);
	$y = $hikaConfig->get('thumbnail_y', 100);
	$image_force_size = $hikaConfig->get('image_force_size');
	if (! $x) $x = 100;
	if (! $y) $y = 100;

	$hikashop_images_path = FS_BASE_URL . $image_path .'thumbnails/'. $x .'x'. $y;
	if ($image_force_size)
		$hikashop_images_path .= 'f';
	$hikashop_images_path .= '/';
//'media/com_hikashop/upload/thumbnail_100x100/';
	if ($product['image'])
		echo '<img src="'. $hikashop_images_path . $product['image'] .
			'" class="ui-namecell-product-image" data-pid="'. $product['pid'] .'" />';
}

?>
	<div class="ui-namecell-name<?php if ($product['published'] == 0) echo ' grayed' ?>">
<?php

	// sanitize search keyword
	if ($q) {
		$remove = array("(", ")");
		$q = str_replace($remove, "", $q);
	}

	$product_name = $product['product_name'] ? $product['product_name']
		: $product['product_code'];
	if ($q) {
		echo preg_replace("/($q)/i", '<b>$1</b>', $product_name);
	} else {
		echo $product_name;
	}

	if ($showProductCode) {
		echo '<div class="ui-namecell-sku">';
		if ($q) {
			echo preg_replace("/($q)/i", '<b>$1</b>', $product['product_code']);
		} else {
			echo $product['product_code'];
		}
		echo '</div>';
	}

?>
	</div></td>
	<td id="<?php echo $currentRow ?>-filters-cell" valign="top" class="fs-cell-params row-cell-border">
		<form name="<?php echo $currentRow ?>-form">
			<input type="hidden" name="i" value="ASSIGN" />
			<input type="hidden" name="pid" value="<?php echo $product['pid'] ?>" />
			<input type="hidden" name="row" value="<?php echo $currentRow ?>" />
			<input type="hidden" name="urlptid" value="<?php echo $urlptid ?>" />
			<div id="<?php echo $currentRow ?>-dynamic-container" style="padding:0 0 0 5px">
<?php

$rowsExpanded = false;
$outerContExpandedClass = '';
$curtainExpandedClass = '';
if (isset($_COOKIE['rows-expanded']) && $_COOKIE['rows-expanded']) {
	$rowsExpanded = true;
	$outerContExpandedClass = ' expanded';
	$curtainExpandedClass = ' hid';
}

$tabs = '<div class="product-types-tabs" id="ptTabs_'. $currentRow
	.'" data-row="'. $currentRow .'">';
$cont = '<div class="pt-cont-outer'. $outerContExpandedClass
	.'"><div class="pt-cont-inner" id="ptContInner_'. $currentRow .'">';


if ($assignedFilters = $product['assigned_filters']) {

	$tabSelected = ($urlptid) ? '' : ' pt-tab-selected';
	$tabs .= '<span class="pt-tab'. $tabSelected .'" data-contid="all">'.
		JText::_('COM_FS_ALL') .'</span>';

	foreach ($assignedFilters as $i => $pt) {
		$ptid = $pt['ptid'];
		$ptName = FSAssignFiltersModel::getProductTypeName($ptid);
		$recordId = $pt['id'];
		$ptUniqueId = $ptid .'_'. $recordId;

		$tabSelected = ($urlptid && $urlptid == $ptid) ? ' pt-tab-selected' : '';

		$tabs .= '<span class="pt-tab'. $tabSelected .'" id="tab_ptCont_'. $ptUniqueId .'" data-contid="ptCont_'.
			$ptUniqueId .'">'. $ptName .'</span>';
	}

	$cont .= self::getProductFilterData($assignedFilters);

} else {

	$newPTUniqueId = 'ptNew_'. self::getNextNewProductTypeIndex();
	$tabs .= '<span class="pt-tab" data-contid="all">All</span>'.
		'<span class="pt-tab pt-tab-selected" id="tab_ptCont_'. $newPTUniqueId .'" data-contid="ptCont_'.
		$newPTUniqueId .'">'. JText::_('COM_FS_NEW_PRODUCT_TYPE') .'</span>';

	$cont .= self::getProductTypesSelectContainer();

}

$tabs .= '<span class="pt-tab-addnew">'. JText::_('COM_FS_ADD_NEW') .'</span></div>';
$cont .= '<div class="pt-curtain'. $curtainExpandedClass .'"></div></div>';
echo $tabs;
echo $cont;

?>
			</div>
		</form>
	</td>
</tr>
<?php

	} // end for foreach

} else	{
	$urlcid = JRequest::getVar('cid', null);
	echo '<div style="margin:10px 0;text-align:center;font:italic 14px Tahoma, Arial;">'.
		JText::_('COM_FS_NO_PRODUCTS') .'</div>';

//	if (!$urlcid && !$urlptid && !$q) {
//		echo '<div style="margin:15px 0 0;text-align:center;font:14px Arial;">';
//		echo '<div>'. JText::_('COM_FS_WRONG_LANG') .'</div></div>';
//	}
}


?>
</table>
<div id="blanket" class="blanket hid"></div>
</div>

<div id="filterSelectMenu" class="hid">
	<ul id="filterInputBox" class="filter-input-box">
		<li class="filter-input-box-elem fib-input">
			<input type="text" id="filterInputBoxInput" autocomplete="off" style="width:25px" />
		</li>
	</ul>
	<div id="filterParameterLabel"></div>
	<div id="availableFilters"></div>
</div>

<div id="productTypeSelectMenu" class="hid">
<?php

$ptsData = FSAssignFiltersModel::getAvailableProductTypes();

if ($ptsData) {
	$perColumn = 7;
	$current = 0;
	echo '<ul class="pt-select-menu-list">';
	foreach ($ptsData as $ptData) {
		if ($current != 0 && $current % $perColumn == 0) echo '</ul><ul class="pt-select-menu-list">';
		echo '<li class="pt-select-menu-list-elem" data-ptid="'. $ptData['id'] .'">'. $ptData['name'] .'</li>';
		$current++;
	}
	echo '</ul>';
} else {
	echo '<div style="padding:10px 20px;font:italic 12px Arial">'.
		JText::_('COM_FS_WARNING_1') .'</div>';
}

?>
</div>

<div id="productTypeRemoveMenu" class="hid">
	<div>
	<button id="pt-remove-confirm"><?php echo JText::_('COM_FS_DELETE') ?></button>
	<button id="pt-remove-cancel"><?php echo JText::_('COM_FS_CANCEL') ?></button>
	</div>
	<div style="text-align:center;padding:5px 0 0;"><div class="pt-remove-menu-arrow"></div></div>
</div>
<?php

	if ($showPDescButton) {
?>
<div id="product-description" class="popwindow hid">
	<div id="pdesc-draghandle"><?php echo JText::_('COM_FS_PRODUCT_DETAILS') ?></div>
	<div id="product-description-inner"><div class="pdesc-preload">
		<img src="<?php echo FS_URL ?>static/img/desc-loader.gif" width="28" height="28" border="0" /></div></div>
	<div style="background-color:#F7F7F7;border-top:1px solid #5EAD2E;text-align:right;">
	<button type="button" class="pdesc-popup-close-button"><?php
		echo JText::_('COM_FS_CLOSE') ?></button>
		<div id="pdesc-resizehandle"></div>
	</div>
</div>
<?php

	}

?>
<script>
var	newProductTypeTabText = "<?php echo JText::_('COM_FS_NEW_PRODUCT_TYPE') ?>",
	showChildrenProductsTextDefault = "<?php echo JText::_('COM_FS_PARENT') ?>",
	showChildrenProductsTextHovered = "<?php echo JText::_('COM_FS_SHOW_CHILDREN') ?>";

var NO_AVAIL_FILTERS_MSG = '<div style="font:italic 11px Arial, Tahoma;color:#777777;' +
	'margin:10px 0 0 0;text-align:center;"><?php echo JText::_('COM_FS_WARNING_3') ?></div>';

var NO_MATCH_MSG = '<div style="font:italic 11px Arial, Tahoma;color:#777777;' +
	'margin:10px 0 15px 0;text-align:center;"><?php echo JText::_('COM_FS_NO_MATCH') ?></div>';


</script>
