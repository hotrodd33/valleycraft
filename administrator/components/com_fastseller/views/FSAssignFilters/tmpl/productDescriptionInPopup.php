<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

if (! $productData) {
	echo JText::_('COM_FS_NO_PRODUCT_INFO');
	die();
}


$image_path = FS_BASE_URL .'media/com_hikashop/upload/'.
	$productData['image'];

?>
<div class="pdesc-details">
	<div class="pdesc-product-name"><?php echo $productData['product_name'] ?></div>

	<div class="pdesc-image-cont"><img src="<?php echo $image_path ?>" style="max-width:100%" /></div>

	<div class="pdesc-devider"><?php echo JText::_('COM_FS_PRODUCT_DESCRIPTION') ?></div>
	<div class="pdesc-product-description"><?php
		echo ($productData['product_description']) ?
			$productData['product_description'] : '<div class="grayed">'.
			JText::_('COM_FS_NO_PRODUCT_DESCRIPTION') .'</div>'; ?>
	</div>
</div>
