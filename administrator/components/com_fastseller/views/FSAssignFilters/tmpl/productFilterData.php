<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

//echo '<pre style="font-size:13px">';
//print_r($assignedFilters);
//echo '</pre>';


$fixedWidthClass = (FSConf::get('param_button_width')) ? ' fixed-width' : '';
$urlptid = JRequest::getVar('ptid', null);
$currentRow = self::getCurrentRow();
$cont = '';

foreach ($assignedFilters as $i => $pt) {
	$ptid = $pt['ptid'];
	//$ptName = FSAssignFiltersModel::getProductTypeName($ptid);
	$recordId = $pt['id'];
	$ptUniqueId = $ptid .'_'. $recordId;
	$containerSelected = '';
	if ($urlptid) {
		$containerSelected = ($urlptid == $ptid) ? '' : ' hid';
	}

	$cont .= '<div class="pt-container'. $containerSelected .'" data-ptid="'. $ptid .'" data-recordid="'. $recordId .'" '.
		'data-row="'. $currentRow .'" id="ptCont_'. $ptUniqueId .'">';

	$ptParameters = FSAssignFiltersModel::getProductTypeParameters($ptid);

	if (! $ptParameters) {
		$cont .= '<div style="font:italic 12px Arial;margin:6px 0 7px">'.
			JText::_('COM_FS_WARNING_2') .'</div></div>';

		continue;
	}

	foreach ($ptParameters as $parameter) {
		$paramName = $parameter['parameter_name'];
		$paramType = $parameter['parameter_type'];
		$uniqueId = $paramName .'_'. $ptid .'_'. $recordId;

		$parameterAssignedFilters = $pt[$paramName];
		if ($parameterAssignedFilters) {
			$btnValue = '';

			if ($paramType == 'T') {
				//$btnValue .= ' <sup class="filter-select-button-count" style="color:#DA6C00;">R</sup>';
				$btnValue .= '<span class="filter-select-button-value button-data-rich">'.
					self::squeeze(strip_tags($parameterAssignedFilters), 18) .
					'</span>';
			} else {
				$filtersArr = explode(';', $parameterAssignedFilters);
				$newFiltersArr = array();
				foreach ($filtersArr as $f) if (trim($f)) $newFiltersArr[] = $f;
				$parameterAssignedFilters = implode(';', $newFiltersArr);
				$filtersCount = count($newFiltersArr);
				if ($filtersCount > 1) {
					$btnValue .= ' <sup class="filter-select-button-count">'. $filtersCount .'</sup>';
				}
				$btnValue .= '<span class="filter-select-button-value">'.
					self::squeeze(strip_tags($parameterAssignedFilters), 18) .'</span>';
			}

		} else {
			$innerCont = ($parameter['parameter_label']) ?
				$parameter['parameter_label'] : $parameter['parameter_name'];
			$btnClass = $paramType == 'T' ? ' button-data-rich' : '';
			$btnValue = '<span class="filter-select-button-value'. $btnClass .
				'"><span class="fsb-value-unavail">['. $innerCont .']</span></span>';
		}


		$cont .= '<input type="hidden" value="'.
			htmlentities($parameterAssignedFilters, ENT_QUOTES, "UTF-8") .
			'" name="'. $uniqueId .'" '.
			'id="paramInput_'. $uniqueId .'" data-name="'. $paramName .'" />';

		$data_type = $paramType == 'T' ? 'html' : 'cp';
		$cont .= '<div class="param-button-cont"><div class="param-button-cont-inner">'.
			'<button type="button" class="filter-select-button'. $fixedWidthClass .
			'" id="paramButton_'. $uniqueId .'" data-uniqueId="'. $uniqueId .
			'" data-label="'.
			htmlentities($parameter['parameter_label'], ENT_QUOTES, "UTF-8") .
			'" data-name="'. $paramName .
			'" data-type="'. $data_type .'">';
		$cont .= $btnValue;
		$cont .= '</button></div></div>';

	}

	$cont .= '<div class="clear"></div></div>';
}

?>
