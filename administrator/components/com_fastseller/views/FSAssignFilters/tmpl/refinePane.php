<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

$cid= (int)JRequest::getVar('cid', null);
$ptid = JRequest::getVar('ptid', null);

// this will be executed only when browser fully reloaded
if ($cid) {
	$categoryName = FSAssignFiltersModel::getCategoryName($cid);
	$categoryNameForButton = self::squeeze($categoryName, 18);
	$categoryNameForMenuTitle = self::squeeze($categoryName, 21);
}

if ($ptid) {
	if ($ptid == 'wopt') {
		$ptNameForButton = JText::_('COM_FS_WITHOUT_PT');
		$ptNameForMenuTitle = JText::_('COM_FS_WITHOUT_PT');;
	} else {
		$ptName = FSAssignFiltersModel::getProductTypeName($ptid);
		$ptNameForButton = self::squeeze($ptName, 18);
		$ptNameForMenuTitle = self::squeeze($ptName, 21);
	}
}

$orderby = JRequest::getVar('orderby','cat');
$scending = JRequest::getVar('sc','Asc');

$rowsExpanded = (isset($_COOKIE['rows-expanded'])) ? $_COOKIE['rows-expanded'] : 0;

?>
<div id="notificationBox" class="hid"></div>
<div id="refinePane">
	<button type="button" id="refinePaneMutliSelectButton"
			class="refine-pane-button multi-select-control-button"
			data="select-groups-menu">
		<div class="ui-select-groups-img" id="selGrpImg" style="vertical-align:top;"> </div>
		<div  class="down-arrow" style="margin-top:4px"> </div>
	</button>
	<div id="refinePaneMutliSelectMenu" class="refine-pane-simple-menu hid" style="padding:3px 0;">
		<div class="rps-menu-element" data-select="all"><?php echo JText::_('COM_FS_ALL') ?></div>
		<div class="rps-menu-element" data-select="none"><?php echo JText::_('COM_FS_NONE') ?></div>
		<div class="rps-menu-element" data-select="wpt"><?php echo JText::_('COM_FS_WITH_PT_SHORT') ?></div>
		<div class="rps-menu-element" data-select="wopt"><?php echo JText::_('COM_FS_WITHOUT_PT_SHORT') ?></div>
	</div>

	<button id="refinePaneCategoriesButton"
			class="refine-pane-button cat-refine-btn"
			type="button"
			data="showcat-menu"
			data-branchid="category-branch">
		<span style="line-height:16px;"><?php echo ($cid) ?
			$categoryNameForButton : JText::_('COM_FS_CATEGORY') ?></span>
		<div class="down-arrow"> </div>
	</button>

	<div id="refinePaneCategoriesMenu" class="popmenu hid">
		<div id="cat-mhead"><span><?php echo ($cid) ?
			$categoryNameForMenuTitle : JText::_('COM_FS_CATEGORY') ?></span>
			<span id="removeCategorySelection" class="remove<?php echo ($cid) ? '' : ' hid' ?>"
					data-catid="" data-catname="Category"> -
				<span style="border-bottom:1px dashed #444444;"><?php
					echo JText::_('COM_FS_REMOVE') ?></span>
			</span>
		</div>
		<div id="category-branch"><div id="category-branch-loader" class="hid" style="padding:15px 0;" align="center">
			<img src="<?php echo FS_URL ?>static/img/ajax-loader.gif" width="16" height="11" /></div>
		</div>
		<div width="100%" style="border-top:1px solid #BBBBBB;margin:3px 5px 8px;padding:3px 5px 0;">
			<span class="help"
				onclick="$('cat-help-content').toggleClass('hid');"><?php
				echo JText::_('COM_FS_HELP_AND_TIPS') ?></span>
			<div id="cat-help-content" class="hid" style="padding-top:5px;"><?php
				echo JText::_('COM_FS_CATEGORY_MANU_TIP') ?>
			</div>
		</div>
	</div>

	<button id="refinePaneProductTypesButton" class="refine-pane-button pt-refine-btn" type="button" data="showpt-menu">
		<span style="line-height:16px;"><?php echo ($ptid) ?
			$ptNameForButton : JText::_('COM_FS_PROUDCT_TYPE') ?></span>
		<div class="down-arrow"> </div>
	</button>

	<div id="refinePaneProductTypesMenu" class="popmenu hid">
		<div id="pt-mhead"><span><?php echo ($ptid) ?
			$ptNameForMenuTitle : JText::_('COM_FS_PROUDCT_TYPE') ?></span>
			<span id="removeProductTypeSelection" class="remove<?php echo ($ptid) ? '' : ' hid' ?>"
					data-ptid=""
					data-ptname="Product Type"> -
				<span style="border-bottom:1px dashed #444444;"><?php
					echo JText::_('COM_FS_REMOVE') ?></span></span>
		</div>
		<div id="product-type-branch"><div style="padding:15px 0;" align="center">
			<img src="<?php echo FS_URL ?>static/img/ajax-loader.gif" width="16" height="11" /></div>
		</div>

	</div>


	<button id="refinePaneOrderByButton"
			class="refine-pane-button orderby-btn"
			data="orderby-menu"
			type="button"><?php echo JText::_('COM_FS_ORDER_BY') ?>
		<div class="down-arrow-sm"> </div>
	</button>
	<div id="refinePaneOrderByMenu" class="refine-pane-simple-menu hid" style="padding:3px 0;">
		<div class="rps-menu-element"
			data-order="cat"><?php
				echo JText::_('COM_FS_CATEGORY');
				if ($orderby == 'cat') echo '*' ?></div>
		<div class="rps-menu-element"
			data-order="pname"><?php
				echo JText::_('COM_FS_PRODUCT_NAME');
				if ($orderby == 'pname') echo '*' ?></div>
		<div class="rps-menu-element"
			data-order="pid"><?php
				echo JText::_('COM_FS_PRODUCT_ID');
				if ($orderby == 'pid') echo '*' ?></div>
		<div class="rps-menu-element"
			data-order="ptid"><?php
				echo JText::_('COM_FS_PT_ID');
				if ($orderby == 'ptid') echo '*' ?></div>
	</div>

	<button id="refinePaneAscDescButton" type="button" class="refine-pane-button orderby-sc-btn"
		title="<?php
		echo JText::_('COM_FS_CURRENT_ORDER') .': '. $scending ?>"><?php
		echo $scending ?>
	</button>

	<button id="refinePaneDeletePTFromSelected"
			class="refine-pane-button delete-pt-from-selected-button hid"
			type="button"><?php echo JText::_('COM_FS_DELETE_PT_FROM_SELETED') ?>
	</button>
	<div id="refinePaneDeletePTFromSelectedMenu" class="refine-pane-simple-menu hid" style="padding:3px 0">
	</div>

	<button id="refinePaneExpandCollapseButton"
		type="button"
		class="refine-pane-button expand-collapse-all-btn"
		title="<?php echo JText::_('COM_FS_EXPAND_HINT') ?>"
		data-expanded="<?php echo $rowsExpanded ?>">
		<?php echo ($rowsExpanded) ?
			JText::_('COM_FS_EXPAND') : JText::_('COM_FS_COLLAPSE') ?> </button>

	<br clear="all" />
</div>
