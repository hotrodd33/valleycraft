<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

// data not always passed here. In case we call for a new form, we check data.
$parameterDataExists = ($parameter) ? true : false;

if (!$tabIndex) $tabIndex = JRequest::getVar('tabno', 0);

$key = ($parameterDataExists) ? $parameter['parameter_name'] : 'new'. $tabIndex;

if ($parameterDataExists) {
	$parameterName = $parameter['parameter_name'];
	$parameterLabel = $parameter['parameter_label'];
	$parameterType = $parameter['parameter_type'];
	$parameterFilters = $parameter['parameter_values'];
	$parameterDescription = $parameter['parameter_description'];
	$parameterUnit = $parameter['parameter_unit'];
	// $parameterMode = $parameter['mode'];
	$parameterCPAttributes = new JRegistry;
	$parameterCPAttributes->loadString($parameter['cherry_picker_attribs']);
	// $parameterShowQuickrefine = $parameter['show_quickrefine'];
	// $parameterCollapse = $parameter['collapse'];
	$parameterMultiassigned = ($parameter['parameter_type'] == 'V') ? true : false;
	$defineFiltersManually = $parameter['define_filters_manually'];

} else {
	$parameterName = '';
	$parameterLabel = '';
	$parameterType = '';
	$parameterFilters = '';
	$parameterDescription = '';
	$parameterUnit = '';
	// $parameterMode = 0;
	$parameterCPAttributes = new JRegistry;
	// $parameterShowQuickrefine = 0;
	// $parameterCollapse = 0;
	$parameterMultiassigned = false;
	$defineFiltersManually = false;
}

?>
<button class="glass-btn ptp-delete-parameter-button" title="Delete Parameter"
	data-tab="<?php echo $tabIndex ?>" data-key="<?php echo $key ?>" style="font:11px Tahoma;float:right">
	<img src="<?php echo FS_URL ?>static/img/Delete16.png" width="10" />
	&nbsp;<?php echo JText::_('COM_FS_DELETE') ?>
</button>
<div class="clear"></div>

<input type="hidden" name="key[]" value="<?php echo $key ?>" />
<input type="hidden" name="parameter_name_active_<?php echo $key ?>" id="parameter_name_active_<?php echo $key ?>"
	value="<?php echo $parameterName ?>" />
<input type="hidden" name="list_order_<?php echo $key ?>" id="list_order_<?php echo $tabIndex ?>"
	value="<?php echo $tabIndex ?>" />

<table class="ptp-paramTbl">
	<tr>
		<td class="titleCell"><?php echo JText::_('COM_FS_NAME') ?></td>
		<td class="valueCell">
			<input type="text" name="parameter_name_<?php echo $key ?>" id="parameter_name_<?php echo $key ?>"
				value="<?php echo $parameterName ?>" class="ptp-parameter-name ptp-hint-listener"
				data-tab="<?php echo $tabIndex ?>" /></td>
		<td>
			<span id="parameter_name_<?php echo $key ?>_hint" class="simpleHint">
				<?php echo JText::_('COM_FS_HINT_1') ?>
			</span>
		</td>
	</tr>

	<tr>
		<td class="titleCell"><?php echo JText::_('COM_FS_LABEL') ?></td>
		<td class="valueCell">
			<input type="text" name="parameter_label_<?php echo $key ?>" id="parameter_label_<?php echo $tabIndex ?>"
				value="<?php echo $parameterLabel ?>" class="input ptp-parameter-label ptp-hint-listener"
				data-tab="<?php echo $tabIndex ?>" /></td>
		<td>
			<span id="parameter_label_<?php echo $tabIndex ?>_hint" class="simpleHint">
				<?php echo JText::_('COM_FS_HINT_2') ?>
			</span>
		</td>
	</tr>

	<tr>
		<td class="titleCell"><?php echo JText::_('COM_FS_DESCRIPTION') ?></td>
		<td class="valueCell">
			<textarea name="parameter_description_<?php echo $key ?>" rows="1" style="width:254px;max-width:400px"><?php
				echo $parameterDescription
			?></textarea></td>
		<td></td>
	</tr>

	<tr>
		<td class="titleCell"><?php echo JText::_('COM_FS_UNIT') ?></td>
		<td class="valueCell">
			<input type="text" name="parameter_unit_<?php echo $key ?>" id="parameter_unit_<?php echo $tabIndex ?>"
			value="<?php echo htmlspecialchars($parameterUnit) ?>" class="input ptp-hint-listener" /></td>
		<td><span id="parameter_unit_<?php echo $tabIndex ?>_hint" class="simpleHint">
			<?php echo JText::_('COM_FS_HINT_3') ?>
			</span>
		</td>
	</tr>

	<tr><td colspan="3" style="padding-top:20px"></td></tr>

	<tr>
		<td class="titleCell"><?php echo JText::_('COM_FS_TYPE') ?></td>
		<td colspan="2">
			<div id="parameter_type_<?php echo $key ?>" class="ptp-paramter-type-select">
				<input type="radio"
					id="parameter_type_<?php echo $key ?>_1"
					value="<?php echo $parameterType ?>"
					data-i="0"
					name="parameter_type_<?php echo $key ?>"<?php
				if ($parameterType != 'T')
					echo ' checked="checked"';
				?>>
				<label for="parameter_type_<?php echo $key ?>_1"
					data-i="0">Cherry Picker</label>
				<input type="radio"
					id="parameter_type_<?php echo $key ?>_2"
					value="T"
					data-i="1"
					name="parameter_type_<?php echo $key ?>"<?php
				if ($parameterType == 'T')
					echo ' checked="checked"';
				?>>
				<label for="parameter_type_<?php echo $key ?>_2"
					data-i="1">HTML</label>
			</div>
			<script>
			jQuery(document).ready(function() {
				jQuery("#parameter_type_<?php echo $key ?>").buttonset();
				//jQuery("#parameter_type_<?php echo $key ?> label").click(function() {
				jQuery("input[name='parameter_type_<?php echo $key ?>']").change(function() {
					var index = jQuery(this).attr('data-i'),
						containers = jQuery(this).closest('.ptp-formCont').children('.ptp-parameter-type-variant');
<?php
		if ($parameterType == 'T' &&
			FSCreateFiltersModel::parameterHasDataAssignedToProducts($parameterName))
		{
?>
					if (index == 0) {
						var proceed = confirm("<?php echo JText::_('COM_FS_HINT_12') ?>");
						if (proceed == false) {
							jQuery("input[name='parameter_type_<?php echo $key ?>']")[1].checked = true;
							jQuery("#parameter_type_<?php echo $key ?>").buttonset("refresh");
							return;
						}
					}
<?php
		}
?>
					containers.addClass('hid');
					containers[index].removeClass('hid');
				});
			});
			</script>
		</td>
	</tr>
</table>

<div class="ptp-parameter-type-variant<?php
	if ($parameterType == 'T')
		echo ' hid';
?>">
<table class="ptp-paramTbl">
	<tr>
		<td></td>
		<td colspan="2" style="font:bold 14px Arial;padding:0 0 15px">
			<?php echo JText::_('COM_FS_CP_PARAM_OPTIONS') ?>
		</td>
	</tr>
	<tr>
		<td class="titleCell">
			<label for="show_in_cherry_picker_<?php echo $tabIndex ?>"><?php
				echo JText::_('COM_FS_SHOW_IN_CP') ?></label>
		</td>
		<td class="valueCell">
			<input type="checkbox" name="show_in_cherry_picker_<?php echo $key ?>"
				id="show_in_cherry_picker_<?php echo $tabIndex ?>" value="1"<?php
				// if ($parameterShowQuickrefine)
				if ($parameterCPAttributes->get('show_in_cherry_picker') !== 0)
					echo ' checked' ?> />
		</td>
		<td></td>
	</tr>

	<tr>
		<td class="titleCell" style="vertical-align:top;padding-top:7px"><?php echo JText::_('COM_FS_MODE') ?></td>
		<td class="valueCell" colspan="2" style="vertical-align:top;padding-top:5px;">
			<select name="mode_<?php echo $key ?>" id="mode_<?php echo $tabIndex ?>"
				class="tpt-parameter-mode" data-tab="<?php echo $tabIndex ?>">
				<option value="0"<?php
					if ($parameterCPAttributes->get('mode') == 0)
						echo ' selected="selected"' ?>><?php echo JText::_('COM_FS_DEFAULT') ?></option>
				<option value="1"<?php
					if ($parameterCPAttributes->get('mode') == 1)
						echo ' selected="selected"' ?>><?php echo JText::_('COM_FS_MODE_1') ?></option>
				<option value="4"<?php
					if ($parameterMultiassigned) {
						echo ' disabled';
					} else if ($parameterCPAttributes->get('mode') == 4) {
						 echo ' selected="selected"';
					}
				?>><?php echo JText::_('COM_FS_MODE_2') ?></option>
				<option value="2"<?php
					if ($parameterMultiassigned) {
						echo ' disabled';
					} else if ($parameterCPAttributes->get('mode') == 2) {
						 echo ' selected="selected"';
					}
				?>><?php echo JText::_('COM_FS_MODE_3') ?></option>
				<option value="3"<?php
					if ($parameterCPAttributes->get('mode') == 3)
						echo ' selected="selected"' ?>><?php
						echo JText::_('COM_FS_COLOR') ?></option>
			</select><?php
					if ($parameterMultiassigned) {
						echo '<div style="font:11px Arial;color:#587F9F;margin-top:5px">'.
							JText::_('COM_FS_HINT_4') .'</div>';
					}
?>
			</td>
	</tr>

	<tr>
		<td class="titleCell">
			<label for="show_quickrefine_<?php echo $tabIndex ?>"><?php
			echo JText::_('COM_FS_SHOW_QUICKREFINE') ?></label>
		</td>
		<td class="valueCell">
			<input type="checkbox" name="show_quickrefine_<?php echo $key ?>"
				id="show_quickrefine_<?php echo $tabIndex ?>" value="1"<?php
				// if ($parameterShowQuickrefine)
				if ($parameterCPAttributes->get('show_quickrefine'))
					echo ' checked' ?> />
		</td>
		<td></td>
	</tr>

	<tr>
		<td class="titleCell"><?php echo JText::_('COM_FS_COLLAPSE_STATE') ?></td>
		<td class="valueCell">
			<input type="radio" name="collapse_<?php echo $key ?>"
				id="collapse_global_<?php echo $tabIndex ?>" value="0"<?php
					if ($parameterCPAttributes->get('collapse') == 0)
						echo ' checked';
				?> />
			<label for="collapse_global_<?php echo $tabIndex ?>"><?php
				echo JText::_('COM_FS_COLLAPSE_1') ?></label>
			<br/>
			<input type="radio" name="collapse_<?php echo $key ?>"
				id="collapse_yes_<?php echo $tabIndex ?>" value="1"<?php
					if ($parameterCPAttributes->get('collapse') == 1)
						echo ' checked';
				?> />
			<label for="collapse_yes_<?php echo $tabIndex ?>"><?php
				echo JText::_('COM_FS_COLLAPSE_2') ?></label>
			<br/>
			<input type="radio" name="collapse_<?php echo $key ?>"
				id="collapse_no_<?php echo $tabIndex ?>" value="2"<?php
					if ($parameterCPAttributes->get('collapse') == 2)
						echo ' checked';
				?> />
			<label for="collapse_no_<?php echo $tabIndex ?>"><?php
				echo JText::_('COM_FS_COLLAPSE_3') ?></label>
		</td>
		<td></td>
	</tr>

	<tr><td colspan="3" style="padding:6px"></td></tr>

	<tr>
		<td class="titleCell"><?php echo JText::_('COM_FS_HIDING_FILTERS') ?></td>
		<td class="valueCell" colspan="2">
			<input type="radio" name="hiding_filters_<?php echo $key ?>"
				id="hiding_filters_global_<?php echo $tabIndex ?>" value="0"<?php
					if ($parameterCPAttributes->get('hiding_filters') == 0)
						echo ' checked';
				?> />
			<label for="hiding_filters_global_<?php echo $tabIndex ?>"><?php
				echo JText::_('COM_FS_HIDING_1') ?></label>
			<br/>
			<input type="radio" name="hiding_filters_<?php echo $key ?>"
				id="hiding_filters_no_<?php echo $tabIndex ?>" value="1"<?php
					if ($parameterCPAttributes->get('hiding_filters') == 1)
						echo ' checked';
				?> />
			<label for="hiding_filters_no_<?php echo $tabIndex ?>"><?php
				echo JText::_('COM_FS_HIDING_2') ?></label>
			<br/>
			<input type="radio" name="hiding_filters_<?php echo $key ?>"
				id="hiding_filters_see_more_<?php echo $tabIndex ?>" value="2"<?php
					if ($parameterCPAttributes->get('hiding_filters') == 2)
						echo ' checked';
				?> />
			<label for="hiding_filters_see_more_<?php echo $tabIndex ?>"><?php
				echo JText::_('COM_FS_HIDING_3') ?></label>
			<label for="see_more_size_<?php echo $tabIndex ?>"
				style="margin:0 5px 0 10px"><?php
					echo JText::_('COM_FS_NUMBER_OF_FILTERS_BEFORE') ?>:</label>
			<input type="text" name="see_more_size_<?php echo $key ?>"
				id="see_more_size_<?php echo $tabIndex ?>"
				class="input" style="width:50px;" value="<?php
					if ($v = $parameterCPAttributes->get('see_more_size'))
						echo $v;
				?>" />
			<br/>
			<input type="radio" name="hiding_filters_<?php echo $key ?>"
				id="hiding_filters_scroll_box_<?php echo $tabIndex ?>" value="3"<?php
					if ($parameterCPAttributes->get('hiding_filters') == 3)
						echo ' checked';
				?> />
			<label for="hiding_filters_scroll_box_<?php echo $tabIndex ?>"><?php
				echo JText::_('COM_FS_HIDING_4') ?></label>
			<label for="scroll_size_<?php echo $tabIndex ?>"
				style="margin:0 5px 0 10px"><?php
					echo JText::_('COM_FS_SCROLL_HIGHT') ?>:</label>
			<input type="text" name="scroll_height_<?php echo $key ?>"
				id="scroll_size_<?php echo $tabIndex ?>"
				class="input" style="width:50px;" value="<?php
					if ($v = $parameterCPAttributes->get('scroll_height'))
						echo $v;
				?>" />
		</td>
	</tr>

</table>
<div class="ptp-filtersinfo-title"><?php echo JText::_('COM_FS_FILTER_INFO') ?></div>
<div class="ptp-define-filters-manually-cont">
	<input type="checkbox"
		name="define_filters_manually_<?php echo $key ?>"
		id="define_filters_manually_<?php echo $tabIndex ?>"
		class="ptp-define-filters-manually"
		value="1"

		<?php echo ($defineFiltersManually) ? 'checked' : '' ?> />
	<label for="define_filters_manually_<?php echo $tabIndex ?>" ><?php
		echo JText::_('COM_FS_DEFINE_MANUALLY') ?></label>
</div>
<?php

$hiddenClass = ($defineFiltersManually) ? ' hid' : '';
echo '<div class="ptp-filters-cont-variant'. $hiddenClass .'">';
if ($parameterFilters) {
	echo '<div style="font:13px Arial;margin:20px 5px 0 20px">'.
		JText::_('COM_FS_HINT_5') .'</div>';
	echo '<div class="ptp-used-filters-outer">';

	$filters = explode(';', $parameterFilters);
	foreach ($filters as $i => $filter) {
		echo '<div class="ptp-used-filters-filter">'. $filter .'</div>';
	}
	echo '<div class="clear"></div></div>';
	echo '<div style="font:13px Arial;margin:5px 0 0 20px">'.
		JText::_('COM_FS_FILTERS_COUNT') .': '. ($i + 1) .'</div>';

} else {
	echo '<div style="font:13px Arial;margin:20px 5px 0 20px">'.
		JText::_('COM_FS_HINT_6') .'</div>';
}
echo '</div>';

$filtersCount = ($parameterFilters) ? count(explode(';', $parameterFilters)) : 0;
$hiddenClass = ($defineFiltersManually) ? '' : ' hid';
?>
<div class="ptp-filters-cont-variant<?php echo $hiddenClass ?>"
	style="font:13px Arial;margin:20px 5px 0 20px">
<div style="margin:5px 0 10px"><?php echo JText::_('COM_FS_HINT_7') ?></div>
<textarea name="defined_filters_<?php echo $key ?>"
		id="defined_filters_<?php echo $key ?>"
		class="ptp-parameter-predefined-filters ptp-hint-listener"
		rows="3"
		data-tab="<?php echo $tabIndex ?>"><?php echo $parameterFilters ?>
</textarea>
<div id="numberOfFilters_<?php echo $tabIndex ?>" style="color:#777777;margin:0 0 5px 0;font:11px Arial">
	<span><?php echo JText::_('COM_FS_FILTERS_COUNT') ?>:</span> <span><?php echo $filtersCount ?></span>
</div>
<div style="margin:0 0 0px 0">
	<span id="defined_filters_<?php echo $key ?>_hint" class="simpleHint">
		<?php echo JText::_('COM_FS_HINT_8') ?>
	</span>
</div>
</div>

</div>
<!-- END OF CHERRY PICKER PARAMTER TYPE VARIANT -->

<div class="ptp-parameter-type-variant<?php
	if ($parameterType != 'T')
		echo ' hid';
?>">
<?php echo JText::_('COM_FS_HINT_11') ?>
</div>
