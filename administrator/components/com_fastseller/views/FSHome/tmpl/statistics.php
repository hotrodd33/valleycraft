<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

?>
<div class="hm-stats">
<h2><?php echo JText::_('COM_FS_STATISTICS') ?></h2>
<?php echo JText::sprintf('COM_FS_STAT_1', $d['total_products']) ?>
<?php
	if ($d['total_products']) {
?>
<?php echo ' '. JText::_('COM_FS_STAT_LINE_1') ?>
<ul>
	<li><?php
		echo JText::sprintf('COM_FS_STAT_2',
			$d['parents'],
			$d['parent_published'],
			$d['parents'] - $d['parent_published']) ?>
	</li>
	<li><?php
		echo JText::sprintf('COM_FS_STAT_3',
			$d['children'],
			$d['children_published'],
			$d['children'] - $d['children_published']) ?>
	</li>
</ul>
<?php

	}

?>
<br/>
<?php echo JText::sprintf('COM_FS_STAT_4',
			$d['pt_count'],
			$d['pt_count'] == 1 ? '' : 's') ?>
<?php

	if ($d['pt_count'] > 0) {

?>
<table class="hm-pt">
<tr class="hm-ptttl">
	<td><?php echo JText::_('COM_FS_NO') ?></td>
	<td><?php echo JText::_('COM_FS_ID') ?></td>
	<td><?php echo JText::_('COM_FS_NAME') ?></td>
	<td><?php echo JText::_('COM_FS_STAT_COLUMN_4') ?></td>
	<td><?php echo JText::_('COM_FS_STAT_COLUMN_5') ?></td>
</tr>
<?php

$t1 = 0;
$t2 = 0;

for ($i = 0; $i < $d['pt_count']; $i++) {
	$t1 += $d[$i]['products_assigned'];
	$t2 += $d[$i]['values_assigned'];
?>
	<tr>
		<td><?php echo ($i+1) ?></td>
		<td><?php echo $d[$i]['id'] ?></td>
		<td><?php echo $d[$i]['name'] ?></td>
		<td><?php echo $d[$i]['products_assigned'] ?></td>
		<td><?php echo $d[$i]['values_assigned'] ?></td>
	</tr>
<?php

}

if ($d['pt_count'] > 1) {

?>
<tr class="hm-tptot">
	<td> </td>
	<td> </td>
	<td><?php echo JText::_('COM_FS_STAT_5') ?></td>
	<td><?php echo $t1 ?></td>
	<td><?php echo $t2 ?></td>
</tr>
<?php

}

?>
</table>
<?php

} else {

?>
	<div style="color:#F700A3"><?php echo JText::_('COM_FS_STAT_WARNING_1') ?></div>
<?php

}

?>
</div>
