<?php
defined( '_JEXEC' ) or die( 'Restricted access' );

class FSHikaShopHelper {
	private static $_is_installed = -1;

	public static function isInstalled() {
		if (self::$_is_installed != -1)
			return self::$_is_installed;

		$db = JFactory::getDBO();
		$q = "SELECT COUNT(*) FROM `#__extensions` WHERE `name`='hikashop' OR `name`='com_hikashop'";
		$db->setQuery($q);
		$installed = $db->loadResult() ? true : false;
		self::$_is_installed = $installed;
		return $installed;
	}

	public static function getConfig() {
		if (! self::isInstalled())
			return null;

		require_once(JPATH_ADMINISTRATOR .'/components/com_hikashop/helpers/helper.php');
		$config =& hikashop_config();
		return $config;
	}

}
