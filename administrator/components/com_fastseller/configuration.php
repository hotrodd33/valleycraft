<?php defined('_JEXEC') or die('Restricted access');

$confopt['show_unpublished_products'] = 0;
$confopt['show_pdesc_button'] = 1;
$confopt['show_product_code'] = 1;
$confopt['show_product_image'] = 0;
$confopt['filter_columns'] = 4;
$confopt['products_num_on_page'] = 50;
$confopt['param_button_width'] = 0;
$confopt['debug'] = 0;

?>