<?php
// Set flag that this is a parent file
define( '_JEXEC', 1 );

// Use script path on local set-up and __FILE__ on production
if (isset($_ENV['FASTSELLER_LOCAL'])) {
	define('JPATH_BASE', dirname(dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) );
	// Maximum error reporting for local development
	ini_set('display_errors', 1);
	error_reporting(E_ALL);
} else {
	define('JPATH_BASE', dirname(dirname(dirname(dirname(__FILE__)))) );
}
define('DS', DIRECTORY_SEPARATOR);
require(JPATH_BASE.DS.'includes'.DS.'defines.php');
require(JPATH_BASE.DS.'includes'.DS.'framework.php');

$_SERVER['SCRIPT_NAME'] = dirname(dirname(dirname(dirname($_SERVER['SCRIPT_NAME'])))) .'/index.php';
//$_SERVER['PHP_SELF'] = dirname(dirname(dirname(dirname($_SERVER['PHP_SELF'])))) .'/index.php';
//$_SERVER['SCRIPT_FILENAME'] = dirname(dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))) .'/index.php';
//$_SERVER['HTTP_REFERER'] = '/administrator/index.php?option=com_fastseller';
$app = JFactory::getApplication('administrator');
$app->initialise();

//echo '<pre>';
//print_r($_SERVER);



//echo '<br/>'. JPATH_ROOT;
//echo '<br/>'. JPATH_SITE;
//echo '<br/>'. JPATH_CONFIGURATION;
//echo '<br/>'. JPATH_ADMINISTRATOR;
//echo '<br/>'. JPATH_LIBRARIES;
//echo '<br/>'. JPATH_PLUGINS;
//echo '<br/>'. JPATH_INSTALLATION;
//echo '<br/>'. JPATH_THEMES;
//echo '<br/>'. JPATH_CACHE;
//echo '<br/>'. JPATH_MANIFESTS;

//$config = JFactory::getConfig();
//$config->set('language', 'en-GB');
//$app->initialise();
// Initialise the application.
//$app->initialise(array(
//	'language' => $app->getUserState('application.lang')
//));


$lang = JFactory::getLanguage();
$reload = true;
$lang->load('com_fastseller', JPATH_BASE, $lang->getTag(), $reload);


//echo '<pre>';
//print_r($lang);
//echo '</pre>';


require('helper.php');
