<?php
/**
* @package   holax
* @author    bdthemes http://www.bdthemes.com
* @license   http://www.gnu.org/licenses/gpl.html GNU/GPL
*/

// get theme configuration
include($this['path']->path('layouts:theme.config.php'));
include($this['path']->path('layouts:header.php'));

$has_sticky_header = ($this['config']->get('headertype') === 'sticky') ? 'top:75' : '';

?>
<!DOCTYPE HTML>
<html lang="<?php echo $this['config']->get('language'); ?>" dir="<?php echo $this['config']->get('direction'); ?>"  data-config='<?php echo $this['config']->get('body_config','{}'); ?>'>

<head>
	<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M232KVR');</script>
<!-- End Google Tag Manager -->
	
		 <meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php

echo $this['template']->render('head'); ?>
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
   <!--[if lt IE 9]>
       <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
       <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	   <style type="text/css">
	 /* IE6, IE7, IE8 */
		.container{display:table;width: 100%;}
		.row{height: 100%;display: table-row;}
		.col-sm-4{display: table-cell;}
		.layout-full-wrapper {max-width: 960px; margin: 0 auto; display: block;}
		#rev_slider_6_1_wrapper{display: none;}
		#hikashop_main_slider_hikashop_category_information_module_270{width:850px!important;}
		#hikashop_main_slider_hikashop_category_information_module_270 img{display:block}
	 </style>
     <![endif]-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-38164222-1', 'auto');
  ga('send', 'pageview');

</script>	 
</head>

<body class="<?php echo $this['config']->get('body_classes'); ?><?php echo ($this['widgets']->count('slider')) ? ' tm-has-slider' : ''; ?>">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M232KVR"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<div class="layout-<?php echo $this['config']->get('layout_width');?>-wrapper">

		<?php if ($this['widgets']->count('drawer')) : ?>
			<div class="<?php echo $wrapper_classes['drawer']; ?>">
				<div id="tm-drawer">
					<div class="uk-container uk-container-center">
						<section class="<?php echo $grid_classes['drawer']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('drawer', array('layout'=>$this['config']->get('grid.drawer.layout'))); ?></section>
					</div>
				</div>
				<a href="javascript:void(0);" class="drawer_toggle"><span class="uk-icon-chevron-down"></span></a>
			</div>
		<?php endif; ?>

		<div class="header-top-wrapper">
			<?php echo bdt_headerStyle($this); ?>

		</div>

		<?php if ($this['widgets']->count('heading')) : ?>
			<div class="<?php echo $wrapper_classes['heading']; ?>" id="tmHeading">
				<section class="<?php echo $grid_classes['heading']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>
					<?php echo $this['widgets']->render('heading', array('layout'=>$this['config']->get('grid.heading.layout'))); ?>
				</section>
			</div>
		<?php endif; ?>
		<!-- <?php //if ($this['widgets']->count('menu') and $this['config']->get('header') == 'default') : ?>
			<div class="uk-visible-large <?php //echo $wrapper_classes['menu']; ?>" <?php //echo ($this['config']->get('menu-sticky') === 'sticky') ? 'data-uk-sticky="{'.$has_sticky_header.'}"' : ''; ?>>
				<div class="uk-container uk-container-center">
					<div id="tmMainMenu" class="tm-menu">
						<?php //echo $this['widgets']->render('menu'); ?>
					</div>
				</div>
			</div> -->
		<?php //endif; ?>

		<?php if ($this['widgets']->count('slider')) : ?>
			<div class="<?php echo $wrapper_classes['slider']; ?>" id="tmSlider">
				<div class="<?php echo ($this['config']->get('layout_width') == 'boxed-space') ? 'uk-container ' : ''; ?>uk-container-center">
					<section class="<?php echo $grid_classes['slider']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('slider', array('layout'=>$this['config']->get('grid.slider.layout'))); ?></section>
				</div>
			</div>
		<?php endif; ?>

		<?php if ($this['widgets']->count('features')) : ?>
			<div class="<?php echo $wrapper_classes['features']; ?>" id="tmFeatures">
				<div class="uk-container uk-container-center">
					<section class="<?php echo $grid_classes['features']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('features', array('layout'=>$this['config']->get('grid.features.layout'))); ?></section>
				</div>
			</div>
		<?php endif; ?>

		<?php if ($this['widgets']->count('top-a')) : ?>
			<div class="<?php echo $wrapper_classes['top-a']; ?>" id="tmTopA">
				<div class="uk-container uk-container-center">
					<section class="<?php echo $grid_classes['top-a']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('top-a', array('layout'=>$this['config']->get('grid.top-a.layout'))); ?></section>
				</div>
			</div>
		<?php endif; ?>

		<?php if ($this['widgets']->count('top-b')) : ?>
			<div class="<?php echo $wrapper_classes['top-b']; ?>" id="tmTopB">
				<div class="uk-container uk-container-center">
					<section class="<?php echo $grid_classes['top-b']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('top-b', array('layout'=>$this['config']->get('grid.top-b.layout'))); ?></section>
				</div>
			</div>
		<?php endif; ?>

		<?php if ($this['widgets']->count('top-c')) : ?>
			<div class="<?php echo $wrapper_classes['top-c']; ?>" id="tmTopC">
				<div class="uk-container uk-container-center">
					<section class="<?php echo $grid_classes['top-c']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('top-c', array('layout'=>$this['config']->get('grid.top-c.layout'))); ?></section>
				</div>
			</div>
		<?php endif; ?>

		<?php if ($this['widgets']->count('top-d')) : ?>
			<div class="<?php echo $wrapper_classes['top-d']; ?>" id="tmTopD">
				<div class="uk-container uk-container-center">
					<section class="<?php echo $grid_classes['top-d']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('top-d', array('layout'=>$this['config']->get('grid.top-d.layout'))); ?></section>
				</div>
			</div>
		<?php endif; ?>

		<?php if ($this['widgets']->count('fullwidth-top')) : ?>
			<div class="<?php echo $wrapper_classes['fullwidth-top']; ?>" id="tmFullWidthTop">
				<div class="<?php echo ($this['config']->get('layout_width') == 'boxed-space') ? 'uk-container ' : ''; ?>uk-container-center">
					<section class="<?php echo $grid_classes['fullwidth-top']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('fullwidth-top', array('layout'=>$this['config']->get('grid.fullwidth-top.layout'))); ?></section>
				</div>
			</div>
		<?php endif; ?>

		<?php if ($this['widgets']->count('expanded-top')) : ?>
			<div class="<?php echo $wrapper_classes['expanded-top']; ?>" id="tmExpandedTop">
				<div class="uk-container uk-container-center">
					<section class="<?php echo $grid_classes['expanded-top']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('expanded-top', array('layout'=>$this['config']->get('grid.expanded-top.layout'))); ?></section>
				</div>
			</div>
		<?php endif; ?>

		<?php if ($this['widgets']->count('main-top + main-bottom + sidebar-a + sidebar-b') || $this['config']->get('system_output', true)) : ?>
			<div class="mainbody-wrapper" id="tmMainBody">
				<div class="uk-container uk-container-center">
					<div class="tm-middle uk-grid" data-uk-grid-match data-uk-grid-margin>

						<?php if ($this['widgets']->count('main-top + main-bottom') || $this['config']->get('system_output', true)) : ?>
						<div class="<?php echo $columns['main']['class'] ?>">

							<?php if ($this['widgets']->count('main-top')) : ?>
							<section class="<?php echo $grid_classes['main-top']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('main-top', array('layout'=>$this['config']->get('grid.main-top.layout'))); ?></section>
							<?php endif; ?>

							<?php if ($this['config']->get('system_output', true)) : ?>
							<main class="tm-content">
								<?php echo $this['template']->render('content'); ?>
							</main>
							<?php endif; ?>

							<?php if ($this['widgets']->count('main-bottom')) : ?>
							<section class="<?php echo $grid_classes['main-bottom']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('main-bottom', array('layout'=>$this['config']->get('grid.main-bottom.layout'))); ?></section>
							<?php endif; ?>

						</div>
						<?php endif; ?>

			            <?php foreach($columns as $name => &$column) : ?>
			            <?php if ($name != 'main' && $this['widgets']->count($name)) : ?>
			            <aside class="<?php echo $column['class'] ?>"><?php echo $this['widgets']->render($name) ?></aside>
			            <?php endif ?>
			            <?php endforeach ?>

					</div>
				</div>
			</div>
		<?php endif; ?>

		<?php if ($this['widgets']->count('expanded-bottom')) : ?>
			<div class="<?php echo $wrapper_classes['expanded-bottom']; ?>" id="tmExpandedBottom">
				<div class="uk-container uk-container-center">
					<section class="<?php echo $grid_classes['expanded-bottom']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('expanded-bottom', array('layout'=>$this['config']->get('grid.expanded-bottom.layout'))); ?></section>
				</div>
			</div>
		<?php endif; ?>

		<?php if ($this['widgets']->count('fullwidth-bottom')) : ?>
			<div class="<?php echo $wrapper_classes['fullwidth-bottom']; ?>" id="tmFullWidthBottom">
				<div class="<?php echo ($this['config']->get('layout_width') == 'boxed-space') ? 'uk-container ' : ''; ?>uk-container-center">
					<section class="<?php echo $grid_classes['fullwidth-bottom']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('fullwidth-bottom', array('layout'=>$this['config']->get('grid.fullwidth-bottom.layout'))); ?></section>
				</div>
			</div>
		<?php endif; ?>


		<?php if ($this['widgets']->count('bottom-a')) : ?>
			<div class="<?php echo $wrapper_classes['bottom-a']; ?>" id="tmBottomA">
				<div class="uk-container uk-container-center">
					<section class="<?php echo $grid_classes['bottom-a']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin><?php echo $this['widgets']->render('bottom-a', array('layout'=>$this['config']->get('grid.bottom-a.layout'))); ?></section>
				</div>
			</div>
		<?php endif; ?>

		<?php if ($this['widgets']->count('breadcrumbs')) : ?>
			<div class="breadcrumbs-wrapper" id="tmBreadcrumbs">
				<div class="uk-container uk-container-center">
					<?php echo $this['widgets']->render('breadcrumbs'); ?>
				</div>
			</div>
		<?php endif; ?>

		<?php if ($this['widgets']->count('bottom-b')) : ?>
			<div class="<?php echo $wrapper_classes['bottom-b']; ?>" id="tmBottomB">
				<div class="uk-container uk-container-center">
					<section class="<?php echo $grid_classes['bottom-b']; ?>" data-uk-grid-match="{target:'> div > .uk-panel'}" data-uk-grid-margin>
						<?php echo $this['widgets']->render('bottom-b', array('layout'=>$this['config']->get('grid.bottom-b.layout'))); ?>
					</section>
				</div>
			</div>
		<?php endif; ?>

		<?php if ($this['widgets']->count('footer + footer-r + debug') || $this['config']->get('warp_branding', true) || $this['config']->get('totop_scroller', true)) : ?>
			<div class="footer-wrapper">
				<div class="uk-container uk-container-center">
					<footer class="tm-footer">
						<div class="uk-grid">
							<div class="<?php echo ($this['widgets']->count('footer-r')) ? 'uk-width-large-1-2 ' : '' ?>uk-width-medium-1-1 uk-flex-order-last-small uk-flex-order-first-large footer-l">
								<?php
									echo $this['widgets']->render('footer');
									$this->output('warp_branding');
								?>
							</div>
							<?php if ($this['widgets']->count('footer-r')) : ?>
							<div class="uk-width-large-1-2 uk-width-medium-1-1 uk-flex-order-first-small uk-flex-order-last-large footer-r">
								<?php
									echo $this['widgets']->render('footer-r');
								?>
							</div>
							<?php endif; ?>
						</div>

						<?php echo $this['widgets']->render('debug'); ?>

					</footer>

				</div>
				<?php if ($this['config']->get('totop_scroller', true)) : ?>
					<a class="tm-totop-scroller" data-uk-smooth-scroll href="#"></a>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<?php echo $this->render('footer'); ?>

		<?php if ($this['widgets']->count('offcanvas')) : ?>
			<div id="offcanvas" class="uk-offcanvas">
				<div class="uk-offcanvas-bar<?php echo ($this['config']->get('header') != 'style3') ? ' uk-offcanvas-bar-flip' : ''?>"><?php echo $this['widgets']->render('offcanvas'); ?></div>
			</div>
		<?php endif; ?>

		<?php if ($this['widgets']->count('fixed-l')) : ?>
			<div id="tmFixedLeft" class="uk-fixed-l">
				<div class="uk-fixed-l-wrapper"><?php echo $this['widgets']->render('fixed-l'); ?></div>
			</div>
		<?php endif; ?>

		<?php if ($this['widgets']->count('fixed-r')) : ?>
			<div id="tmFixedRight" class="uk-fixed-r">
				<div class="uk-fixed-r-wrapper"><?php echo $this['widgets']->render('fixed-r'); ?></div>
			</div>
		<?php endif; ?>
	</div>

</body>
</html>
