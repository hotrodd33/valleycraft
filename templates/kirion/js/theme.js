/* Copyright (C) YOOtheme GmbH, http://www.gnu.org/licenses/gpl.html GNU/GPL */

jQuery(document).ready(function($) { 

   $(function(){
    $('.hikashop_product_characteristics_table .active').each(function(){
            $(this).trigger('click');
        });
    });

       
    $(".vci-menu .hikashop_category_list_item").hover(function () {
         $(this).find('ul').fadeToggle('fast');
        });
        $(".vci-sidebar .hikashop_category_list_item").click(function () {
            $(this).find('ul').slideToggle('fast');
           });
   
    var config = $('html').data('config') || {},
            win    = $(window),
            navbar = $('.tm-navbar'),
            body   = $('body');

    // Social buttons
    $('article[data-permalink]').socialButtons(config);

    $(".drawer_toggle").click(function() {

        if (!$.easing["easeOutExpo"]) {
            $.easing["easeOutExpo"] = function(x, t, b, c, d) {
                return t == d ? b + c : c * (-Math.pow(2, -10 * t / d) + 1) + b
            }
        }
        
        //Expand
        if (drawer_state == 0) {
            $("div#tm-drawer").slideDown(400, 'easeOutExpo');
            $('.drawer_toggle span').removeClass('uk-icon-chevron-down');
            $('.drawer_toggle span').addClass('uk-icon-chevron-up');
            drawer_state = 1;
            //Collapse
        } else if (drawer_state == 1) {
            $("div#tm-drawer").slideUp(400, 'easeOutExpo');
            $('.drawer_toggle span').removeClass('uk-icon-chevron-up');
            $('.drawer_toggle span').addClass('uk-icon-chevron-down');
            drawer_state = 0;
        }
    });

    var drawer_state = 0;

    if ($('body').hasClass('header-style3')) {
        var nav      = $('.tm-nav-wrapper'),
            navitems = nav.find('.uk-navbar-nav > li'),
            logo     = $('div.tm-logo-large');

        if (navitems.length && logo.length) {
            navitems.eq(Math.floor(navitems.length/2) - 1).after('<li class="tm-nav-logo-centered" data-uk-dropdown>'+logo[0].outerHTML+'</li>');
            logo.remove();
        }
    };

    if ($('#tmMainMenu ul.uk-navbar-nav li a').hasClass('dropdown-fullwidth')) {
        $('#tmMainMenu ul.uk-navbar-nav li .dropdown-fullwidth').parent().attr('data-uk-dropdown', "{justify: '#tmMainMenu'}");
    }

});
jQuery(document).ready(function(){
    jQuery('input:checked').parent().addClass("active");
    jQuery('input[type="radio"]').click(function() {
    jQuery('input:not(:checked)').parent().removeClass("active");
    jQuery('input:checked').parent().addClass("active");
});
});
 