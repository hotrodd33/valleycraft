jQuery(document).ready(function($) {
	var central = ["IA","IL","IN","MI","MN","MO","OH","WI","ND","SD","NE","KS"];
    var south = ['AL','AR','CT','DE', 'DC', 'FL','GA','KY','LA','MA','MD','ME','MS','NC','NH','NJ','NY','PA','RI','SC','TN','VA','VT','WV'];
    $('#map').usmap({
    
    	stateStyles: {fill: '#0856a1', stroke: '#fff', opacity: '1.0'},	
    	stateHoverStyles: {opacity: '0.75'},
    	stateSpecificStyles: {
    		'IA': {fill: '#74C068'}, 
    		'IL': {fill: '#74C068'}, 
    		'IN': {fill: '#74C068'}, 
    		'MI': {fill: '#74C068'}, 
    		'MN': {fill: '#74C068'}, 
    		'MO': {fill: '#74C068'},
            'ND': {fill: '#74C068'},
            'SD': {fill: '#74C068'},
            'NE': {fill: '#74C068'},
            'KS': {fill: '#74C068'}, 
    		'OH': {fill: '#74C068'}, 
    		'WI': {fill: '#74C068'},
    		'AL': {fill: '#52C6E9'},
    		'AR': {fill: '#52C6E9'},
    		'CT': {fill: '#52C6E9'},
    		'DE': {fill: '#52C6E9'},
    		'FL': {fill: '#52C6E9'},
    		'GA': {fill: '#52C6E9'},
    		'KY': {fill: '#52C6E9'},
    		'LA': {fill: '#52C6E9'},
    		'MA': {fill: '#52C6E9'},
    		'MD': {fill: '#52C6E9'},
    		'ME': {fill: '#52C6E9'},
    		'MS': {fill: '#52C6E9'},
    		'NC': {fill: '#52C6E9'},
    		'NH': {fill: '#52C6E9'},
    		'NJ': {fill: '#52C6E9'},
    		'NY': {fill: '#52C6E9'},                
    		'PA': {fill: '#52C6E9'},
    		'RI': {fill: '#52C6E9'},
    		'SC': {fill: '#52C6E9'},                
    		'TN': {fill: '#52C6E9'},
    		'VA': {fill: '#52C6E9'},
    		'VT': {fill: '#52C6E9'}, 
    		'WV': {fill: '#52C6E9'}
  },
 
  
      		 click : function(event, data) {
      		 
	        if ($.inArray(data.name, central) != -1)
	        {
                $('#central-rep').show("slow").css({'background':'#e2e2e2', 'margin-left':'-100px'});
      			$('#east-rep').hide("fast");
       			$('#west-rep').hide("fast");
	        }
	        else {
	        if ($.inArray(data.name, south) != -1)
	        {
				$('#central-rep').hide("fast");
				$('#east-rep').show("slow").css({'background':'#e2e2e2', 'margin-left':'-100px'});
				 $('#west-rep').hide("fast");
	        }
	        else
	       {
			$('#central-rep').hide("fast");
			$('#east-rep').hide("fast");
			 $('#west-rep').show("slow").css({'background':'#e2e2e2', 'margin-left':'-100px'});
	        		
	        }		
	    } },
  
  showLabels: true
    });
    $(".canada").click(function() {
        $('#central-rep').hide("fast");  
       $('#east-rep').hide("fast");    
       $('#west-rep').show("slow").css({'background':'#e2e2e2', 'margin-left':'-100px'});
   });
   $(".canada1").click(function() {
       $('#central-rep').show("slow").css({'background':'#e2e2e2', 'margin-left':'-100px'});
       $('#east-rep').hide("fast");
       $('#west-rep').hide("fast");
   });     
   $(".international").click(function() {
        $('#central-rep').hide("fast");
       $('#east-rep').hide("fast");
       $('#west-rep').show("slow").css({'background':'#e2e2e2', 'margin-left':'-100px'});
        
   });

  });

