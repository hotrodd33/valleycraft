<?php
/**
 * @package	HikaShop for Joomla!
 * @version	2.3.5
 * @author	hikashop.com
 * @copyright	(C) 2010-2015 HIKARI SOFTWARE. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><div id="hikashop_product_files_main" class="hikashop_product_files_main">
	<?php
	
	if (!empty ($this->element->files)) {
		$skip = true;
		foreach ($this->element->files as $file) {
			if ($file->file_free_download)
				$skip = false;
		}
		if (!$skip) {
			global $Itemid;
			$url_itemid='';
			if(!empty($Itemid)){
				$url_itemid='&Itemid='.$Itemid;
			}
		?><div id="vci-files">
			
			<?php
			$html = array ();
			//echo '<legend>' . JText :: _('DOWNLOADS') . '</legend>';
			foreach ($this->element->files as $file) {
				if (empty ($file->file_name)) {
					$file->file_name = $file->file_path;
				}
				$fileHtml = '';
				if (!empty ($file->file_free_download)) {
					$thefile = $file->file_path;
					$ext = pathinfo($thefile, PATHINFO_EXTENSION);
					if ($ext == 'jpg' || $ext == 'png') {
					$fileHtml = '<a class="vci-image hikashop_product_file_link" href="' . hikashop_completeLink('product&task=download&file_id=' . $file->file_id.$url_itemid) . '"><img src="/media/com_hikashop/upload/safe/'.$file->file_path .'" alt="' . $file->file_name . '"/></a>';
				} else {
					$fileHtml = '<a class="pdf-icon hikashop_product_file_link" href="' . hikashop_completeLink('product&task=download&file_id=' . $file->file_id.$url_itemid) . '"><img src="/images/pdf.png" alt="PDF Download"/>' . $file->file_name . '</a>';
				}
			}
				$html[] = $fileHtml;
			}
			echo implode('</div><div>', $html);
			?>
			
		</div>
			<?php
		}
	}

	?>
</div>
