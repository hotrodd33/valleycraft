<?php
/**
* @package	HikaShop for Joomla!
* @version	2.6.0
* @author	hikashop.com
* @copyright	(C) 2010-2015 HIKARI SOFTWARE. All rights reserved.
* @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
*/
defined('_JEXEC') or die('Restricted access');
?>
	<?php


hikashop_loadJslib('jquery');
$js = '';
$params = null;
$this->params->set('vote_type','product');
if(isset($this->element->main)){
	$product_id = $this->element->main->product_id;
}else{
	$product_id = $this->element->product_id;
}
$this->params->set('vote_ref_id',$product_id);
$this->params->set('productlayout','show_tabular');
$layout_vote_mini = hikashop_getLayout('vote', 'mini', $this->params, $js);
$layout_vote_listing = hikashop_getLayout('vote', 'listing', $this->params, $js);
$layout_vote_form = hikashop_getLayout('vote', 'form', $this->params, $js);
$config =& hikashop_config();
$status_vote = $config->get('enable_status_vote');
$hide_specs = 1;
if($this->element->product_manufacturer_id != 0 || $this->element->product_weight != 0 || $this->element->product_width != 0 || $this->element->product_height != 0 || $this->element->product_length != 0 || @$this->element->main->product_weight != 0 || @$this->element->main->product_width != 0 || @$this->element->main->product_height != 0 || @$this->element->main->product_length != 0)
$hide_specs = 0;
foreach ($this->fields as $fieldName => $oneExtraField) {
	$value = '';
	if(empty($this->element->$fieldName) && !empty($this->element->main->$fieldName))$this->element->$fieldName = $this->element->main->$fieldName;
	if(isset($this->element->$fieldName))
	$value = trim($this->element->$fieldName);
	if(!empty($value)){
		$hide_specs = 0;
		break;
	}
}

?>

		<?php if(HIKASHOP_RESPONSIVE){ ?>
		<div class="container" data-uk-grid-margin="">
			<div class="row">
				<?php } ?>
				<div id="hikashop_product_left_part" class="col-sm-6">
					<?php
		if(!empty($this->element->extraData->leftBegin)) { echo implode("\r\n",$this->element->extraData->leftBegin); }
		$this->row = & $this->element;
		$this->setLayout('show_block_img');
		echo $this->loadTemplate();
		?>
						<div id="hikashop_product_description_main_mini" class="hikashop_product_description_main_mini">
							<?php
				if(!empty($this->element->product_description)){
					$resume = substr(strip_tags(preg_replace('#<hr *id="system-readmore" */>.*#is','',$this->element->product_description)),0,300);
					if (!empty($this->element->product_description) && strlen($this->element->product_description)>300)
					$resume .= " ...<a href='#hikashop_show_tabular_description'>".JText::_('READ_MORE')."</a>";
					echo JHTML::_('content.prepare',$resume);
				}
				
				?>
						</div>
						<?php if(!empty($this->element->extraData->leftEnd)) { echo implode("\r\n",$this->element->extraData->leftEnd); } ?>
				</div>
				<div id="hikashop_product_right_part" class="col-sm-6">

					<div id="product-title" class="row">
						<div id="part-number" class="title col-sm-8">
							<span id="hikashop_product_code_main" class="hikashop_product_code_main hikashop_product_name_main">
								<?php echo $this->element->product_code; ?>
							</span>
						</div>
						<div class="hikashop_product_price_main col-sm-4 text-right">
							<span id="hikashop_product_price_main">
								<?php
			$user = JFactory::getUser();        // Get the user object
			$app  = JFactory::getApplication(); // Get the application
			
			if ($user->id != 0)
			{
				if ($this->params->get('show_price')) {
					$this->row = & $this->element;
					$this->setLayout('listing_price2');
					//print_r($this->element);
					echo $this->loadTemplate();
				}
				?>
								</span>
								<?php if(count($this->element->options)) {
	?>
								<span id="hikashop_product_price_with_options_main" class="hikashop_product_price_with_options_main"></span>
								<?php
		} 
			}
			else 
			{
				echo " ";
			} ?>
			

						</div>
					</div>
					<div id="product-name" class="row">
						<span id="product_name_main">
							<?php		
			if (hikashop_getCID('product_id')!=$this->element->product_id && isset ($this->element->main->product_name))
			{
				echo $this->element->main->product_name;
			}
				else
			{
				echo $this->element->product_name;
			}
			?>
						</span>
					</div>
					<?php	if(!empty($this->element->extraData->rightBegin))
			echo implode("\r\n",$this->element->extraData->rightBegin);
			if($this->params->get('characteristic_display')!='list'){
				$this->setLayout('show_block_characteristic');
				echo $this->loadTemplate();
			}
			$form = ',0';
			if (!$this->config->get('ajax_add_to_cart', 1)) {
				$form = ',\'hikashop_product_form\'';
			}
			if (hikashop_level(1) && !empty ($this->element->options)) {
				?>
						<div id="product-options" class="tm-product-options row">
							<?php
		$this->setLayout('option');
		echo $this->loadTemplate();
		?>
						</div>
						<?php
	$form = ',\'hikashop_product_form\'';
	if ($this->config->get('redirect_url_after_add_cart', 'stay_if_cart') == 'ask_user') {
		?>
							<input type="hidden" name="popup" value="1" />
							<?php
	}
	}
	if (!$this->params->get('catalogue') && ($this->config->get('display_add_to_cart_for_free_products') || !empty ($this->element->prices))) {
	if (!empty ($this->itemFields)) {
		$form = ',\'hikashop_product_form\'';
		if ($this->config->get('redirect_url_after_add_cart', 'stay_if_cart') == 'ask_user') {
			?>
								<input type="hidden" name="popup" value="1" />
								<?php
			}
		$this->setLayout('show_block_custom_item');
		echo $this->loadTemplate();
		}
	}
	$this->formName = $form;
	$contact = $this->config->get('product_contact',0); ?>
									<div id="hikashop_product_contact_main" class="row hikashop_product_contact_main">

									</div>
									<?php
	if(!empty($this->element->extraData->rightMiddle))
	echo implode("\r\n",$this->element->extraData->rightMiddle);
	?>
										<span id="hikashop_product_id_main" class="hikashop_product_id_main">
											<input type="hidden" name="product_id" value="<?php echo $this->element->product_id; ?>" />
										</span>
										<?php if(empty ($this->element->characteristics) || $this->params->get('characteristic_display')!='list'){ ?>
										<div id="hikashop_product_quantity_main" class="hikashop_product_quantity_main row">
											<?php
		$this->row = & $this->element;
		$this->ajax = 'if(hikashopCheckChangeForm(\'item\',\'hikashop_product_form\')){ return hikashopModifyQuantity(\'' . $this->row->product_id . '\',field,1' . $form . ',\'cart\'); } else { return false; }';
		$this->setLayout('quantity');
		echo $this->loadTemplate();
		?>
										</div>
										<?php
		}
		
		
		$this->setLayout('show_block_tags');					
		echo $this->loadTemplate();

		if(!empty($this->element->extraData->rightEnd))
		echo implode("\r\n",$this->element->extraData->rightEnd);
		?>
											<div id="hikashop_tabs_div">
												<ul class="hikashop_tabs_ul cols-sm-12">
													<li id="hikashop_show_tabular_description_li" class="vci-tab hikashop_tabs_li">About</li>
													<li id="hikashop_show_tabular_specs_li" class="vci-tab hikashop_tabs_li">Specs</li>
													<li id="hikashop_show_tabular_media_li" class="vci-tab hikashop_tabs_li">Media</li>
													<li id="hikashop_show_tabular_related_li" class="vci-tab hikashop_tabs_li">Accessories</li>
													<li id="hikashop_show_tabular_sales_li" class="vci-tab hikashop_tabs_li">Contact</a></li>
												</ul>
												<?php
		if(!empty($this->element->extraData->bottomBegin))
		echo implode("\r\n",$this->element->extraData->bottomBegin);
		?>
													<div class="hikashop_tabs_content row" id="hikashop_show_tabular_description">
														<div id="hikashop_product_description_main" class="hikashop_product_description_main">
															<?php
					echo JHTML::_('content.prepare',preg_replace('#<hr *id="system-readmore" */>#i','',$this->element->product_description));
					?>
														</div>
														<span id="hikashop_product_url_main" class="hikashop_product_url_main">
															<?php
					if (!empty ($this->element->product_url)) {
						echo JText :: sprintf('MANUFACTURER_URL', '<a href="' . $this->element->product_url . '" target="_blank">' . $this->element->product_url . '</a>');
					}
					?>
														</span>
													</div>
													<div class="hikashop_tabs_content row" id="hikashop_show_tabular_specs">
														<?php
				$this->setLayout('show_block_dimensions');
				echo $this->loadTemplate();
				if(!empty($this->fields)){
					$this->setLayout('show_block_custom_main');
					echo $this->loadTemplate();
				}
				?>
													</div>
													<div class="hikashop_tabs_content row" id="hikashop_show_tabular_media">
														<p>Download files related to his product.</p>
														<?php 	$this->setLayout('show_block_product_files');
																echo $this->loadTemplate(); ?>
													</div>
													<div class="hikashop_tabs_content row" id="hikashop_show_tabular_related">
													<?php
														if(!empty ($this->modules) && is_array($this->modules)) {
															jimport('joomla.application.module.helper');
															foreach($this->modules as $module) {
																echo JModuleHelper::renderModule($module);
															}
														}
													?>
													</div>
													<div class="hikashop_tabs_content row" id="hikashop_show_tabular_sales">
														<?php echo JHtml::_('content.prepare', '{loadposition vci-sales}'); ?>
													</div>
													<input type="hidden" name="selected_tab" id="selected_tab" value="hikashop_show_tabular_description" />
													<?php
			if(!empty($this->element->extraData->bottomEnd))
			echo implode("\r\n",$this->element->extraData->bottomEnd);
			?>
											</div>
				</div>
			</div>
		</div>
		<input type="hidden" name="cart_type" id="type" value="cart" />
		<input type="hidden" name="add" value="1" />
		<input type="hidden" name="ctrl" value="product" />
		<input type="hidden" name="task" value="updatecart" />
		<input type="hidden" name="return_url" value="<?php echo urlencode(base64_encode(urldecode($this->redirect_url)));?>" />
		</form>

		<script type="text/javascript">
			if (typeof (hkjQuery) == "undefined") window.hkjQuery = window.jQuery;
			window.hikashop.ready(function () {
				var selectedTab = hkjQuery("#selected_tab").val();
				hkjQuery("#hikashop_tabs_div").children("div").css("display", "none");
				hkjQuery("#" + selectedTab + "_li").addClass("hikashop_tabs_li_selected");
				hkjQuery("#" + selectedTab).css("display", "inherit");
				hkjQuery("#hikashop_tabs_div .hikashop_tabs_ul li").click(function () {
					var currentLi = hkjQuery(this).attr("id");
					var currentDiv = currentLi.replace("_li", "");
					hkjQuery("#hikashop_tabs_div").children("div").css("display", "none");
					hkjQuery("#hikashop_tabs_div").children("form").children("div").css("display", "none");
					hkjQuery("#" + currentDiv).css("display", "inherit");
					hkjQuery(".hikashop_tabs_li_selected").removeClass("hikashop_tabs_li_selected");
					hkjQuery("#" + currentLi).addClass("hikashop_tabs_li_selected");
				});
			});
		</script>