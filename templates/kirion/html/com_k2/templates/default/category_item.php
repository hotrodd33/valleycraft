<?php
/**
 * @version		$Id: category_item.php 1812 2013-01-14 18:45:06Z lefteris.kavadas $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2013 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

// Define default image size (do not change)
K2HelperUtilities::setDefaultImage($this->item, 'itemlist', $this->params);

$hasCatImage = ($this->item->params->get('catItemImage') && !empty($this->item->image)) ? ' hasCatImage' : '';


?>

<!-- Start K2 Item Layout -->
<div class="catItemView catItemBlog<?php echo $hasCatImage; ?> group<?php echo ucfirst($this->item->itemGroup); ?><?php echo ($this->item->featured) ? ' catItemIsFeatured' : ''; ?><?php if($this->item->params->get('pageclass_sfx')) echo ' '.$this->item->params->get('pageclass_sfx'); ?>">
	
	<?php if($this->item->params->get('catItemImage') && !empty($this->item->image)): ?>
		<div class="catItemImage" style="background-image: url('<?php echo $this->item->image; ?>')"></div>
  	<?php endif; ?>
			
	<div class="catItemDetails">
	  	<?php if($this->item->params->get('catItemTitle')): ?>
	  	<!-- Item title -->
	  	<h3 class="catItemTitle">
			<?php if(isset($this->item->editLink)): ?>
			<!-- Item edit link -->
			<span class="catItemEditLink">
				<a class="modal" rel="{handler:'iframe',size:{x:990,y:550}}" href="<?php echo $this->item->editLink; ?>">
					<?php echo JText::_('K2_EDIT_ITEM'); ?>
				</a>
			</span>
			<?php endif; ?>

		  	<?php if ($this->item->params->get('catItemTitleLinked')): ?>
				<a href="<?php echo $this->item->link; ?>">
		  		<?php echo $this->item->title; ?>
		  	</a>
		  	<?php else: ?>
		  	<?php echo $this->item->title; ?>
		  	<?php endif; ?>

		  	<?php if($this->item->params->get('catItemFeaturedNotice') && $this->item->featured): ?>
		  	<!-- Featured flag -->
		  	<span>
			  	<sup>
			  		<?php echo JText::_('K2_FEATURED'); ?>
			  	</sup>
		  	</span>
		  	<?php endif; ?>
	  	</h3>
	  	<?php endif; ?>
		<div class="catItemText">
			<!-- Plugins: BeforeDisplayContent -->
			<?php echo $this->item->event->BeforeDisplayContent; ?>

			<?php if($this->item->params->get('catItemIntroText')): ?>
			<!-- Item introtext -->
			<div class="catItemIntroText">
				<?php echo $this->item->introtext; ?>
			</div>
			<?php endif; ?>

			<div class="clr"></div>

			<?php if($this->item->params->get('catItemExtraFields') && count($this->item->extra_fields)): ?>
			<!-- Item extra fields -->
			<div class="catItemExtraFields">
				<h4><?php echo JText::_('K2_ADDITIONAL_INFO'); ?></h4>
				<ul>
				<?php foreach ($this->item->extra_fields as $key=>$extraField): ?>
				<?php if($extraField->value != ''): ?>
				<li class="<?php echo ($key%2) ? "odd" : "even"; ?> type<?php echo ucfirst($extraField->type); ?> group<?php echo $extraField->group; ?>">
					<?php if($extraField->type == 'header'): ?>
					<h4 class="catItemExtraFieldsHeader"><?php echo $extraField->name; ?></h4>
					<?php else: ?>
					<span class="catItemExtraFieldsLabel"><?php echo $extraField->name; ?></span>
					<span class="catItemExtraFieldsValue"><?php echo $extraField->value; ?></span>
					<?php endif; ?>
				</li>
				<?php endif; ?>
				<?php endforeach; ?>
				</ul>
				<div class="clr"></div>
			</div>
			<?php endif; ?>
			<!-- Plugins: AfterDisplayContent -->
			<?php echo $this->item->event->AfterDisplayContent; ?>
			<div class="clr"></div>
	  	</div>
	  	<?php if ($this->item->params->get('catItemReadMore')): ?>
	  		<div class="uk-float-left">
				<a class="k2ReadMore readon" href="<?php echo $this->item->link; ?>">
					<?php echo JText::_('K2_READ_MORE'); ?>
				</a>
	  		</div>
		<?php endif; ?>
		<div class="catItemToolbar uk-float-right">
			<?php if($this->item->params->get('catItemAuthor')): ?>
			<!-- Item Author -->
			<span class="catItemAuthor">
				<i class="uk-icon-user"></i>
				<?php if(isset($this->item->author->link) && $this->item->author->link): ?>
				<a rel="author" href="<?php echo $this->item->author->link; ?>"><?php echo $this->item->author->name; ?></a>
				<?php else: ?>
				<?php echo $this->item->author->name; ?>
				<?php endif; ?>
			</span>
			<?php endif; ?>
			<!-- Item tags -->
		 <?php if(
		  $this->item->params->get('catItemHits') ||
		  $this->item->params->get('catItemTags') ||
		  $this->item->params->get('catItemAttachments')
		  ): ?>
		  	<div class="catItemLinks">

				<?php if($this->item->params->get('catItemHits')): ?>
				<!-- Item Hits -->
				<div class="catItemHitsBlock">
					<span class="catItemHits">
						<i class="uk-icon-book"></i> <?php echo $this->item->hits; ?> <?php echo JText::_('K2_TIMES'); ?>
					</span>
				</div>
				<?php endif; ?>
			</div>
			<?php endif; ?>

			<?php if($this->item->params->get('catItemCommentsAnchor') && ( ($this->item->params->get('comments') == '2' && !$this->user->guest) || ($this->item->params->get('comments') == '1')) ): ?>
			<!-- Anchor link to comments below -->
			<div class="catItemCommentsLink">
				<i class="uk-icon-comment"></i>
				<?php if(!empty($this->item->event->K2CommentsCounter)): ?>
					<!-- K2 Plugins: K2CommentsCounter -->
					<?php echo $this->item->event->K2CommentsCounter; ?>
				<?php else: ?>
					<?php if($this->item->numOfComments > 0): ?>
					<a href="<?php echo $this->item->link; ?>#itemCommentsAnchor">
						<?php echo $this->item->numOfComments; ?> <?php echo ($this->item->numOfComments>1) ? JText::_('K2_COMMENTS') : JText::_('K2_COMMENT'); ?>
					</a>
					<?php else: ?>
					<a href="<?php echo $this->item->link; ?>#itemCommentsAnchor">
						<?php echo '0 ' . JText::_('K2_COMMENT'); ?>
					</a>
					<?php endif; ?>
				<?php endif; ?>
			</div>
			<?php endif; ?>
			<div class="clr"></div>
		</div>
		<div class="clr"></div>
	</div>
</div>
<!-- End K2 Item Layout -->
