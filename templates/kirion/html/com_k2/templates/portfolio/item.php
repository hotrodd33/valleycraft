<?php
/**
 * @version		$Id: item.php 1812 2013-01-14 18:45:06Z lefteris.kavadas $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2013 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

$document = JFactory::getDocument();
$app = JFactory::getApplication();
$current_tmpl = $app->getTemplate();

$document->addScript("templates/".$current_tmpl."/js/prettySocial.min.js");
$document->addScriptDeclaration("
	jQuery(document).ready(function($) {
		$('.prettySocial').prettySocial();
	});
	");

?>

<?php if(JRequest::getInt('print')==1): ?>
<!-- Print button at the top of the print page only -->
<a class="itemPrintThisPage" rel="nofollow" href="#" onclick="window.print();return false;">
	<span><?php echo JText::_('K2_PRINT_THIS_PAGE'); ?></span>
</a>
<?php endif; ?>

<!-- Start K2 Item Layout -->
<span id="startOfPageId<?php echo JRequest::getInt('id'); ?>"></span>

<div id="k2Container" class="itemView ItemPortfolio<?php echo ($this->item->featured) ? ' itemIsFeatured' : ''; ?><?php if($this->item->params->get('pageclass_sfx')) echo ' '.$this->item->params->get('pageclass_sfx'); ?>">
	<?php if($this->item->params->get('itemImage') && !empty($this->item->image)): ?>
	  	<!-- Item Image -->
	    <div class="itemImageBlock">
		  	<a class="modal" rel="{handler: 'image'}" href="<?php echo $this->item->imageXLarge; ?>" title="<?php echo JText::_('K2_CLICK_TO_PREVIEW_IMAGE'); ?>">
		  		<img src="<?php echo $this->item->image; ?>" alt="<?php if(!empty($this->item->image_caption)) echo K2HelperUtilities::cleanHtml($this->item->image_caption); else echo K2HelperUtilities::cleanHtml($this->item->title); ?>" style="width:<?php echo $this->item->imageWidth; ?>px; height:auto;" />
		  	</a>
	    </div>
  	<?php endif; ?>
  	<div class="IpInfoBlock">
		<div class="uk-grid" data-uk-grid>
			<!-- K2 Plugins: K2AfterDisplayContent -->
			<?php echo $this->item->event->K2AfterDisplayContent; ?>
			<div class="uk-width-medium-2-5">
			    <div class="ItemPortfolioToolbar">
			    	<ul>
			    		<li>
					    	<?php if($this->item->params->get('catItemDateCreated')): ?>
					    		<!-- Date created -->
					    		<?php 
					    			$createDay = date('d', strtotime( $this->item->created ));
					    			$createMonth = JText::_(strtoupper(date('F', strtotime( $this->item->created )))."_SHORT");
					    			$createYear = date('Y', strtotime( $this->item->created ));
					    		?>
					    		<span class="pLabel">
					    			Created Date:
					    		</span>
					    		 <span class="catItemDateCreated">
					    			<span class="blog-date"><?php echo $createDay; ?></span>
					    			<span class="blog-month"><?php echo $createMonth ."  ". $createYear;?></span>
					    		</span>
					    	<?php endif; ?>
			    		</li>
			    		<li>
					    	<?php if($this->item->params->get('itemAuthor')): ?>
							<!-- Item Author -->
							<span class="pLabel">
								Author:
							</span>
							<span class="itemAuthor">
								<?php if(empty($this->item->created_by_alias)): ?>
								<a rel="author" href="<?php echo $this->item->author->link; ?>"><?php echo $this->item->author->name; ?></a>
								<?php else: ?>
								<?php echo $this->item->author->name; ?>
								<?php endif; ?>
							</span>
							<?php endif; ?>
			    		</li>
			    		<li>
							<?php if($this->item->params->get('itemCategory')): ?>
							<!-- Item category -->
							<span class="pLabel">Category:</span>
							<span class="itemCategory">
								<a href="<?php echo $this->item->category->link; ?>"><?php echo $this->item->category->name; ?></a>
							</span>
							<?php endif; ?>
			    		</li>
			    		<li>		    			
					  		<?php if($this->item->params->get('itemRating')): ?>
							<!-- Item Rating -->
							<span class="pLabel">Rate us:</span>
							<div class="itemRatingBlock">
								<div class="itemRatingForm">
									<ul class="itemRatingList">
										<li class="itemCurrentRating" id="itemCurrentRating<?php echo $this->item->id; ?>" style="width:<?php echo $this->item->votingPercentage; ?>%;"></li>
										<li><a href="#" rel="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_1_STAR_OUT_OF_5'); ?>" class="one-star">1</a></li>
										<li><a href="#" rel="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_2_STARS_OUT_OF_5'); ?>" class="two-stars">2</a></li>
										<li><a href="#" rel="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_3_STARS_OUT_OF_5'); ?>" class="three-stars">3</a></li>
										<li><a href="#" rel="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_4_STARS_OUT_OF_5'); ?>" class="four-stars">4</a></li>
										<li><a href="#" rel="<?php echo $this->item->id; ?>" title="<?php echo JText::_('K2_5_STARS_OUT_OF_5'); ?>" class="five-stars">5</a></li>
									</ul>
									<div id="itemRatingLog<?php echo $this->item->id; ?>" class="itemRatingLog"><?php echo $this->item->numOfvotes; ?></div>
								</div>
							</div>
							<?php endif; ?>
			    		</li>
			    	</ul>
			    </div>
			</div>

			<div class="uk-width-medium-3-5">
				<?php if($this->item->params->get('itemExtraFields') && count($this->item->extra_fields)): ?>
					<!-- Item extra fields -->
					<div class="itemExtraFields">
						<ul>
							<?php foreach ($this->item->extra_fields as $key=>$extraField): ?>
							<?php if($extraField->value != ''): ?>
							<li class="<?php echo ($key%2) ? "odd" : "even"; ?> type<?php echo ucfirst($extraField->type); ?> group<?php echo $extraField->group; ?>">
								<?php if($extraField->type == 'header'): ?>
								<h4 class="itemExtraFieldsHeader"><?php echo $extraField->name; ?></h4>
								<?php else: ?>
								<span class="pLabel"><?php echo $extraField->name; ?>:</span>
								<span class="itemExtraFieldsValue"><?php echo $extraField->value; ?></span>
								<?php endif; ?>
							</li>
							<?php endif; ?>
							<?php endforeach; ?>
						</ul>
						<div class="clr"></div>
					</div>
				<?php endif; ?>
			</div>
		</div>
  	</div>
	<div class="ItemPortfolioDesc">
	  	
		<?php if($this->item->params->get('itemTitle')): ?>
		<!-- Item title -->
			<h2 class="itemTitle">
			<?php if(isset($this->item->editLink)): ?>
			<!-- Item edit link -->
			<span class="itemEditLink">
				<a class="modal" rel="{handler:'iframe',size:{x:990,y:550}}" href="<?php echo $this->item->editLink; ?>">
					<?php echo JText::_('K2_EDIT_ITEM'); ?>
				</a>
			</span>
			<?php endif; ?>

		  	<?php echo $this->item->title; ?>

		  	<?php if($this->item->params->get('itemFeaturedNotice') && $this->item->featured): ?>
			  	<!-- Featured flag -->
			  	<span class="uk-hidden-small">
				  	<?php echo JText::_('K2_FEATURED'); ?>
			  	</span>
		  	<?php endif; ?>
		</h2>
		<?php endif; ?>

		<!-- Plugins: AfterDisplayTitle -->
	  	<?php echo $this->item->event->AfterDisplayTitle; ?>
	  	<div class="ItemText">
			<?php if(!empty($this->item->fulltext)): ?>
			<?php if($this->item->params->get('itemIntroText')): ?>
			<!-- Item introtext -->
			<div class="itemIntroText">
				<?php echo $this->item->introtext; ?>
			</div>
			<?php endif; ?>
			<?php if($this->item->params->get('itemFullText')): ?>
			<!-- Item fulltext -->
			<div class="itemFullText">
				<?php echo $this->item->fulltext; ?>
			</div>
			<?php endif; ?>
			<?php else: ?>
			<!-- Item text -->
			<div class="itemFullText">
				<?php echo $this->item->introtext; ?>
			</div>
			<?php endif; ?>
	  	</div>
		<div class="ItemPortShareBlock">
			<h3>
				I Would Like To Share it.
			</h3>
	    	<ul>
		    	<li class="facebook_share">
		    		<a class="prettySocial" href="#" data-type="facebook" data-title="<?php echo $this->item->title; ?>" data-description="<?php echo strip_tags($this->item->introtext); ?>" data-url="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$this->item->link; ?>" data-media="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$this->item->image; ?>">
		    			<i class="uk-icon-facebook uk-icon-button"></i>
		    		</a>
		    	</li>
		    	<li class="twitter_share">
		    		<a class="prettySocial" href="#" data-type="twitter" data-title="<?php echo $this->item->title; ?>" data-description="<?php echo strip_tags($this->item->introtext); ?>" data-url="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$this->item->link; ?>" data-media="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$this->item->image; ?>">
    		    		<i class="uk-icon-twitter uk-icon-button"></i>
		    		</a>
		    	</li>
		    	<li class="google_share">
		    		<a class="prettySocial" href="#" data-type="googleplus" data-title="<?php echo $this->item->title; ?>" data-description="<?php echo strip_tags($this->item->introtext); ?>" data-url="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$this->item->link; ?>" data-media="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$this->item->image; ?>">
		    			<i class="uk-icon-google-plus uk-icon-button"></i>
		    		</a>
		    	</li>

		    	<?php if($this->item->params->get('itemPrintButton') && !JRequest::getInt('print')): ?>
		    	<!-- Print Button -->
		    	<li class="print_share">
		    		<a class="itemPrintLink" rel="nofollow" href="<?php echo $this->item->printLink; ?>" onclick="window.open(this.href,'printWindow','width=900,height=600,location=no,menubar=no,resizable=yes,scrollbars=yes'); return false;">
		    			<i class="uk-icon-print uk-icon-button"></i>
		    		</a>
		    	</li>
		    	<?php endif; ?>

		    	<?php if($this->item->params->get('itemEmailButton') && !JRequest::getInt('print')): ?>
		    	<!-- Email Button -->
		    	<li class="email_share">
		    		<a class="itemEmailLink" rel="nofollow" href="<?php echo $this->item->emailLink; ?>" onclick="window.open(this.href,'emailWindow','width=400,height=350,location=no,menubar=no,resizable=no,scrollbars=no'); return false;">
		    			<i class="uk-icon-envelope uk-icon-button"></i>
		    		</a>
		    	</li>
		    	<?php endif; ?>
		    </ul>
		</div>
	</div>	
</div>
<!-- End K2 Item Layout -->
