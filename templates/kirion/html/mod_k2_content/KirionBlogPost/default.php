<?php
/**
 * @version		$Id: default.php 1618 2012-09-21 11:23:08Z lefteris.kavadas $
 * @package		K2
 * @author		JoomlaWorks http://www.joomlaworks.net
 * @copyright	Copyright (c) 2006 - 2012 JoomlaWorks Ltd. All rights reserved.
 * @license		GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;


Jhtml::_('behavior.modal');

?>

<div id="k2ModuleBox<?php echo $module->id; ?>" class="KirionBPost<?php if($params->get('moduleclass_sfx')) echo ' '.$params->get('moduleclass_sfx'); ?>">

    <?php if(count($items)): ?>
    <div class="blog-post">

        <?php foreach ($items as $key=>$item):  ?>
        <div class="blogPostItem">
            
            <?php if($params->get('itemImage') && isset($item->image)): ?>
                <a class="moduleItemImage" style="background-image: url('<?php echo $item->image; ?>');" href="<?php echo $item->link; ?>" title="<?php echo K2HelperUtilities::cleanHtml($item->title); ?>">
                </a>
            <?php endif; ?>

            <?php if($params->get('itemIntroText') or $params->get('itemTitle') or $params->get('itemDateCreated')): ?>
                <div class="detailHolder">
                    <?php if($params->get('itemTitle')): ?>
                        <h3 class="moduleItemTitle">
                            <a href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
                        </h3>
                    <?php endif; ?>
                    <div class="meta">
                        <?php if($params->get('itemDateCreated')): ?>
                            <span class="dateTime">
                                <?php echo JHTML::_('date', $item->modified, JText::_('DATE_FORMAT_LC3')); ?>
                            </span>
                        <?php endif; ?>

                        <?php if($params->get('itemCommentsCounter') && $componentParams->get('comments')): ?>      
                            <?php if(!empty($item->event->K2CommentsCounter)): ?>
                                <!-- K2 Plugins: K2CommentsCounter -->
                                <?php echo $item->event->K2CommentsCounter; ?>
                            <?php else: ?>
                                <?php if($item->numOfComments>0): ?>
                                <a class="moduleItemComments" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
                                    <?php echo $item->numOfComments; ?> <?php if($item->numOfComments>1) echo JText::_('K2_COMMENTS'); else echo JText::_('K2_COMMENT'); ?>
                                </a>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                   

                    <?php if($params->get('itemIntroText')): ?>
                        <p>
                            <?php echo $item->introtext; ?>
                        </p>
                    <?php endif; ?>

                    <?php if($params->get('itemReadMore') && $item->fulltext): ?>
                    <div class="moduleLink">
                        <a class="modal zoom" data-uk-lightbox="" href="<?php echo $item->imageXLarge; ?>" title="<?php echo JText::_('K2_CLICK_TO_PREVIEW_IMAGE'); ?>">
                            <i class="uk-icon-search"></i>
                        </a>
                        <a class="readmore" href="<?php echo $item->link; ?>">
                            <i class="uk-icon-link"></i>
                        </a>
                    </div>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
        <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>
