<?php
define( 'SITEGUARDING_FIREWALL_STATUS', false);		  // true - firewall works, false - disabled

define( 'SITEGUARDING_SCAN_PATH', '/home/valley67/public_html/');		  // Full path e.g. /home/aaa/public_html/
define( 'SITEGUARDING_DIRSEP', '/');		                          // for Unix leave /

define( 'SITEGUARDING_DEFAULT_ACTION', 'allow');		// Defaut action for session
define( 'SITEGUARDING_EMAIL_FOR_ALERTS', 'team@siteguarding.com');		// Email for alerts
define( 'SITEGUARDING_SINGLE_LOG_FILE', false);		// false - For each file creates own log file, false - single log file
define( 'SITEGUARDING_SAVE_EMPTY_REQUESTS', false);	// true - save all requests (if (count($_REQUEST) =>0)
define( 'SITEGUARDING_FLOAT_FILE_FOLDER', false);	// true - for global server analyze, false for single website
define( 'SITEGUARDING_LOG_FILE_MAX_SIZE', 5);	    // log file in Mb
define( 'SITEGUARDING_BLOCK_BASE64_REQUESTS', false);	            // will block any requests what possible to decode by base64	(default: false)
define( 'SITEGUARDING_BLOCK_REQUESTS_NOSPACES_OVER_BYTES', 50000);	// will block any requests with no spaces if the lenth over xxx bytes (0 - for not blocking)
define( 'SITEGUARDING_BLOCK_EMPTY_FILES', true);	// will block access to empty files (size 0 bytes) true - block , false - allow
define( 'SITEGUARDING_PHP_ERROR_CONTROL', false);	// will create register_shutdown_function hook, saves to _php_errors.log
define( 'SITEGUARDING_DUPS', false);
define( 'SITEGUARDING_UNSET_PASSWORD_DATA', false);	// true - will unset pass, password all requests, will crypt with PGP
define( 'SITEGUARDING_HTTP_FOR_ALERTS', true);	// true - will send alert to SG server
?>