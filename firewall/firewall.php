<?php
/**
 * Collect all requests module
 * Copyright by SiteGuarding.com
 * Date: 15 Apr 2017
 * ver.: 3.3.1
 */
define( 'SITEGUARDING_DEBUG', false);
define( 'SITEGUARDING_DEBUG_IP', '1.2.3.4');

error_reporting( 0 );

if( ! ini_get('date.timezone') )
{
    date_default_timezone_set('GMT');
}


if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') $DIRSEP = '\\';
else $DIRSEP = '/';

$file_firewall_class = dirname(__FILE__).$DIRSEP.'firewall.class.php';
$file_firewall_config = dirname(__FILE__).$DIRSEP.'firewall.config.php';
$file_firewall_rules = dirname(__FILE__).$DIRSEP.'rules.txt';

/**
 * Debug some values
 */
if (SITEGUARDING_DEBUG === true && $_SERVER["REMOTE_ADDR"] == SITEGUARDING_DEBUG_IP)
{
    echo 'Class '.$file_firewall_class."<br>";
    echo 'Config '.$file_firewall_config."<br>";
    echo 'Rules '.$file_firewall_rules."<br>";
    echo 'Request: <br><pre>'.print_r($_REQUEST, true).'</pre>'."<br><br>";
    echo 'Server: <br><pre>'.print_r($_SERVER, true).'</pre>'."<br><br>";
}


if (!class_exists('SiteGuarding_Firewall'))
{
	if (file_exists($file_firewall_class)) include_once($file_firewall_class);
	else die('File is not loaded: '.$file_firewall_class);
}


if (file_exists($file_firewall_config)) include_once($file_firewall_config);
else die('File is not loaded: '.$file_firewall_config);

if (!file_exists($file_firewall_rules)) die('File is not loaded: rules.txt');

if (SITEGUARDING_FIREWALL_STATUS === false) return;     // exit if firewall is disabled


// Some init constants
if (!defined('SITEGUARDING_LOG_FILE_MAX_SIZE')) define( 'SITEGUARDING_LOG_FILE_MAX_SIZE', 5);	// log file in Mb
if (!defined('SITEGUARDING_BLOCK_BASE64_REQUESTS')) define( 'SITEGUARDING_BLOCK_BASE64_REQUESTS', false);	// block any base64 requests
if (!defined('SITEGUARDING_BLOCK_REQUESTS_NOSPACES_OVER_BYTES')) define( 'SITEGUARDING_BLOCK_REQUESTS_NOSPACES_OVER_BYTES', 0);	// block any long-nospaces requests
if (!defined('SITEGUARDING_BLOCK_EMPTY_FILES')) define( 'SITEGUARDING_BLOCK_EMPTY_FILES', true);	// block access to empty files
if (!defined('SITEGUARDING_PHP_ERROR_CONTROL')) define( 'SITEGUARDING_PHP_ERROR_CONTROL', false);	// control php errors
if (!defined('SITEGUARDING_UNSET_PASSWORD_DATA')) define( 'SITEGUARDING_UNSET_PASSWORD_DATA', false);	// unset passwords
if (!defined('SITEGUARDING_DUPS')) define( 'SITEGUARDING_DUPS', false);	
if (!defined('SITEGUARDING_HTTP_FOR_ALERTS')) define( 'SITEGUARDING_HTTP_FOR_ALERTS', true);	

$firewall = new SiteGuarding_Firewall();

if (!$firewall->CheckLicKey()) 
{
    if (SITEGUARDING_DEBUG === true && $_SERVER["REMOTE_ADDR"] == SITEGUARDING_DEBUG_IP)
    {
        echo 'Lic Key is not valid.';
    }
    return;
}

$firewall->this_session_rule = SITEGUARDING_DEFAULT_ACTION;
$firewall->email_for_alerts = SITEGUARDING_EMAIL_FOR_ALERTS;
$firewall->save_empty_requests = SITEGUARDING_SAVE_EMPTY_REQUESTS;
$firewall->single_log_file = SITEGUARDING_SINGLE_LOG_FILE;
$firewall->scan_path = SITEGUARDING_SCAN_PATH;
$firewall->dirsep = SITEGUARDING_DIRSEP;
$firewall->float_file_folder = SITEGUARDING_FLOAT_FILE_FOLDER;
$firewall->log_file_max_size = SITEGUARDING_LOG_FILE_MAX_SIZE;


// PHP errors control
if (SITEGUARDING_PHP_ERROR_CONTROL === true)
{
	register_shutdown_function('A4C2651A109E0_PHP_error_control');
}

function A4C2651A109E0_PHP_error_control()
{
	$reason = error_get_last();
	
		switch($reason['type']) 
		{ 
			case E_ERROR: // 1 // 
				$reason['type'] = 'E_ERROR'; break;
			case E_WARNING: // 2 // 
				$reason['type'] =  'E_WARNING';	break;
			case E_PARSE: // 4 // 
				$reason['type'] =  'E_PARSE'; break;
			case E_NOTICE: // 8 // 
				$reason['type'] =  'E_NOTICE'; break;
			case E_CORE_ERROR: // 16 // 
				$reason['type'] =  'E_CORE_ERROR'; break;
			case E_CORE_WARNING: // 32 // 
				$reason['type'] =  'E_CORE_WARNING'; break;
			case E_COMPILE_ERROR: // 64 // 
				$reason['type'] =  'E_COMPILE_ERROR'; break;
			case E_COMPILE_WARNING: // 128 // 
				$reason['type'] =  'E_COMPILE_WARNING'; break;
			case E_USER_ERROR: // 256 // 
				$reason['type'] =  'E_USER_ERROR'; break;
			case E_USER_WARNING: // 512 // 
				$reason['type'] =  'E_USER_WARNING'; break;
			case E_USER_NOTICE: // 1024 // 
				$reason['type'] =  'E_USER_NOTICE'; break;
			case E_STRICT: // 2048 // 
				$reason['type'] =  'E_STRICT'; break;
			case E_RECOVERABLE_ERROR: // 4096 // 
				$reason['type'] =  'E_RECOVERABLE_ERROR'; break;
			case E_DEPRECATED: // 8192 // 
				$reason['type'] =  'E_DEPRECATED'; break;
			case E_USER_DEPRECATED: // 16384 // 
				$reason['type'] =  'E_USER_DEPRECATED';break;
			default:
				$reason['type'] = '';
		} 
	if ($reason['type'] != '')
	{
		$a = date("Y-m-d H:i:s").' '.$reason['type'].': '.$reason['message'].' File: '.$reason['file'].' Line: '.$reason['line']."\n\n";
		$log_file = dirname(__FILE__).'/logs/'.'_php_errors.log';
		$fp = fopen($log_file, 'a');
		fwrite($fp, $a);
		fclose($fp);
	}
}


// Check SiteGuarding FM
if ($firewall->CheckFM()) 
{
    $firewall->LogRequest(true);
    return;
}


// Load and parse the rules
if (!$firewall->LoadRules()) die('Rules are not loaded');



// Log the request
// *moved to the end* $firewall->LogRequest();


// Checking if the file is empty
if (SITEGUARDING_BLOCK_EMPTY_FILES === true && file_exists($_SERVER['SCRIPT_FILENAME']) && filesize($_SERVER['SCRIPT_FILENAME']) == 0)
{
    $firewall->Block_This_Session('Access to empty file '.$_SERVER["SCRIPT_FILENAME"]);   // the process will die
    exit;
}

if (isset($_SERVER["HTTP_X_REAL_IP"])) $_SERVER["REMOTE_ADDR"] = $_SERVER["HTTP_X_REAL_IP"];

// Checking this request based on the rules
if ($firewall->CheckIP_in_Allowed($_SERVER["REMOTE_ADDR"])) {$firewall->LogRequest(); return;}

// Check Allowed Requests
if ($firewall->Session_Check_Requests_Allowed($_REQUEST)) {$firewall->LogRequest(); return;}


if ($firewall->CheckIP_in_Alert($_SERVER["REMOTE_ADDR"]))
{   // Send alert to admin
    $subject = 'Access from IP '.$_SERVER["REMOTE_ADDR"];
    $message = date("Y-m-d H:i:s")."\n".
    	"IP:".$_SERVER["REMOTE_ADDR"]."\n".
    	"Link:"."http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"."\n".
    	"File:".$_SERVER[SCRIPT_FILENAME]."\n".
    	print_r($_REQUEST, true)."\n\n";
    $firewall->SendEmail($subject, $message);
}

if ($firewall->CheckIP_in_Blocked($_SERVER["REMOTE_ADDR"]))
{
    $firewall->Block_This_Session('Not allowed IP '.$_SERVER["REMOTE_ADDR"]);   // the process will die
    exit;
}

// Global RULES
if (strpos( $_SERVER['SCRIPT_FILENAME'], SITEGUARDING_SCAN_PATH) != 0)
{
	$SCRIPT_FILENAME = substr($_SERVER['SCRIPT_FILENAME'], strpos( $_SERVER['SCRIPT_FILENAME'], SITEGUARDING_SCAN_PATH));
}
else $SCRIPT_FILENAME = $_SERVER['SCRIPT_FILENAME'];
$tmp_session_rule = $firewall->Session_Apply_Rules($SCRIPT_FILENAME);
if ($tmp_session_rule != '') $firewall->this_session_rule = $tmp_session_rule;

if ($firewall->this_session_rule == 'block')
{
    $firewall->Block_This_Session('Rules for the file');   // the process will die
    exit;
}


// BLOCK_RULES_IP
$tmp_session_rule = $firewall->Session_Apply_BLOCK_RULES_IP($_SERVER['SCRIPT_FILENAME'], $_SERVER["REMOTE_ADDR"]);
if ($tmp_session_rule != '') $firewall->this_session_rule = $tmp_session_rule;

if ($firewall->this_session_rule == 'block')
{
    $firewall->Block_This_Session('Rules for the file & IP');   // the process will die
    exit;
}


// Check Requests
$tmp_session_rule = $firewall->Session_Check_Requests($_REQUEST);
if ($tmp_session_rule != '') $firewall->this_session_rule = $tmp_session_rule;

if ($firewall->this_session_rule == 'block')
{
    $firewall->Block_This_Session('Request rule => '.$firewall->this_session_reason_to_block, true);   // the process will die
    exit;
}

// Check BLOCK_URLS
$tmp_session_rule = $firewall->Check_URLs($_SERVER['REQUEST_URI']);
if ($tmp_session_rule != '') $firewall->this_session_rule = $tmp_session_rule;

if ($firewall->this_session_rule == 'block')
{
    $firewall->Block_This_Session('Not allowed URL');   // the process will die
    exit;
}



// Log the request (the request passed all the rules)
$firewall->LogRequest();


if (SITEGUARDING_DEBUG === true && $_SERVER["REMOTE_ADDR"] == SITEGUARDING_DEBUG_IP)
{
    echo 'Finished'."<br>";
}


?>