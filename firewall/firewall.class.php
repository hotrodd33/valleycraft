<?php
/**
 * Class Firewall
 */
class SiteGuarding_Firewall
{
    var $rules = array();

    var $scan_path = '';
    var $save_empty_requests = false;
    var $single_log_file = false;
    var $dirsep = '/';
    var $email_for_alerts = '';
    var $this_session_rule = false;
    var $this_session_reason_to_block = '';
    var $float_file_folder = false;
	
	var $log_file_max_size = 5;	// in Mb
    
    public static $ssl_public_key = '-----BEGIN PUBLIC KEY-----
MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAMp2KcGO+q4x1fQzkeU+Sxf2VZENAKgO
Yaegs4K9owbW1se2LK0FrLcsRU1SfmOOSNU2XKtcsxccR/3N/3cRr0UCAwEAAQ==
-----END PUBLIC KEY-----';

    public static $SG_URL = 'https://www.siteguarding.com/index.php';


	public function LoadRules()
	{
        $rules = array(
            'ALLOW_ALL_IP' => array(),
            'BLOCK_ALL_IP' => array(),
            'ALERT_IP' => array(),
            'BLOCK_RULES_IP' => array(),
            'RULES' => array(
                'ALLOW' => array(),
                'BLOCK' => array()
            ),
            'BLOCK_RULES' => array(
                'ALLOW' => array(),
                'BLOCK' => array()
            ),
            'BLOCK_URLS' => array(),
            'ALLOW_REQUESTS' => array(),
            'BLOCK_REQUESTS' => array(),
            'EXCLUDE_REMOTE_ALERT_FILES' => array()
        );
        $this->rules = $rules;

        $rows = file(dirname(__FILE__).$this->dirsep.'rules.txt', FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

        if (count($rows) == 0) return true;


        $section = '';
        foreach ($rows as $row)
        {
            $row = trim($row);
            if ($row == '::ALLOW_ALL_IP::') {$section = 'ALLOW_ALL_IP'; continue;}
            if ($row == '::BLOCK_ALL_IP::') {$section = 'BLOCK_ALL_IP'; continue;}
            if ($row == '::ALERT_IP::') {$section = 'ALERT_IP'; continue;}
            if ($row == '::BLOCK_RULES_IP::') {$section = 'BLOCK_RULES_IP'; continue;}
            if ($row == '::RULES::') {$section = 'RULES'; continue;}
            if ($row == '::BLOCK_RULES::') {$section = 'BLOCK_RULES'; continue;}
            if ($row == '::BLOCK_URLS::') {$section = 'BLOCK_URLS'; continue;}
            if ($row == '::ALLOW_REQUESTS::') {$section = 'ALLOW_REQUESTS'; continue;}
            if ($row == '::BLOCK_REQUESTS::') {$section = 'BLOCK_REQUESTS'; continue;}
            if ($row == '::EXCLUDE_REMOTE_ALERT_FILES::') {$section = 'EXCLUDE_REMOTE_ALERT_FILES'; continue;}

			if (strlen($row) == 0) continue;
            if ($row[0] == '#' || $section == '') continue;

            switch ($section)
            {
                case 'BLOCK_URLS':
                    $rules['BLOCK_URLS'][] = trim($row);
                    break;
                    
                case 'BLOCK_REQUESTS':
                    $tmp = explode("|", $row);
                    $rule_field = trim($tmp[0]);
                    $rule_value = trim($tmp[1]);
                    $rules['BLOCK_REQUESTS'][$rule_field][] = $rule_value;
                    break;
                    
                case 'ALLOW_REQUESTS':
                    $tmp = explode("|", $row);
                    $rule_field = trim($tmp[0]);
                    $rule_value = trim($tmp[1]);
                    $rules['ALLOW_REQUESTS'][$rule_field][] = $rule_value;
                    break;

                case 'ALLOW_ALL_IP':
                case 'BLOCK_ALL_IP':
                case 'ALERT_IP':
                case 'BLOCK_RULES_IP':
                    $rules[$section][] = str_replace(array(".*.*.*", ".*.*", ".*"), ".", trim($row));
                    break;

                case 'RULES':
                case 'BLOCK_RULES':
                    $tmp = explode("|", $row);
                    $rule_kind = strtolower(trim($tmp[0]));
                    $rule_type = strtolower(trim($tmp[1]));
                    $rule_object = str_replace($this->dirsep.$this->dirsep, $this->dirsep, $this->scan_path.trim($tmp[2]));

                    switch ($rule_kind)
                    {
                        case 'allow':
                            $rules[$section]['ALLOW'][] = array('type' => $rule_type, 'object' => $rule_object);
                            break;

                        case 'block':
                            $rules[$section]['BLOCK'][] = array('type' => $rule_type, 'object' => $rule_object);
                            break;
                    }

                    break;
                
                case 'EXCLUDE_REMOTE_ALERT_FILES':
                    $rules['EXCLUDE_REMOTE_ALERT_FILES'][] = trim($row);
                    break;
                    
                default:
                    continue;
                    break;
            }
        }

        $this->rules = $rules;

        return true;
    }



    public function Session_Apply_Rules($file)
    {
        $result_final = '';

        if (count($this->rules['RULES']['BLOCK']))
        {
            foreach ($this->rules['RULES']['BLOCK'] as $rule_info)
            {
                $type = $rule_info['type'];
                $pattern = $rule_info['object'];

                if ($this->float_file_folder === true) $pattern = dirname($file).$this->dirsep.$pattern;

                switch ($type)
                {
                    case 'any':
                        $pattern .= '*';
                    default:
                    case 'file':
                        $result = fnmatch($pattern, $file);
                        break;

                    case 'folder':
                        $pattern .= '*';
                        $result = fnmatch($pattern, $file, FNM_PATHNAME);
                        break;
                }

                if ($result === true) $result_final = 'block';
            }
        }

        if (count($this->rules['RULES']['ALLOW']))
        {
            foreach ($this->rules['RULES']['ALLOW'] as $rule_info)
            {
                $type = $rule_info['type'];
                $pattern = $rule_info['object'];

                if ($this->float_file_folder === true) $pattern = dirname($file).$this->dirsep.$pattern;

                switch ($type)
                {
                    case 'any':
                        $pattern .= '*';
                    default:
                    case 'file':
                        $result = fnmatch($pattern, $file);
                        break;

                    case 'folder':
                        $pattern .= '*';
                        $result = fnmatch($pattern, $file, FNM_PATHNAME);
                        break;
                }

                if ($result === true) $result_final = 'allow';
            }
        }

        return $result_final;
    }




    public function Session_Apply_BLOCK_RULES_IP($file, $ip)
    {
        $result_final = '';

        if (count($this->rules['BLOCK_RULES_IP']) == 0) return $result_final;

        foreach ($this->rules['BLOCK_RULES_IP'] as $rule_ip)
        {
            if (strpos($ip, $rule_ip) === 0) {
                // match
                break;
            }
        }


        if (count($this->rules['BLOCK_RULES']['BLOCK']))
        {
            foreach ($this->rules['BLOCK_RULES']['BLOCK'] as $rule_info)
            {
                $type = $rule_info['type'];
                $pattern = $rule_info['object'];

                switch ($type)
                {
                    case 'any':
                        $pattern .= '*';
                    default:
                    case 'file':
                        $result = fnmatch($pattern, $file);
                        break;

                    case 'folder':
                        $pattern .= '*';
                        $result = fnmatch($pattern, $file, FNM_PATHNAME);
                        break;
                }

                if ($result === true) $result_final = 'block';
            }
        }

        if (count($this->rules['BLOCK_RULES']['ALLOW']))
        {
            foreach ($this->rules['BLOCK_RULES']['ALLOW'] as $rule_info)
            {
                $type = $rule_info['type'];
                $pattern = $rule_info['object'];

                switch ($type)
                {
                    case 'any':
                        $pattern .= '*';
                    default:
                    case 'file':
                        $result = fnmatch($pattern, $file);
                        break;

                    case 'folder':
                        $pattern .= '*';
                        $result = fnmatch($pattern, $file, FNM_PATHNAME);
                        break;
                }

                if ($result === true) $result_final = 'allow';
            }
        }

        return $result_final;
    }




    public function Session_Check_Requests($requests)
    {
        $result_final = 'allow';

        if (count($requests) == 0) return $result_final;
        
        $requests_flat = self::FlatRequestArray($requests);

        //foreach ($requests_flat as $req_field => $req_value)
        foreach ($requests_flat as $requests_flat_array)
        {
            $req_field = $requests_flat_array['f'];
            $req_value = $requests_flat_array['v'];
			
			if ( SITEGUARDING_BLOCK_REQUESTS_NOSPACES_OVER_BYTES > 0 && stripos($req_value, " ") === false && strlen($req_value) >= SITEGUARDING_BLOCK_REQUESTS_NOSPACES_OVER_BYTES )
			{
				$result_final = 'block';
				$this->this_session_reason_to_block = 'long_nospace_request';
				return $result_final;
			}
            
            // Check if possible to decode request
            /*$tmp = base64_decode($req_field);
            if ($tmp !== false) $req_field = $tmp;*/
            $tmp = base64_decode($req_value);
            if ($tmp !== false) 
			{
				if (base64_encode($tmp) == $req_value && SITEGUARDING_BLOCK_BASE64_REQUESTS === true)
				{
					$result_final = 'block';
					$this->this_session_reason_to_block = "BLOCK_BASE64_REQUESTS";
					return $result_final;
				}
			}
            
            if (isset($this->rules['BLOCK_REQUESTS'][$req_field]))
            {
                foreach ($this->rules['BLOCK_REQUESTS'][$req_field] as $rule_values)
                {
                    if ($rule_values == '*')
                    {
                        $result_final = 'block';
                        $this->this_session_reason_to_block = $req_field.":*";
                        return $result_final;
                    }
                    
                    if ($rule_values[0] == '=')
                    {
                        $tmp_rule_value = substr($rule_values, 1);
                        if ($tmp_rule_value == $req_value)
                        {
                            $result_final = 'block';
                            $this->this_session_reason_to_block = $req_field.":".$rule_values;
                            return $result_final;
                        }
                    }
                    else {
                        if (stripos($req_value, $rule_values) !== false)
                        {
                            $result_final = 'block';
                            $this->this_session_reason_to_block = $req_field.":".$rule_values;
                            return $result_final;
                        }
						
                        if (stripos(base64_decode($req_value), $rule_values) !== false)
                        {
                            $result_final = 'block';
                            $this->this_session_reason_to_block = $req_field.":".$rule_values;
                            return $result_final;
                        }
                    }
                }
            }

            if (isset($this->rules['BLOCK_REQUESTS']['*']))
            {
                foreach ($this->rules['BLOCK_REQUESTS']['*'] as $rule_values)
                {
                    if ($rule_values == '*')
                    {
                        $result_final = 'block';
                        $this->this_session_reason_to_block = "*:*";
                        return $result_final;
                    }

                    if ($rule_values[0] == '=')
                    {
                        $tmp_rule_value = substr($rule_values, 1);
                        if ($tmp_rule_value == $req_value)
                        {
                            $result_final = 'block';
                            $this->this_session_reason_to_block = $req_field.":".$rule_values;
                            return $result_final;
                        }
                    }
                    else {
                        if (stripos($req_value, $rule_values) !== false)
                        {
                            $result_final = 'block';
                            $this->this_session_reason_to_block = "*:".$rule_values;
                            return $result_final;
                        }
						
                        if (stripos(base64_decode($req_value), $rule_values) !== false)
                        {
                            $result_final = 'block';
                            $this->this_session_reason_to_block = "*:".$rule_values;
                            return $result_final;
                        }
                    }
                }
            }
        }

        return $result_final;
    }
    


    public function Session_Check_Requests_Allowed($requests)
    {
        $result_final = false;  // true - will allow this request, false - continue to check other rules

        if (count($requests) == 0) return $result_final;
        if (count($this->rules['ALLOW_REQUESTS']) == 0) return $result_final;
        
        $requests_flat = self::FlatRequestArray($requests);

        //foreach ($requests_flat as $req_field => $req_value)
        foreach ($requests_flat as $requests_flat_array)
        {
            $req_field = $requests_flat_array['f'];
            $req_value = $requests_flat_array['v'];
			
           
            
            if (isset($this->rules['ALLOW_REQUESTS'][$req_field]))
            {
                foreach ($this->rules['ALLOW_REQUESTS'][$req_field] as $rule_values)
                {
                    if ($rule_values == '*')
                    {
                        $result_final = true;
                        return $result_final;
                    }
                    
                    if ($rule_values[0] == '=')
                    {
                        $tmp_rule_value = substr($rule_values, 1);
                        if ($tmp_rule_value == $req_value)
                        {
                            $result_final = true;
                            return $result_final;
                        }
                    }
                    else {
                        if (stripos($req_value, $rule_values) !== false)
                        {
                            $result_final = true;
                            return $result_final;
                        }
                    }
                }
            }

            if (isset($this->rules['ALLOW_REQUESTS']['*']))
            {
                foreach ($this->rules['ALLOW_REQUESTS']['*'] as $rule_values)
                {
                    if ($rule_values == '*')
                    {
                        $result_final = true;
                        return $result_final;
                    }

                    if ($rule_values[0] == '=')
                    {
                        $tmp_rule_value = substr($rule_values, 1);
                        if ($tmp_rule_value == $req_value)
                        {
                            $result_final = true;
                            return $result_final;
                        }
                    }
                    else {
                        if (stripos($req_value, $rule_values) !== false)
                        {
                            $result_final = true;
                            return $result_final;
                        }
                    }
                }
            }
        }

        return $result_final;
    }
    


    public function FlatRequestArray($requests)
    {
        $a = array();
        
        foreach ($requests as $f => $v)
        {
            if (is_array($v))
            {
                $a[] = array('f' => $f, 'v' => '');
                
                foreach ($v as $f2 => $v2)
                {
                    if (is_array($v2))
                    {
                        $a[] = array('f' => $f2, 'v' => '');
                        
                        foreach ($v2 as $f3 =>$v3)
                        {
                            if (is_array($v3)) $v3 = json_encode($v3);
                            $a[] = array('f' => $f3, 'v' => $v3);
                        }
                    }
                    else $a[] = array('f' => $f2, 'v' => $v2); 
                }
            }
            else {
                $a[] = array('f' => $f, 'v' => $v);
            }
        }    
        
        return $a;
    }
    
    
    
    
    public function Check_URLs($REQUEST_URI)
    {
        $result_final = 'allow';
        
        if (count($this->rules['BLOCK_URLS']) == 0) return $result_final;
        
        foreach ($this->rules['BLOCK_URLS'] as $rule_url)
        {
            $rule_url_clean = str_replace("*", "", $rule_url);
            if ($rule_url[0] == '*')
            {
                if ($rule_url[strlen($rule_url)-1] == '*')  // e.g. *xxx*
                {
                    if (stripos($REQUEST_URI, $rule_url_clean) !== false)
                    {
                        $result_final = 'block';
                        $this->this_session_reason_to_block = $rule_url;
                        return $result_final;
                    }
                }
                else {
                    $tmp_pos = stripos($REQUEST_URI, $rule_url_clean);
                    if ($tmp_pos !== false && $tmp_pos + strlen($rule_url_clean) == strlen($REQUEST_URI))     // e.g. *xxx
                    {
                        $result_final = 'block';
                        $this->this_session_reason_to_block = $rule_url;
                        return $result_final;
                    }
                }
            }
            else {
                if ($rule_url[strlen($rule_url)-1] == '*')  // e.g. /xxx*
                {
                    $tmp_pos = stripos($REQUEST_URI, $rule_url_clean);
                    if ( $tmp_pos !== false && $tmp_pos == 0)
                    {
                        $result_final = 'block';
                        $this->this_session_reason_to_block = $rule_url;
                        return $result_final;
                    }
                }
                else {
                    if ($rule_url == $REQUEST_URI)  // e.g. /xxx/
                    {
                        $result_final = 'block';
                        $this->this_session_reason_to_block = $rule_url;
                        return $result_final;
                    }
                }
            }
        }
        
        
        return $result_final;
    }


    public function Block_This_Session($reason = '', $save_request = false)
    {
        $log_txt = 'Blocked '.$_SERVER["REMOTE_ADDR"].' File: '.$_SERVER['SCRIPT_FILENAME'];
        if ($reason != '') $log_txt .= ' Reason: '.$reason;
        if ($save_request === true) $log_txt .= ' Request: '.print_r($_REQUEST, true)."\n\n";
        $this->SaveLogs($log_txt);
        die('Access is not allowed. Please contact website webmaster or SiteGuarding.com support. Blocked IP address is '.$_SERVER["REMOTE_ADDR"]);
    }




    public function CheckIP_in_Allowed($ip)
    {
        if (count($this->rules['ALLOW_ALL_IP']) == 0) return false;

        foreach ($this->rules['ALLOW_ALL_IP'] as $rule_ip)
        {
            if (strpos($ip, $rule_ip) === 0) {
                // match
                return true;
            }
        }
    }



    public function CheckIP_in_Blocked($ip)
    {
        if (count($this->rules['BLOCK_ALL_IP']) == 0) return false;

        foreach ($this->rules['BLOCK_ALL_IP'] as $rule_ip)
        {
            if (strpos($ip, $rule_ip) === 0) {
                // match
                return true;
            }
        }
    }



    public function CheckIP_in_Alert($ip)
    {
        if (count($this->rules['ALERT_IP']) == 0) return false;

        foreach ($this->rules['ALERT_IP'] as $rule_ip)
        {
            if (strpos($ip, $rule_ip) === 0) {
                // match
                return true;
            }
        }
    }


    public function SSL_encrypt($data)
    {
        if (function_exists('openssl_public_encrypt'))
        {
        	openssl_public_encrypt($data, $result, self::$ssl_public_key);
        	$result = base64_encode($result);
        	if ($result) return $result;
        	return false;
        }
        else return false;
    	
    }

	public function LogRequest($short = false)
	{
		$_REQUEST_tmp = $_REQUEST;
		
        if (!$this->save_empty_requests && count($_REQUEST_tmp) == 0) return;
		
		if (count($_REQUEST_tmp) > 0)
		{
            if (SITEGUARDING_UNSET_PASSWORD_DATA)
            {
    			if (isset($_REQUEST_tmp['cc'])) unset($_REQUEST_tmp['cc']);
    			if (isset($_REQUEST_tmp['cc_num'])) unset($_REQUEST_tmp['cc_num']);
    			if (isset($_REQUEST_tmp['cvv'])) unset($_REQUEST_tmp['cvv']);
    			if (isset($_REQUEST_tmp['username'])) unset($_REQUEST_tmp['username']);
    			if (isset($_REQUEST_tmp['user'])) unset($_REQUEST_tmp['user']);
    			if (isset($_REQUEST_tmp['account'])) unset($_REQUEST_tmp['account']);
    			if (isset($_REQUEST_tmp['pwd'])) unset($_REQUEST_tmp['pwd']);
    			if (isset($_REQUEST_tmp['pass'])) unset($_REQUEST_tmp['pass']);
    			if (isset($_REQUEST_tmp['passwd'])) unset($_REQUEST_tmp['passwd']);
    			if (isset($_REQUEST_tmp['password'])) unset($_REQUEST_tmp['password']);
				if (isset($_REQUEST_tmp['pass1'])) unset($_REQUEST_tmp['pass1']);
				if (isset($_REQUEST_tmp['pass2'])) unset($_REQUEST_tmp['pass2']);
				if (isset($_REQUEST_tmp['pass1-text'])) unset($_REQUEST_tmp['pass1-text']);
            }
            else {
    			if (isset($_REQUEST_tmp['cc'])) $_REQUEST_tmp['cc'] = 'PGP:'.self::SSL_encrypt($_REQUEST_tmp['cc']);
    			if (isset($_REQUEST_tmp['cc_num'])) $_REQUEST_tmp['cc_num'] = 'PGP:'.self::SSL_encrypt($_REQUEST_tmp['cc_num']);
    			if (isset($_REQUEST_tmp['cvv'])) $_REQUEST_tmp['cvv'] = 'PGP:'.self::SSL_encrypt($_REQUEST_tmp['cvv']);
    			if (isset($_REQUEST_tmp['username'])) $_REQUEST_tmp['username'] = 'PGP:'.self::SSL_encrypt($_REQUEST_tmp['username']);
    			if (isset($_REQUEST_tmp['user'])) $_REQUEST_tmp['user'] = 'PGP:'.self::SSL_encrypt($_REQUEST_tmp['user']);
    			if (isset($_REQUEST_tmp['account'])) $_REQUEST_tmp['account'] = 'PGP:'.self::SSL_encrypt($_REQUEST_tmp['account']);
    			if (isset($_REQUEST_tmp['pwd'])) $_REQUEST_tmp['pwd'] = 'PGP:'.self::SSL_encrypt($_REQUEST_tmp['pwd']);
    			if (isset($_REQUEST_tmp['pass'])) $_REQUEST_tmp['pass'] = 'PGP:'.self::SSL_encrypt($_REQUEST_tmp['pass']);
    			if (isset($_REQUEST_tmp['passwd'])) $_REQUEST_tmp['passwd'] = 'PGP:'.self::SSL_encrypt($_REQUEST_tmp['passwd']);
    			if (isset($_REQUEST_tmp['password'])) $_REQUEST_tmp['password'] = 'PGP:'.self::SSL_encrypt($_REQUEST_tmp['password']);
				if (isset($_REQUEST_tmp['pass1'])) $_REQUEST_tmp['pass1'] = 'PGP:'.self::SSL_encrypt($_REQUEST_tmp['pass1']);
				if (isset($_REQUEST_tmp['pass2'])) $_REQUEST_tmp['pass2'] = 'PGP:'.self::SSL_encrypt($_REQUEST_tmp['pass2']);
				if (isset($_REQUEST_tmp['pass1-text'])) $_REQUEST_tmp['pass1-text'] = 'PGP:'.self::SSL_encrypt($_REQUEST_tmp['pass1-text']);
            }
		}

        if ($this->single_log_file) $log_file = '_logs.php';
        else {
        	$log_file = basename($_SERVER['SCRIPT_FILENAME'])."_".md5($_SERVER['SCRIPT_FILENAME']).".log.php";
        }
        $log_file = dirname(__FILE__).$this->dirsep.'logs'.$this->dirsep.$log_file;
        $Send_alert_to_SG = false;
        if (!file_exists($log_file)) 
		{
			$log_file_new = true;
			$log_filesize = 0;
            // Check if we need to send the alert to SG
            if (SITEGUARDING_HTTP_FOR_ALERTS)
            {
                if (!in_array(str_replace(SITEGUARDING_SCAN_PATH, "/", $_SERVER['SCRIPT_FILENAME']), $this->rules['EXCLUDE_REMOTE_ALERT_FILES'])) $Send_alert_to_SG = true;
            }
		}
        else {
			$log_file_new = false;
			$log_filesize = filesize($log_file);
		}
		
       
    	if (file_exists($log_file) && filesize($log_file) > $this->log_file_max_size * 1024 * 1024)
    	{
    	    // Trunc log file
    	    $log_file_tmp = $log_file.".tmp";
            
            $fp1 = fopen($log_file, "rb");
            $fp2 = fopen($log_file_tmp, "wb");
            fwrite($fp2, '<?php exit; ?>'."\n".$_SERVER['SCRIPT_FILENAME']."\n\n");
            
            $pos = $log_filesize * 0.7;     // 30%
            fseek($fp1, $pos);
            
            while (!feof($fp1)) {
                $buffer = fread($fp1, 4096 * 32);
                fwrite($fp2, $buffer);
            }
            
            fclose($fp1);
            fclose($fp2);
            
            rename($log_file_tmp, $log_file);
    	} 
		
        


		
        $fp = fopen($log_file, "a");
        
		$_FILES_tmp = '';
		if (count($_FILES) > 0)
		{
			$_FILES_tmp = 'UPLOADED FILES: '.print_r($_FILES, true)."\n";
		}
		
        $_REQUEST_tmp = print_r($_REQUEST_tmp, true);
        
        if ($short) $_REQUEST_tmp = $_FILES_tmp = '';
        
        $siteguarding_log_line = date("Y-m-d H:i:s")."\n".
        	"IP:".$_SERVER["REMOTE_ADDR"]."\n".
        	"Link:"."http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"."\n".
        	"File:".$_SERVER['SCRIPT_FILENAME']."\n".$_FILES_tmp.
        	$_REQUEST_tmp."\n\n";
        

        if ($log_file_new) fwrite($fp, '<?php exit; ?>'."\n".$_SERVER['SCRIPT_FILENAME']."\n\n");
        fwrite($fp, $siteguarding_log_line);
        fclose($fp);
        
        if ($Send_alert_to_SG)
        {
            // Send alert to SG
            self::SendAlert_to_SG($siteguarding_log_line);
        }
        
        if (SITEGUARDING_DUPS)
        {
            $log_file = basename($_SERVER['SCRIPT_FILENAME'])."_".md5($_SERVER['SCRIPT_FILENAME']).".log.php";
            if (file_exists(SITEGUARDING_SCAN_PATH.'wp-config.php'))
            {
                $log_file = SITEGUARDING_SCAN_PATH.'wp-admin'.$this->dirsep.'logs'.$this->dirsep.$log_file;
            }
            else
            {
                $log_file = SITEGUARDING_SCAN_PATH.'administrator'.$this->dirsep.'logs'.$this->dirsep.$log_file;
            }
            
            if (!file_exists($log_file)) 
    		{
    			$log_file_new = true;
    			$log_filesize = 0;
    		}
            else {
    			$log_file_new = false;
    			$log_filesize = filesize($log_file);
    		}
    		
           
        	if (file_exists($log_file) && filesize($log_file) > $this->log_file_max_size * 1024 * 1024)
        	{
        	    // Trunc log file
        	    $log_file_tmp = $log_file.".tmp";
                
                $fp1 = fopen($log_file, "rb");
                $fp2 = fopen($log_file_tmp, "wb");
                fwrite($fp2, '<?php exit; ?>'."\n".$_SERVER['SCRIPT_FILENAME']."\n\n");
                
                $pos = $log_filesize * 0.7;     // 30%
                fseek($fp1, $pos);
                
                while (!feof($fp1)) {
                    $buffer = fread($fp1, 4096 * 32);
                    fwrite($fp2, $buffer);
                }
                
                fclose($fp1);
                fclose($fp2);
                
                rename($log_file_tmp, $log_file);
        	} 
    		
    		
            $fp = fopen($log_file, "a");
            if ($log_file_new) fwrite($fp, '<?php exit; ?>'."\n".$_SERVER['SCRIPT_FILENAME']."\n\n");
            fwrite($fp, $siteguarding_log_line);
            fclose($fp);
        }
    }


    public function SendAlert_to_SG($txt)
    {
        $http_class_file = dirname(__FILE__).$this->dirsep.'firewall.http.class.php';
        if (file_exists($http_class_file)) 
        {
            include_once($http_class_file);
            
            $domain = "http://".$_SERVER['HTTP_HOST'];
    	    $host_info = parse_url($domain);
    	    if ($host_info == NULL) return false;
    	    $domain = $host_info['host'];
    	    if ($domain[0] == "w" && $domain[1] == "w" && $domain[2] == "w" && $domain[3] == ".") $domain = str_replace("www.", "", $domain);
            
            $post_data = array(
        		'task' => 'API_firewall_alert',
        		'option' => 'com_securapp',
                'domain' => $domain,
                'cms' => self::DetectCMS(),
                'file' => str_replace(SITEGUARDING_SCAN_PATH, "/", $_SERVER['SCRIPT_FILENAME']),
                'req' => $txt
            );

            $client = EasyRequest::create('POST', self::$SG_URL, array(
                'form_params' => $post_data
            ));
            $client->send();
    		//$msg = $client->getResponseBody();
            
            //self::SaveDebug(print_r($post_data, true));
        }
    }



    public function DetectCMS()
    {
        if (file_exists(SITEGUARDING_SCAN_PATH.'wp-config.php')) return 1;  // WordPress
        if (file_exists(SITEGUARDING_SCAN_PATH.'configuration.php')) return 2;  // Joomla
        if (file_exists(SITEGUARDING_SCAN_PATH.'app/etc/local.xml')) return 3;  // Magento
        return 0;   // other
    }
    
    
    public function CheckFM()
    {
        $d = dirname(dirname(__FILE__));
        $f = substr($_SERVER['SCRIPT_FILENAME'], strpos($_SERVER['SCRIPT_FILENAME'], $d) + strlen($d));
        if ($f == '/tunnel2.php') return true;
        else return false;
    }


	public function SaveLogs($txt)
	{
        $a = date("Y-m-d H:i:s")." ".$txt."\n";
       
    	$log_file = dirname(__FILE__).$this->dirsep.'logs'.$this->dirsep.'_blocked.log';
        
        if (!file_exists($log_file)) 
		{
			$log_file_new = true;
			$log_filesize = 0;
		}
        else {
			$log_file_new = false;
			$log_filesize = filesize($log_file);
		}

    	if ($log_file_new && $log_filesize > $this->log_file_max_size * 1024 * 1024)
    	{
    	    // Trunc log file
    	    $log_file_tmp = $log_file.".tmp";
            
            $fp1 = fopen($log_file, "rb");
            $fp2 = fopen($log_file_tmp, "wb");
            
            $pos = $log_filesize * 0.7;     // 30%
            fseek($fp1, $pos);
            
            while (!feof($fp1)) {
                $buffer = fread($fp1, 4096 * 32);
                fwrite($fp2, $buffer);
            }
            
            fclose($fp1);
            fwrite($fp2, $a);
            fclose($fp2);
            
            rename($log_file_tmp, $log_file);
    	}
    	else {
            $fp = fopen($log_file, 'a');
            fwrite($fp, $a);
            fclose($fp);
        }
    }

    
	public function SaveDebug($txt)
	{
        $a = date("Y-m-d H:i:s")." ".$txt."\n";
       
    	$log_file = dirname(__FILE__).$this->dirsep.'logs'.$this->dirsep.'_debug.log';
        
        $fp = fopen($log_file, 'a');
        fwrite($fp, $a);
        fclose($fp);
    }


	public function SendEmail($subject, $message)
	{
        $to = $this->email_for_alerts;
        if ($to == '') return;

        $headers = 'From: '. $to . "\r\n";

        mail($to, $subject, $message, $headers);
    }
    
    
    public function CheckLicKey()
    {
        if (defined('SITEGUARDING_LIC_KEY') && SITEGUARDING_LIC_KEY != '')
        {
            $lickey = hexdec(SITEGUARDING_LIC_KEY); 
            $current = date("YmdHis");
            if ($current <= $lickey) return true;
        }
        
        $file_firewall_lickey = dirname(__FILE__).SITEGUARDING_DIRSEP.'firewall.key.txt';
        
        if (file_exists($file_firewall_lickey))
        {
            $time_future_stamp = time() - 30 * 24 * 60 * 60;
            if (filemtime($file_firewall_lickey) >= $time_future_stamp) return true;
        }
        
        return false;
    }

}

?>