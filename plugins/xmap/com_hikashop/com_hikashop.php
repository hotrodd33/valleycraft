<?php
/**
* @author Hikashop Team
* @email dev@hikashop.com
* @version $Id: com_hikashop.php
* @package Xmap
* @license GNU/GPL
* @description Xmap plugin for Hikashop
*/

defined( '_JEXEC' ) or die( 'Restricted access.' );

if(!defined('DS'))
	define('DS', DIRECTORY_SEPARATOR);

class xmap_com_hikashop {

	var $depth=0;
	var $productNumber=0;
	var $max_depth=-1;
	var $max_product=-1;
	var $categoriesCheck = array();
	var $productsCheck = array();
	var $same_category;
	var $same_product;
	var $priority;
	var $changefreq;
	var $exist=0;
	var $currentCategory=0;
	var $image_changefreq;
	var $image_width = 50;
	var $image_height = 50;

	//main fct called by xmap
	function getTree( &$xmap, &$parent, &$params ){
		static $obj = null;
		if(is_null($obj)){
			$obj = new xmap_com_hikashop();
		}
		$obj->getTreeFct($xmap, $parent, $params );
	}

	function getTreeFct(&$xmap, &$parent, &$params ){
		if(!empty($params['max_depth']))
			$this->max_depth=$params['max_depth'];
		if(isset($params['max_product']))
			$this->max_product=$params['max_product'];
		if(empty($this->max_product))
			$this->max_product=-1;
		if(isset($params['show_same_product']))
			$this->same_product=$params['show_same_product'];
		if(isset($params['show_same_category']))
			$this->same_category=$params['show_same_category'];
		if(isset($params['priority']))
			$this->priority=$params['priority'];
		if(isset($params['changefreq']))
			$this->changefreq=$params['changefreq'];
		if(isset($params['image_changefreq']))
			$this->image_changefreq=$params['image_changefreq'];

		if(!include_once(rtrim(JPATH_ADMINISTRATOR,DS).DS.'components'.DS.'com_hikashop'.DS.'helpers'.DS.'helper.php')) return true;
		$this->imageHelper = hikashop_get('helper.image');

		$link_vars=null;
		$link_query = parse_url( $parent->link );
		parse_str( html_entity_decode($link_query['query']), $link_vars);
		$task = JArrayHelper::getValue($link_vars,'task','');
		$ctrl = JArrayHelper::getValue($link_vars,'ctrl','');
		$itemid = $parent->id;

		if (empty($task))
			$task = JArrayHelper::getValue($link_vars,'layout','');
		if (empty($ctrl))
			$ctrl = JArrayHelper::getValue($link_vars,'view','');
		if (empty($itemid))
			echo 'itemid vide';

		if($ctrl == 'product' && $task == 'listing')
			$this->getListProduct($xmap, $parent, $itemid);
		if($ctrl == 'category' && $task == 'listing')
			$this->getListCategory($xmap, $parent, $itemid);
	}

	function getListCategory(&$xmap, &$parent, $itemid){
		if(!include_once(rtrim(JPATH_ADMINISTRATOR,DS).DS.'components'.DS.'com_hikashop'.DS.'helpers'.DS.'helper.php')) return true;
		$menuClass = hikashop_get('class.menus');
		$this->menuData = $menuClass->get($itemid);
		$modules = explode(',',$this->menuData->hikashop_params['modules']);
		$this->currentCategory=$this->menuData->hikashop_params['selectparentlisting'];
		$this->printListCategory($xmap, $parent, $this->menuData->hikashop_params['filter_type'], $this->menuData->hikashop_params['selectparentlisting'], $modules);
	}


	function getListProduct(&$xmap, &$parent, $itemid){
		if(!include_once(rtrim(JPATH_ADMINISTRATOR,DS).DS.'components'.DS.'com_hikashop'.DS.'helpers'.DS.'helper.php')) return true;
		$menuClass = hikashop_get('class.menus');
		$menuData = $menuClass->get($itemid);
		$this->currentCategory=$menuData->hikashop_params['selectparentlisting'];
		$this->printListProduct($xmap, $parent, $menuData->hikashop_params['filter_type'], $menuData->hikashop_params['selectparentlisting']);
	}

	function printListCategory(&$xmap, &$parent, $filterType, $parentListing, $modules){
		$app = JFactory::getApplication();
		$categoryClass = hikashop_get('class.category');
		if($this->depth < $this->max_depth || $this->max_depth==-1){
			$db = JFactory::getDBO();
			$idGroupe=$this->getGroup();
			if ($filterType == 0){
				$query = 'SELECT * FROM '.hikashop_table('category').' WHERE (category_access=\'all\' OR category_access LIKE \'%,'.$idGroupe.',%\') AND ' .
						'category_published=1 AND category_parent_id='.(int)$parentListing.'';
				$db->setQuery($query);
				$categories = $db->loadObjectList('category_id');

				if(!empty($categories)){
					$xmap->changeLevel(1);
					$this->depth++;

					$query = 'SELECT * FROM '.hikashop_table('file').' WHERE file_ref_id IN ('.implode(',',array_keys($categories)).') AND file_type=\'category\' ';
					$db->setQuery($query);
					$files = $db->loadObjectList();

					foreach($categories as $category) {
						foreach($this->categoriesCheck as $valeur){
							if($this->same_category==0 && $valeur==$category->category_id){
								$this->exist=1;
							}
						}
						if($this->exist==0){
							$this->categoriesCheck[$category->category_id]=$category->category_id;
							$this->getCategory(0,$category);
							$node = new stdclass;
							$node->id   = $parent->id;
							$node->uid  = $parent->uid.'category'.$category->category_id.'_'.$parentListing;
							$node->name = $category->category_name;
							$categoryClass->addAlias($category);
							if(!empty($category->category_canonical)){
								$node->link = hikashop_cleanURL($category->category_canonical);
							}else{
								$node->link = 'index.php?option=com_hikashop&ctrl=category&task=listing&cid='.$category->category_id.'&name='.$category->alias.'&Itemid='.$node->id;
							}
							if($this->priority==-1){ $node->priority = $parent->priority;	}
							else{ $node->priority = $this->priority; }
							if($this->changefreq==-1){ $node->changefreq = $parent->changefreq;	}
							else{ $node->changefreq = $this->changefreq; }
							$node->expandible = false;
							if($this->image_changefreq && !empty($files)){
								$node->isImages = 1;
								$node->images = array();
								foreach($files as $file){
									if($file->file_ref_id != $category->category_id) continue;
									$img = $this->imageHelper->getThumbnail($file->file_path, array($this->image_width, $this->image_height), array('forcesize' => true, 'scale' => 'outside'));
									if(!$img->success) continue;
									if(substr($img->url, 0, 3) == '../')
										$image = str_replace('../', HIKASHOP_LIVE, $img->url);
									else
										$image = substr(HIKASHOP_LIVE, 0, strpos(HIKASHOP_LIVE, '/', 9)) . $img->url;
									$node->images[0] = new stdClass();
									$node->images[0]->src = $image;
									$node->images[0]->title = $file->file_name;
								}
							}
							$xmap->printNode($node);
							$this->printListCategory($xmap, $parent, $filterType, $category->category_id, $modules);
						}
						$this->exist=0;
					}
					$xmap->changeLevel(-1);
					$this->depth--;
				}
				
				if(!empty($modules)){
					foreach($modules as $module){
						$moduleData = $this->getModule($module);
						$this->printListModule($xmap, $parent, $moduleData, $parentListing);
					}
				}else{
					$this->printListProduct($xmap, $parent, 'product', $parentListing );
				}
			}
			else{
				$cat = $this->getCategory($parentListing,null);
				$query = 'SELECT * FROM '.hikashop_table('category').' WHERE (category_access=\'all\' OR category_access LIKE \'%,'.$idGroupe.',%\')AND ' .
						'category_published=1 AND (category_left > '.(int)$cat->category_left.' ' .
						'AND category_right < '.(int)$cat->category_right.')';
				$db->setQuery($query);
				$categories = $db->loadObjectList('category_id');
				if(!empty($categories)){
					$xmap->changeLevel(1);
					$this->depth++;

					$query = 'SELECT * FROM '.hikashop_table('file').' WHERE file_ref_id IN ('.implode(',',array_keys($categories)).') AND file_type=\'category\' ';
					$db->setQuery($query);
					$files = $db->loadObjectList();

					foreach($categories as $category) {
						foreach($this->categoriesCheck as $valeur){
							if($this->same_category==0 && $valeur==$category->category_id){
								$this->exist=1;
							}
						}
						if($this->exist==0){
							$this->categoriesCheck[$category->category_id]=$category->category_id;
							$this->getCategory(0,$category);
							$node = new stdclass;
							$node->id   = $parent->id;
							$node->uid  = $parent->uid.'category'.$category->category_id.'_'.$parentListing;
							$node->name = $category->category_name;
							$categoryClass->addAlias($category);
							if(!empty($category->category_canonical)){
								$node->link = hikashop_cleanURL($category->category_canonical);
							}else{
								$node->link = 'index.php?option=com_hikashop&ctrl=category&task=listing&cid='.$category->category_id.'&name='.$category->alias.'&Itemid='.$node->id;
							}
							if($this->priority==-1){ $node->priority = $parent->priority;	}
							else{ $node->priority = $this->priority; }
							if($this->changefreq==-1){ $node->changefreq = $parent->changefreq;	}
							else{ $node->changefreq = $this->changefreq; }
							$node->expandible = false;
							if($this->image_changefreq && !empty($files)){
								$node->isImages = 1;
								$node->images = array();
								foreach($files as $file){
									if($file->file_ref_id != $category->category_id) continue;
									$img = $this->imageHelper->getThumbnail($file->file_path, array($this->image_width, $this->image_height), array('forcesize' => true, 'scale' => 'outside'));
									if(!$img->success) continue;
									if(substr($img->url, 0, 3) == '../')
										$image = str_replace('../', HIKASHOP_LIVE, $img->url);
									else
										$image = substr(HIKASHOP_LIVE, 0, strpos(HIKASHOP_LIVE, '/', 9)) . $img->url;
									$node->images[0] = new stdClass();
									$node->images[0]->src = $image;
									$node->images[0]->title = $file->file_name;
								}
							}
							$xmap->printNode($node);
							$this->printListCategory($xmap, $parent, $filterType, $category->category_id, $modules);
						}
						$this->exist=0;
					}
					$xmap->changeLevel(-1);
					$this->depth--;
				}
				$filterType=0;
				if(!empty($modules)){
					foreach($modules as $module){
						$moduleData = $this->getModule($module);
						$this->printListModule($xmap, $parent, $moduleData, $parentListing);
					}
				}else{
					$this->printListProduct($xmap, $parent, 'product', $parentListing );
				}
			}
		}
	}

	function printListProduct(&$xmap, &$parent, $filterType, $parentListing ){
		$app = JFactory::getApplication();
		$productClass = hikashop_get('class.product');
		$db = JFactory::getDBO();
		$idGroupe=$this->getGroup();
		if ($filterType == 0){
			$query = 'SELECT * FROM '.hikashop_table('product').' jhp INNER JOIN '.hikashop_table('product_category').' jhpc ON jhp.product_id=jhpc.product_id ' .
					'INNER JOIN '.hikashop_table('category').' jhc ON jhc.category_id=jhpc.category_id ' .
					'WHERE (product_access=\'all\' OR jhp.product_access LIKE \'%,'.$idGroupe.',%\')AND product_published=1 AND jhpc.category_id='.(int)$parentListing.' AND category_published=1 AND product_type=\'main\'';
			$db->setQuery($query);
			$products = $db->loadObjectList('product_id');
			if(!empty($products)){
				$xmap->changeLevel(1);
				$this->productNumber = 0;

				$query = 'SELECT * FROM '.hikashop_table('file').' WHERE file_ref_id IN ('.implode(',',array_keys($products)).') AND file_type=\'product\' ';
				$db->setQuery($query);
				$files = $db->loadObjectList();

				foreach($products as $product) {
					if($this->productNumber < $this->max_product || $this->max_product==-1){
						foreach($this->productsCheck as $valeur){
							if($this->same_product==0 && $valeur==$product->product_id){
								$this->exist=1;
							}
						}
						if($this->exist==0){
							$this->productsCheck[$product->product_id]=$product->product_id;
							$node = new stdclass;
							$node->id   = $parent->id;
							$node->uid  = $parent->uid.'product'.$product->product_id.'_'.$parentListing.'_'.$parent->id;
							$node->name = $product->product_name;
							$productClass->addAlias($product);
							if($parentListing==$this->currentCategory)
								$parentListing=0;
							if(!empty($product->product_canonical)){
								$node->link = hikashop_cleanURL($product->product_canonical);
							}else{
								$node->link = 'index.php?option=com_hikashop&ctrl=product&task=show&cid='.$product->product_id.'&name='.$product->alias.'&Itemid='.$node->id.'&category_pathway='.$parentListing;
							}
							if($this->priority==-1){ $node->priority = $parent->priority;	}
							else{ $node->priority = $this->priority; }
							if($this->changefreq==-1){ $node->changefreq = $parent->changefreq;	}
							else{ $node->changefreq = $this->changefreq; }
							$node->expandible = false;

							if($this->image_changefreq && !empty($files)){
								$node->isImages = 1;
								$node->images = array();
								foreach($files as $file){
									if($file->file_ref_id != $product->product_id) continue;
									$img = $this->imageHelper->getThumbnail($file->file_path, array($this->image_width, $this->image_height), array('forcesize' => true, 'scale' => 'outside'));
									if(!$img->success) continue;
									if(substr($img->url, 0, 3) == '../')
										$image = str_replace('../', HIKASHOP_LIVE, $img->url);
									else
										$image = substr(HIKASHOP_LIVE, 0, strpos(HIKASHOP_LIVE, '/', 9)) . $img->url;
									$image = new stdClass();
									$image->src = $image;
									$image->title = $file->file_name;
									$node->images[] = $image;
								}
							}

							$xmap->printNode($node);
							$this->productNumber++;
						}
						$this->exist=0;
					}
				}
				 $xmap->changeLevel(-1);
			}
			 $this->productNumber=0;
		}
		else{
			$cat = $this->getCategory($parentListing,null);
			$query = 'SELECT * FROM ('.hikashop_table('product').' jhp INNER JOIN '.hikashop_table('product_category').' jhpc ON jhp.product_id=jhpc.product_id) ' .
					'INNER JOIN '.hikashop_table('category').' jhc ON jhc.category_id=jhpc.category_id ' .
					'WHERE (jhp.product_access=\'all\' OR jhp.product_access LIKE \'%,'.$idGroupe.',%\') AND jhp.product_published=1 AND jhc.category_published=1 AND ' .
							'(jhc.category_left >= ' .(int)$cat->category_left. ' AND jhc.category_right <= ' .(int)$cat->category_right. ')  AND product_type=\'main\'';
			$db->setQuery($query);
			$products = $db->loadObjectList('product_id');
			if(!empty($products)){
				$xmap->changeLevel(1);

				$query = 'SELECT * FROM '.hikashop_table('file').' WHERE file_ref_id IN ('.implode(',',array_keys($products)).') AND file_type=\'product\' ';
				$db->setQuery($query);
				$files = $db->loadObjectList();

				foreach($products as $product) {
					if($this->productNumber < $this->max_product || $this->max_product==-1){
						foreach($this->productsCheck as $valeur){
							if($this->same_product==0 && $valeur==$product->product_id){
								$this->exist=1;//not display
							}
						}
						if($this->exist==0){
							$this->productsCheck[$product->product_id]=$product->product_id;
							$node = new stdclass;
							$node->id   = $parent->id;
							$node->uid  = $parent->uid.'product'.$product->product_id.'_'.$parentListing.'_'.$parent->id;
							$node->name = $product->product_name;
							$productClass->addAlias($product);
							if($parentListing==$this->currentCategory)
								$parentListing=0;
							if(!empty($product->product_canonical)){
								$node->link = hikashop_cleanURL($product->product_canonical);
							}else{
								$node->link = 'index.php?option=com_hikashop&ctrl=product&task=show&cid='.$product->product_id.'&name='.$product->alias.'&Itemid='.$node->id.'&category_pathway='.$parentListing;
							}
							if($this->priority==-1){ $node->priority = $parent->priority;	}
							else{ $node->priority = $this->priority; }
							if($this->changefreq==-1){ $node->changefreq = $parent->changefreq;	}
							else{ $node->changefreq = $this->changefreq; }
							$node->expandible = false;

							if($this->image_changefreq && !empty($files)){
								$node->isImages = 1;
								$node->images = array();
								foreach($files as $file){
									if($file->file_ref_id != $product->product_id) continue;
									$img = $this->imageHelper->getThumbnail($file->file_path, array($this->image_width, $this->image_height), array('forcesize' => true, 'scale' => 'outside'));
									if(!$img->success) continue;
									if(substr($img->url, 0, 3) == '../')
										$image = str_replace('../', HIKASHOP_LIVE, $img->url);
									else
										$image = substr(HIKASHOP_LIVE, 0, strpos(HIKASHOP_LIVE, '/', 9)) . $img->url;
									$image = new stdClass();
									$image->src = $image;
									$image->title = $file->file_name;
									$node->images[] = $image;
								}
							}

							$xmap->printNode($node);
							$this->productNumber++;
						}
						$this->exist=0;
					}
				}
				 $xmap->changeLevel(-1);
			}
			 $this->productNumber=0;
		}
	}

	function printListModule(&$xmap, &$parent, &$moduleData, $parentListing){
		if($moduleData->hikashop_params['content_type']=='product'){
			$this->printListProduct($xmap, $parent, @$moduleData->hikashop_params['filter_type'], $parentListing);
		}
	}

	function getGroup(){
		//joomla public group id
		if(version_compare(JVERSION,'1.6','<')){
			return 29;
		}else{
			return 1;
		}

	}

	function getModule($id){
		static $modules = array();
		if(!isset($modules[$id])){
			$moduleClass = hikashop_get('class.modules');
			$modules[$id] = $moduleClass->get($id);
		}
		return $modules[$id];
	}

	function getCategory($id,$new){
		static $categories = array();
		if(!empty($new)){
			$categories[$new->category_id]=&$new;
			return;
		}
		if(!isset($categories[$id])){
			$catClass = hikashop_get('class.category');
			$categories[$id] = $catClass->get($id);
		}
		return $categories[$id];
	}
}
