<?php
/**
 * @package		iStoreLocator
 * @author		Douglas Machado {@link http://idealextensions.com}
 * @author		Created on 29-Jun-2016
 * @license		GNU/GPL, see license.txt
 */
// no direct access
defined('_JEXEC') or die;

if( $this->get_browser_name() == 'Chrome' && $detect->version('Chrome') >= 50 )
{
	if(!$this->httpIsSecure())
	{
		$browserAllowsGeolocation = false;
	}
}

$geolocateAuto 	=  $browserAllowsGeolocation && ($this->params->get('geolocate') == 'auto' OR $this->params->get('geolocate', 'both') == 'both' ? true : false);

$searchValue = $input->getString('islsearch','');
if(empty($searchValue))
{
	$searchValue	= array();
	$searchValue[]	= $input->getString('address');
	$searchValue[]	= $input->getString('city',$input->getString('suburb'));
	$searchValue[]	= $input->getString('state');
	$searchValue[]	= $input->getString('country');
	
	$searchValue	= array_filter($searchValue, function($val){
		return (!empty($val));
	});
	$searchValue	= implode(', ', $searchValue);
}

if ($this->params->get('loadAllOnFirstLoad') AND !$geolocateAuto)
{	
	if ($this->params->get('syntax-latitude') && $this->params->get('syntax-longitude')){
		$script = "$('#'+canvas).gmap('getData',{lat:{$this->params->get('syntax-latitude')}, lng:{$this->params->get('syntax-longitude')}});";
	}else{
		$script = "$('#'+canvas).gmap('getData',{lat:0, lng:0});";
	}
	
}
elseif($geolocateAuto)
{
	$script = "$('#'+canvas).gmap('geolocate');";
}
elseif(!empty($searchValue))
{
	$script = "$('#'+canvas).gmap('search',{'address':$('#{$mapCanvas}-location-search').val()}, function(results, status) {
		if (status === 'OK' ) {
			var lat = results[0].geometry.location.lat();
			var lng = results[0].geometry.location.lng();
			$('#'+canvas).gmap('getData',{lat:lat, lng:lng});
		}
	});";
}

if ($this->params->get('searchPostion'))
{
	$html = '<div id="'.$mapCanvas.'-search-bar" 
		class="isl-search-bar input-prepend input-append input-group"
		'.(($this->params->get('searchPostion') == 'hide') ? 'style="display:none"' : '').'
		>';
	
	$geolocateButton= $browserAllowsGeolocation && ($this->params->get('geolocate') == 1 OR $this->params->get('geolocate', 'both') == 'both' ? true : false);
	
	if ($geolocateButton){
		$html .= '<button id="'.$mapCanvas.'_btn_geolocate" class="isl-geolocate-btn btn btn-'.$this->params->get('geolocate-class','primary').'" />'
				.'<i class=" icon-flag glyphicon glyphicon-flag"> </i> <span>'
				.$this->params->get('language_geolocate',JText::_('PLG_SYSTEM_ISTORELOCATOR_GEOLOCATE'))
				.'</span></button>';
		$html .= '<span class="add-on input-group-addon">'.$this->params->get('language_or',JText::_('PLG_SYSTEM_ISTORELOCATOR_OR')).'</span>';
		$script .= "$('.isl-geolocate-btn').on('click',function(){ $('#{$mapCanvas}').gmap('geolocate',this)});"."\n";
	}
	
	
	
	
	
	$html .= '<input id="'.$mapCanvas.'-location-search" 
			class="isl-location-search" 
			type="text"
			name="islsearch"
			value="'.$searchValue.'"
			placeholder="'.$this->params->get('language_enter_location',JText::_('PLG_SYSTEM_ISTORELOCATOR_ENTER_LOCATION')).'">';
	
	$maxDistanceOptions = explode(',', str_replace(' ','',$this->params->get('maxDistanceList')));
	
	if (count($maxDistanceOptions) > 1){
		if($this->params->get('unitSystem') == 'METRIC'){
			$unitAbbreviation 	= 'Km';
			$unit 				= $unitAbbreviation;
		}else{
			$unitAbbreviation 	= 'Mi';
			$unit				= 'Miles';
		}
		$maxDistanceList = array();
			
		foreach ($maxDistanceOptions as $maxDistanceOption){
			if(intval($maxDistanceOption) > 0){
				if($maxDistanceOption == 123456 
					OR strtolower($maxDistanceOption) == strtolower(JText::_('PLG_SYSTEM_ISTORELOCATOR_ANYWHERE')) 
					OR strtolower($maxDistanceOption) == 'anywhere'
				){
					$maxDistanceList[123456] = $this->params->get('language_anywhere',JText::_('PLG_SYSTEM_ISTORELOCATOR_ANYWHERE'));
				}else{
					$maxDistanceList[$maxDistanceOption] = $maxDistanceOption.' '.$unit;
				}
					
			}
		}
			
		$default = trim($this->params->get('maxDistanceDefault',100));
		$html .= JHtml::_('select.genericlist',$maxDistanceList,'maxdistance','class="isl-maxdistance"',null,null,$default);
	}
	//
	$html .= '</div> '; //<!-- END isl-search-bar -->
	
	if ($this->params->get('searchPostion','inside') == 'inside'){
		//$html = str_replace(array("\n","\t"), '', $html);
		
		$script .= "$('#{$mapCanvas}').gmap('addControl',$('#{$mapCanvas}-search-bar'), google.maps.ControlPosition.TOP_LEFT);";
		$html = '<div style="display:none">'.$html.'</div>';
	}
}