<?php
/**
 * @package		iStoreLocator
 * @author		Douglas Machado {@link http://idealextensions.com}
 * @author		Created on 29-Jun-2016
 * @license		GNU/GPL, see license.txt
 */
// no direct access
defined('_JEXEC') or die;

$html .='<div style="display:none" id="'.$mapCanvas.'_listTemplate">';
//		$html .='{{#each locations}}';
$html .='<li id="loc-list-id-{{id}}"
			data-gmapping=\'{"id":"{{id}}","lat":{{lat}},"lng":{{lng}} {{#if icon}},"icon":"{{icon}}" {{/if}},"tags":"{{tags}}" }\'
			data-markerid="{{id}}"
			class="img-thumbnail img-polaroid isl-visible list-item">';
$html .='<div class="list-content">';
$html .='{{#if image}}<div class="list-image '.$imageClass.'"><img src={{image}} alt="Image" /></div>{{/if}}';
$html .='<div class="list-label isl-hideInMap"><img src={{icon}} alt="Marker" /></div>';
if($params->get('showDistance',1)){
	$html .='{{#if distance}}<div class="loc-dist label label-'.$params->get('showDistance','info').'">{{distance}} {{unit}}</div>{{/if}}';
}

$html .='<div class="loc-name">{{name}}</div>';
$html .='<address class="loc-addr">';
$html .='<span itemprop="streetAddress" class="loc-address">{{address}}</span> ';
$html .='<span itemprop="addressLocality" class="loc-city">{{city}}</span> ';
$html .='{{#if state}}';
$html .='<span itemprop="addressRegion" class="loc-state">{{state}}</span> ';
$html .='{{/if}}';
$html .='{{#if postcode}}';
$html .='<span itemprop="postalCode" class="loc-postcode">{{postcode}}</span> ';
$html .='{{/if}}';
$html .='{{#if country}}';
$html .='<span itemprop="addressCountry" class="loc-country">{{country}}</span> ';
$html .='{{/if}}';
$html .='</address>';
$html .='{{#if misc}}<div class="loc-description '.$miscClass.'">{{allowHTML misc}}</div>{{/if}}';
$html .='</div>'; // end list-content

$listItem = array();
if($params->get('show_telephone',1)){
	$attr 	= array(
		'title'	=> '{{phone}}',
		'target'=> "_blank",
		'rel'	=> 'nofollow'
	);
	$listItem[] = '{{#if phone}}<div class="loc-phone"><i class="fa fa-phone-square"> </i> '.($isMobile
		?  JHtml::_('link','tel:{{phone}}', '{{phone}}', $attr)
		: '{{phone}}').'</div>{{/if}}';
}
if($params->get('show_mobile',1)){
	$attr 	= array(
		'title'	=> '{{mobile}}',
		'target'=> "_blank",
		'rel'	=> 'nofollow'
	);
	$listItem[] = '{{#if mobile}}<div class="loc-mobile"><i class="fa fa-mobile"> </i> '.($isMobile
		?  JHtml::_('link','tel:{{mobile}}', '{{mobile}}', $attr)
		: '{{mobile}}').'</div>{{/if}}';
}
if(		$params->get('show_webpage'	,1)
	&& $params->get('show_skype'	,1)
	&& $params->get('show_facebook'	,1)
	&& $params->get('show_twitter'	,1)
	&& $params->get('show_linkedin'	,1)
){
	$listItem[] = '<div class="loc-social-links btn-group" role="group">';
	if($params->get('show_webpage',1)){
		$attr 	= array(
			'class'	=> 'loc-webpage btn btn-default',
			'title'	=> JText::_($params->get('language_webpage',	'PLG_SYSTEM_ISTORELOCATOR_WEBPAGE')).': {{niceURL webpage}}'
		);
		if($params->get('show_webpage_new_window',1)){
			$attr['target']	= "_blank";
			$attr['rel']	= "nofollow";
		}
		$listItem[] = '{{#if webpage}}<a href="#{{webpage}}" '.JArrayHelper::toString($attr).'><i class="fa fa-link"></i></a>{{/if}}';
	}
	if($params->get('show_skype',1)){
		$attr 	= array(
			'class'	=> 'loc-skype btn btn-default',
			'title'	=> JText::_('PLG_SYSTEM_ISTORELOCATOR_SKYPE'),
			'target'=> "_blank",
			'rel'	=> 'nofollow'
		);
		$listItem[] = '{{#if skype}}<a href="skype:{{skype}}?call" '.JArrayHelper::toString($attr).'><i class="fa fa-skype"> </i></a>{{/if}}';
	}
	if($params->get('show_facebook',1)){
		$attr 	= array(
			'class'	=> 'loc-facebook btn btn-default',
			'title'	=> JText::_('PLG_SYSTEM_ISTORELOCATOR_FACEBOOK').': {{niceURL facebook}}',
			'target'=> "_blank",
			'rel'	=> 'nofollow'
		);
		$listItem[] = '{{#if facebook}}<a href="#{{facebook}}" '.JArrayHelper::toString($attr).'><i class="fa fa-facebook"> </i></a>{{/if}}';
	}

	if($params->get('show_twitter'	,1)){
		$attr 	= array(
			'class'	=> 'loc-twitter btn btn-default',
			'title'	=> JText::_('PLG_SYSTEM_ISTORELOCATOR_TWITTER').': @{{twitter}}',
			'target'=> "_blank",
			'rel'	=> 'nofollow'
		);
		$listItem[] = '{{#if twitter}}<a href="http://twitter.com/#!/{{twitter}}" '.JArrayHelper::toString($attr).'><i class="fa fa-twitter"> </i></a>{{/if}}';
	}
	if($params->get('show_linkedin'	,1)){
		$attr 	= array(
			'class'	=> 'loc-linkedin btn btn-default',
			'title'	=> JText::_('PLG_SYSTEM_ISTORELOCATOR_LINKEDIN').': {{niceURL linkedin}}',
			'target'=> "_blank",
			'rel'	=> 'nofollow'
		);
		$listItem[] = '{{#if linkedin}}<a href="#{{linkedin}}" '.JArrayHelper::toString($attr).'><i class="fa fa-linkedin"> </i></a>{{/if}}';
	}
	if($params->get('show_google_plus'	,1)){
		$attr 	= array(
			'class'	=> 'loc-linkedin btn btn-default',
			'title'	=> JText::_('PLG_SYSTEM_ISTORELOCATOR_GOOGLE_PLUS').': {{niceURL google_plus}}',
			'target'=> "_blank",
			'rel'	=> 'nofollow'
		);
		$listItem[] = '{{#if google_plus}}<a href="#{{google_plus}}" '.JArrayHelper::toString($attr).'><i class="fa fa-google-plus"> </i></a>{{/if}}';
	}
	$listItem[] = '</div>';
}

$html .= implode(' ', $listItem);

if($params->get('showDirections',1) OR ($params->get('showDetails',1) AND !empty($item->link)) ){
	$html .= '<div class="row-fluid loc-directions-details  isl-hideInMap">'; //row for bootstrap 3
	$html .= '<span class="btn-group" role="group">';



	if($params->get('showDirections',1)){
		$html .= '{{#if showRoute}}<button class="loc-btn-showDirections btn btn-'.$params->get('showDirections','primary').'">'
			.JText::_($params->get('language_directions',	'PLG_SYSTEM_ISTORELOCATOR_SHOW_DIRECTIONS'))
			.'</button>{{/if}}';
	}
	if($params->get('showDetails',1)){
		$html .= '{{#if link}}<a class="loc-btn-showDetails btn btn-'.$params->get('showDetails','primary').'"
		href="{{link}}" >'
			.JText::_($params->get('language_details',	'PLG_SYSTEM_ISTORELOCATOR_SHOW_DETAILS'))
			.'</a>{{/if}}';
	}
	$html .='</span>';
	$html .='</div>';
}
$html .='</li>';
//$html .='{{/each}}';
$html .='</div>';