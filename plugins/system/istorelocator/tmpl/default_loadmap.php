<?php
/**
 * @package		iStoreLocator
 * @author		Douglas Machado {@link http://idealextensions.com}
 * @author		Created on 29-Jun-2016
 * @license		GNU/GPL, see license.txt
 */
// no direct access
defined('_JEXEC') or die;

if($isMobile){
	// IF mobile we are changing the width of container via css so we can still set mapDraggable to true
	// Disables street view control on mobile
	$params->set('streetviewControl',false);
	$script .="
				// Prevent mobile devices to zoom in when clicking on the distance select list; Problem specially with iPhone
				if(jQuery('meta[name=viewport]').attr('content'))
				{
					jQuery('meta[name=viewport]').attr('content',jQuery('meta[name=viewport]').attr('content')+ ', user-scalable=0');
				}else{
					jQuery('head').append('<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0\">');
				}";
		
}

//$html 	= "\n<!-- DIV container for the map -->\n";
$style = 'height:'.(int)$params->get('mapHeight',450).'px;';

$mapCanvas = 'iStoreLocator_'.mt_rand();

$searchbar = $this->getSearchBar($mapCanvas);
$html 		.= $searchbar['html'];

$geolocateAuto 	=  ($this->params->get('geolocate') == 'auto' OR $this->params->get('geolocate', 'both') == 'both' ? true : false);

$listWidth = (int)$params->get('listWidth',4);
$storelocator = '<div class="isl-list-container col-md-'.$listWidth.' span'.$listWidth.'" style="'.$style.($listWidth == 0 ? 'display:none' : '').'">
	<div id="isl-search-result"></div>
	<ul id="isl-listings"><li style="cursor:auto" class="img-thumbnail img-polaroid"><p class="bg-info text-info">'
	.JText::sprintf($params->get('language_instructions','PLG_SYSTEM_ISTORELOCATOR_INSTRUCTIONS'),JText::_('PLG_SYSTEM_ISTORELOCATOR_'.$this->params->get('searchPostion','inside')))
	.'</p></li>'
		.'<li style="cursor:auto" class="img-thumbnail img-polaroid"><p class="bg-warning text-warning">'
		.JText::sprintf($params->get('language_distanceNote','PLG_SYSTEM_ISTORELOCATOR_DISTANCE_TOOL_NOTE'),JText::_($params->get('language_directions',	'PLG_SYSTEM_ISTORELOCATOR_SHOW_DIRECTIONS')) )
		.'</p></li>'
			.'</ul>
</div>';
// create div for the map canvas

$mapWidth = (12-$listWidth > 0 ? 12-$listWidth : 12);

$url	= JUri::base().'index.php?option=com_ajax&plugin=istorelocator&tmpl=component&format=json&';
//		echo $url; exit;
if($params->get('filter_city'))
{
	$url	.= 'filter_city='.$params->get('filter_city').'&';
}
if($params->get('filter_state'))
{
	$url	.= 'filter_state='.$params->get('filter_state').'&';
}
if($params->get('filter_country'))
{
	$url	.= 'filter_country='.$params->get('filter_country').'&';
}
if($params->get('filter_tags'))
{
	$url	.= 'filter_tags='.$params->get('filter_tags').'&';
}

$html 	.= '
				<div class="isl_container '.($params->get('bootstrap',2) ? 'row-fluid' : 'row').'">
					<!-- removed row-fluid in order to avoid problems with Bootstrap v3 -->
					<div style="display:none">
						<div id="'.$mapCanvas.'"
							style="'.$style.'"
							data-url="'.$url.'"
							class="isl_map_canvas col-md-'.($mapWidth).' span'.($mapWidth).'">
						</div>
						'.$storelocator.'
						<br style="clear:both" />
					</div>
				</div>
				';


$defaultLatitude = $params->get('default-latitude');
$defaultLongitude = $params->get('default-longitude');
$center = '';
if($defaultLatitude AND $defaultLongitude){
	$center =  'center: new google.maps.LatLng('.$defaultLatitude.', '.$defaultLongitude.'),';
}

if($params->get('unitSystem','METRIC') == 'METRIC')
{
	$unitAbbreviation 	= 'Km';
	$unit 				= $unitAbbreviation;
	$radius 			= 6367.0;
}else{
	$unitAbbreviation 	= 'Mi';
	$unit				= 'Miles';
	$radius 			= 3956.0;
}

switch($params->get('mapstyle_template','default'))
{
	case 'default':
		$mapstyle = '[]';
		break;
	case 'custom':
		$mapstyle = $params->get('mapstyle_custom','[]');
		break;
	default:
		if(is_readable(JPATH_PLUGINS.'/system/istorelocator/map_styles/'.$params->get('mapstyle_template','default').'/style.json'))
		{
			$mapstyle = file_get_contents(JPATH_PLUGINS.'/system/istorelocator/map_styles/'.$params->get('mapstyle_template','default').'/style.json');
		}else{
			$mapstyle = '[]';
		}
		break;
}
$language	= array(); // addslashes because users usually forget to add slashes when needed
$language['searching']	= JText::_($params->get('language_searching',	'PLG_SYSTEM_ISTORELOCATOR_SEARCHING'),true);
$language['results']	= addslashes(str_replace('{results}', '<span class="label label-warning">{results}</span>', $params->get('language_results',	JText::_('PLG_SYSTEM_ISTORELOCATOR_RESULTS'))));
$language['noresult']	= JText::_($params->get('language_noresult',	'PLG_SYSTEM_ISTORELOCATOR_NO_RESULTS_FOUND'),true);
$language['previous']	= JText::_($params->get('language_previous',	'JPREVIOUS'),true);
$language['geoLocationFailed']	= JText::_($params->get('language_geoLocationFailed',	'PLG_SYSTEM_ISTORELOCATOR_GEOLOCATION_FAILED'),true);
$language['openInGoogleMaps']	= JText::_($params->get('language_openInGoogleMaps',	'PLG_SYSTEM_ISTORELOCATOR_OPEN_IN_GOOGLE_MAPS'),true);

$script 	.= "
	function iStotelStart(canvas){
		$('#'+canvas).parent().css('display','');
		var mapOptions = {
			isMobile:			".($isMobile ? 'true' :  'false').",
			linkMobileToGmaps:	'{$params->get('showDirections-linkMobileToGmaps','coordinates')}',
			linkToGoogleMaps:	{$params->get('linkToGoogleMaps',1)},
			unitSystem: 		google.maps.UnitSystem.{$params->get('unitSystem','METRIC')},
			sourceType:			'{$params->get('source','com_contactenhanced')}',
			sourceFile:			'{$params->get('file','')}',
			scrollwheel:		{$params->get('scrollwheel', true)},
			mapTypeId:			google.maps.MapTypeId.{$params->get('MapTypeId','ROADMAP')},
			draggable:			{$params->get('mapDraggable','true')},
			mapTypeControl:		{$params->get('mapTypeControl','true')},
			mapTypeControlOptions:{style: google.maps.MapTypeControlStyle.{$params->get('mapTypeControlStyle','DEFAULT')}},
			scaleControl:		true,
			styles:				{$mapstyle},
			".($params->get('panControl') ? "
				panControl:			true,
				panControlOptions: {
				position:	google.maps.ControlPosition.{$params->get('panControl','LEFT_TOP')}
}, " : ""
		)."
		".($params->get('streetviewControl') == 'default' ? '' :"
			streetViewControl:	".($params->get('streetviewControl') ? 'true' : 'false').",
			streetViewControlOptions: {
				position:	google.maps.ControlPosition.".($params->get('streetviewControl') ? $params->get('streetviewControl') : 'LEFT_TOP')."
			},"
		)."
			zoomControl:		".($params->get('zoomControl') ? 'true' : 'false').",
		".($params->get('zoomControl') ? "
			zoomControlOptions: {
			position:	google.maps.ControlPosition.{$params->get('zoomControl','LEFT_CENTER')}
}, " : ""
)."
".$center."
zoom:				{$params->get('defaultZoom', 10)},
lengthUnit:			'{$unitAbbreviation}',
unit:				'{$unit}',
radius:				{$radius},
category:			'{$params->get('category','')}',
homeTitle:			'{$params->get('markerTitleContent','')}',
home_icon:			'".JUri::root().'/plugins/system/istorelocator/assets/images/markers/'.$params->get('home_icon')."',
			listTemplate: 		$('#{$mapCanvas}_listTemplate').html(),
				resultLimit:		{$params->get('resultLimit',1000)},
				autocomplete:		".($params->get('autocomplete',1) ?	'true' : 'false').",
			".($params->get('markerClusterer',1) ? "
			markerClusterer:
			".($this->getMarkerClustererOptions())."
				,
				"
				:'markerClusterer:	false,')."
			first_load_zoom:	".($params->get('first_load_zoom')? $params->get('first_load_zoom') : 'null').",
				first_load_maxdistance: ".($params->get('syntax-maxdistance')? $params->get('syntax-maxdistance') : 'null').",
				first_load_limit:	".($params->get('syntax-limit')? $params->get('syntax-limit') : 'null').",
					filterTags:			{
					operator:	'{$params->get('filter_groups-operator','OR')}',
					element:	'isl-filter_tags',
						tags:		[],
						position:	google.maps.ControlPosition.{$params->get('filter_groups-controlPosition','RIGHT_TOP')},
						source:		'{$params->get('filter_groups-source','metakey')}',
						enabled:	".($params->get('show_filter_groups') ? 'true' : 'false').",
						addedToMap:	false
								},
							circle:				{
								enabled:	".($params->get(	'radius-circle') ? 	'true' : 'false').",
								fillColor:	'{$params->get(		'radius-circle-fillColor',	'#4285f4')}',
								fillOpacity:{$params->get(		'radius-circle-fillOpacity',0.35)},
								strokeColor:'{$params->get(		'radius-circle-strokeColor','#4285f4')}',
								strokeOpacity:	{$params->get(	'radius-circle-strokeOpacity',0.8)},
								strokeWeight:	{$params->get(	'radius-circle-strokeWeight',3)}
},
language:			{
searching:	'{$language['searching']}',
	results:	'{$language['results']}',
	noresult:	'{$language['noresult']}',
		previous:	'{$language['previous']}',
		geoLocationFailed: '{$language['geoLocationFailed']}',
			openInGoogleMaps:	'{$language['openInGoogleMaps']}'
}
}

jQuery('#'+canvas).gmap(mapOptions).bind('init', function(event, map) {
//AutoClose infoWindow (balloon)
			google.maps.event.addListener(map,'click',function(){jQuery('#'+canvas).gmap('closeInfoWindow');});
			".$searchbar['script']."
			$('#'+canvas).gmap('placesAutocomplete','{$mapCanvas}-location-search');
			$('#'+canvas).gmap('addEvents');
				
}); // End Bind


} // END iStotelStart
iStotelStart('{$mapCanvas}');

";

if($params->get('show_misc') == 'list'){
$miscClass = 'isl-hideInMap';
}elseif($params->get('show_misc') == 'infowindow'){
$miscClass= 'isl-hideInList';
}else{
$miscClass = '';
}

			if($params->get('show_image') == 'list'){
			$imageClass = 'isl-hideInMap';
}elseif($params->get('show_misc') == 'infowindow'){
$imageClass= 'isl-hideInList';
}else{
$imageClass = '';
		}

		if($params->get('source','com_contactenhanced') != 'com_contactenhanced')
		{
			$tmpl_file = 'listtemplate_standalone.php';
			// Render the template
			include $path;
		}


if($params->get('show_filter_groups')){
$html .='<div id="isl-filter_tags" class="isl-box-shadow" style="margin:5px;padding:5px 5px 5px 10px;display:none">
			<fieldset><legend>'.JText::_($params->get('language_filter','PLG_SYSTEM_ISTORELOCATOR_FILTER_LABEL')).'</legend>
			<div></div>
		</fieldset>
	</div>';
}



		$html .= $this->getJS('jQuery(document).ready(function($){'.$script."\n}); // End jQuery Ready\n");