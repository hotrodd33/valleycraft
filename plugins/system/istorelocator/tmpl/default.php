<?php
/**
 * @package		iStoreLocator
 * @author		Douglas Machado {@link http://idealextensions.com}
 * @author		Created on 29-Jun-2016
 * @license		GNU/GPL, see license.txt
 */
// no direct access
defined('_JEXEC') or die;

$layout ='default_';
if(is_readable(__DIR__.'/'.$layout.$tmpl_file))
{
	include __DIR__ . '/'.$layout.$tmpl_file;
}
else // fallback to default layout
{
	include JPATH_PLUGINS . '/system/istorelocator/tmpl/default_'.$tmpl_file;
}