<?php
/**
 * @package		iStoreLocator
 * @author		Douglas Machado {@link http://idealextensions.com}
 * @author		Created on 29-Jun-2016
 * @license		GNU/GPL, see license.txt
 */
// no direct access
defined('_JEXEC') or die;

$listItem[] = '<li id="loc-list-id-'.$item->id.'"
						data-gmapping=\''.$marker.'\' data-markerId="'.$item->id.'" class="img-thumbnail img-polaroid isl-visible list-item">';
$listItem[] = '<div class="list-content">';
if(!empty($item->image)  && $params->get('show_misc',1)){
	$item->image = JUri::root().'/'.$item->image;
	$attr= array();
	if($params->get('show_image') == 'list'){
		$attr['class'] = 'isl-hideInMap';
	}elseif($params->get('show_misc') == 'infowindow'){
		$attr['class'] = 'isl-hideInList';
	}

	$listItem[] = '<div class="list-image">'.JHtml::_('image',$item->image, JText::_('PLG_SYSTEM_ISTORELOCATOR_IMAGE'),$attr).'</div>';
}
if($params->get('showDistance',1) AND isset($item->distance)){
	$distance_unit	= ($this->params->get('unitSystem','METRIC') == 'METRIC' ? 'Km' : 'Mi');
	$listItem[] = '<div class="loc-dist label label-'.$params->get('showDistance','info').'">'.round($item->distance,2).' '.$distance_unit.'</div>';
}
	
$listItem[] = '<div class="list-label isl-hideInMap">'.JHtml::_('image',$item->icon,'icon').'</div>';
	
$listItem[] = '<div class="loc-name">'.$item->name.'</div>';
	
	
if(!empty($item->address) OR !empty($item->city) OR !empty($item->state) OR !empty($item->country) OR !empty($item->postalcode))
{
	$addressParts	= array();
	$addressFormat	= str_replace('suburb', 'city', $this->params->get('address_format','address,suburb,state,postcode,country'));
	$addressFormat 	= explode(',', $addressFormat);
	$addressSchema	= array('address'	=> 'streetAddress'
		,'city'			=> 'addressLocality'
		,'state'		=> 'addressRegion'
		,'postcode'		=> 'postalCode'
		,'country'		=> 'addressCountry'
	);
		
	$listItem[] = '<address class="loc-addr">';
	foreach ($addressFormat as $key){
		if(!empty($item->$key) && $params->get('show_'.$key,1))
		{
			$addressParts[] = $item->$key;
			$listItem[] = '<span itemprop="'.$addressSchema[$key].'" class="loc-'.$key.'">';
			$listItem[] = nl2br($item->$key);
			$listItem[] = '</span>';
		}
	}
	$listItem[] = '</address>';
}
	
if(!empty($item->misc) && $params->get('show_misc',1)){
	$listItem[] = '<div class="loc-misc">'.$item->misc.'</div>';
}
	
$listItem[] = '</div> '; // list-content
	
if(!empty($item->phone) && $params->get('show_telephone',1)){
	$attr 	= array(
		'title'	=> $item->phone,
		'target'=> "_blank",
		'rel'	=> 'nofollow'
	);
	$listItem[] = '<div class="loc-phone"><i class="fa fa-phone-square"> </i> '.($isMobile
		?  JHtml::_('link','tel:'.preg_replace('[(?!\+\b)\D]', '', $item->phone), $item->phone, $attr)
		: $item->phone).'</div>';
}
if(!empty($item->mobile) && $params->get('show_mobile',1)){
	$attr 	= array(
		'title'	=> $item->phone,
		'target'=> "_blank",
		'rel'	=> 'nofollow'
	);
	$listItem[] = '<div class="loc-mobile"><i class="fa fa-mobile"> </i> '.($isMobile
		?  JHtml::_('link','tel:'.preg_replace('[(?!\+\b)\D]', '', $item->mobile), $item->mobile, $attr)
		: $item->mobile).'</div>';
}
if(
	!empty($item->webpage) 		&& $params->get('show_webpage'	,1)
	OR !empty($item->skype) 	&& $params->get('show_skype'	,1)
	OR !empty($item->facebook) 	&& $params->get('show_facebook'	,1)
	OR !empty($item->twitter) 	&& $params->get('show_twitter'	,1)
	OR !empty($item->linkedin) 	&& $params->get('show_linkedin'	,1)
	OR !empty($item->google_plus)&& $params->get('show_google_plus'	,1)
){
	$listItem[] = '<div class="loc-social-links btn-group" role="group">';
	if(!empty($item->webpage) && $params->get('show_webpage',1)){
		$attr 	= array(
			'class'	=> 'loc-webpage btn btn-default',
			'title'	=> JText::_($params->get('language_webpage',	'PLG_SYSTEM_ISTORELOCATOR_WEBPAGE')).': '.preg_replace('/https?:\/\/|www./', '', $item->webpage)
		);
		if($params->get('show_webpage_new_window',1)){
			$attr['target']	= "_blank";
			$attr['rel']	= "nofollow";
		}
		$listItem[] = JHtml::_('link',$item->webpage, '<i class="fa fa-link"> </i>', $attr);
	}
	if(!empty($item->skype) 	&& $params->get('show_skype',1)){
		$attr 	= array(
			'class'	=> 'loc-skype btn btn-default',
			'title'	=> JText::_($params->get('language_skype',	'PLG_SYSTEM_ISTORELOCATOR_SKYPE')).': '.$item->skype,
			'target'=> "_blank",
			'rel'	=> 'nofollow'
		);
		$listItem[] = JHtml::_('link', 'skype:'.$item->skype.'?call', '<i class="fa fa-skype"> </i>', $attr);
	}
	if(!empty($item->facebook) 	&& $params->get('show_facebook',1)){
		$attr 	= array(
			'class'	=> 'loc-facebook btn btn-default',
			'title'	=> JText::_($params->get('language_facebook',	'PLG_SYSTEM_ISTORELOCATOR_FACEBOOK')).': '.$item->facebook,
			'target'=> "_blank",
			'rel'	=> 'nofollow'
		);
		$listItem[] = JHtml::_('link', (substr($item->facebook,0,4) == 'http'
			? $item->facebook
			: 'https://facebook.com/'.$item->facebook), '<i class="fa fa-facebook"> </i>', $attr);
	}

	if(!empty($item->twitter) 	&& $params->get('show_twitter'	,1)){
		$attr 	= array(
			'class'	=> 'loc-twitter btn btn-default',
			'title'	=> JText::_($params->get('language_twitter',	'PLG_SYSTEM_ISTORELOCATOR_TWITTER')).': @'.$item->twitter,
			'target'=> "_blank",
			'rel'	=> 'nofollow'
		);
		$listItem[] = JHtml::_('link', 'http://twitter.com/#!/'.$item->twitter, '<i class="fa fa-twitter"> </i>', $attr);
	}
	if(!empty($item->linkedin) 	&& $params->get('show_linkedin'	,1)){
		$attr 	= array(
			'class'	=> 'loc-linkedin btn btn-default',
			'title'	=> JText::_($params->get('language_linkedin',	'PLG_SYSTEM_ISTORELOCATOR_LINKEDIN')).': '.$item->linkedin,
			'target'=> "_blank",
			'rel'	=> 'nofollow'
		);
		$listItem[] = JHtml::_('link', (substr($item->linkedin,0,4) == 'http'
			? $item->linkedin
			: 'https://linkedin.com/in/'.$item->linkedin), '<i class="fa fa-linkedin"> </i>', $attr);
	}
	if(!empty($item->google_plus) 	&& $params->get('show_google_plus'	,1)){
		$attr 	= array(
			'class'	=> 'loc-linkedin btn btn-default',
			'title'	=> JText::_($params->get('language_google_plus',	'PLG_SYSTEM_ISTORELOCATOR_GOOGLE_PLUS')).': '.$item->google_plus,
			'target'=> "_blank",
			'rel'	=> 'nofollow'
		);
		if (substr ( $item->google_plus, 0, 4 ) != 'http' and substr ( $item->google_plus, 0, 1 ) != '+') {
			$item->google_plus = '+' . $item->google_plus;
		}
		$listItem[] = JHtml::_('link', (substr($item->google_plus,0,4) == 'http'
			? $item->google_plus
			: 'https://google.com/'.$item->google_plus), '<i class="fa fa-google-plus"> </i>', $attr);
	}
	$listItem[] = '</div>';
}
	
if($params->get('showDirections',1) OR ($params->get('showDetails',1) AND !empty($item->link)) ){
	$listItem[] = '<div class="row-fluid loc-directions-details isl-hideInMap">';
	$listItem[] = '<span class="btn-group" role="group">';
	if($params->get('showDirections',1) AND $lat AND $lng AND $item->lat AND $item->lng){
		$listItem[] = '<a href="#showDirections"
							class="loc-btn-showDirections btn btn-'.$params->get('showDirections','primary').'">'
								.'<i class="icon-road glyphicon glyphicon-road"> </i>'
									.JText::_($params->get('language_directions',	'PLG_SYSTEM_ISTORELOCATOR_SHOW_DIRECTIONS'))
									.'</a>';
	}

	if($params->get('showDetails',1) AND !empty($item->link)){
		$listItem[] = '<a class="loc-show-showDetails btn btn-'.$params->get('showDetails','primary').'"
							href="'.$item->link.'" >'
								.JText::_($params->get('language_details',	'PLG_SYSTEM_ISTORELOCATOR_SHOW_DETAILS'))
								.'</a>';
	}
	$listItem[] = '</span>';
	$listItem[] = '</div>';
}
$listItem[] = '</li>';
	
