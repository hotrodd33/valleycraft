<?php
/**
 * @package		iStoreLocator
 * @author		Douglas Machado {@link http://idealextensions.com}
 * @author		Created on 09-Dec-2014
 * @license		GNU/GPL, see license.txt
 */
defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');

$lang = JFactory::getLanguage();
//Load English always, useful if file is partially translated
$lang->load('plg_system_istorelocator',	JPATH_PLUGINS.'/system/istorelocator', 'en-GB');
$lang->load('plg_system_istorelocator', JPATH_PLUGINS.'/system/istorelocator',	null,	true);

/**
 * @package		com_iformbuilder
* @since		1.6
 */
class JFormFieldAjaxbutton extends JFormFieldText
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'Ajaxbutton';

	
	/**
	 * Method to get the field input markup.
	 *
	 * @return  string  The field input markup.
	 *
	 * @since   11.1
	 */
	protected function getInput()
	{
		// Translate placeholder text
		$hint = $this->translateHint ? JText::_($this->hint) : $this->hint;

		// Initialize some field attributes.
		$class			= !empty($this->class) ? ' class="' . $this->class . '"' : '';
		$containerID	= 'log-container-'.$this->id;

		// Get the label text from the XML element, defaulting to the element name.
		$text	= $this->element['label'] ? (string) $this->element['label'] : (string) $this->element['name'];
		$text	= $this->translateLabel ? JText::_($text) : $text;
		$url	= $this->element['url'] ? (string) $this->element['url'] : '';
		// Including fallback code for HTML5 non supported browsers.
		JHtml::_('jquery.framework');
		
		$doc = JFactory::getDocument();
		$doc->addScriptDeclaration("
jQuery(document).ready(function($){
	$('#{$this->id}').on('click',function(){
		var button = this;
		$(button).text('Geocoding, please wait.');
		///$(button).attr('disabled','disabled').text('Geocoding, please wait.');
		// AJAX request
		var request = $.post( '{$url}');
		request.always(function( result ) {
			if(result.success == true){	
				$('#{$containerID}').html('<pre>'+result.data[0].log+'</pre>');
			}
			$(button).text('Geocoded.');
		});
	
	});
	
});
");
		$html	= array();

		$html[] = '<button type="button" name="' . $this->name . '" id="' . $this->id . '" ' . $class 
			. ' >'.htmlspecialchars($text, ENT_COMPAT, 'UTF-8').'</button>';
		$html[] = '<div id="'.$containerID.'"></div>';

		return implode($html);
	}
	
	

	
}