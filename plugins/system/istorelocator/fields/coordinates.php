<?php
/**
 * @package		iStoreLocator
 * @author		Douglas Machado {@link http://idealextensions.com}
 * @author		Created on 09-Dec-2014
 * @license		GNU/GPL, see license.txt
 */
defined('JPATH_BASE') or die;

jimport('joomla.html.html');
jimport('joomla.form.formfield');

$lang = JFactory::getLanguage();
//Load English always, useful if file is partially translated
$lang->load('plg_system_istorelocator',	JPATH_PLUGINS.'/system/istorelocator', 'en-GB');
$lang->load('plg_system_istorelocator', JPATH_PLUGINS.'/system/istorelocator',	null,	true);
require_once JPATH_LIBRARIES.'/joomla/form/fields/text.php';
/**
 * @package		com_iformbuilder
* @since		1.6
 */
class JFormFieldCoordinates extends JFormFieldText
{
	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since	1.6
	 */
	protected $type = 'Coordinates';

	/**
	 * Method to get the field input markup.
	 *
	 * @return	string	The field input markup.
	 * @since	1.6
	 */
	protected function getLabel()
	{
		$title = $this->element['label'] ? (string) $this->element['label'] : ($this->element['title'] ? (string) $this->element['title'] : '');
		$heading = $this->element['heading'] ? (string) $this->element['heading'] : 'h4';
		$description = (string) $this->element['description'];
		$class = !empty($this->class) ? ' class="' . $this->class . '"' : '';
		$close = (string) $this->element['close'];

		$html = array();

		$html[] = !empty($title) ? '<' . $heading . '>' . JText::_($title) . '</' . $heading . '>' : '';
		$html[] = !empty($description) ? JText::_($description) : '';
		$html[] = parent::getInput();
		$html[] = '<div id="coordinates-map"  class="span12 col-md-12" style="height:300px;margin-left:0"></div>';
		
		$html[] = "<script>jQuery(document).ready(function($){
		
	var lat = $('#jform_params_default_latitude').val();
	var lng = $('#jform_params_default_longitude').val();
	$('#coordinates-map').locationpicker({
		location: {latitude: lat, longitude: lng},
		radius: 0,
		inputBinding: {
			latitudeInput: $('#jform_params_default_latitude'),
			longitudeInput: $('#jform_params_default_longitude'),
			radiusInput: null,
			locationNameInput: $('#{$this->id}')
		},
		enableAutocomplete: true
	});
	$('dt span h3 a, .accordion-toggle, ul.nav-tabs a').mouseout(function(e){
		setTimeout(function(){ 
			google.maps.event.trigger($('#coordinates-map').locationpicker('map').map, 'resize');
		},500);
	});
});
</script>";
		JHtml::_('jquery.framework');
		$doc = JFactory::getDocument();
		$comParam = JComponentHelper::getParams('com_contactenhanced');
		$key = $comParam->get('gmaps_api_key');
		$key = (empty($key) ? '' : '&amp;key='.$key );
		$doc->addScript('//maps.google.com/maps/api/js?libraries=places'.$key);
		$doc->addScript(JUri::root(true).'/plugins/system/istorelocator/assets/js/locationpicker.jquery.js');
		$doc->addStyleDeclaration("
.font-family-arial{font-family:arial,helvetica,sans-serif}
.font-family-helvetica{font-family:helvetica,arial,sans-serif}
.font-family-comic{font-family:comic sans ms,cursive}
.font-family-courier{font-family:courier new,courier,monospace}
.font-family-georgia{font-family:georgia,serif}
.font-family-lucida{font-family:lucida sans unicode,lucida grande,sans-serif}
.font-family-tahoma{font-family:tahoma,geneva,sans-serif}
.font-family-times{font-family:times new roman,times,serif}
.font-family-trebuchet{font-family:trebuchet ms,helvetica,sans-serif}
.font-family-verdana{font-family:verdana,geneva,sans-serif}
");
		return '</div><div ' . $class . ' style="margin-left:0">' . implode('', $html);
	}
	/**
	 * Method to get the field input markup.
	 *
	 * @return  string  The field input markup.
	 *
	 * @since   11.1
	 */
	protected function getInput()
	{
		return '&nbsp;';
	}
}