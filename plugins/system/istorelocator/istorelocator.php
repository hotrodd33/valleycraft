<?php
/**
 * @package		iStoreLocator
 * @author		Douglas Machado {@link http://idealextensions.com}
 * @author		Created on 09-Dec-2014
 * @license		GNU/GPL, see license.txt
 */
// no direct access
defined('_JEXEC') or die;
use Joomla\Utilities\ArrayHelper;
jimport('joomla.plugin.plugin');


/**
 * iStoreLocator Plugin
 *
 * @package		iStoreLocator
 * @subpackage	Plugin
 */
class plgSystemIstorelocator extends JPlugin
{
	private $updatelatLng_log = '';
	private $locations	= array();
	/**
	 * Object Constructor.
	 *
	 * @access	public
	 * @param	object	The object to observe -- event dispatcher.
	 * @param	object	The configuration object for the plugin.
	 * @return	void
	 * @since	1.0
	 */
	function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
		$lang =JFactory::getLanguage();
		$lang->load('com_contactenhanced',		JPATH_SITE.'/components/com_contactenhanced', 'en-GB');
		$lang->load('com_contactenhanced',		JPATH_SITE.'/components/com_contactenhanced');
		$lang->load('plg_system_istorelocator',	dirname(__FILE__), 'en-GB');
		$lang->load('plg_system_istorelocator',	dirname(__FILE__), null, true);

		// jQuery is required.. In the future we add jQuery onAfterRender() 
		
	}
	
	function onAfterDispatch(){
		$lang =JFactory::getLanguage();
		$lang->load('com_contactenhanced',		JPATH_SITE.'/components/com_contactenhanced', 'en-GB');
		$lang->load('com_contactenhanced',		JPATH_SITE.'/components/com_contactenhanced');
		$lang->load('plg_system_istorelocator',	dirname(__FILE__), 'en-GB');
		$lang->load('plg_system_istorelocator',	dirname(__FILE__), null, true);
		
//		JFactory::getDocument()->addStyleSheet('https://maxcdn.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css');
		if($this->params->get('force_bootstrap',1)){

			$app 	= JFactory::getApplication();
		
			if(	$app->isSite()
					AND (
							is_array($this->params->get('force_bootstrap_menuitems',array())) AND
							(
								in_array($app->input->getInt( 'Itemid'),$this->params->get('force_bootstrap_menuitems',array()))
								OR in_array('-1',$this->params->get('force_bootstrap_menuitems',array()))
							)
							OR 
							(
								is_string($this->params->get('force_bootstrap_menuitems')) AND
								(
									$app->input->getInt( 'Itemid') ==$this->params->get('force_bootstrap_menuitems')
									OR '-1' ==$this->params->get('force_bootstrap_menuitems')
								)
							)
					)
			){
				JHtml::_('jquery.framework');
				// Bootstrap has to load last in order to avoid incompatibility with jQuery UI
				JHtml::_('bootstrap.framework');
				JHtml::_('bootstrap.loadCss');
				
			}
			
			
			
		}
	}
	
	

	
	/**
	 * Changes the string {istorelocator params=|option|} for the map
	 *
	 * Method is called by the view
	 *
	 * @param	string	The context of the content being passed to the plugin.
	 * @param	object	The content object.  Note $article->text is also available
	 * @param	object	The content params
	 * @param	int		The 'page' number
	 * @since	1.6
	 */
	public function onAfterRender()
	{
		// Abort if cannot load
		if (!$this->canLoad()){
			return '';
		}
		$this->replacer();
		$this->addCSS();
		$this->addJS();
		//echo $body; exit;
				
	}
	
	public function canLoad(){
		$app 	= JFactory::getApplication();
		$text	= JResponse::getBody();
		if($app->isAdmin() OR !strpos($text, '{istorelocator')){
			return false;
		}
		return true;
	}
	
	public function replacer()
	{
		$app 	= JFactory::getApplication();
		$text	= JResponse::getBody();
		// expression to search for
		$regex	= "#{istorelocator(.*?)}#s";
		//echo '<pre>'; print_r(JRequest::get('request')); exit;
				
		// find all instances of plugin and put in $matches
		preg_match_all( $regex, $text, $matches );
		// Number of plugins
		$count = count( $matches[0] );
		// plugin only processes if there are any instances of the plugin in the text
		if ( $count < 1) {
			return '';
		}
		
		
		
		for ( $i=0; $i < $count; $i++ )
		{
			$pluginParams = &$this->params;
	
			$inline_params = str_replace( '{istorelocator', '', $matches[0][$i] );
			$inline_params = str_replace( '}', '', $inline_params );
			$inline_params = trim( $inline_params );

			
			
			$source_matches = array();
			preg_match( "#source=\|(.*?)\|#s", $inline_params, $source_matches );
			if (isset($source_matches[1]) ){
				$pluginParams->set('source', $source_matches[1]);
			}
			
			$pluginParams->def('source', $this->params->get('source','com_contactenhanced'));
			
			$file = array();
			preg_match( "#file=\|(.*?)\|#s", $inline_params, $file );
			if (isset($file[1]) ){
				$pluginParams->set('file', $file[1]);
			}
			
			switch ($pluginParams->get('source')) {
				case 'csv':
					$pluginParams->def('file', $this->params->get('csvFile'));
					// getMarkers just to make sure all of them have latLng
					break;
				case 'json':
					$pluginParams->def('file', $this->params->get('jsonFile'));
				break;
				case 'kml':
					$pluginParams->def('file', $this->params->get('kmlFile'));	
				break;
				case 'xml':
					$pluginParams->def('file', $this->params->get('xmlFile'));
				break;
						
				case 'com_contactenhanced':
				default:
					
					$category = array();
					preg_match( "#category=\|(.*?)\|#s", $inline_params, $category );
					if (isset($category[1]) ){
						
						$pluginParams->set('category', $category[1]);
					}
					break;
			}
			
			$height_matches = array();
			preg_match( "#height=\|(.*?)\|#s", $inline_params, $height_matches );
			if (isset($height_matches[1]) ){
				$pluginParams->set('mapHeight', $height_matches[1]);
			}

			$zoom_matches = array();
			preg_match( "#zoom=\|(.*?)\|#s", $inline_params, $zoom_matches );
			if (isset($zoom_matches[1]) ){
				$pluginParams->set('first_load_zoom', $zoom_matches[1]);
			}
			
			$lat_matches = array();
			preg_match( "#lat=\|(.*?)\|#s", $inline_params, $lat_matches );
			
			if (isset($lat_matches[1]) ){
				$pluginParams->set('default-latitude', (float)$lat_matches[1]);
				$pluginParams->set('syntax-latitude', (float)$lat_matches[1]);
			}
			
			$lng_matches = array();
			preg_match( "#lng=\|(.*?)\|#s", $inline_params, $lng_matches );
			if (isset($lng_matches[1]) ){
				$pluginParams->set('default-longitude', (float)$lng_matches[1]);
				$pluginParams->set('syntax-longitude', (float)$lng_matches[1]);
			}
			
			$maxdistance_matches = array();
			preg_match( "#firstload-maxdistance=\|(.*?)\|#s", $inline_params, $maxdistance_matches );
			if (isset($maxdistance_matches[1]) ){
				$pluginParams->set('syntax-maxdistance', (int)$maxdistance_matches[1]);
			}
			
			$limit_matches = array();
			preg_match( "#firstload-limit=\|(.*?)\|#s", $inline_params, $limit_matches );
			if (isset($limit_matches[1]) ){
				$pluginParams->set('syntax-limit', (int)$limit_matches[1]);
			}

			$loadall_matches = array();
			preg_match( "#firstload-loadall=\|(.*?)\|#s", $inline_params, $loadall_matches );
			if (isset($loadall_matches[1]) ){
				$pluginParams->set('loadAllOnFirstLoad', (int)$loadall_matches[1]);
			}
			
			$filter_matches = array();
			preg_match( "#filter_city=\|(.*?)\|#s", $inline_params, $filter_matches );
			if (isset($filter_matches[1]) ){
				$pluginParams->set('filter_city', trim($filter_matches[1]));
			}
			
			$filter_matches = array();
			preg_match( "#filter_state=\|(.*?)\|#s", $inline_params, $filter_matches );
			if (isset($filter_matches[1]) ){
				$pluginParams->set('filter_state', trim($filter_matches[1]));
			}
			
			$filter_matches = array();
			preg_match( "#filter_country=\|(.*?)\|#s", $inline_params, $filter_matches );
			if (isset($filter_matches[1]) ){
				$pluginParams->set('filter_country', trim($filter_matches[1]));
			}
			
			$filter_matches = array();
			preg_match( "#filter_contact_tags=\|(.*?)\|#s", $inline_params, $filter_matches );
			if (isset($filter_matches[1]) ){
				$pluginParams->set('filter_tags', trim($filter_matches[1]));
			}
			
			$searchbar_matches = array();
			preg_match( "#search_position=\|(.*?)\|#s", $inline_params, $searchbar_matches );
			if (isset($searchbar_matches[1]) ){
				$pluginParams->set('searchPostion', trim($searchbar_matches[1]));
			}
				
			
			$listWidth_matches = array();
			preg_match( "#listWidth=\|(.*?)\|#s", $inline_params, $listWidth_matches );
			if (isset($listWidth_matches[1]) ){
				$pluginParams->set('listWidth', (int)$listWidth_matches[1]);
			}
			
			
			$output = $this->loadMap($pluginParams);
			$text 	= str_replace($matches[0][$i], $output, $text );
			
			// only allow one Store locator per page;
			break;
		}	
		// removes tags without matchings
		$text = preg_replace( $regex, '', $text );
		JResponse::setBody($text);
	}
	
	protected function addCSS(){
		$body 	= JResponse::getBody();
		$style = $this->params->get('customcss');
		$stylestring = "\n".'<link rel="stylesheet" href="'.JURI::base(true).'/plugins/system/istorelocator/assets/css/istorelocator.css" type="text/css" />'."\n";
		if(is_readable(JPATH_ROOT.'/plugins/system/istorelocator/assets/css/custom.css'))
		{
			$stylestring .= "\n".'<link rel="stylesheet" href="'.JURI::base(true).'/plugins/system/istorelocator/assets/css/custom.css" type="text/css" />'."\n";
		}
		if($this->params->get('loadFontAwesome',1))
		{
			$stylestring .= '<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">'."\n";
		}
		if(!empty($style))
		{
			$stylestring .= '<style type="text/css">'."\n".$style."\n".'</style>'."\n";
		}
		$body = preg_replace('#</head>#',$stylestring.'</head>',$body,1);
		//$body = preg_replace('#</body>#',$script.'</body>',$body,1);
		JResponse::setBody($body);
	}
	
	protected function addJS()
	{
		$body 	= JResponse::getBody();
		$script = '';
		$customJS = $this->params->get('customjs');
		if(!empty($customJS)){
			$script .= '<script type="text/javascript">'."\n".$customJS."\n".'</script>'."\n";
		}
		$body = preg_replace('#</body>#',$script.'</body>',$body,1);
		JResponse::setBody($body);
	}
	
	public function getSearchBar($mapCanvas)
	{
		$app 	= JFactory::getApplication();
		$input 	= $app->input;
		$html = '';
		$script = "";
		$browserAllowsGeolocation = true;
		
		if(!class_exists('Mobile_Detect')){
			if (is_readable(JPATH_ROOT.'/components/com_contactenhanced/helpers/Mobile_Detect.php')){
				require_once(JPATH_ROOT.'/components/com_contactenhanced/helpers/Mobile_Detect.php');
			}else{
				require_once(JPATH_PLUGINS.'/system/istorelocator/helpers/Mobile_Detect.php');
			}
		}
		$detect	= new Mobile_Detect();
		
		$tmpl_file = 'searchbar.php';
		$path = JPluginHelper::getLayoutPath('system', 'istorelocator');
		// Render the SearchBar
		
		include $path;
		
		
		return array('html' => $html, 'script' => $script);
	}
	
	/**
	 * Load the map
	 * @param object $obj Contact object
	 * @param  $params  this parameter has been deprecated
	 */
	public function loadMap($params)
	{
		if(!class_exists('Mobile_Detect'))
		{
			if (is_readable(JPATH_ROOT.'/components/com_contactenhanced/helpers/Mobile_Detect.php'))
			{
				require_once(JPATH_ROOT.'/components/com_contactenhanced/helpers/Mobile_Detect.php');
			}
			else
			{
				require_once(JPATH_PLUGINS.'/system/istorelocator/helpers/Mobile_Detect.php');
			}
		}
		$isMobile = new Mobile_Detect();
		$isMobile = $isMobile->isMobile();
		
		$doc	= JFactory::getDocument();
		$script = '';
		$html 	= '';
		
		
		$tmpl_file = 'loadmap.php';
		$path = JPluginHelper::getLayoutPath('system', 'istorelocator');
		// Render the LoadMap

		include $path;
		
		
		
		
		
		return $html;
		
	}
	public function toJsJSON($object){
		$json = array();
	//	ceHelper::print_r($object);
	//var_dump($object); exit;
		foreach ($object as $key => $value) {
			if(is_int($value))
			{
				$json[$key] = $value;
			}
			elseif(is_string($value))
			{
				if(strpos($value, "'") === false && strpos($value, "{") === false) 
				{
					$json[$key] = "'".$value."'";
				}
				else
				{
					$json[$key] = $value; // is already enclosed by  single quotes
				}
				
			}
			elseif(is_array($value))
			{
				if(!empty($value[0]) && is_object($value[0]))
				{
					$children	= array();
					foreach ($value as $child){
						$children[] = $this->toJsJSON($child);
					}
					$json[$key] = "[".implode(",", $children)."]";
				}
				else
				{
					$json[$key] = "[".implode(",", $value)."]";
				}
				
			}
			elseif(is_object($value))
			{
				$json[$key] = $this->toJsJSON($value);
			}
		}
		
		$json	= '{'.$this->arrayToString($json,':',",\n",'').'}';

		return $json;
	}
	
	/**
	 * Utility function to map an array to a string.
	 *
	 * @param   array    $array         The array to map.
	 * @param   string   $inner_glue    The glue (optional, defaults to '=') between the key and the value.
	 * @param   string   $outer_glue    The glue (optional, defaults to ' ') between array elements.
	 * @param   boolean  $keepOuterKey  True if final key should be kept.
	 *
	 * @return  string   The string mapped from the given array
	 *
	 * @since   1.0
	 */
	public function arrayToString(array $array, $inner_glue = '=', $outer_glue = ' ', $enclose = '"', $keepOuterKey = false)
	{
		$output = array();
	
		foreach ($array as $key => $item)
		{
			if (is_array($item))
			{
				if ($keepOuterKey)
				{
					$output[] = $key;
				}
	
				// This is value is an array, go and do it again!
				$output[] = $this->toString($item, $inner_glue, $outer_glue,$enclose, $keepOuterKey);
			}
			else
			{
				if($enclose == 'auto')
				{
					if (is_string($item))
					{
						if(strpos($value, "'") === 0 && strpos($value, '"') === 0)
						{
							$output[] = $key . $inner_glue . '"' . $item . '"';
						}
						else
						{
							$output[] = $key . $inner_glue  . $item ;
						}
						
					}
					elseif (is_int($item))
					{
						if (is_string($item))
						{
							$output[] = $key . $inner_glue  . $item ;
						}
					}
				}
				else
				{
					$output[] = $key . $inner_glue . $enclose . $item . $enclose;
				}
				
			}
		}
	
		return implode($outer_glue, $output);
	}
	public function getMarkerClustererOptions()
	{
		$params		= &$this->params;
		$options	= array();
		$styleOpts	= array();
		$options['maxZoom']		= (int)$params->get('markerClusterer-maxZoom',	14);
		$options['gridSize']	= (int)$params->get('markerClusterer-gridSize',	60);
		
		if( $params->get('markerClusterer-style',	false))
		{
			for ($i=1;$i<=5;$i++)
			{
				if($params->get('markerClusterer-style-g'.$i.'-url') != -1)
				{
					$styleOpts[($i-1)] = new stdClass();
					$styleOpts[($i-1)]->url			= JUri::root().'plugins/system/istorelocator/assets/images/clusters/'.$params->get('markerClusterer-style-g'.$i.'-url');
					$styleOpts[($i-1)]->height		= $params->get('markerClusterer-style-g'.$i.'-height');
					$styleOpts[($i-1)]->width		= $params->get('markerClusterer-style-g'.$i.'-width');
					$styleOpts[($i-1)]->textColor	= $params->get('markerClusterer-style-g'.$i.'-textColor');
					$styleOpts[($i-1)]->textSize	= (int)$params->get('markerClusterer-style-g'.$i.'-textSize');
					$styleOpts[($i-1)]->fontFamily	= $params->get('markerClusterer-style-g'.$i.'-fontFamily');
					$styleOpts[($i-1)]->anchorText	= (explode(',', str_replace(' ', '', $params->get('markerClusterer-style-g'.$i.'-anchorText','0,0'))));
				}
			}
			$options['styles'] = $styleOpts;
		}
		$json = $this->toJsJSON($options);
		//echo '<pre>';print_r($json); exit;
		return $json;
	}
	
	
	/**
	 * loadJS
	 * @desc Adds the necessary Javascript for the Google Map API v3 and the jQuery files
	 *
	 * @return s nothing
	 */
	public function getJS($inlineScript = '') {
	
		$config = JFactory::getConfig();
		$debug 	= $config->get('debug') OR $config->get('error_reporting') == 'development';
		$comParam = JComponentHelper::getParams('com_contactenhanced');
		$key = $this->params->get('gmaps_api_key', $comParam->get('gmaps_api_key'));
		$key = (empty($key) ? '' : '&amp;key='.$key );
		$script = ''; //
		//$script .= "\n".'<script src="//maps.googleapis.com/maps/api/js?v=3.exp&amp;libraries=places&amp;language='.$this->getLanguage().$key.'" type="text/javascript" ></script>';
		$script .= "\n".'<script src="//maps.googleapis.com/maps/api/js?libraries=places&amp;language='.$this->getLanguage().$key.'" type="text/javascript" ></script>';
		if($this->params->get('markerClusterer',1)){
		//	$script .= "\n".'<script src="'.JURI::base(true).'/plugins/system/istorelocator/assets/js/jquery.ui.map/markerclusterer.min.js" type="text/javascript" ></script>';
		//http://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclustererplus/docs/reference.html#MarkerClustererOptions
			//$script .= "\n".'<script src="'.JURI::base(true).'/plugins/system/istorelocator/assets/js/jquery.ui.map/markerclusterer-plus.js" type="text/javascript" ></script>';
			$script .= "\n".'<script src="'.JURI::base(true).'/plugins/system/istorelocator/assets/js/jquery.ui.map/markerclusterer_packed.js" type="text/javascript" ></script>';
		}
		$script .= "\n".'<script src="'.JURI::base(true).'/plugins/system/istorelocator/assets/js/jquery.ui.map/jquery.ui.map.full'.($debug ? '' : '.min').'.js" type="text/javascript" ></script>';
		$script .= "\n".'<script src="'.JURI::base(true).'/plugins/system/istorelocator/assets/js/jquery.ui.map/jquery.ui.map.storelocator.min.js" type="text/javascript" ></script>'."\n";
		//$script .= "\n".'<script src="'.JURI::base(true).'/plugins/system/istorelocator/assets/js/jquery.ui.map/jquery.ui.map.storelocator'.($debug ? '' : '.min').'.js" type="text/javascript" ></script>'."\n";
		$script .= "\n".'<script src="'.JURI::base(true).'/plugins/system/istorelocator/assets/js/handlebars'.($debug ? '' : '.min').'.js" type="text/javascript" ></script>'."\n";
		if(!empty($inlineScript)){
			$script .= '<script type="text/javascript">'."\n".$inlineScript."\n".'</script>'."\n";
		}
		return $script;
	}
	
	public function getLanguage() {
	
		if($this->params->get('language') == 'joomla') {
			$gmapsLanguage	=	array();
			$gmapsLanguage[]	="ar";	// ARABIC
			$gmapsLanguage[]	="bg";	// BULGARIAN
			$gmapsLanguage[]	="bn";	// BENGALI
			$gmapsLanguage[]	="ca";	// CATALAN
			$gmapsLanguage[]	="cs";	// CZECH
			$gmapsLanguage[]	="da";	// DANISH
			$gmapsLanguage[]	="de";	// GERMAN
			$gmapsLanguage[]	="el";	// GREEK
			$gmapsLanguage[]	="en";	// ENGLISH
			$gmapsLanguage[]	="en-AU";	// ENGLISH (AUSTRALIAN)
			$gmapsLanguage[]	="en-GB";	// ENGLISH (GREAT BRITAIN)
			$gmapsLanguage[]	="es";	// SPANISH
			$gmapsLanguage[]	="eu";	// BASQUE
			$gmapsLanguage[]	="eu";	// BASQUE
			$gmapsLanguage[]	="fa";	// FARSI
			$gmapsLanguage[]	="fi";	// FINNISH
			$gmapsLanguage[]	="fil";	// FILIPINO
			$gmapsLanguage[]	="fr";	// FRENCH
			$gmapsLanguage[]	="gl";	// GALICIAN
			$gmapsLanguage[]	="gu";	// GUJARATI
			$gmapsLanguage[]	="hi";	// HINDI
			$gmapsLanguage[]	="hr";	// CROATIAN
			$gmapsLanguage[]	="hu";	// HUNGARIAN
			$gmapsLanguage[]	="id";	// INDONESIAN
			$gmapsLanguage[]	="it";	// ITALIAN
			$gmapsLanguage[]	="iw";	// HEBREW
			$gmapsLanguage[]	="ja";	// JAPANESE
			$gmapsLanguage[]	="kn";	// KANNADA
			$gmapsLanguage[]	="ko";	// KOREAN
			$gmapsLanguage[]	="lt";	// LITHUANIAN
			$gmapsLanguage[]	="lv";	// LATVIAN
			$gmapsLanguage[]	="ml";	// MALAYALAM
			$gmapsLanguage[]	="mr";	// MARATHI
			$gmapsLanguage[]	="nl";	// DUTCH
			$gmapsLanguage[]	="no";	// NORWEGIAN
			$gmapsLanguage[]	="pl";	// POLISH
			$gmapsLanguage[]	="pt";	// PORTUGUESE
			$gmapsLanguage[]	="pt-BR";	// PORTUGUESE (BRAZIL)
			$gmapsLanguage[]	="pt-PT";	// PORTUGUESE (PORTUGAL)
			$gmapsLanguage[]	="ro";	// ROMANIAN
			$gmapsLanguage[]	="ru";	// RUSSIAN
			$gmapsLanguage[]	="sk";	// SLOVAK
			$gmapsLanguage[]	="sl";	// SLOVENIAN
			$gmapsLanguage[]	="sr";	// SERBIAN
			$gmapsLanguage[]	="sv";	// SWEDISH
			$gmapsLanguage[]	="ta";	// TAMIL
			$gmapsLanguage[]	="te";	// TELUGU
			$gmapsLanguage[]	="th";	// THAI
			$gmapsLanguage[]	="tl";	// TAGALOG
			$gmapsLanguage[]	="tr";	// TURKISH
			$gmapsLanguage[]	="uk";	// UKRAINIAN
			$gmapsLanguage[]	="vi";	// VIETNAMESE
			$gmapsLanguage[]	="zh-CN";	// CHINESE (SIMPLIFIED)
			$gmapsLanguage[]	="zh-TW";	// CHINESE (TRADITIONAL)
	
			$lang 	=	JFactory::getLanguage();
			$tag	=	$lang->getTag();
			if(in_array($tag,$gmapsLanguage)){
				return $tag;
			}elseif(in_array(substr($tag,0,2),$gmapsLanguage)){
				return substr($tag,0,2);
			}
		}elseif($this->params->get('language')  == 'auto') {
			return '';
		}
		return $this->params->get('language') ;
	}
	
	public function getCEContacts()
	{
		// Create a new query object.
		$db		= JFactory::getDbo();
		$app 	= JFactory::getApplication();
		$input 	= $app->input;
//		echo '<pre>'; print_r($input->get('data')); exit;
		// http://www.plumislandmedia.net/mysql/haversine-mysql-nearest-loc/
		$distance_unit	= ($this->params->get('unitSystem','METRIC') == 'METRIC' ? 111.045 : 69.0); //Km in miles = 69.0
		$listsWithin 	= $input->getInt('maxdistance') ? $input->getInt('maxdistance') : $this->params->get('maxDistanceDefault', 100); // will use the unit above km OR miles
		

		$latpoint 		= $input->getFloat('lat', $this->params->get('default-latitude'));
		$longpoint 		= $input->getFloat('lng', $this->params->get('default-longitude'));
		$limit 			= $input->getInt('resultLimit', $this->params->get('resultLimit',500));
		$catid			= $input->getString('category', $this->params->get('category'));
		$showTags		= ($this->params->get('show_filter_groups') ? $this->params->get('filter_groups-source','metakey') : 'metakey');
		
		switch ($showTags) {
			case 'city':
				$showTags = 'suburb';
			break;
		}
		
		if($catid)
		{
			if (strpos($catid, ','))
			{
				$catid = 'AND z.catid IN ('.$catid.')';
			}
			else
			{
				$catid = 'AND z.catid = '.$catid;
			}
		}
		else
		{
			$catid = '';
		}
		
		if ($latpoint == 0 AND $longpoint == 0){
			$latpoint 		= ($this->params->get('default-latitude'));
			$longpoint 		= ($this->params->get('default-longitude'));
		}
		
		$languageWhere = '';
		if (JLanguageMultilang::isEnabled())
		{
			$languageWhere = ' AND (z.language=' . $db->quote(JFactory::getLanguage()->getTag()) . ' OR z.language=' . $db->quote('*').')';
		}
		
		$filterWhere = '';
		if($input->getString('filter_city'))
		{
			$filterWhere .= ' AND suburb = '. $db->quote($input->getString('filter_city')	);
		}
		if($input->getString('filter_state'))
		{
			$filterWhere .= ' AND state = '. $db->quote($input->getString('filter_state')	);
		}
		if($input->getString('filter_country'))
		{
			$filterWhere .= ' AND country = '. $db->quote($input->getString('filter_country')	);
		}
		$filterTags = '';
		if($input->getString('filter_tags'))
		{
			
			$filterTags = " AND z.id IN ( SELECT ci.content_item_id 
					FROM #__contentitem_tag_map AS ci
					LEFT JOIN #__tags AS tags ON ci.tag_id = tags.id 
					WHERE tags.title IN ('". implode("','", explode(',', $input->getString('filter_tags')))."') )";
		}	
		$select1 = "id, name, image, params, alias, slug,
	        	address, city, state, country, postcode, phone, mobile, catid, 
	        	birthdate, department,misc, con_position, skype, twitter, facebook, linkedin, google_plus, webpage, 
	       		lat, lng, distance, metakey"
				;
		//
		$select2 = "z.id, z.name, z.image, z.params, z.alias, 
				        z.address, z.suburb as city, z.state, z.country, z.postcode, z.telephone AS phone, z.mobile, z.catid,
				        z.birthdate, z.department,z.misc, z.con_position, z.skype, z.twitter, z.facebook, z.linkedin, z.google_plus, z.webpage, 
				        z.lat, z.lng, z.metakey,
						".($showTags == 'category' ? 'c.title as tags,' : ($showTags == 'tags' ? '' : 'z.'.$showTags.' as tags,'))." 
				        CASE WHEN CHAR_LENGTH(z.alias) THEN CONCAT_WS(':', z.id, z.alias) ELSE z.id END as slug";
		if($latpoint AND $longpoint){
			$query = "SELECT {$select1}"
			.
			//($showTags == 'tags' ? ",(SELECT GROUP_CONCAT(ts.title) FROM #__contentitem_tag_map AS ci LEFT JOIN #__tags AS ts ON ts.id = ci.tag_id WHERE ci.content_item_id = id) AS tags" : ', tags').
				 " FROM (
				 SELECT $select2,
				        p.radius,
				        p.distance_unit
				                 * DEGREES(ACOS(COS(RADIANS(p.latpoint))
				                 * COS(RADIANS(z.lat))
				                 * COS(RADIANS(p.longpoint - z.lng))
				                 + SIN(RADIANS(p.latpoint))
				                 * SIN(RADIANS(z.lat)))) AS distance
				  FROM #__ce_details AS z
				  JOIN (   /* these are the query parameters */
				        SELECT  {$latpoint} AS latpoint,   {$longpoint} AS longpoint,
				                {$listsWithin} AS radius,      {$distance_unit} AS distance_unit
				    ) AS p ON 1=1
				  ".($showTags == 'category' ? 'LEFT JOIN #__categories as c ON z.catid = c.id' : '')."
				  
				  WHERE z.published = 1  AND (z.lat <> 0 OR z.lng <> 0) 
				  	{$catid}
				  	AND z.lat
				     BETWEEN p.latpoint  - (p.radius / p.distance_unit)
				         AND p.latpoint  + (p.radius / p.distance_unit)
				    AND z.lng
				     BETWEEN p.longpoint - (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
				         AND p.longpoint + (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
				    {$languageWhere}
				    {$filterWhere}
				    {$filterTags}
				 ) AS d
				 WHERE distance <= radius
				 ORDER BY distance";
		}else{
			$query = "SELECT $select2
			FROM #__ce_details AS z
			WHERE z.published = 1  AND (z.lat <> 0 OR z.lng <> 0)
			{$catid}";
		}
		$db->setQuery($query, 0, $this->params->get('limit', $limit));
		
		$contacts = $db->loadObjectList();
		foreach ($contacts as &$contact)
		{
			$contact->misc = $this->replaceRelativeLinks($contact->misc);
			// add tags 
			if($showTags == 'tags')
			{
				$query = "SELECT GROUP_CONCAT(ts.title) 
					FROM #__contentitem_tag_map AS ci 
					LEFT JOIN #__tags AS ts ON ts.id = ci.tag_id 
					WHERE ci.content_item_id = ".$contact->id
					." AND published = 1 "
				//	." ORDER BY ts.lft ASC"
						;
				$db->setQuery($query);
				$tags	= $db->loadResult();
				if(!empty($tags))
				{
					$contact->tags = $tags;
				}
			}
		}
		
		return $contacts;
	}

	public function getCETotalContacts()
	{
		// Create a new query object.
		$db		= JFactory::getDbo();
		$query = "SELECT count(z.id)
		FROM #__ce_details AS z
		WHERE z.published = 1  AND (z.lat <> 0 OR z.lng <> 0)";
	
		$db->setQuery($query);
		return $db->loadResult();
	}
	

	public function getLocationFilePath($type='')
	{
		$file = JFactory::getApplication()->input->getString('file',$this->params->get($type));
	
		if(is_readable(JPATH_PLUGINS.'/system/istorelocator/data/'.$file)){
			$file = JPATH_PLUGINS.'/system/istorelocator/data/'.$file;
		}elseif(is_readable(JPATH_ROOT.'/'.$file)){
			$file = JPATH_ROOT.'/'.$file;
		}else{
			return false;
		}
		return $file;
	}
	
	public function updateLatLng()
	{
		$input = JFactory::getApplication()->input;
		
		// if we are editing the latLng then set this flag
		if($input->getString('action') == 'updateLatLng' && JFactory::getUser()->authorise('core.edit'))
		{
			switch ($input->getCmd('source')) {
				case 'xml':
					$response = $this->getXMLMarkers();
					$response = $this->saveXMLFile($response->items);
					return array('response' => $response, 'log' => $this->updatelatLng_log);
					break;
						
				case 'csv':
					$response = $this->getCSVMarkers();
					$response = $this->saveCSVFile($response->items);
					return array('response' => $response, 'log' => $this->updatelatLng_log);
					break;
				case 'com_contactenhanced':
					
					$response = $this->updateCEContactsCoordinates();
					return array('response' => $response, 'log' => $this->updatelatLng_log);
					break;
			}
		}
		else
		{
			return array('response' => false, 'log' => JText::_('JERROR_ALERTNOAUTHOR'));
		}
	}
	
	public function saveXMLFile($items)
	{
		$markers = new SimpleXMLElement('<markers/>');
		foreach ($items as $item)
		{
			$marker	= $markers->addChild('marker', (!empty($item->misc) ? $item->misc : '') );
			$item	= (array)$item;
			foreach ($item as $key => $value)
			{
				if($key != 'misc')
				{
					$marker->addAttribute($key, $value);
				}
				
			}
		}
		$filename	= $this->getLocationFilePath('xmlFile');
		$fileContent= $markers->asXML();
		return $this->saveFile($filename,$fileContent,2);
	}
	
	public function saveCSVFile($items)
	{
		if(!class_exists('csvHandler')){
			require_once(JPATH_PLUGINS.'/system/istorelocator/helpers/csvhandler.php');
		}
		$csv	= new csvHandler();
		$filename	= $this->getLocationFilePath('csvFile');
		if(!empty($items) && count($items) > 1 && $filename)
		{
			$headers =  get_object_vars($items[0]);
			$csv->addHeaderLine($headers);
			
			foreach ($items as $item)
			{
				$csv->addRow((array)$item);
			}
			$fileContent = $csv->render(false);
			return $this->saveFile($filename,$fileContent,2);
		}
	}
	
	/**
	 *
	 * @param string $filename
	 * @param number $override: 0 = doesn't override; 1= overrides; 2= overrides, but creates a backup copy first
	 */
	public function saveFile($filename,$fileContent,$override=2)
	{
		if(file_exists($filename) && $override == 0)
		{
			return false;
		}
		elseif(file_exists($filename) && $override == 2) //  overrides, but creates a backup copy first
		{
			jimport('joomla.filesystem.file');
			$backupFilename = str_replace('.'.JFile::getExt($filename), '-backup-'.date('Y-m-d-H-i-s').'.'.JFile::getExt($filename), $filename);
			JFile::move($filename, $backupFilename);
		}
		return JFile::write($filename, $fileContent);
	}
	
	public function geocode(&$item, &$geoCodeCount, &$executionTime, $line)
	{
		
		$address = (!empty($item->address) ? $item->address.' ' : '')
			.(!empty($item->city)	? $item->city.' ' : '')
			.(!empty($item->suburb)	? $item->suburb.' ' : '')
			.(!empty($item->state)	? $item->state.' ' : '')
			.(!empty($item->postcode) ? $item->postcode.' ' : '')
			.(!empty($item->country)? $item->country.' ' : '');
		
		if(empty($item->address) && empty($item->postcode) )
		{
			$this->updatelatLng_log .= "\nOn line {$line} 'address' and 'postcode' variables are missing. Google will provide the Latitude and Longitude for the city instead of the address";
		}
		
		if(!empty($address) && $geoCodeCount <= 2500)
		{
			++$geoCodeCount;
			if($geoCodeCount >= 2500)
			{
				$this->updatelatLng_log .= "\nThe daily limit by IP of 2500 was reached on line {$line}. Try again in 24 hours OR try from another IP address";
			}
			else
			{
				if ($geoCodeCount%5 == 0)
				{
					if($executionTime > 1)
					{
						$executionTime = 0;
					}
					else
					{
						sleep(1); // sleep for 1 second if the 5 requests per second limitation is reached
					}
				}
					
				// Increase timeout in X seconds
				set_time_limit(1);
					
				$url = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($address);
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_HEADER,0);
				curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				$data = curl_exec($ch);
				$data = json_decode($data, true);
					
				
				if($data['status'] == 'OVER_QUERY_LIMIT')
				{
					$geoCodeCount = 2500;
					$this->updatelatLng_log .= "\n\t Goolge's Error Message: ".$data['error_message'];
				}
				elseif($data['status'] == 'ZERO_RESULTS' || empty($data['results'][0]['geometry']['location']['lat']) || empty($data['results'][0]['geometry']['location']['lng']))
				{
					$this->updatelatLng_log .= "\nGoogle Failed to accuratly geocode the address ({$address}) on line {$line}";
				}
				else
				{
					$item->lat = $data['results'][0]['geometry']['location']['lat'];
					$item->lng = $data['results'][0]['geometry']['location']['lng'];
				}
			} // END ELSE
		}
	}
	

	public function getXMLMarkers(){
		$app 	= JFactory::getApplication();
		$input 	= $app->input;
		$xmlFile = $this->getLocationFilePath('xmlFile');
		
		if(!$xmlFile)
		{
			return false;
		}
		
		// if we are editing the latLng then set this flag
		$editLatLng = ($input->getString('action') == 'updateLatLng' && JFactory::getUser()->authorise('core.edit'));
	
		if ($editLatLng)
		{
			$time_start_overall = $this->microtime_float();
			$this->updatelatLng_log .= "\nStarted Geocoding file ".str_replace(JPATH_ROOT, '', $xmlFile);
		}
		
		$items 	= array();
		$xmlFile= file_get_contents($xmlFile);
		$xml 	= new SimpleXMLElement($xmlFile);
		
		$i=1;
		// Google has a limitation of 5 requests
		$geoCodeCount=0;
		$executionTime=0;
		foreach ($xml as $placemark)
		{
			$time_start = $this->microtime_float();
			
			$item 		= new stdClass();
			$item->id 	= $i++;
			foreach($placemark->attributes() as $key => $value)
			{
				$item->$key = (string)$value;
			}
			
			$item->misc = (string) $placemark;
			// Filter Group tags
			$tagSource = $this->params->get('filter_groups-source','metakey');
			if($this->params->get('show_filter_groups') && !empty($item->$tagSource))
			{
				$item->tags = $item->$tagSource;
			}
			
			if ($editLatLng)
			{
				$time_start = $this->microtime_float();
				if ((empty($item->lat) || empty($item->lng) ) )
				{
					$this->geocode($item,$geoCodeCount,$executionTime, $i);
				}
				$time_end = $this->microtime_float();
				$time = $time_end - $time_start;
				$executionTime +=$time;
			}
			
			if($this->matchSearchFilters($item))
			{
				$items[] = $item;
			}
		}
		
		if ($editLatLng)
		{
			$time_end = $this->microtime_float();
			$time = $time_end - $time_start_overall;
			
			$this->updatelatLng_log .= "\nGeocoded {$geoCodeCount} locations.";
			$this->updatelatLng_log .= "\nFINISHED in ".(round($time,2))." seconds.";
		}
		
		$response = new stdClass();
		$response->latLng	= false;
		$this->locations	= &$items;
		$response->items	= &$items;
	
		return $response;
	}
	
	public function updateCEContactsCoordinates(){
		$app 	= JFactory::getApplication();
		$input 	= $app->input;
		$db		= JFactory::getDbo();
		
		// if we are editing the latLng then set this flag
		$editLatLng = ($input->getString('action') == 'updateLatLng' && JFactory::getUser()->authorise('core.edit'));
		
		$query	= $db->getQuery(true)->select('*')->from('#__ce_details')
					->where("(lat = 0 OR lat = '')")
					->where("(lng = 0 OR lng = '')")
					;
		$db->setQuery($query,0,1000);
		$items 	= $db->loadObjectList();
		
		$i=1;
		$geoCodeCount=0;
		$executionTime=0;
		
		if ($editLatLng && is_readable(JPATH_ADMINISTRATOR.'/components/com_contactenhanced/contactenhanced.xml'))
		{
			$time_start_overall = $this->microtime_float();
			$this->updatelatLng_log .= "\nStarted Geocoding Contact Enhanced Contacts";
			
			foreach ($items as $item) {
				$time_start = $this->microtime_float();
				$this->geocode($item,$geoCodeCount,$executionTime, $i);
				
				if ((!empty($item->lat) && !empty($item->lng) ) )
				{
					$query	= $db->getQuery(true)->update('#__ce_details')
					->set(("lat = ".$db->q($item->lat)))
					->set(("lng = ".$db->q($item->lng)))
					->where('id = '.$db->q($item->id))
					;
					$db->setQuery($query);
					$db->execute();
				}
				$time_end = $this->microtime_float();
				$time = $time_end - $time_start;
				$executionTime +=$time;
			}
			$time_end = $this->microtime_float();
			$time = $time_end - $time_start_overall;
				
			$this->updatelatLng_log .= "\nGeocoded {$geoCodeCount} locations.";
			$this->updatelatLng_log .= "\nFINISHED in ".(round($time,2))." seconds.";
		}
		
		

		$response = new stdClass();
		$response->latLng	= false;
		$this->locations	= &$items;
		$response->items	= &$items;
		return $response;
		
		
	}
	public function getCSVMarkers(){
		$app 	= JFactory::getApplication();
		$input 	= $app->input;
		
		$csvFile = $this->getLocationFilePath('csvFile');
		
		if(!$csvFile)
		{
			return false;
		}
		
		// if we are editing the latLng then set this flag
		$editLatLng = ($input->getString('action') == 'updateLatLng' && JFactory::getUser()->authorise('core.edit'));
		
		$items 	= array();

		if(!class_exists('csvHandler')){
			require_once(JPATH_PLUGINS.'/system/istorelocator/helpers/csvhandler.php');
		}
		$csv	= new csvHandler();
		$csv->readFile($csvFile);
		
		$i=1;
		$geoCodeCount=0;
		$executionTime=0;
		
		if ($editLatLng)
		{
			$time_start_overall = $this->microtime_float();
			$this->updatelatLng_log .= "\nStarted Geocoding file ".str_replace(JPATH_ROOT, '', $csvFile);
		}
		foreach ($csv->itemList as $marker) {
			
			$item 		= new stdClass();
			$i++;
			if(empty($item->id)){
				$item->id 	= $i;
			}
			
			foreach ($marker as $key => $value)
			{
				if(!empty($key))
				{
					$item->$key = (string)$value;
				}
			}
			
			if ($editLatLng)
			{
				$time_start = $this->microtime_float();
				if ((empty($item->lat) || empty($item->lng) ) )
				{
					$this->geocode($item,$geoCodeCount,$executionTime, $i);
				}
				$time_end = $this->microtime_float();
				$time = $time_end - $time_start;
				$executionTime +=$time;
			}
			else
			{

				// Filter Group tags
				$tagSource = $this->params->get('filter_groups-source','metakey');
				if($this->params->get('show_filter_groups') && !empty($item->$tagSource)){
					$item->tags = $item->$tagSource;
				}
				
				
			}
			
			
			if($this->matchSearchFilters($item))
			{
				$items[] = $item;
			}
		}
		
		if ($editLatLng)
		{
			$time_end = $this->microtime_float();
			$time = $time_end - $time_start_overall;
			
			$this->updatelatLng_log .= "\nGeocoded {$geoCodeCount} locations.";
			$this->updatelatLng_log .= "\nFINISHED in ".(round($time,2))." seconds.";
		}

		$response = new stdClass();
		$response->latLng	= false;
		$this->locations	= &$items;
		$response->items	= &$items;
	//	self::print_r($response);
		return $response;
	}
	
	protected function matchSearchFilters(&$item)
	{
		$app 	= JFactory::getApplication();
		$input 	= $app->input;
		
		
		if($input->getString('filter_city')
			|| $input->getString('filter_state')
			|| $input->getString('filter_country')
		){
			
			if($input->getString('filter_city')		&& (empty($item->city)	|| strtolower($item->city) != strtolower($input->getString('filter_city'))) )
			{
				return false;
			}
			
			if($input->getString('filter_state')	&& (empty($item->state)	|| strtolower($item->state) != strtolower($input->getString('filter_state'))) )
			{
				return false;
			}
			
			if($input->getString('filter_country')	&& (empty($item->country)	|| strtolower($item->country) != strtolower($input->getString('filter_country'))) )
			{
				return false;
			}
		}
		return true;
	}
	
	public function getKMLPlacemarkers(){
		$app 	= JFactory::getApplication();
		$input 	= $app->input;

		$kmlFile = $this->getLocationFilePath('kmlFile');
		
		if(!$kmlFile)
		{
			return false;
		}
		
		$items 	= array();
		$kmlFile= file_get_contents($kmlFile);
		$kml 	= new SimpleXMLElement($kmlFile);


		if(!empty($kml->Document->Placemark)){
			$placemarks = &$kml->Document->Placemark;
		}elseif(!empty($kml->Folder->Placemark)){
			$placemarks = &$kml->Folder->Placemark;
		}else{
			$placemarks = array();
		}
		$i=1;
		foreach ($placemarks as $placemark) {
			
			$item 		= new stdClass();
			$item->id 	= $i++;
			$item->name = (string)$placemark->name;
			list($item->lng, $item->lat) = explode(',', (string)$placemark->Point->coordinates);
			$item->misc = (string) $placemark->description;
			
			// Filter Group tags
			$tagSource = $this->params->get('filter_groups-source','metakey');
			if($this->params->get('show_filter_groups') && !empty($item->$tagSource)){
				$item->tags = $item->$tagSource;
			}
			if($this->matchSearchFilters($item))
			{
				$items[] = $item;
			}
		}
		$this->locations	= &$items;
		return $items;
	}
	
	
	
	public function onAjaxIstorelocator()
	{
		require_once (dirname(__FILE__).'/helpers/image.php');
		if(!class_exists('Mobile_Detect')){
			if (is_readable(JPATH_ROOT.'/components/com_contactenhanced/helpers/Mobile_Detect.php')){
				require_once(JPATH_ROOT.'/components/com_contactenhanced/helpers/Mobile_Detect.php');
			}else{
				require_once(JPATH_PLUGINS.'/system/istorelocator/helpers/Mobile_Detect.php');
			}
		}
		$isMobile = new Mobile_Detect();
		$isMobile = $isMobile->isMobile();
		
		$app 	= JFactory::getApplication();
		$input 	= $app->input;
		
		
		$source	= $input->getCmd('source','com_contactenhanced');
		$limit 	= $input->getInt('maxdistance');
		if($limit > 500){
			$limit = 500;
		}
		$list 	= array();

		if($input->getCmd('action') == 'updateLatLng')
		{
			return $this->updateLatLng();
		}
		else
		{
			$lat 	= $input->getFloat('lat');
			$lng 	= $input->getFloat('lng');
			$catid 	= $input->getInt('category');
			
			switch ($source) {
				case 'sql':
					$comParams = $this->params;
					break;
				case 'json':
					$comParams = $this->params;
					break;
			
				case 'kml':
					$items = $this->getKMLPlacemarkers();
					return array('list' => '', 'locations' =>$items, 			'listTotal' => count($items), 			'total' => 0);
					break;
			
				case 'xml':
					$response = $this->getXMLMarkers();
					return array('list' => '', 'locations' =>$response->items,	'listTotal' => count($response->items), 'total' => 0,	'latLng' =>$response->latLng);
					break;
			
				case 'csv':
					$response = $this->getCSVMarkers();
					//self::print_r($response->items);
					return array('list' => '', 'locations' =>$response->items,	'listTotal' => count($response->items), 'total' => 0,	'latLng' =>$response->latLng);
					break;
			
				case 'com_contactenhanced':
				default:
					$items = $this->getCEContacts();
					$total = $this->getCETotalContacts();
					// component params
					$comParams 	= JComponentHelper::getParams('com_contactenhanced');
					$comParams->merge($this->params);
					break;
			}
			
			$count = 0;
			$list	= array();
			foreach($items as &$item){
				// resize image;
					
					
				//$item->image = $item->image;
					
				require_once (JPATH_ROOT.'/components/com_contactenhanced/helpers/route.php');
				$item->link = JRoute::_(ContactenchancedHelperRoute::getContactRoute($item->slug, $item->catid));
				// Copies the component parameters and add the item param if any
				$params 	= $comParams;
				// $registry will hold the params for the current record only
				$registry	= new JRegistry();
				if(!empty($item->params)){
					$registry->loadString($item->params);
					$params->merge($registry);
					// unset so it is not included in the locations array
					unset($item->params);
				}
					
				$marker	= new stdClass();
				$marker->id = $item->id;
				$marker->lat = $item->lat;
				$marker->lng = $item->lng;
				$marker->lat = $item->lat;
				$tagSource = $this->params->get('filter_groups-source','metakey');
				if($this->params->get('show_filter_groups') && !empty($item->$tagSource)){
					$marker->tags = $item->$tagSource;
				}
					
				// If $item  don't have icon
				if (empty($item->icon)){
					$item->icon	= $registry->get('gmaps_icon') ? JUri::root().''.$registry->get('gmaps_icon') : null;
				}
				if($item->icon){
					if(substr($item->icon, 0,4) != 'http')
					{
						$item->icon = JUri::root().''.$item->icon;
					}
					$marker->icon = $item->icon;
				}else{
					$letter = $this->num2alpha($count++);
					if(strlen($letter) < 3){
						$fontsize = 14;
					}elseif(strlen($letter) < 4){
						$fontsize = 10;
					}else{
						$fontsize = 9;
					}
					$marker->icon 	= 'https://mt.googleapis.com/vt/icon/name=icons/spotlight/spotlight-waypoint-b.png&text='.$letter.'&psize='.$fontsize.'&font=fonts/Roboto-Regular.ttf&color=ff333333&ax=44&ay=48';
					$item->icon 	= $marker->icon;
				}
					
				$marker = json_encode($marker);

				$listItem = array();
				
				$tmpl_file = 'listtemplate_com_contactenhanced.php';
				$path = JPluginHelper::getLayoutPath('system', 'istorelocator');
				// Render the LoadMap
				
				include $path;
				
				$list[] = implode("\n", $listItem);
					
			}
			$list = implode("\n", $list);
			
			return array('list' => $list, 'locations' =>$items, 'listTotal' => count($items), 'total' => 0);
		}// END else
		
	}
	

	public function num2alpha($n)
	{
		for($r = ""; $n >= 0; $n = intval($n / 26) - 1)
			$r = chr($n%26 + 0x41) . $r;
		return $r;
	}
	
	/**
	 * print_r()
	 * Does a var_export of the array and returns it between <pre> tags
	 *
	 * @param mixed $var any input you can think of
	 * @return string HTML
	 */
	public static function print_r($var, $exit= true)
	{
		$input =var_export($var,true);
		$input = preg_replace("! => \n\W+ array \(!Uims", " => Array ( ", $input);
		$input = preg_replace("!array \(\W+\),!Uims", "Array ( ),", $input);
	
		$input = ("<pre>".str_replace('><?', '>', highlight_string('<'.'?'.$input, true))."</pre>");
		if($exit){
			echo $input; exit;
		}else{
			return $input;
		}
	}
	
	function microtime_float()
	{
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}
	
	protected function replaceRelativeLinks($html)
	{
		/* $DOM = new DOMDocument;
		 $DOM->loadHTML(); */
		$useLibXMLErrors = libxml_use_internal_errors(true);
		/* Make sure to set encoding to UTF-8 */
		$DOM = new DOMDocument('1.0',  'utf-8');
		$DOM->loadHTML('<?xml version=1.0" encoding="utf-8"?>' . $html);
		
		/* Removes <?xml version=1.0" encoding="utf-8"?> from the text */
		for ($n = 0; $n < $DOM->childNodes->length; $n++) {
			if ($DOM->childNodes->item($n)->nodeType === XML_PI_NODE) {
				$DOM->removeChild($DOM->childNodes->item($n));
				break;
			}
		}
		$imgs = $DOM->getElementsByTagName('img');
		foreach($imgs as $img){
			$src = $img->getAttribute('src');
			if(strpos($src, JUri::root()) === FALSE AND strpos($src, 'http') === FALSE){
				$src = JUri::root().$src;
			}
			$img->setAttribute('src', $src);
		}
		$links = $DOM->getElementsByTagName('a');
		foreach($links as $link){
			$href = $link->getAttribute('href');
			if(strpos($href, JUri::root()) === FALSE AND strpos($href, 'http') === FALSE){
				$link->setAttribute('href', JUri::root().$href);
			}
		}
		// remove <!DOCTYPE
		$DOM->removeChild($DOM->doctype);
		$DOM->encoding = 'UTF-8';
		$html = $DOM->saveHTML();
		libxml_use_internal_errors($useLibXMLErrors);
		return $html;
	}

	/**
	 * Determine if the request was over SSL (HTTPS).
	 * @return bool
	 */
	public static function httpIsSecure() {
		if (isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) && (strtolower($_SERVER['HTTPS']) != 'off')) {
			return true;
		}
		else{
			return false;
		}
	}
	function get_browser_name($user_agent=null)
	{
		if(empty($user_agent))
		{
			$user_agent	= $_SERVER['HTTP_USER_AGENT'];
		}
		if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) return 'Opera';
		elseif (strpos($user_agent, 'Edge')) return 'Edge';
		elseif (strpos($user_agent, 'Chrome')) return 'Chrome';
		elseif (strpos($user_agent, 'Safari')) return 'Safari';
		elseif (strpos($user_agent, 'Firefox')) return 'Firefox';
		elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) return 'Internet Explorer';
	
		return 'Other';
	}
}