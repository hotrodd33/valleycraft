 /*!
	* Added by Douglas <support@idealextensions.com>
	* Using 'as the crow flies' to avoid reaching Maximum of 25 origins or 25 destinations per request and
	* at most 100 elements (origins times destinations) per request, see:
	* https://developers.google.com/maps/documentation/javascript/distancematrix#usage_limits_and_requirements
	* 
 * Depends:
 *			jquery.ui.map.js http://code.google.com/p/jquery-ui-map/
 *			Handlebarsjs
 */
( function($) {

	/*
	 * Non related to the storelocator, however we wanbt to avoid one more file.
	 * Auto select the contents of the Search Field in order to increase usability specially in Mobile devices
	 */
	$(".isl-location-search").focus(function() {
	    var $this = $(this);
	    $this.select();

	    // Work around Chrome's little problem
	    $this.mouseup(function() {
	        // Prevent further mouseup intervention
	        $this.unbind("mouseup");
	        return false;
	    });
	});
	
	$.extend($.ui.gmap.prototype, {
		options: {
			linkToGoogleMaps:1,
			mapTypeId: 		'roadmap',
			zoom: 			5,
			origin:			{},
			firstRun: 		true,
			maxdistance:	null,
			radius:			6367.0, // mi = 3956.0 , metric/km: 6367.0
			lengthUnit:		'Km',
			listMarkIDSelector: 'li[data-markerid]',
			listContainerSelector: 	'.isl-list-container',
			homeTitle:		'',
			listTemplate:	'',
			autocomplete:	true,
			geocoder:		null,
			circle:			{
								enabled:	true,
								radius:		0,
								center:		null,
								fillColor:	'#4285f4',
								fillOpacity:0.3,
								radius:		0,
								strokeColor:'#4285f4',
								strokeOpacity:	0.8,
								strokeWeight:	3,
								showMarkersOutOfRadius: true
							},
			filterTags:		{
								operator:	'OR',
								element:	'isl-filter_tags',
								tags:		[],
								position:	google.maps.ControlPosition.RIGHT_TOP,
								source:		'metakey',
								enabled:	true,
								addedToMap:	false
							},
			markerClusterer:{
								maxZoom:	14,
								gridSize:	50,
								styles:		[]
							},
			language:		{
								searching:	'Searching for locations near you...',
								results:	'Showing the closest {results} locations in range.',
								noresult:	'No result found. Please broaden your search.',
								previous:	'Previous',
								geoLocationFailed:	'Geo Location failed. With Error: {1}',
								openInGoogleMaps:	'Open in Google Maps'
							}
		},
		/**
		 * Gets the current position
		 * @param callback:function(position, status)
		 * @param geoPositionOptions:object, see https://developer.mozilla.org/en/XPCOM_Interface_Reference/nsIDOMGeoPositionOptions
		 */
		getData: function(options, callbackBeforeSend) {
			var self = this;
			self.closeInfoWindow();
			if(callbackBeforeSend){
				callbackBeforeSend(self, options);
			}
			console.log(this.el);
			$('#isl-search-result').html('');
		//	console.log('Before: options.maxdistance: '+options.maxdistance);
			if(self.options.firstRun && self.options.first_load_maxdistance)
			{
				this.options.maxdistance= self.options.first_load_maxdistance;
			}
			else
			{
			this.options.maxdistance= $('#maxdistance').val();
			}
			
			if(typeof(options.maxdistance) == 'undefined' || options.maxdistance == 0)
			{
				options.maxdistance		= this.options.maxdistance;
			}
			options.maxdistance		= this.options.maxdistance;
			
			if(self.options.firstRun && self.options.first_load_limit)
			{
				options.limit = 123456;
			}
			else
			{
				if(typeof(options.limit) == 'undefined' || options.limit == 0)
				{
					options.limit	= self.options.resultLimit;
				}
			}
			
			options.source	= this.options.sourceType;
			options.file	= this.options.sourceFile;
			options.category= this.options.category;
			//options.editLatLng	= this.options.editLatLng;
			
			// already loaded source
			if(self.get('locations') &&  (self.options.sourceType == 'kml' || self.options.sourceType == 'xml' || self.options.sourceType == 'csv' || self.options.sourceType == 'json') ){
				var data = {};
				data.locations = self.get('locations');
				self.setupLocations(data,options.lat, options.lng);
			}else{
				var searching = $('<div class="isl-loading-search"></div>').html('<h3>'+self.options.language.searching+'</h3>');
				searching.append('<div class="progress-bar progress-bar-striped progress progress-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"><div class="bar" style="width: 100%;"></div></div>');
				searching.prependTo('.isl-list-container');
				
				// AJAX request
				var request = $.post( 'index.php?option=com_ajax&plugin=istorelocator&tmpl=component&format=json&'+decodeURIComponent($.param(options)));
				request.always(function( result ) {
					if(result.success == true){					
						self.setupLocations(result.data[0],options.lat, options.lng);
					}
					$('.isl-loading-search').remove();
				});
				return request;
			}
			$('.isl-loading-search').remove();
		},
		addHomeMarker: function(){
			var self= this;
			var lat	= this.options.origin.lat;
			var lng	= this.options.origin.lng;
			// Add Home Icon
			if(typeof lat !== 'undefined' && typeof lng !== 'undefined' && (lat !=0 || lng != 0)){ // Geolocation or search worked
				var clientPosition = new google.maps.LatLng(lat,lng);
				self.addMarker({'id':'0' // Home marker is 0
					, 'position': clientPosition
					, 'bounds':	true
					, 'icon': 	self.options.home_icon
					, 'title':	self.options.homeTitle
				});
				self.addCircle(clientPosition, 'homeMarkerCircle');
				
			}
		},
		addCircle:	function(center, id){
			if(this.options.circle.enabled && this.options.maxdistance < 5000){
				var circle = this.get('overlays > Circle['+id+']');
				if(circle){
					circle.setMap(null);
				}				
				this.options.circle.center = center;
				this.options.circle.radius = (this.options.maxdistance / this.options.radius) * 6378100; // 6378100 in meters
				var circle = new google.maps.Circle(jQuery.extend({'map': this.get('map')}, this.options.circle));
				this.set('overlays > Circle['+id+']',circle);
				this.get('map').fitBounds(circle.getBounds());
			}
		},
		deleteMarkers: function(){
			var self = this;
			var mCluster = self.get('MarkerClusterer');
			if(typeof mCluster !== 'undefined' && mCluster !== null){
				mCluster.setMap(null);
			}
			this.clear('markers');
			this.deleteFilterTags();
			this.set('bounds',new google.maps.LatLngBounds());
			$(this.options.listContainerSelector+' ul').html('').css('display', '');
			$('#isl-search-result').html('').css('display', '');
			self.removeDirections(); // If any
		},

		/**
		 * 
		 * */
		addMarkersGmapping: function() {
			var self= this;
			
			var locationMarkers = [];
			this.deleteFilterTags();
			this.clearMarkerClusterer();

			
			$(self.options.listContainerSelector+' '+self.options.listMarkIDSelector).each(function(i,el) {
				var data = $(el).data('gmapping');
				
				if(typeof data !== 'object'){
					//data = $.parseJSON(data);
				}
				
				if(data.tags)
				{
					data.tags = $.map(data.tags.split(","), $.trim);
					self.addFilterTag(data.tags);
				}else
				{
					data.tags = [];
				}
				
				if(data.lat && data.lng)
				{
					self.addMarker({'id': data.id, 'listID': $(el).attr('id'),'icon': data.icon, 'tags':data.tags, 'position': new google.maps.LatLng(data.lat, data.lng), 'bounds':true}, //, 'bounds':true //'bounds':(self.options.firstRun ? false : true) 
						//callback
						function(map,marker) {
						locationMarkers.push(marker);
						$('#'+$(el).attr('id')+' .list-content').click(function() {
							marker.setAnimation(google.maps.Animation.BOUNCE);
							map.panTo(marker.getPosition());
							$(marker).triggerEvent('click');
							self.option('zoom',17);
							setTimeout(function(){	marker.setAnimation(null);	},700);
						});
					}).click(function() {
						// Focus on the list
						$(self.options.listMarkIDSelector).removeClass('list-focus');	
						$(el).addClass('list-focus');
						var iwContent = '<div class="list-item" data-markerid="'+$(el).attr('data-markerid')+'">'+$(el).html()+'</div>';
						self.openInfoWindow({ 'content': iwContent }, this);
						var container = $(self.options.listContainerSelector);
						container.animate({
							scrollTop: $(el).offset().top - container.offset().top + container.scrollTop()
						});
						self.attachGetDirectionsEvent('.gm-style-iw .loc-dist');
					});
				}
				
			});
			// Show all list items, if hidden. It might not be necessary.. double check
			$(self.options.listContainerSelector+' ul li').removeClass('isl-hidden').addClass('isl-visible');
			
			self.setMarkerClusterer(locationMarkers);
			
			self.attachGetDirectionsEvent();
			
			// Deal with Filter tags
			if(self.options.filterTags.enabled && self.options.filterTags.tags.length)
			{
				var filterContainer = $('#'+self.options.filterTags.element);
				filterContainer.css('display','');
				if(self.options.filterTags.addedToMap == false || typeof self.options.filterTags.addedToMap == 'undefined'){
					self.options.filterTags.addedToMap = true;
					self.addControl(self.options.filterTags.element, self.options.filterTags.position);
				}
				
				var filterList = $('#'+self.options.filterTags.element+' div');
				
				$.each(self.options.filterTags.tags, function(i,filterTag){
					filterTag	= $('<label />').html('<input type="checkbox" value="'+filterTag+'" /> '+filterTag);
					filterList.append(filterTag);
				});
				
				$('#'+self.options.filterTags.element+' input:checkbox').click(function() {
					var mCluster = self.get('MarkerClusterer');
					//
					var markersInFilter = [];
					self.closeInfoWindow();
					self.set('bounds', null);
					var filters	= [];
					$('#'+self.options.filterTags.element+' input:checkbox:checked').each(function(i, checkbox) {
						filters.push($(checkbox).val());
					});
					if (filters.length > 0)
					{
						// Hide all list items first, then make only the matching markers visible
						$(self.options.listContainerSelector+' ul li.isl-visible').addClass('isl-hidden').removeClass('isl-visible');
						self.find('markers', { 'property': 'tags', 'value': filters, 'operator': self.options.filterTags.operator }, function(marker, found) {
							if (found)
							{
								self.addBounds(marker.position);
								// Display the listing for teh marker
								$('#'+marker.listID).removeClass('isl-hidden').addClass('isl-visible');
								markersInFilter.push(marker);
							}
							marker.setVisible(found); 
						});
						// always display home marker
						var homeMarker = self.get('markers')[0];
						if(homeMarker)
						{
							homeMarker.setVisible(true); 
							self.addBounds(homeMarker.position);
						}
					} else {
						$.each(self.get('markers'), function(i, marker) {
							if(typeof marker !== 'undefined'){
								self.addBounds(marker.position);
								marker.setVisible(true); 
								markersInFilter.push(marker);
							}
						});
						
						// Show all list items, if hidden
						$(self.options.listContainerSelector+' ul li').removeClass('isl-hidden').addClass('isl-visible');
					}
					self.clearMarkerClusterer();
					self.setMarkerClusterer(markersInFilter);
					
					self.reDrawMarkerClusterer();
					self.updateSearchResult();
				});
			}
		},
		clearMarkerClusterer:	function(){
			var mCluster = this.get('MarkerClusterer');
			if(typeof mCluster !== 'undefined' && mCluster !== null){
				mCluster.setMap(null);
			}
		},
		reDrawMarkerClusterer:	function(){
			var mCluster = this.get('MarkerClusterer');
			if(typeof mCluster !== 'undefined' && mCluster !== null){
				mCluster.repaint();
			}
		},
		setMarkerClusterer: function(locationMarkers)
		{
			var self = this;
			if(self.options.markerClusterer && locationMarkers){
				self.set('MarkerClusterer', new MarkerClusterer(self.get('map'), locationMarkers, self.options.markerClusterer)); 
			}
		},
		getLocationsInRange: function(locations)
		{
			//var locations = this.get('locations');
			var locationsInRange = [];
			
			for(var i = 0; i < locations.length; i++){
				if (this.options.origin.lat && this.options.origin.lng) {
					locations[i].distance = this.roundNumber(this.geoCodeCalcCalcDistance(locations[i].lat, locations[i].lng),2);
				}
				if (this.options.origin)
				{
					if(locations[i].distance < this.options.maxdistance){
						locationsInRange.push(locations[i]);	
					}
				}else{
					locationsInRange.push(locations[i]);
				}
			}
				
			if(locationsInRange.length == 0 && false){
				locationsInRange = locations;
			}
			locationsInRange = this.sortLocationsByDistance(locationsInRange);
			this.set('locationsInRange',locationsInRange);
			return locationsInRange;
		},
		/**
		 * Location distance sorting function
		 *
		 * @param locations {array} 
		 */
		sortLocationsByDistance: function (locations) {
			locations = locations.sort(function (a, b) {
				a.distance = parseFloat(a.distance);
					return ((a.distance < b.distance) ? -1 : ((a.distance > b.distance) ? 1 : 0));
				});
			return locations;
		},
		/**
		 * A service to predict the desired Place based on user input. The service is attached to an <input> field in the form of a drop-down list. The list of predictions is updated dynamically as text is typed into the input field.
		 * @param panel:jquery/node/string
		 * @param autocompleteOptions:google.maps.places.AutocompleteOptions, http://code.google.com/apis/maps/documentation/javascript/reference.html#AutocompleteOptions
		 */
		placesAutocomplete: function(input) {
			var self 	= this;
			var map 	= self.get('map');
			var opt		= self.options;
			var inputObj= $('#'+input);
			input = /** @type {HTMLInputElement} */(document.getElementById(input));
			if(opt.autocomplete)
			{
				var autocomplete = new google.maps.places.Autocomplete(input);
				autocomplete.bindTo('bounds', map);
				google.maps.event.addListener(autocomplete, 'place_changed', function() {
					var place = autocomplete.getPlace();
					var options = {lat:place.geometry.location.lat(), lng:place.geometry.location.lng()};
					self.getData(options);
				});
			}
			else
			{
				opt.geocoder = new google.maps.Geocoder();
				inputObj.on('keypress', function(e){
					if (e.which == 13) {
						e.preventDefault();
						opt.geocoder.geocode({'address' : inputObj.val()},function(results, status) {
							if (status == google.maps.GeocoderStatus.OK) {
								var options = {lat:results[0].geometry.location.lat(), lng:results[0].geometry.location.lng()};
								self.getData(options);
							} else {
								console.log('Geocode was not successful for the following reason: '+ status);
							}
						});
					}
				});
			}
		},
		
		setupLocations: function(data, lat, lng){
			var self = this;
			this.options.origin.lat = lat;
			this.options.origin.lng = lng;
			
			self.deleteMarkers();
			self.addHomeMarker();

			$('#isl-search-result').html('');

			
			if(data.locations){
				this.set('locations',data.locations);
			}
			
			if(data.list){
				$(self.options.listContainerSelector+' ul').html(data.list);
			}else
			if(data.locations){
				// if there's no lat and lng in CSV or XML file, then don't filter location, so we can assing the lat and lng
				if(typeof lat !== 'undefined' && typeof lng !== 'undefined' && (lat != 0 || lng != 0)){ // Geolocation or search worked
					data.locations =this.getLocationsInRange(data.locations);
				}
				Handlebars.registerHelper('niceURL', function(url) {
					if(url){
						return url.replace('https://', '').replace('http://', '').replace('www.', '');
					}
				});
				Handlebars.registerHelper('allowHTML', function(html) {
					return new Handlebars.SafeString(html);
				});

				this.options.listTemplate = this.options.listTemplate.replace('href="#{{webpage}}"','href="{{webpage}}"')
						.replace('href="#{{facebook}}"','href="{{facebook}}"')
						.replace('href="#{{linkedin}}"','href="{{linkedin}}"')
						.replace('href="#{{google_plus}}"','href="{{google_plus}}"')
						;
				listTemplate = Handlebars.compile(this.options.listTemplate);
				var i=1;
				var list ='';
				$.each(data.locations, function(i,location){
					if(!location.icon)
					{
						var iconText = (self.intToAlpha(++i));
						var iconTextSize = (iconText.length < 3 ? 14 : 10);
						location.icon = 'https://mt.googleapis.com/vt/icon/name=icons/spotlight/spotlight-waypoint-b.png&text='+iconText+'&psize='+iconTextSize+'&font=fonts/Roboto-Regular.ttf&color=ff333333&ax=44&ay=48';
					}
					if(typeof lat !== 'undefined' && typeof lng !== 'undefined' && (lat != 0 || lng != 0)){
						location.showRoute = true;
					}
					
					location.unit = self.options.lengthUnit;
					list = list + listTemplate(location);
				});
				$(self.options.listContainerSelector+' ul').html(list);				
			}			
			this.updateSearchResult();
			this.addMarkersGmapping();
			console.log('self.options.firstRun: '+self.options.firstRun);
			if(self.options.firstRun){
				if(self.options.first_load_zoom)
				{
					self.set('bounds',new google.maps.LatLngBounds());
					self.option('zoom',self.options.first_load_zoom);
				}
				$('.loc-dist').css('display','none');
			}
			else
			{
				$('.loc-dist').css('display','');
			}
			self.options.firstRun = false;
		},
		
		updateSearchResult: function () {
			if($(this.options.listContainerSelector+' ul li.isl-visible').length > 0){
				var searchResult = this.options.language.results.replace('{results}', $(this.options.listContainerSelector+' ul li.isl-visible').length);
			}else{
				var searchResult = this.options.language.noresult;
			}
			$('#isl-search-result').html(searchResult);
		},
		/**
		 * Distance calculations
		 */
		geoCodeCalcToRadian: function (v) {
			return v * (Math.PI / 180);
		},
		geoCodeCalcDiffRadian: function (v1, v2) {
			return this.geoCodeCalcToRadian(v2) - this.geoCodeCalcToRadian(v1);
		},
		geoCodeCalcCalcDistance: function (lat2, lng2) {
			var distance = this.options.radius * 2 * Math.asin(Math.min(1, Math.sqrt(( Math.pow(Math.sin((this.geoCodeCalcDiffRadian(this.options.origin.lat, lat2)) / 2.0), 2.0) + Math.cos(this.geoCodeCalcToRadian(this.options.origin.lat)) * Math.cos(this.geoCodeCalcToRadian(lat2)) * Math.pow(Math.sin((this.geoCodeCalcDiffRadian(this.options.origin.lng, lng2)) / 2.0), 2.0) ))));
			return distance;
		},
		geolocate:	function(button) {
			var self = this;
			if(typeof button !== 'undefined' && button !== null){
				$(button).off('click'); // remove handler to prevent multiple requests, which is an issue specially in IE
			}
			this.getCurrentPosition(function(position, status) {
				if ( status === 'OK' ) {
					var options = {lat:position.coords.latitude, lng:position.coords.longitude};
					self.getData(options);
					if(typeof button !== 'undefined' && button !== null){
						$(button).on('click',function(){ self.geolocate(button);}); // add handler back after ajax is done
					}
					self.updateSearchBar(options);
				}else if(status == 'NOT_SUPPORTED' || (typeof status.PERMISSION_DENIED != 'undefined' && status.PERMISSION_DENIED == 1) ){
					if(typeof status.message != 'undefined'){
						errorMessage = status.message;
					}else{
						errorMessage = status;
					}
					console.log(errorMessage);
					var searching = $('<div class="isl-loading-search"></div>').html('<h3>'+self.options.language.geoLocationFailed.replace('{1}',errorMessage)+'</h3>').prependTo('.isl-list-container');
					self.getData({lat:0,lng:0});
				}
			},{enableHighAccuracy:true, timeout:5000});
			
		},
		
		updateSearchBar: function(options){
			var self = this;
			self.search({'location':self._latLng(options.lat+','+options.lng)}, function(results, status) {
				if(status === 'OK' || status == google.maps.GeocoderStatus.OK){
					$('.isl-location-search').val(results[0].formatted_address);
					var homeMarker = self.get('markers')[0];
					if(homeMarker){
						$(homeMarker).click(function() {
							self.openInfoWindow({ 'content': results[0].formatted_address+' <br />('+options.lat+', '+options.lng+')'}, this);
						});
					}
				}
			});
		},
		/**
		 * Attach Click even to directions button to Handle inline direction requests
		 */
		attachGetDirectionsEvent: function(elSelector){
			var self = this;
			// Open directions
			elSelector = elSelector || '.loc-btn-showDirections';
			$(elSelector).on('click', function (e) {
				e.preventDefault();
				var destination = $(this).closest('.list-item').attr('data-markerid');
				console.log($(this).closest('.list-item'));
				destination = self.get('markers > '+destination).position;
				
				if(self.options.isMobile && 
						(self.options.linkMobileToGmaps == 'coordinates' || self.options.linkMobileToGmaps == 'address')
				){
					if(self.options.linkMobileToGmaps == 'coordinates'){
						window.open('https://www.google.com/maps/dir/My+Location/'+destination.lat()+','+destination.lng());
					}else{
						window.open('https://www.google.com/maps/dir/My+Location/'+$(this).closest('.list-item').find('address').text().trim().replace(/(?:\r\n|\r\r\r|\n\n\n|\r\r|\n\n|\r|\n)/g, ',+').replace(/(?:\s)/g, '+'));
					}
				}
				else
				{
					var directionsRequest = {
							origin: 		self._latLng(self.options.origin.lat+','+self.options.origin.lng),
							destination: 	destination,
							'travelMode': 	google.maps.DirectionsTravelMode.DRIVING
						};
						
						var panel = 'isl-directions-panel';
						//create a div to hold the panel
						$(self.options.listContainerSelector).append('<div id="'+panel+'-container"><a class="btn btn-danger pull-right" data-dismiss="alert"><i class="glyphicon glyphicon-circle-arrow-left  icon-circle-arrow-left"></i> '+self.options.language.previous+'</a><br style="clear:left"><br style="clear:left"><div id="'+panel+'"></div></div>');
						
						var directionsRendererOptions  ={
							'panel': 		document.getElementById(panel),
							draggable:		true
						};
						self.displayDirections(directionsRequest,directionsRendererOptions, function(result, status) {
							if ( status === 'OK' ) {
								// hides list
								$(self.options.listContainerSelector+' ul').css('display', 'none');
								$('#isl-search-result').css('display', 'none');
								var dirService = result;
								var markers =self.get('markers');
								$.each(markers,function( i, marker ) {
									if(marker){
										marker.setVisible(false);
									}
								});
								var mCluster = self.get('MarkerClusterer');
								if(typeof mCluster !== 'undefined'){
									mCluster.setMap(null);
									self.set('MarkerClusterer',null);
								}
								$('#'+panel+'-container a').on('click',function(){
									$(self.options.listContainerSelector+' ul').css('display', '');
									$('#isl-search-result').css('display', '');
									self.removeDirections();
									$.each(markers,function( i, marker ) {
										if(marker){
											marker.setVisible(true);
										}
									});
									mCluster.setMap(self.get('map'));
									self.set('MarkerClusterer',mCluster);
								});
							}
				        });
						if(self.options.linkToGoogleMaps && $('.isl-linkToGoogleMaps').length < 1)
						{
							jQuery('<a/>',{
							    href: 'https://maps.google.com/?saddr='
							    		+self.options.origin.lat+'+'+self.options.origin.lng
							    		+'&daddr='+destination.lat()+'+'+destination.lng(),
							    rel: 'external',
							    target: '_blank',
								class: 'btn btn-default isl-linkToGoogleMaps',
							    text: self.options.language.openInGoogleMaps
							}).appendTo('#'+panel);
						}
				} // END ELSE
				
				
			});
		},
		addEvents: function(){
			var self = this;
			// Add Event to Change Radius list
			$('#maxdistance').on('change',function(){
				if(self.options.origin.lat && self.options.origin.lng){
					self.getData(self.options.origin);
				}
			});
		},
		intToAlpha: function(int){
			var alpha = "";
				while(int > 0) {
						alpha = String.fromCharCode(65 + ((int-1) % 26)) + alpha;
						int = Math.floor((int-1)/26);
				}
				return alpha;
		},
		/**
		 * Adds a Marker tag if not already added and sorts the MarkerTags array
		 * @param tag	the marker tag to be added;
		 */
		addFilterTag: function(newTags){
			var self = this;
			if(self.options.filterTags.enabled){
				$.each(newTags, function(i,newTag){
					if($.inArray(newTag, self.options.filterTags.tags) === -1){
						self.options.filterTags.tags.push(newTag);
						self.options.filterTags.tags.sort();
					}
				});
			}
		},
		
		deleteFilterTags: function(){
			this.options.filterTags.tags = [];
			$('#'+this.options.filterTags.element).css('display','none');
			var filterList = $('#'+this.options.filterTags.element+' div');
			filterList.html('');
		},
		
		/**
		 * Rounding function used for distances
		 *
		 * @param num {number} the full number
		 * @param dec {number} the number of digits to show after the decimal
		 * @returns {number}
		 */
		roundNumber: function (num, dec) {
			return Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
		},
		/**
		 * Issues a distance matrix request.
		 * @param distanceMatrixRequest:google.maps.DistanceMatrixRequest, http://code.google.com/apis/maps/documentation/javascript/reference.html#DistanceMatrixRequest
		 * @param callback:function(result:google.maps.DistanceMatrixResponse, status: google.maps.DistanceMatrixStatus), http://code.google.com/apis/maps/documentation/javascript/reference.html#DistanceMatrixResponse
		 */
		displayDistanceMatrix: function(distanceMatrixRequest, callback) {
			this.get('services > DistanceMatrixService', new google.maps.DistanceMatrixService()).getDistanceMatrix(distanceMatrixRequest, callback);
		}
	});	
} (jQuery) );