<?php
/**
 * @package		plg_captcha_securimage
 * @copyright	Copyright (C) 2006 - 2014 IdealExtensions.com, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

class plgSystemIstorelocatorInstallerScript
{

	/*
	 * $parent is the class calling this method.
	* $type is the type of change (install, update or discover_install, not uninstall).
	* preflight runs before anything else and while the extracted files are in the uploaded temp folder.
	* If preflight returns false, Joomla will abort the update and undo everything already done.
	*/
	function preflight( $type, $parent ) {
		$this->installer	= method_exists($parent, 'getParent') ? $parent->getParent() : $parent->parent;
		$this->manifest		= $this->installer->getManifest();

		// get version from xml file
		if (!$this->manifest) {
			$this->manifest = JApplicationHelper::parseXMLInstallFile($installer->getPath('manifest'));
			if (is_array($this->manifest)) {
				$this->new_version = (string) $this->manifest['version'];
			}
		}else{
			$this->new_version	= ((string) $this->manifest->version);
		}

		// the current version
		$this->current_version	= $this->new_version;

	}

	function update($parent) {
		 $this->install($parent);
	}

	function install($parent) {
		// I activate the plugin
		$db = JFactory::getDbo();
		$tableExtensions = $db->quoteName("#__extensions");
		 $columnElement	 = $db->quoteName("element");
		 $columnType			= $db->quoteName("type");
		 $columnEnabled	 = $db->quoteName("enabled");

		 // Enable plugin
		 $db->setQuery("UPDATE $tableExtensions SET $columnEnabled=1 WHERE $columnElement='istorelocator' AND $columnType='plugin'");
		 $db->query();

		 JFactory::getApplication()->enqueueMessage(JText::_('Plugin Enabled') );
	}

	function postflight($type, $parent) {
		$this->addUpdateSite();
	}

	function addUpdateSite(){
		
		$extension_name = 'istorelocator';
		$pid = 33;

		$db = JFactory::getDbo();
		$query="SELECT update_site_id FROM #__update_sites WHERE location LIKE '%{$extension_name}%' AND type LIKE 'extension'";
		$db->setQuery($query);
		$update_site_id = $db->loadResult();

		$object = new stdClass();
		$object->name= $extension_name;
		$object->type='extension';
		$domain = parse_url(JURI::root());
		$domain = $domain['host'];
		$domain = urlencode(base64_encode($domain));



		$object->location='http://idealextensions.com/?extension='.$extension_name.'&pid='.$pid.'&output=updatexml&currentversion='.$this->new_version.'&dm='.$domain.'&file-extension.xml';

		$object->enabled=1;
		if(empty($update_site_id)){
			$db->insertObject("#__update_sites",$object);
			$update_site_id = $db->insertid();
		}else{
			$object->update_site_id = $update_site_id;
			$db->updateObject("#__update_sites",$object,'update_site_id');
		}
		$query="SELECT extension_id FROM #__extensions WHERE `element` = '{$extension_name}' AND type LIKE 'plugin'";
		$db->setQuery($query);
		$extension_id = $db->loadResult();
		if(empty($update_site_id) OR empty($extension_id))	return false;
		$query='INSERT IGNORE INTO #__update_sites_extensions (update_site_id, extension_id) values ('.$update_site_id.','.$extension_id.')';
		$db->setQuery($query);
		$db->query();
		
		JFactory::getApplication()->enqueueMessage(JText::_('Update Site Installed') );
				
		return true;
	}
}