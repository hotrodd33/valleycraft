<?php
/**
 * @version 	1.3
 * @date        30 Oct 2016
 * @developer 	SiteGuarding.com 
 * @copyright 	(c) 2016 www.safetybis.com , www.siteguarding.com
 * @contacts 	support@safetybis.com, support@siteguarding.com
 * @license		GNU/GPL, http://www.gnu.org/licenses/gpl-3.0.html
 */
 
defined('_JEXEC') or die('Restricted access');

jimport('joomla.plugin.plugin');
class plgSystemjAdminBruteforceProtection extends JPlugin
{

	function plgSystemjAdminBruteforceProtection(& $subject, $config)
	{
		parent::__construct($subject, $config);
	}
	
	function onAfterDispatch()
	{
	    $config = JFactory::getConfig();
		$mainframe 	= JFactory::getApplication();
		$user 		= JFactory::getUser();
		$session	= JFactory::getSession();
        
        $language =& JFactory::getLanguage();
        $extension = 'plg_system_jadminbruteforceprotection'; 
        $language_tag = $language->getTag();
        $language->load($extension, JPATH_ADMINISTRATOR, null, true);

     
		if (!defined('DIRSEP'))
		{
			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') define('DIRSEP', '\\');
			else define('DIRSEP', '/');
		}
		
		$key_file = dirname(__FILE__).DIRSEP.'sgantivirus.login.keys.php';

        if ($mainframe->isAdmin() && !$user->get('guest'))
        {
            if (file_exists($key_file))
            {
                //require_once($key_file);
                $handle = fopen($key_file, "r");
                $contents = fread($handle, filesize($key_file));
                fclose($handle);
                
                $contents = str_replace(array('<?php $', '"; ?>'), "", $contents);
                $contents = str_replace('="', "=", $contents);
                $contents = str_replace('";$', "|", $contents);
                $contents = explode("|", $contents);
                if (count($contents))
                {
                    foreach ($contents as $v)
                    {
                        $v = trim($v);
                        if ($v != '')
                        {
                            $v = explode("=", $v);
                            define(trim($v[0]), trim($v[1]));
                        }
                    }
                }
        
        
        
                // Create session cookie
                $value = md5(captcha_key_session.$_SERVER['REMOTE_ADDR'].date("Y-m-d"));
                $a = setcookie("siteguarding_session_code", $value, time() + 24*60*60);   // set 1 day
                $a = setcookie("siteguarding_login_code", "", time()- 3600);   // delete
                
                // Create session tmp file
                $this->CREATE_session_code();
                
                
                
                //echo 'set siteguarding_session_code';
            }
            

            
            
            if (!isset($_REQUEST['option']) || $_REQUEST['option'] != 'com_plugins') return;
            

            
            if (intval($this->params->get('captcha_active')) == 1)
            {
                $data = array();
                $data['captcha_site_key'] = trim($this->params->get('captcha_site_key'));
                $data['captcha_secret_key'] = trim($this->params->get('captcha_secret_key'));
				
				$wrappers = stream_get_wrappers();
				if (!in_array('https', $wrappers))
				{
					// wrapper https is not installed
                    JError::raiseWarning( 100, JText::_('JABP_PLG_ERROR_HTTPS_WRAPPER_ABSENT') ); 
				}
                
                if ($data['captcha_site_key'] == '' || $data['captcha_secret_key'] == '') 
                {
                    JError::raiseWarning( 100, JText::_('JABP_PLG_ERROR_WRONGKEYS') ); 
                    return;   
                }
                
                // Check the keys before save
                    
                    /*echo 'read keys'.captcha_site_key.' = '.$data['captcha_site_key']."<br>";
                    echo 'read keys'.captcha_secret_key.' = '.$data['captcha_secret_key']."<br>";
                    
                    if (captcha_site_key == $data['captcha_site_key']) echo '-1-';
                    if (captcha_secret_key == $data['captcha_secret_key']) echo '-2-';*/
                    
                    if (captcha_site_key == $data['captcha_site_key'] && captcha_secret_key == $data['captcha_secret_key']) return;
                    //echo 'Save keys';
                
                
                // Save keys to file
                $fp = fopen($key_file, 'w');
                fwrite($fp, '<?php $captcha_key_session="'.md5(time().mt_rand()).'";$captcha_site_key="'.$data['captcha_site_key'].'";$captcha_secret_key="'.$data['captcha_secret_key'].'"; ?>');
                fclose($fp);
                
                // check and patch /administrator/index.php
                if (!$this->Patch_Login_file(true))
                {
                    JError::raiseWarning( 100, JText::_('JABP_PLG_ERROR_CANTPATCH') ); 
                    return;   
                }
            }
            else {
                // check and remove module from /administrator/index.php
                if (!$this->Patch_Login_file(false))
                {
                    JError::raiseWarning( 100, JText::_('JABP_PLG_ERROR_CANTPATCH') ); 
                    return;   
                }
		
				if (file_exists($key_file)) unlink($key_file);
            }
            
            return;
        }
        

	}
    
    
	function Patch_Login_file($action = true)   // true - insert, false - remove
	{
		if (!defined('DIRSEP'))
		{
			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') define('DIRSEP', '\\');
			else define('DIRSEP', '/');
		}
        
		$file = dirname(__FILE__).DIRSEP."sgantivirus.login.php";

        $integration_code = '<?php /* Siteguarding Block 6DBB86C229DE-START */include_once("'.$file.'");/* Siteguarding Block 6DBB86C229DE-END */?>';
        
        // Insert code
		$scan_path = JPATH_ROOT;
        
        $filename = $scan_path.DIRSEP.'administrator'.DIRSEP.'index.php';
        $handle = fopen($filename, "r");
        if ($handle === false) return false;
        $contents = fread($handle, filesize($filename));
        if ($contents === false) return false;
        fclose($handle);
        
        $pos_code = stripos($contents, $integration_code);
        
        if ($action === false)
        {
            // Remove block
            $contents = str_replace($integration_code, "", $contents);
        }
        else {
            // Insert block
            if ( $pos_code !== false && $pos_code == 0)
            {
                // Skip double code injection
                return true;
            }
            else {
                // Insert
                $contents = $integration_code.$contents;
            }
        }
        
        $handle = fopen($filename, 'w');
        if ($handle === false) 
        {
            // 2nd try , change file permssion to 666
            $status = chmod($filename, 0666);
            if ($status === false) return false;
            
            $handle = fopen($filename, 'w');
            if ($handle === false) return false;
        }
        
        $status = fwrite($handle, $contents);
        if ($status === false) return false;
        fclose($handle);

        
        return true;
	}
    
    function CREATE_session_code()
    {
		global $_SERVER;
		
	    if (!defined('DIRSEP'))
		{
			if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') define('DIRSEP', '\\');
			else define('DIRSEP', '/');
		}
        
        $path = JPATH_ROOT;
		
        $filename = 'bf_session_'.md5($_SERVER['REMOTE_ADDR']).'.login';
        $session_file = $path.DIRSEP.'tmp'.DIRSEP.$filename;
        if (file_exists($session_file)) 
        {
            $ctime = filectime($session_file);
            if (date("Y-m-d") == date("Y-m-d", $ctime)) return;
            
            unlink($session_file);
        }
        $fp = fopen($session_file, 'w');
        fwrite($fp, date("Y-m-d H:i:s"));
        fclose($fp);
    }
    

}


?>