<?php
/**
 * @package     pwebbox
 * @version 	2.0.0
 *
 * @copyright   Copyright (C) 2015 Perfect Web. All rights reserved. http://www.perfect-web.co
 * @license     GNU General Public Licence http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die;
?>
<!-- PWebBox Module plugin -->
<div class="pwebbox-module-container">
    <div id="pwebbox_module_<?php echo $id; ?>"> 
        <?php echo $text; ?>
    </div>
</div>
<!-- PWebBox Module plugin end -->
