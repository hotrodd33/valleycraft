<?php
// no direct access
defined('_JEXEC') or die('Restricted access');

//ini_set('display_errors', 1);

$app = JFactory::getApplication();
if (! $app->isAdmin()) {
	$exists = file_exists(JPATH_BASE . '/modules/mod_cherry_picker/models/parameters.php');
	if ($exists) {
		require_once(JPATH_BASE . '/modules/mod_cherry_picker/models/parameters.php');
		require_once(JPATH_BASE . '/modules/mod_cherry_picker/models/manufacturers.php');
		require_once(JPATH_BASE . '/modules/mod_cherry_picker/models/prices.php');
		require_once(JPATH_BASE . '/modules/mod_cherry_picker/defines.php');
	}
}

class PlgHikashopCherrypicker extends JPlugin
{
	private $_module;
	private $_cherry_picker_installed = false;

	public function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
		$this->_cherry_picker_installed =
			file_exists(JPATH_BASE .'/modules/mod_cherry_picker/models/parameters.php');

		//$this->loadLanguage();
	}

	public function onBeforeProductListingLoad(&$filters, &$order, &$parent,
									&$select, &$select2, &$a, &$b, &$on)
	{
		$app = JFactory::getApplication();
		if ($app->isAdmin() || $this->_cherry_picker_installed == false)
			return false;

		if ($this->params->get('debug', 0)) {
			echo '<p style="border-bottom:1px solid;color:orange;"><b>BEFORE</b></p>';
			$this->printState($filters, $order, $parent, $select, $select2, $a, $b, $on);
		}


		$parameters = new CPParameters();
		$layout = $this->moduleOption('layout');
		$ignore_mode = !($layout == CP_LAYOUT_SIMPLE_LIST || $layout == CP_LAYOUT_CHECKBOX_LIST);
		$joins = $parameters->sqlJoins('b');
		$wheres = $parameters->sqlWheres($ignore_mode);


		$manufacturers = new CPHikaShopManufacturers();
		$manufacturers_wheres = $manufacturers->sqlWheres();
		if ($manufacturers_wheres) {
			$found = false;
			// If manufacturer has already been added by HikaShop, we
			// replace it with our string as it includes wider refinements.
			foreach ($filters as &$filter) {
				if (strpos($filter, 'product_manufacturer_id') !== false) {
					$filter = implode(' AND ', $manufacturers_wheres);
					$found = true;
					break;
				}
			}
			if (! $found)
				$wheres = array_merge($wheres, $manufacturers_wheres);
		}


		$options = array(
			"include_taxes" => $this->moduleOption('include_taxes')
		);
		$prices = new CPPrices($options);
		$joins = array_merge($joins, $prices->sqlJoins('b'));
		$wheres = array_merge($wheres, $prices->sqlWheres());

		$wheres_str = implode(' AND ', $wheres);

		if ($this->params->get('search_variants', 0) && $wheres_str) {
			// maks: pull parent products from its children
			// Note 1: In order for Parent to appear in results it must have
			// ALL Product Types that its child product has.
			$childWhere = "b.`product_id` IN (".
				" SELECT `product_parent_id`".
				" FROM `#__hikashop_product` as b ".
				implode(' ', $joins) .
				" WHERE `product_parent_id` <> 0".
				" AND ". $wheres_str .
			")";
			$wheres_str = "(". $wheres_str . " OR ". $childWhere .")";
		}


		if ($joins)
			$on .= ' '. implode(' ', $joins);
		if ($wheres_str)
			$filters[] = $wheres_str;
//		if ($wheres)
//			$filters = array_merge($filters, $wheres);


		if ($this->params->get('debug', 0)) {
			echo '<p style="border-bottom:1px solid;color:orange"><b>AFTER</b></p>';
			$this->printState($filters, $order, $parent, $select, $select2, $a, $b, $on);
		}

		return true;
	}


	private function printState($filters, $order, $parent, $select, $select2,
								$a, $b, $on)
	{
		echo '<pre>';
		echo '<p style="border-bottom:1px solid;"><b>Filters:</b></p>';
		print_r($filters);
		echo '<p style="border-bottom:1px solid;"><b>Order:</b></p>';
		print_r($order);
		//echo '<p style="border-bottom:1px solid;"><b>Parent:</b></p>';
		//print_r($parent);
		echo '<p style="border-bottom:1px solid;"><b>Select:</b></p>';
		print_r($select);
		echo '<p style="border-bottom:1px solid;"><b>Select2:</b></p>';
		print_r($select2);
		echo '<p style="border-bottom:1px solid;"><b>a:</b></p>';
		print_r($a);
		echo '<p style="border-bottom:1px solid;"><b>b:</b></p>';
		print_r($b);
		echo '<p style="border-bottom:1px solid;"><b>On:</b></p>';
		print_r($on);
		echo '<p><b>End HikaShop/CherryPicker plugin data output</b></p>';
		echo '</pre>';
	}

	
	private function moduleOption($optionName) {
		if ($this->_module)
			return $this->_module->params->get($optionName);

		jimport('joomla.application.module.helper');
		$ERROR = -1;

		/* For Cherry Picker users:
		* If you want to use options of some specific copy of CP module
		* provide its title in $specificModuleTitle.
		* More info can be found at Joomla Docs:
		* http://docs.joomla.org/JModuleHelper/getModule
		*/
		$specificModuleTitle = '';
		$module = JModuleHelper::getModule('mod_cherry_picker', $specificModuleTitle);
		if (! $module)
			return $ERROR;

		$params = new JRegistry;
		$params->loadString($module->params);
		$module->params = $params;
		$this->_module = $module;
		return $params->get($optionName);
	}
}
?>
