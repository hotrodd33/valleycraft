<?php

/**
 * @copyright	Copyright (C) 2012 Cedric KEIFLIN alias ced1870
 * http://www.joomlack.fr
 * Module Maximenu CK
 * @license		GNU/GPL
 * */
// no direct access
defined('_JEXEC') or die('Restricted access');

class modMaximenuckhikashopHelper {

    /**
     * Get a list of the menu items.
     *
     * @param	JRegistry	$params	The module options.
     *
     * @return	array
     */
    static function getItems(&$params) {
        jimport('joomla.application.module.helper');
		$app = JFactory::getApplication();

        $usehikashopsuffix = $params->get('usehikashopsuffix', '0');
        $hikashopimagesuffix = $params->get('hikashopimagesuffix', '_mini');
        $usehikashopimages = $params->get('usehikashopimages', '0');
        $hikashopcategoryroot = $params->get('hikashopcategoryroot', '0');
        $hikashopcategorydepth = $params->get('hikashopcategorydepth', '0');
        $hikashopshowall = $params->get('hikashopshowall', '1');
        $hikashopitemid = $params->get('hikashopitemid', '');

        $active_path = array();
        $db = JFactory::getDBO();

        $query = "SELECT *,"
                . " #__hikashop_category.category_id as id,"
                . " #__hikashop_category.category_depth-1 as level,"
                . " #__hikashop_category.category_parent_id as parent,"
                . " #__hikashop_category.category_ordering as ordering"
                . " FROM #__hikashop_category"
                . " LEFT OUTER JOIN #__hikashop_file"
                . " ON #__hikashop_file.file_ref_id = #__hikashop_category.category_id"
                . " WHERE #__hikashop_category.category_type = 'product'"
                . " AND #__hikashop_category.category_published = 1"
                . " AND #__hikashop_category.category_depth > 1"
                . " ORDER BY level DESC, ordering ASC";

        $db->setQuery($query);

        if ($db->query()) {
            $rows = $db->loadObjectList('id');
        } else {
            echo '<p style="color:red;font-weight:bold;">Error loading SQL data : loading the hikashop categories in Maximenu CK</p>';
            return false;
        }

        $active_category_id = (JRequest::getVar('ctrl', 'category') == 'category') ? JRequest::getInt('cid', '0') : self::getActiveCategory(JRequest::getInt('cid', '0'));
        $level = 0;
        $items = array();
        $i = 0;
        $hikashopcategoryrootitem = new stdClass();
        $hikashopcategoryrootitem->level = 0;
        $user	= JFactory::getUser();
		$groups	= implode(',', $user->getAuthorisedGroups());

        foreach ($rows as $k => &$item) {

            if ($item->id == $hikashopcategoryroot)
                $hikashopcategoryrootitem = $item;

            // saves childs into parents items
            if ($item->level > 1) {
                $rows[$item->parent]->haschild = 'yes';
                if (isset($item->haschild)) {
                    $rows[$item->parent]->enfants.=$item->id . '|' . $item->enfants;
                } else {
                    $rows[$item->parent]->enfants.=$item->id . '|';
                }

                // add parent-child classes
                if (isset($active_category_id) && $active_category_id == $item->id) {
                    $active_path[] = $item->id;
                    $j = $item->level;

                    $tempitemID = $item->parent;

                    while ($j != 1) {

                        $rows[$tempitemID]->classe .= " active";
                        $active_path[] = $tempitemID;
                        $tempitemID = $rows[$tempitemID]->parent;

                        $j--;
                    }
                }
            }
            // create childs after respective parent
            if ($item->level == 1) { //gestion des droits des parents niveau 0
                $items[$i] = $item;
                if (isset($active_category_id) && $active_category_id == $item->id) {
                    $active_path[] = $item->id;
                }
                $item->path = array();
                $item->path[] = $item->id;
                if (isset($item->haschild)) {
                    $childs = explode("|", $item->enfants);
                    foreach ($childs as $c) {
                        if ($c) {
                            $i++;
                            $item->path[] = $rows[$c]->id;
                            $rows[$c]->path = $item->path;
                            $items[$i] = $rows[$c];
                        }
                    }
                }
            } else {
                $i--;
            }




            $i++;
        }


        $lastitem = 0;
        foreach ($items as $i => &$item) {

            // check if the item is in the active tree
            if (!$hikashopshowall AND !in_array($item->parent, $active_path) AND ($item->level - $hikashopcategoryrootitem->level > 1)) {
                unset($items[$i]);
                continue;
            }

            // check the access level             
            if (isset($rows[$item->parent]) && $rows[$item->parent]->category_access != 'all' && $item->category_access == 'all') $item->category_access = $rows[$item->parent]->category_access;
            if ($item->category_access != 'all') {
                if (!count(array_intersect(explode(',',$groups), explode(',',$item->category_access)))) {
                    unset($items[$i]);
                    continue;
                }
            }
            
            // check if the item is in the path for the selected root category
            if ($hikashopcategoryroot != 0
                    AND (
                    ($item->id == $hikashopcategoryroot)
                    OR (!in_array($hikashopcategoryroot, $item->path))
                    )
            ) {
                unset($items[$i]);
                continue;
            }

            // check the depth
            if ($hikashopcategorydepth AND (($item->level - $hikashopcategoryrootitem->level) > $hikashopcategorydepth) OR ($item->level <= $hikashopcategoryrootitem->level)) {
                unset($items[$i]);
                continue;
            }


            $item->params = new JRegistry();
            $itemid = $hikashopitemid ? '&Itemid=' . $hikashopitemid : '';
			if(empty($element->category_alias)){
				$item->alias = $item->category_name;
			}else{
				$item->alias = $item->category_alias;
			}
			if(method_exists($app,'stringURLSafe')){
				$itemalias = $app->stringURLSafe(strip_tags($item->alias));
			}else{
				$itemalias = JFilterOutput::stringURLSafe(strip_tags($item->alias));
			}
            $item->flink = JRoute::_('index.php?option=com_hikashop&ctrl=category&task=listing&cid=' . $item->id . '&name=' . $itemalias . $itemid);

            $item->deeper = false;
            $item->shallower = false;
            $item->level_diff = 0;

            // TODO : test
            $item->level = $item->level - $hikashopcategoryrootitem->level;

            if (isset($items[$i - 1])) {
                $items[$i - 1]->deeper = ($item->level > $items[$i - 1]->level);
                $items[$i - 1]->shallower = ($item->level < $items[$i - 1]->level);
                $items[$i - 1]->level_diff = ($items[$i - 1]->level - $item->level);
                if ($items[$i - 1]->deeper AND $params->get('layout', 'default') != '_:flatlist')
                    $items[$i - 1]->classe .= " parent";
            }

            // test if it is the last item
            $item->is_end = !isset($items[$i + 1]);

            // add some classes
            $item->classe .= " item" . $item->id;
            if (isset($active_category_id) && $active_category_id == $item->id) {
                $item->classe .= " current active";
            }

            // search for parameters
            $patterns = "#{maximenu}(.*){/maximenu}#Uis";
            $result = preg_match($patterns, stripslashes($item->category_description), $results);

            $item->desc = '';
            $item->colwidth = '';
            $item->tagcoltitle = 'none';
            $item->tagclass = '';
            $item->leftmargin = '';
            $item->topmargin = '';
            $item->submenuwidth = '';

            if (isset($results[1])) {
                $hikashopparams = explode('|', $results[1]);
                for ($j = 0; $j < count($hikashopparams); $j++) {
                    $item->desc = stristr($hikashopparams[$j], "desc=") ? str_replace('desc=', '', $hikashopparams[$j]) : $item->desc;
                    $item->colwidth = stristr($hikashopparams[$j], "col=") ? str_replace('col=', '', $hikashopparams[$j]) : $item->colwidth;
                    $item->tagcoltitle = stristr($hikashopparams[$j], "taghtml=") ? str_replace('taghtml=', '', $hikashopparams[$j]) : $item->tagcoltitle;
                    $item->tagclass = stristr($hikashopparams[$j], "tagclass=") ? ' ' . str_replace('tagclass=', '', $hikashopparams[$j]) : $item->tagclass;
                    $item->leftmargin = stristr($hikashopparams[$j], "leftmargin=") ? str_replace('leftmargin=', '', $hikashopparams[$j]) : $item->leftmargin;
                    $item->topmargin = stristr($hikashopparams[$j], "topmargin=") ? str_replace('topmargin=', '', $hikashopparams[$j]) : $item->topmargin;
                    $item->submenucontainerwidth = stristr($hikashopparams[$j], "submenuwidth=") ? str_replace('submenuwidth=', '', $hikashopparams[$j]) : $item->submenuwidth;
                }
            }

            $item->classe .= $item->tagclass;
            // variables definition
            $item->ftitle = stripslashes(htmlspecialchars($item->category_name));
            $item->content = "";
            $item->rel = "";

            // manage images
            if (!$usehikashopsuffix)
                $hikashopimagesuffix = '';
            $item->menu_image = '';
            if ($usehikashopimages) {
                $imageurl = $item->file_path ? explode(".", $item->file_path) : '';
                $imagename = isset($imageurl[0]) ? $imageurl[0] : '';
                $imageext = isset($imageurl[1]) ? $imageurl[1] : '';
                if (JFile::exists(JPATH_ROOT . DS . 'media' . DS . 'com_hikashop' . DS . 'upload' . DS . $imagename . $hikashopimagesuffix . '.' . $imageext)) {
                    $item->menu_image = 'media/com_hikashop/upload/' . $imagename . $hikashopimagesuffix . '.' . $imageext;
                }
            }
            
            // manage columns
            if ($item->colwidth) {
                $item->colonne = true;
                $parentItem = self::getParentItem($item->parent, $items);

                if (isset($parentItem->submenuswidth)) {
                    $parentItem->submenuswidth = strval($parentItem->submenuswidth) + strval($item->colwidth);
                } else {
                    if ($parentItem) $parentItem->submenuswidth = strval($item->colwidth);
                }
                if (isset($items[$i - 1]) AND $items[$i - 1]->deeper) {
                    $items[$i - 1]->nextcolumnwidth = $item->colwidth;
                }
                $item->columnwidth = $item->colwidth;
            }
            if (isset($parentItem->submenucontainerwidth) AND $parentItem->submenucontainerwidth)
                $parentItem->submenuswidth = $parentItem->submenucontainerwidth;


            $item->name = $item->ftitle;


            // pour compat avec default.php
            $item->anchor_css = '';
            $item->anchor_title = '';
            $item->type = '';

            // get plugin parameters that are used directly in the layout
            $item->liclass = $item->params->get('maximenu_liclass', '');
            $item->colbgcolor = $item->params->get('maximenu_colbgcolor', '');
        }

        // give the correct deep infos for the last item
        if (isset($items[$i])) {
            $items[$i]->level_diff = ($items[$i]->level - 1);
        }

        return $items;
    }

    function getParentItem($id, $items) {
        foreach ($items as $item) {
            if ($item->id == $id)
                return $item;
        }
		
		return false;
    }
    
    static function getActiveCategory($productid) {

        $query = "SELECT category_id"
                . " FROM #__hikashop_product_category"
                . " WHERE product_id = " . $productid . ";";
        $db = JFactory::getDBO();
        $db->setQuery($query);

        if ($db->query()) {
            $categoryid = $db->loadResult();
        } else {
            echo '<p style="color:red;font-weight:bold;">Error loading SQL data : loading the active hikashop category in Maximenu CK</p>';
            return false;
        }
        
        return $categoryid;
    }

}

?>