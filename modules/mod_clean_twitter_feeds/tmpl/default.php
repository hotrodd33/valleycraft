<?php
/**
 * @package		 Joomla.Site
 * @subpackage	 mod_clean_twitter_feeds
 * @copyright    Copyright (C) 2012-2015 Extenstions 4 Joomla. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 */
// no direct access
defined( '_JEXEC' ) or die('Restricted Access');
//fetch parameters result from xml file
$username = $params->get('username');
$widgetId = trim($params->get('widgetId'));
$width = trim($params->get('width'));
$height = trim($params->get('height'));
$theme = $params->get('theme');
$linkColor = $params->get('linkColor');
$footer = $params->get('footer');
$header = $params->get('header');
$border = $params->get('border');
$scrollbar = $params->get('scrollbar');
$transparent = $params->get('transparent');
$bColor = trim($params->get('bColor'));
if($bColor=="1"){
$backgroundColor = $params->get('backgroundColor');}
else{$backgroundColor = "";}
$borderColor = $params->get('borderColor');
$padding = $params->get('padding');
$border_radius = $params->get('border_radius');
$language = $params->get('language');
$tweetLimit = trim($params->get('tweetLimit'));
$related = $params->get('related');
$polite = $params->get('polite');
?>
<div class="clean_twitter_feeds <?php echo $params->get('moduleclass_sfx');?>" style="background:<?php echo $backgroundColor; ?>; width: <?php echo $width;?>px; padding: <?php echo $padding; ?>px;">
<a class="twitter-timeline" href="https://twitter.com/<?php echo $username; ?>" data-widget-id="<?php echo $widgetId; ?>" data-chrome="<?php echo $footer." ".$header." ".$scrollbar." ".$transparent;?>" data-border-color="<?php echo $borderColor;?>" data-theme="<?php echo $theme; ?>" data-link-color="<?php echo $linkColor; ?>"  data-related="<?php echo $related;?>" data-tweet-limit="<?php echo $tweetLimit; ?>" data-aria-polite="<?php echo $polite; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" lang="<?php echo $language; ?>">Tweets by @<?php echo $username; ?></a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</div>
