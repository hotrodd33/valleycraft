<?php
/**
 * @package		 Joomla.Site
 * @subpackage	 mod_clean_twitter_feeds
 * @copyright    Copyright (C) 2012-2015 Extenstions 4 Joomla. All rights reserved.
 * @license      GNU General Public License version 2 or later; see LICENSE.txt
 */
// no direct access
defined( '_JEXEC' ) or die('Restricted Access');
require JModuleHelper::getLayoutPath('mod_clean_twitter_feeds', $params->get('layout'));