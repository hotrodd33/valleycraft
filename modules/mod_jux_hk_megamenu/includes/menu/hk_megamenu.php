<?php
/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla.Site
 * @subpackage	mod_jux_hk_megamenu
 * @copyright	Copyright (C) 2008 - 2013 JoomlaUX. All rights reserved.
 * @license		License GNU General Public License version 2 or later; see LICENSE.txt, see LICENSE.php
 */ 
defined('_JEXEC') or die('Restricted access'); 

require_once dirname(__FILE__).'/hk_megamenu.tpl.php';

if (file_exists(JPATH_ADMINISTRATOR.'/components/com_hikashop/helpers/helper.php')){
	include_once(JPATH_ADMINISTRATOR.'/components/com_hikashop/helpers/helper.php');
}


class HKMegamenu {
	
	protected $children = array();
	protected $children_order = array();
	protected $_items = array();
	protected $menuHtml = '';
	protected $params = null;
	protected $settings = null;
	protected $top_level_caption =  false;
	protected $_categoriesId = array();
	protected $_isFrontEnd = false;
	
	protected $_active = null;
	
	function fectchMenu(){
		$app = JFactory::getApplication();
		$menu = $app->getMenu('site');
		$component = JComponentHelper::getComponent('com_hikashop');
		$menus = $menu->getItems('component_id',$component->id);
		return $menus;
	}
	
	function fetchItemByCategory(&$params = array()){
		$items = array();
		if (count($this->_categoriesId)){
			$user = JFactory::getUser();
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$date = JFactory::getDate();
			$now =  $date->toSql();
			$app = JFactory::getApplication();
			$nullDate = $db->getNullDate();

			$rows = array();
			foreach($this->_categoriesId as $catId){
				$query = $db->getQuery(true);
				$query->select('p.product_id AS id,p.product_name AS name,p.product_alias AS alias,pc.category_id AS catid')
					->from('#__hikashop_product AS p')
					->innerJoin('#__hikashop_product_category AS pc ON pc.product_id = p.product_id')
					->where('p.product_published = 1')
					->where('pc.category_id ='.(int)$catId)
					->order('pc.ordering DESC');
					$db->setQuery($query);
					
				if($data = $db->loadObjectList()){
					foreach($data as $d){
						$rows[$d->id.'_'.$d->catid] = $d;
					}
				}
			}
			
	        if (count($rows))
	        {
	            foreach ($rows as $row){
	            	if(empty($row->alias)){
						$row->alias = $row->name;
					}
					
					if(method_exists($app,'stringURLSafe')){
						$row->alias = $app->stringURLSafe(strip_tags($row->alias));
					}else{
						$row->alias = JFilterOutput::stringURLSafe(strip_tags($row->alias));
					}
	            	$row->link = hikashop_completeLink('product&task=show&cid='.$row->id.'&name='.$row->alias);
	            	$row->id = $row->id.'_'.$row->catid; /* $row->id.'_0'; $row->id.'_'.$row->catid*/;
					$row->parent = $row->catid;
					$row->level = 0;
					$row->isitem = true;
	            	$items[$row->id] = $row;
	            }
	        }
		}
       return $items;
	}
	function fetchCategory(&$params = array()){
		$app = JFactory::getApplication();
		$db = JFactory::getDbo();
		$user = JFactory::getUser();
		$query = $db->getQuery(true);
		$query->select('c.category_id AS id,c.category_name AS name,c.category_alias AS alias,c.category_parent_id AS parent')
			->from('#__hikashop_category AS c')
			->where('c.category_published=1')
			//->where("c.access IN(".implode(',', $user->getAuthorisedViewLevels()).")")
			->where('c.category_type = \'product\'')
			->where('c.category_id != 2')
			->order('c.category_ordering');
		$db->setQuery($query);
		$items = array();
	 	$categories = $db->loadObjectList('id');
	 	$items = array();
	 	
	 	if ($this->_isFrontEnd){
	 		if ($params->get('show_items',1)){
			 	$this->_categoriesId  = array_keys($categories);
			 	$items = $this->fetchItemByCategory($params);
	 		}
	 	}else{
	 		$this->_categoriesId  = array_keys($categories);
		 	$items = $this->fetchItemByCategory($params);
	 	}

	 	$mitems = array_merge($categories,$items);
        
	 	$children = array();
        if ($mitems)
        {
            foreach ($mitems as $v)
            {
           		if(empty($v->alias)){
					$v->alias = $v->name;
				}
				
				if(method_exists($app,'stringURLSafe')){
					$v->alias = $app->stringURLSafe(strip_tags($v->alias));
				}else{
					$v->alias = JFilterOutput::stringURLSafe(strip_tags($v->alias));
				}
            	if ($v->parent == 2)
            		$v->parent = 0;
            		
            	$v->tree = $v->id;
                $v->title = $v->name;
                $v->parent_id = $v->parent;
                $v->level = 0;
                $pt = $v->parent;
                $list = @$children[$pt] ? $children[$pt] : array();
                array_push($list, $v);
                $children[$pt] = $list;
            }
        }
        $lists = $this->treerecurse(0,array(), $children, 9999, 0);
        return $lists;
	}
	
	function treerecurse($id,$list, &$children, $maxlevel = 9999, $level = 0){
		if (@$children[$id] && $level <= $maxlevel)
		{
			foreach ($children[$id] as $v)
			{
				$id = $v->id;
				$list[$id] = $v;
				$list[$id]->children = count(@$children[$id]);
				$list[$id]->level = $level;
				$list = $this->treerecurse($id,$list, $children, $maxlevel, $level + 1);
			}
		}
		return $list;
	}
	
	function getCountItem($categoryId){
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$date = JFactory::getDate();
		$now =  $date->toSql();
		$nullDate = $db->getNullDate();
		$query->select('COUNT(*)')
			->from('#__hikashop_product AS p')
			->innerJoin('#__hikashop_product_category AS pc ON pc.product_id = p.product_id')
			->where('p.product_published = 1')
			->where('pc.category_id = '.(int)$categoryId)
			;
		$db->setQuery($query);
		return (int)$db->loadResult();
	}
	
	function parseMenu($params = array()){
		
		$uri = JUri::getInstance();
		$router = JRouter::getInstance('site');
		$vars = $router->parse($uri);
		
		$_hkMenus = array();
		$hkMenu = $this->fectchMenu();
		foreach ($hkMenu as &$mitem){
			if (isset($mitem->query['view'])){
				$view = strtolower($mitem->query['view']);
				$key = '';
				if($view == 'product') {
					$layout = $mitem->query['layout'];
					if ($layout=='show'){
						if ($mitem->params->get('product_id')){
							foreach ($mitem->params->get('product_id',array()) as $pid){
								$_hkMenus[$pid.'_0'] = $mitem;
							}
						}
					}elseif ($layout == 'listing'){
						$menuClass = hikashop_get('class.menus');
						$menuData = $menuClass->get($mitem->id);
						$_hkMenus[$menuData->hikashop_params['selectparentlisting']] = $mitem;
					}
				}elseif ($view == 'category'){
					$menuClass = hikashop_get('class.menus');
					$menuData = $menuClass->get($mitem->id);
					$_hkMenus[$menuData->hikashop_params['selectparentlisting']] = $mitem;
				}
			}
			
		}
		
		$items = array();
		$items = $this->fetchCategory($params);

		$this->params = $params;
		$this->settings =json_decode($params->get('mega_config',''),true);

		$mega_order = json_decode($params->get('mega_order',''),true);
		
		foreach ($items as &$item){
			$parent_tree = array();
			if (isset($items[$item->parent_id]))
			{
				$parent_tree  = $items[$item->parent_id]->tree;
			}
			$parent_tree[] = $item->id;
			$item->tree = $parent_tree;
			if (!isset($item->link)){
				$item->link = hikashop_completeLink('product&task=listing&cid='.$item->id.'&name='.$item->alias/*'category&task=listing&cid='.$item->id.'&name='.$item->alias*/);
			}
			$item->anchor_css = '';
			$item->anchor_title = '';
			$item->menu_image = '';
			$item->type = '';
			$item->browserNav  = 0;
			$item->level = $item->level + 1;
			
			$active = $this->getActive($vars, $item);
			$active_id = $active ? $active->id : 0 ;
			$active_tree = $active ? $active->tree : array();
			
			$itemId = $item->id;
			if (isset($item->isitem)){
				$idArr = explode('_',$itemId);
				$itemId = $idArr[0].'_0';
			}
			
			if (array_key_exists($itemId,$_hkMenus)){
				$item->type = $_hkMenus[$itemId]->type;
				$item->link = $_hkMenus[$itemId]->link.'&Itemid='.$_hkMenus[$itemId]->id;
				$item->params = $_hkMenus[$itemId]->params;
				$item->title = $_hkMenus[$itemId]->title;
				$item->browserNav = $_hkMenus[$itemId]->browserNav;			
				$item->anchor_css   = htmlspecialchars($item->params->get('menu-anchor_css', ''), ENT_COMPAT, 'UTF-8', false);
				$item->anchor_title = htmlspecialchars($item->params->get('menu-anchor_title', ''), ENT_COMPAT, 'UTF-8', false);
				$item->menu_image   = $item->params->get('menu_image', '') ? htmlspecialchars($item->params->get('menu_image', ''), ENT_COMPAT, 'UTF-8', false) : '';
					
			}
			
			$item->title        = htmlspecialchars($item->title, ENT_COMPAT, 'UTF-8', false);
			$item->num_item = $this->getCountItem($item->id);
			
			$parent = isset($this->children[$item->parent_id]) ? $this->children[$item->parent_id] : array();
			$parent[] = $item;
			
			$this->children[$item->parent_id] = $parent;
			$this->_items[$item->id] = $item;
		}
		foreach ($items as &$item){
			// bind setting for this item
			$key = 'item-'.$item->id;
			$setting = isset($this->settings[$key]) ? $this->settings[$key] : array();
			
			// decode html tag
			if (isset($setting['caption']) && $setting['caption']) $setting['caption'] = str_replace(array('[lt]','[gt]'), array('<','>'), $setting['caption']);
			if ($item->level == 1 && isset($setting['caption']) && $setting['caption']) $this->top_level_caption = true;
			
			//active and current
			$class = '';		
			if ($item->id == $active_id){
				$class .= ' current ';
			}
			if (in_array($item->id,$active_tree)){
				$class .= ' active ';
			}
			
			$item->class = $class;
			$item->mega = 0;
			$item->group = 0;
			$item->dropdown = 0;
			$item->display = 1;
			
			if (isset($setting['display'])){
				$item->display = $setting['display'];
			}

			if (isset($setting['group'])) {
					$item->group = 1;
			} else {
				if ($this->_isFrontEnd){
					if ((isset($this->children[$item->id]) && (!isset($setting['hidesub']))) || isset($setting['sub'])) {
						$item->dropdown = 1;
					}
				}else{
					if ((isset($this->children[$item->id]) ) || isset($setting['sub'])) {
							$item->dropdown = 1;
					}
				}
			}
		
			
			$item->mega = $item->group || $item->dropdown;
			
			if ($item->mega) {
			 	if (!isset($setting['sub'])) $setting['sub'] = array();
			 	if (isset($this->children[$item->id]) && (!isset($setting['sub']['rows']) || !count($setting['sub']['rows']))) {
					$c = $this->children[$item->id][0]->id;
					$setting['sub'] = array('rows'=>array(array(array('width'=>12, 'item'=>$c))));
				}
			}
			
			$oldOrder = isset($mega_order[$item->id]) ? $mega_order[$item->id] : array();
			$newOrder = isset($this->children_order[$item->id]) ? $this->children_order[$item->id] : array();
			
			if (!$this->compareOrder($oldOrder,$newOrder)){
				$setting['sub'] = array('rows'=>array(array(array('width'=>12, 'item'=>-1))));
			}
			
			$item->setting = $setting;
			$item->flink  = JRoute::_($item->link);
		}
	}
	
	function render(/*$return = false, */$params = array(),$isFrontEnd = false){
		$this->_isFrontEnd = $isFrontEnd;
		$this->parseMenu($params);

		
		$this->menuHtml = '';
		$this->menuHtml .= $this->__('beginmenu');
		$keys = array_keys($this->_items);

		if(count($keys)){	
			$this->menuHtml .= $this->nav(null, $keys[0]);
		}
		$this->menuHtml .= $this->__('endmenu');

//		
//		if ($return) {
//			return $this->menuHtml;
//		} else {
//			echo $this->menuHtml;			
//		}
		return array($this->menuHtml,json_encode($this->children_order));
	}
	
	function nav ($pitem, $start = 0, $end = 0) {
		if ($start > 0) {
			if (!isset($this->_items[$start]))
				return;
			$pid     = $this->_items[$start]->parent_id;
			$items   = array();
			$started = false;
			foreach ($this->children[$pid] as $item) {
				if ($started) {
					if ($item->id == $end)
						break;
					$items[] = $item;
				} else {
					if ($item->id == $start) {
						$started = true;
						$items[] = $item;
					}
				}
			}
			if (!count($items))
				return;
		} else if ($start === 0) {
			$pid = $pitem->id;
			if (!isset($this->children[$pid]))
				return;
			$items = $this->children[$pid];
		} else {
			//empty menu
			return;
		}
		
		$beginnav = $this->__('beginnav', array(
			'item' => $pitem,
			'show_items'=>$this->getParam('show_items',1)
		));
		$itemHtml = '';
		foreach ($items as $item) {
			$setting = $item->setting;
			if($this->_isFrontEnd && isset($setting['display']) && $setting['display'] == -1){
				continue;
			}
			$itemHtml .= $this->item($item);
		}
		
		$endnav = $this->__('endnav', array(
			'item' => $pitem
		));
		if ($itemHtml == ''){
			return '';
		}
		return $beginnav.$itemHtml.$endnav;
	}

	function item ($item,$ishkItem = false) {
		// item content
		$setting = $item->setting;
		
		
		$megaHtml = '';
		if ($item->mega) {
			$megaHtml .= $this->mega($item);
		}
		if ($megaHtml == ''){
			$item->group = 0;
			$item->dropdown = 0;	
			$item->mega = 0;
		}
		
		$html = $this->__('beginitem', array ('item'=>$item, 'setting'=>$setting));		
		
		$itemHtml = $this->__('item', array ('item'=>$item, 'setting'=>$setting));
		$html .= $itemHtml;
		if ($megaHtml != ''){
			$html .= $megaHtml;
		}
		$html .= $this->__('enditem', array ('item'=>$item));
		return $html;
	}

	protected function mega ($item) {
		
		$key       = 'item-' . $item->id;
		$setting   = $item->setting;
		$sub       = $setting['sub'];
		$items     = isset($this->children[$item->id]) ? $this->children[$item->id] : array();
		$firstitem = count($items) ? $items[0]->id : 0;
		
		
		$endItems = array();
		$k1       = $hk = 0;
		foreach ($sub['rows'] as $row) {
			foreach ($row as $col) {
				if (!isset($col['position'])) {
					if ($k1) {
						$hk = $col['item'];
						if (!isset($this->_items[$hk]) || $this->_items[$hk]->parent_id != $item->id)
							break;
						$endItems[$k1] = $hk;
					}
					$k1 = $col['item'];
				}
			}
		}

		$html = '';
		$endItems[$k1] = 0;
		$beginmega = $this->__('beginmega', array(
			'item' => $item
		));
		$firstitemscol = true;
		$rowHtml = '';
		foreach ($sub['rows'] as $row) {
			$beginrow = $this->__('beginrow');
			$colHtml = '';
			foreach ($row as $col) {
				if (isset($col['position'])) {
					$colHtml .= $this->__('begincol', array(
						'setting' => $col
					));
					$colHtml .= $this->module($col['position']);
					$colHtml .= $this->__('endcol');
				} else {
					if (!isset($endItems[$col['item']])){
						continue;
					}
					
					$begincol = $this->__('begincol', array(
						'setting' => $col
					));
					$toitem    = $endItems[$col['item']];
					$startitem = $firstitemscol ? $firstitem : $col['item'];
					$subNav = $this->nav($item, $startitem, $toitem);
					$firstitemscol = false;
					$endcol = $this->__('endcol');
					if ($subNav != ''){
						$colHtml .= $begincol.$subNav.$endcol;
					}
				}
				
			}
			$endrow = $this->__('endrow');
			if ($colHtml != ''){
				$rowHtml .= $beginrow.$colHtml.$endrow;
			}
		}
		$endmega =$this->__('endmega');
		if ($rowHtml == ''){
			return '';
		}
		return $beginmega.$rowHtml.$endmega;
	}
	
	
	function _ ($tmpl, $vars = array()) {
		$vars ['menu'] = $this;
		if (method_exists('HKMegamenuTpl', $tmpl)) {			
			$this->menuHtml .= HKMegamenuTpl::$tmpl($vars)."\n";
		} else {
			$this->menuHtml .= "$tmpl\n";			
		}
	}
	
	function __($tmpl, $vars = array()) {
		$vars ['menu'] = $this;
		if (method_exists('HKMegamenuTpl', $tmpl)) {			
			return HKMegamenuTpl::$tmpl($vars)."\n";
		} else {
			return "$tmpl\n";			
		}
	}
	
	function module($position) {
		
		if ($this->_isFrontEnd){
			$id = intval($position);
			$db = JFactory::getDbo();
			$query = $db->getQuery(true);
			$query->select('m.id, m.title, m.module, m.position, m.content, m.showtitle, m.params');
			$query->from('#__modules AS m');
			$query->where('m.id = '.$id);
			$query->where('m.published = 1');
			$db->setQuery($query);
			$module = $db->loadObject ();
			if($module && $module->id){
				$content = JModuleHelper::renderModule($module,array('style'=>'xhtml'));
				return $content."\n";
			}
		}
		return '';
	}
	
	function get ($prop) {
		if (isset($this->$prop)) return $this->$prop;
		return null;
	}
	
	function getParam ($name, $default=null) {
		if (!$this->params) return $default;
		return $this->params->get($name, $default);
	}
	
	function compareOrder($old,$new){
		foreach ($old as $k=>$v){
			if ((!isset($new[$k])) || (isset($new[$k]) && $new[$k] != $v)){
				return false;
			}
		}
		return true;
	}
	
	function getActive($vars,$item){
		
		if(is_null($this->_active)){
			if (@$vars['option'] == 'com_hikashop'){
				switch (@$vars['ctrl']){
					case 'product':
						$idArr = explode('_',$item->id);
						if (isset($item->isitem) && @$vars['task'] == 'show' && @intval($vars['cid']) == $idArr[0]){
							$this->_active =  $item;
						}
					break;
					case 'category':
						if (!isset($item->isitem) && @$vars['task'] == 'listing' && @intval($vars['cid']) == $item->id){
							$this->_active=  $item;
							
						}
					break;
				}
			}
		}
		return $this->_active;
	}
}
