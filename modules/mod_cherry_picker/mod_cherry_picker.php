<?php
defined( '_JEXEC' ) or die( 'Restricted access' );
/**
* Cherry Picker for HikaShop
*
* @package Cherry Picker for HikaShop
* @copyright Copyright � 2009-2014 Maksym Stefanchuk All rights reserved.
* @license http://www.gnu.org/licenses/gpl.html GNU/GPL
*
* http://www.galt.md
*/

ini_set('display_errors', 1);
error_reporting(E_ALL);


$cid = JRequest::getVar('cid', 0);
$displayMode = $params->get('display_mode', 0);

// if set to display only in Virtuemart
$option = JRequest::getVar('option', null);
// if ($displayMode == 1 && $option != 'com_virtuemart') return;
if ($displayMode == 1 && ($option != 'com_hikashop' ||
	$params->get('hide_in_store_root', 0) == 1 && $cid == 0)) return;


$view = JRequest::getVar('view', null);
$displayOnProductDetailsPage = $params->get('display_on_product_details_page', 0);
if ($view == 'productdetails' && !$displayOnProductDetailsPage) return;



// Collect debug info
$debugTime = (JRequest::getVar('chp','') == 'showtime') ? 1 : 0;
$debugQueries = $params->get('enable_debug');
if ($debugTime || $debugQueries) {
	$time_start = microtime(true);
	echo '<div style="color:orange;margin:15px">CP debug mode is ON</div>';
}

if ($debugQueries) {
	$db = JFactory::getDBO();
	$db->setDebug(1);
	$start_memory = memory_get_usage();

	ini_set('display_errors', 1);
	error_reporting(E_ALL);
}





require_once('defines.php');
require_once('helpers/factory.php');


$conf = CPFactory::getConfiguration();
$conf->initializeOptions($params->toArray());
$conf->set('module_id', $module->id);
$conf->set('module_name', $module->module);
$conf->set('module_url', JURI::base() .'modules/'. $module->module .'/');



require_once(CP_BASEPATH .'models/customfields.php');

$cfm = \cherrypicker\CustomfieldsModel::getInstance();
\cherrypicker\Submodule::add($cfm);



$moduleController = CPFactory::getModuleController();
$moduleController->run();



// release objects to free memory
CPFactory::releaseObjects();
unset($moduleController);
unset($conf);




// print debug info if enabled
if ($debugTime || $debugQueries) {
	$time_end = microtime(true);
	$elapsed = $time_end - $time_start;
	echo '<br />Elapsed: '. $elapsed .'<br />';
}

if ($debugQueries) {
	echo '<pre style="font-size:12px;">';
	echo "<br/>Memory peak usage: ". memory_get_peak_usage() . "<br>";
	$end_memory = memory_get_usage();
	echo "<br />Memory usage: ". ($end_memory - $start_memory);
	echo '<br />Queries made:'. $db->getTicker();
	echo '<br/>';
	print_r($db->getLog());
	echo '</pre>';
}

?>
