<?php
/**
 * @package     Cherry Picker for HikaShop
 * @subpackage  Ajax
 * @author		Maksym Stefanhuck
 * @copyright   Copyright (C) 2009 - 2014 Galt.md. All rights reserved.
 * @license     GNU General Public License version 2 or later; see COPYING.txt
 */

defined( '_JEXEC' ) or die( 'Restricted access' );

// define('JPATH_BASE', dirname(dirname(dirname(dirname(__FILE__)))));
define('JPATH_BASE', dirname(dirname(dirname(dirname($_SERVER['SCRIPT_FILENAME'])))));
define('DS', DIRECTORY_SEPARATOR );
require(JPATH_BASE.DS.'includes'.DS.'defines.php');
require(JPATH_BASE.DS.'includes'.DS.'framework.php');

$app = JFactory::getApplication('site');
