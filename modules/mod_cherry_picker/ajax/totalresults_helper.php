<?php
/**
 * @package     Cherry Picker for HikaShop
 * @subpackage  Ajax
 * @author		Maksym Stefanhuck
 * @copyright   Copyright (C) 2009 - 2014 Galt.md. All rights reserved.
 * @license     GNU General Public License version 2 or later; see COPYING.txt
 */

defined( '_JEXEC' ) or die( 'Restricted access' );


$conf->set('use_seemore_ajax', 0);

$ptids_str = JRequest::getVar('ptids', null);
$productTypeIDs = explode('|', $ptids_str);

$filterDataModel = CPFactory::getFilterDataModel();
$filterDataModel->initFiltersDataLimited($productTypeIDs);

if (!$filterDataModel->thereAreFiltersToShow()) return;

echo $filterDataModel->getTotalProductsCount();
