<?php
/**
 * @package     Cherry Picker for HikaShop
 * @subpackage  Ajax
 * @author		Maksym Stefanhuck
 * @copyright   Copyright (C) 2009 - 2014 Galt.md. All rights reserved.
 * @license     GNU General Public License version 2 or later; see COPYING.txt
 */

// Set flag that this is a parent file
define('_JEXEC', 1 );
// To setisfy JED checkers
defined('_JEXEC') or die;

require('jinit.php');
require('helper.php');
