var cpDropdownLayoutEvents = function(_moduleData) {

	this.moduleData = _moduleData;
	this.toggleFiltersTimeout = [];
	this.delayTimeout = 300;

	this.init = function() {
		var obj = this,
			moduleContainer = obj.moduleData.moduleContainer;

		moduleContainer.addEvents({

			'click:relay(.cp-dd-filter-group-button)': function() {
				obj.processFilterButtonClickEvent(this);
			},

			'mouseenter:relay(.cp-dd-filter-group-button)': function() {
				obj.processFilterButtonMouseEnterEvent(this);
			},


			'mouseleave:relay(.cp-dd-filter-group-button)': function() {
				obj.processFilterButtonMouseLeaveEvent(this);
			},

			'mouseenter:relay(.cp-dd-filter-group)': function() {
				obj.processFilterListMouseEnterEvent(this);
			},

			'mouseleave:relay(.cp-dd-filter-group)': function() {
				obj.processFilterListMouseLeaveEvent(this);
			}

		});

		if (this.moduleData.updateProducts) {
			moduleContainer.addEvents({
				'click:relay(.cp-dd-filter-link)': function(event) {
					event.preventDefault();
					obj.processLinkClickEvent(this);
				},

				'click:relay(.cp-clearlink)': function(event) {
					event.preventDefault();
					obj.processLinkClickEvent(this);
				},

				'click:relay(.cp-price-clear)': function(event) {
					event.preventDefault();
					obj.processLinkClickEvent(this);
				},

				'submit:relay(form)': function(event) {
					event.preventDefault();

					obj.submitFiltersForm();
				}
			});
		}
	}


	this.processLinkClickEvent = function(clickedFilter) {
		var locationURL = clickedFilter.getAttribute('href');
		cpUpdateResutsViaAjaxObj.updateResults(locationURL);
	}


	this.processFilterButtonClickEvent = function(clickedButton) {
		var index = document.id(clickedButton).getAttribute('data-id');

		this.moduleData.moduleContainer.getElements('.cp-dd-filter-group').addClass('hid');

		clearTimeout(this.toggleFiltersTimeout[index]);
		this.toggleFiltersTimeout[index] = null;

		this.showFilters(clickedButton);
	}

	this.processFilterButtonMouseEnterEvent = function(activeElement) {
		var index = document.id(activeElement).getAttribute('data-id'),
			obj = this;

		if (this.toggleFiltersTimeout[index]) {
			clearTimeout(this.toggleFiltersTimeout[index]);
			this.toggleFiltersTimeout[index] = null;
		} else {
			var button = activeElement;
			this.toggleFiltersTimeout[index] = setTimeout(function() {obj.showFilters(button)}, obj.delayTimeout);
		}
	}

	this.processFilterButtonMouseLeaveEvent = function(activeElement) {
		var index = document.id(activeElement).getAttribute('data-id'),
			obj = this;

		if (document.id(index).hasClass('hid') == false) {
			this.toggleFiltersTimeout[index] = setTimeout(function() {obj.hideFilters(index)}, obj.delayTimeout);
		} else {
			clearTimeout(this.toggleFiltersTimeout[index]);
			this.toggleFiltersTimeout[index] = null;
		}
	}

	this.processFilterListMouseEnterEvent = function(activeElement) {
		var index = document.id(activeElement).id;

		clearTimeout(this.toggleFiltersTimeout[index]);
		this.toggleFiltersTimeout[index] = null;
	}

	this.processFilterListMouseLeaveEvent = function(activeElement) {
		var index = document.id(activeElement).id,
			obj = this;

		this.toggleFiltersTimeout[index] = setTimeout(function() {obj.hideFilters(index)}, obj.delayTimeout);
	}


	this.showFilters = function(filterButton) {
		var buttonContainer = document.id(filterButton),
			id = buttonContainer.getAttribute('data-id'),
			filtersContainer = document.getElementById(id),
			left = buttonContainer.offsetLeft,
			top = buttonContainer.offsetTop + document.id(buttonContainer).getHeight();

		if (this.moduleData.loadDropdownFiltersWithAjax && filtersContainer.getAttribute('data-loaded') == 0)
			this.loadFilters(filtersContainer);

		filtersContainer.removeClass('hid');
		filtersContainer.setStyles({top: top, left: left});
	}

	this.hideFilters = function(index) {
		this.toggleFiltersTimeout[index] = null;
		document.id(index).addClass('hid');
	}


	this.submitFiltersForm = function() {
		if (this.moduleData.updateProducts) {
			var url = this.getAppliedRefinementsURL();
			cpUpdateResutsViaAjaxObj.updateResults(url);
		} else {
			var groupInputs = this.moduleData.moduleContainer.getElements('.hidden-filter'),
				form = this.moduleData.moduleContainer.getElement('form');

			groupInputs.each(function(groupInput) {
				if (groupInput.value == '') groupInput.destroy();
			});

			var lowPrice = form['low-price'],
				highPrice = form['high-price'];
			if (lowPrice && lowPrice.value == "") lowPrice.setProperty('disabled', true);
			if (highPrice && highPrice.value == "") highPrice.setProperty('disabled', true);

			form.submit();
		}
	}


	this.getAppliedRefinementsURL = function() {
		var url = this.moduleData.moduleContainer.getElement('#cp' +
				this.moduleData.moduleID  + '_base_url').getAttribute('data-value'),
				//this.moduleData.moduleID  + '_base_url_with_filters').getAttribute('data-value'),
			groupInputs = this.moduleData.moduleContainer.getElements('.hidden-filter'),
			appliedStaticFilterInputs = this.moduleData.moduleContainer.getElements('.hidden-static-filter'),
			form = this.moduleData.moduleContainer.getElement('form'),
			selection = [],
			lowPrice = form['low-price'],
			highPrice = form['high-price'];


		groupInputs.each(function(groupInput) {
			if (groupInput.value) selection.push(groupInput.name + '=' + encodeURIComponent(groupInput.value));
		});

		appliedStaticFilterInputs.each(function(input) {
			if (input.value) selection.push(input.name + '=' + encodeURIComponent(input.value));
		});


		if (lowPrice && lowPrice.value) selection.push('low-price=' + lowPrice.value);
		if (highPrice && highPrice.value) selection.push('high-price=' + highPrice.value);

		if (selection.length) {
			var uri = new URI(url);
			var query = uri.get('query');
			if (query != "") query += '&';
			query += selection.join('&');
			uri.set('query', query);

			url = uri.toAbsolute();
		}

		return url;
	}

 }
