<?php
/*
 * Helper class to manage html tags in a way similar to jQuery.
 *
 * Author Maksym Stefanchuk.
 */

namespace cherrypicker;
defined('_JEXEC') or die;

class html {
	public static $single_tags = array('input','img','hr','br','meta','link');


	public static function tag($nodename) {
		return new HtmlTag($nodename);
	}

}


class HtmlTag {
	private $nodename;
	private $attributes;
	private $text;
	private $childNodes = array();

	function __construct($nodename) {
		$this->nodename = $nodename;
		$this->text = '';
	}

	public function attr($name, $value = null) {
		if ($value === null) {
			return (isset($this->attributes[$name]) ? $this->attributes[$name] : null);
		} else {
			$this->attributes[$name] = $value;
			return $this;
		}
	}

	public function html($value = null) {
		if ($value === null) {
			//return (isset($this->attributes['text']) ? $this->attributes['text'] : '');
			return $this->text;
		} else {
			//$this->attributes['text'] = $value;
			$this->text = $value;
			return $this;
		}
	}

	public function addClass($name) {
		if (gettype($name) == "string" && $name) {
			$classes = explode(" ", $name);
			$orig = isset($this->attributes['class']) ? $this->attributes['class'] : '';
			$cur = $orig ? ' '. $orig .' ' : ' ';
			//$cur = isset($this->attributes['class']) ? ' '. $this->attributes['class'] .' ' : ' ';
			foreach ($classes as $class) {
				if ($class && strpos($cur, ' '. $class .' ') === false)
					$cur .= $class .' ';
			}
			$final = trim($cur);
			if ($final !== $orig)
				$this->attributes['class'] = $final;
		}
		return $this;
	}

	/* remove an attribute */
	//function remove($att)
	//{
	//	if(isset($this->attributes[$att]))
	//	{
	//		unset($this->attributes[$att]);
	//	}
	//}

	/* clear */
	//function clear()
	//{
	//	$this->attributes = array();
	//}


	public function append(HtmlTag $tag) {
		if (@get_class($tag) == __class__) {
			//$this->attributes['text'] .= $tag->build();
			//$this->text .= $tag->build();
			$this->childNodes[] = $tag;
		}
		return $this;
	}

	public function appendTo(HtmlTag $tag) {
		$tag->append($this);
		return $this;
	}

	public function prepend(HtmlTag $tag) {
		if (@get_class($tag) == __class__) {
			array_unshift($this->childNodes, $tag);
		}
		return $this;
	}

	public function prependTo(HtmlTag $tag) {
		$tag->prepend($this);
		return $this;
	}

	public function output() {
		echo $this->build();
	}

	function __toString() {
		return $this->build();
	}


	public function toString() {
		return $this->build();
	}

	public function openingTag() {
		return $this->build(true);
	}

	public function closingTag() {
		if (in_array($this->nodename, html::$single_tags))
			return '';
		else
			return '</'. $this->nodename .'>';
	}

	private function build($opening = false) {
		$html = '<'. $this->nodename;
		if (count($this->attributes)) {
			foreach($this->attributes as $key => $value)
				if ($value !== '' && $key != 'text')
					$html .= ' '. $key .'="'. $value .'"';
		}

		if (! in_array($this->nodename, html::$single_tags)) {
			$html .= '>';
			if (! $opening) {
				$html .= $this->text;
				foreach ($this->childNodes as $node) $html .= $node->build();
				$html .= '</'. $this->nodename .'>';
			}
		} else {
			$html .= ' />';
		}

		return $html;
	}


	function __call($name, $arguments) {
		//var_dump($name, $arguments);
		$allowed_overloads = array("href");
		if (in_array($name, $allowed_overloads))
			return $this->attr($name, ($arguments ? $arguments[0] : null));
	}
}
