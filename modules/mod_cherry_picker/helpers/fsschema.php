<?php
/**
 * @package     Cherry Picker for HikaShop
 * @subpackage  FastSeller schema
 * @author		Maksym Stefanhuck
 * @copyright   Copyright (C) 2009 - 2014 Galt.md. All rights reserved.
 * @license     GNU General Public License version 2 or later; see COPYING.txt
 */

defined('_JEXEC') or die('Restricted access');

class FSSchema {

	static public function tbl_parameter() {
		return  "`#__fastseller_hs_product_type_parameter`";
	}

	static public function tbl_producttype() {
		return "`#__fastseller_hs_product_type`";
	}

	static public function tbl_producttype_id($ptid) {
		return "`#__fastseller_hs_product_type_$ptid`";
	}

}

?>
