<?php
/**
 * @package     Cherry Picker for Virtuemart
 * @subpackage  Submodule View Interface
 * @author		Maksym Stefanhuck
 * @copyright   Copyright (C) 2009 - 2014 Galt.md. All rights reserved.
 * @license     GNU General Public License version 2 or later; see COPYING.txt
 */

namespace cherrypicker;
defined('_JEXEC') or die;

use \JHtml;

interface SubmoduleViewInterface {

	public function display();
	public function js();
	public function displayName();
	public function hasDisplay();
	public function hasJs();
	//public function model();		// model that is associated with view
	public function title();
	public function name();
	public function urlname();
	public function units();
	public function template();
	public function type();
	public function collapsed();
	public function useDropdownAjax();
	public function popupWidth();
	public function uniqueViewId();
	public function showValueText();
	public function showValueImage();
	public function removeValueUrl($value);
	public function removeValuesUrl();
	//public function selecttype();
	//public function showEmptyValues();
	//public function showCount();
	//public function option($name, $default);
	//public function availableValues($withCount, $url, $limit, $selecttype, $sort, array $only_submodules);
	public function selectedValues();
	public function availableValues();
	//public function option($name);

}


class SubmoduleView {

	/*
	 * Helper methods to include CSS and Script files.
	 * It will make sure to include same file just once.
	 * All files could be overriden via template:
	 * http://docs.joomla.org/Understanding_Output_Overrides#Media_Files_Override
	 */
	public function addStylesheet($file, $relative = true) {
		if ($relative) {
			$path_only = true;
			$override = JHtml::stylesheet('mod_cherry_picker/'. $file, false, true, $path_only);
			//var_dump($override);
			if ($override)
				JHtml::stylesheet('mod_cherry_picker/'. $file, false, true, false);
			else
				JHtml::stylesheet('modules/mod_cherry_picker/css/'. $file);
		} else {
			JHtml::stylesheet($file);
		}
	}

	public function addScript($file, $relative = true) {
		if ($relative) {
			$path_only = true;
			$override = JHtml::script('mod_cherry_picker/'. $file, false, true, $path_only);
			//var_dump($override);
			if ($override)
				JHtml::script('mod_cherry_picker/'. $file, false, true, false);
			else
				JHtml::script('modules/mod_cherry_picker/js/'. $file);
		} else {
			JHtml::script($file);
		}
	}

}
