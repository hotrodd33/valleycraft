<?php
/**
 * @package     Cherry Picker for Virtuemart
 * @subpackage  Form
 * @author		Maksym Stefanhuck
 * @copyright   Copyright (C) 2009 - 2014 Galt.md. All rights reserved.
 * @license     GNU General Public License version 2 or later; see COPYING.txt
 */

namespace cherrypicker;
defined('_JEXEC') or die;

require_once(CP_BASEPATH .'helpers/html.php');

use \JText;

//$config = $this->module->config();
//$module_id = $this->module->id();
$config = \CPFactory::getConfiguration();
$module_id = $config->get('module_id');

$show_submit_button = $config->get('submitbutton_show');

$onsubmit_json = $config->get('submitbutton_onsubmit');
$onsubmit = ($onsubmit_json) ? json_decode($onsubmit_json, true) : array();
$update_products = (int)in_array('products', $onsubmit);
$update_filters = (int)in_array('filters', $onsubmit);



if ($show_submit_button) {
	$products_count = $this->module->matchedProductsCount();
	if ($products_count == 1)
		$text = JText::_($config->get('string_submitbutton_single', 'CP_SUBMITBUTTON_SINGLE'));
	else
		$text = JText::sprintf($config->get('string_submitbutton', 'CP_SUBMITBUTTON'), $products_count);

	$submitbutton = html::tag('button')
					->attr('type', 'submit')
					->attr('class', 'cp-submitbutton')
					->html($text);
}


echo '<div id="cp'. $module_id .'_module">';
echo '<form>';

foreach ($displayViews as $view) {
	if ($view->hasDisplay()) $view->display();
	if ($view->hasJs()) $view->js();
}


if ($show_submit_button)
	echo html::tag('div')->append($submitbutton);

echo 'total: '. $this->module->matchedProductsCount();
echo '</form>';

?>
<script>
jQuery(function($) {
	var multimodule_isolated_mode = <?php echo CP_MULTIMODULE_ISOLATED_MODE ?>;
	var update_products = <?php echo $update_products ?>;
	var update_filters = <?php echo $update_filters ?>;
	var module = CherryPicker.module.makeContext(<?php echo $module_id ?>);

	module.content().find("form").submit(function(event, data) {
		event.preventDefault();
		if (data == undefined) {
			data = getData();
		}

		var url = CherryPicker.location.base + (data ? '&' + data : '');
		if (!update_products && !update_filters) {
			location.href = url;
		} else {
			var updater = CherryPicker.updater;
			if (update_filters) {
				updater.updateModule({
					content: module.content(),
					data: 'module_id=' + module.id() + '&' + data,
					//showLoader: true
					loader: {
						relativeTo: $(this).find('.cp-submitbutton')
					}
				});
			}
			if (update_products) {
				updater.updateProducts({
					content: $("#main"),
					url: url,
					showLoader: true,
					data: data,
					module_id: module.id(),
					triggerUpdateEvent: update_filters
				});
			}
		}
	});


	function getData() {
		var data = [],
			s;

		if (! multimodule_isolated_mode) {
			var has_manufacturer = module.submodules('manufacturer').length;
			var has_category = module.submodules('category').length;
			var modules = CherryPicker.module.modules;
			for (var i = 0; i < modules.length; ++i)
				if (modules[i] != module) {
					var exclude = [];
					if (has_manufacturer && ( manufacturer = modules[i].submodules('manufacturer')[0] ))
						exclude.push(manufacturer);
					if (has_category && ( category = modules[i].submodules('category')[0] ))
						exclude.push(category);
					s = modules[i].toString({
						exclude_submodules: exclude,
						urlencode: true,
						originalState: true
					});
					if (s) data.push(s);
				}
		}

		s = module.toString({
			urlencode: true
		});
		if (s) data.push(s);

		return data.join('&');
	}

});
</script>
<?php

echo '</div>';

?>
