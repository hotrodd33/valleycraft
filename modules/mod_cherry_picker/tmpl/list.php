<?php
/**
 * @package     Cherry Picker for Virtuemart
 * @subpackage  List Template
 * @author		Maksym Stefanhuck
 * @copyright   Copyright (C) 2009 - 2014 Galt.md. All rights reserved.
 * @license     GNU General Public License version 2 or later; see COPYING.txt
 */

namespace cherrypicker;
defined('_JEXEC') or die;

use \JModuleHelper;


//require_once(CP_BASEPATH .'helpers/html.class.php');
require_once(CP_BASEPATH .'helpers/html.php');

//$config = Factory::getConfig();
//$module_name = $config->get('module_name');
//$config = CherryPicker::contextModule()->config();
$config = \CPFactory::getConfiguration();
//$module_name = CherryPicker::contextModule()->name();
$module_name = $config->get('module_name');
//$translate = $config->get('translate');
//$template = $this->option('template');
//$collapsed = $this->option('collapsed');
$template = $this->template();
$collapsed = $this->collapsed();
$uniqueId = $this->uniqueViewId();
$selectedValues = $this->selectedValues();

//echo ' list ';
//echo $this->name();

//$text = $translate ? JText::_($this->title()) : $this->title();
$text = $this->title();

//if ($collapsed != CP_TOGGLE_COLLAPSE_NONE)
//	$id = $uniqueId .'_collapse_handle';
//else
//	$id = '';


if ($text) {
	$title = html::tag('h2')->html($text)->attr('class', 'cp-list-title');
	if ($collapsed != CP_TOGGLE_COLLAPSE_NONE)
		$title->attr('id', $uniqueId .'_collapse_handle');
	//echo html::tag('div')->attr('style', 'padding-bottom:3px')->append($title);
	echo $title;
}

//$title->attr('id', 'hello');
//$title->attr('id', '');
//var_dump( $title->attr('id') );
//echo '<pre>';
//print_r($title);
//echo $title;




//echo '<h2';
//if ($id) echo 'id="'. $id .'"';
//echo '>';
//echo $text;
//echo '</h2>';


if ($collapsed != CP_TOGGLE_COLLAPSE_NONE)
	echo '<div id="'. $uniqueId .'_collapse_content">';

require(JModuleHelper::getLayoutPath($module_name, ($template ? $template : $this->type())));

if ($collapsed != CP_TOGGLE_COLLAPSE_NONE)
	echo '</div>';

//echo '<pre>';
//$div = html::tag('div')->html('div');
//$div->addClass('hello');
//$div->addClass('metwo');
//$div->addClass('hello');
//print_r( $div );
//echo $div;




//$a = html::tag('a')->href('index.php?sdflj')->html('link')->appendTo($div);
//$b = html::tag('b')->html('bold')->appendTo($a);
//print_r($div);
//echo $div;


//$a = html::tag('a')->html('aaaa');
//$a->href('index.php?somelink');
//echo $a;
//echo $a->href();

//$a = html::tag('div')->html('aaaa');
//$b = html::tag('div')->html('bbb')->appendTo($a);
//echo $a;
//var_dump($b);

//$attributes = array("id" => $uniqueId .'_collapse_handle');
//$this->tag('h2', $text, $attributes);

//$html = \html::tag('a')->href('#')->html('some content');
//$html = \html::tag('a');
//$html->html('some contentasdf')->attr('href', '/index.php?hello=yes');
////var_dump($html);
//
//$b = \html::tag('b');
//$b->html('some content')->attr('style', 'color:red');
//
//$html->append($b);
//echo $html;

if ($collapsed != CP_TOGGLE_COLLAPSE_NONE) {

?>
<script>
jQuery(function($) {
	CherryPicker.new("collapsible", {
		handle: $("#<?php echo $uniqueId ?>_collapse_handle"),
		content: $("#<?php echo $uniqueId ?>_collapse_content"),
		collapsed: <?php echo ($collapsed == CP_TOGGLE_COLLAPSE_COLLAPSED
								&& !$selectedValues) ? 'true' : 'false' ?>,
		indicatorOpen: '&#xe60a;',
		indicatorClosed: '&#xe609;',
		toggle: function() {
			console.log('toggle. send event here');
		}
	});
});
</script>
<?php

}

?>
