<?php
/**
 * @package     Cherry Picker for HikaShop
 * @subpackage  Select Template
 * @author		Maksym Stefanhuck
 * @copyright   Copyright (C) 2009 - 2014 Galt.md. All rights reserved.
 * @license     GNU General Public License version 2 or later; see COPYING.txt
 */

namespace cherrypicker;
defined('_JEXEC') or die;

use \JHtml;
use \JText;

//$config = Factory::getConfig();
//$config = CherryPicker::contextModule()->config();
//$module_id = CherryPicker::contextModule()->id();
$config = \CPFactory::getConfiguration();
$module_id = $config->get('module_id');
$translate = $config->get('translate');
// todo: string 'Select'
$string_select = $config->get('string_select', 'CP_SELECT');
$units = $this->units();

$availableValues = $this->availableValues();
$selectedValues = $this->selectedValues();
//$selectedValueNames = array_map(function($v) {
//	return $v['name'];
//}, $selectedValues);
$selectedValueAliases = array_map(function($v) {
	return $v['alias'];
}, $selectedValues);
$uniqueId = $this->uniqueViewId();

//echo '<pre>';
//print_r($availableValues);

// todo: remove legacy class
$attributes = 'autocomplete="off" class="cp-filter-select"';
$id = $uniqueId .'_select';
$options = array();
$options[] = JHtml::_('select.option', '', JText::sprintf($string_select, $this->title()) );
foreach ($availableValues as $value) {
	$disabled = ($this->option('show_empty_values') && $value['count'] == 0) ? true : false;
	$text = $translate ? JText::_($value['name']) : $value['name'];
	if ($units)
		$text .= $units;
	if ($this->option('show_count'))
		$text .= ' ('. $value['count'] .')';
	$options[] = JHtml::_('select.option', $value['alias'], $text, 'value', 'text', $disabled);
}
$html = JHtml::_('select.genericlist', $options, $this->urlname(), $attributes, 'value', 'text', $selectedValueAliases, $id);

echo $html;

/*
?>
<script>
jQuery(function($) {
	var multimodule_isolated_mode = <?php echo CP_MULTIMODULE_ISOLATED_MODE ?>;
	var ignore_category = <?php echo (int)$config->get('category_ignored_by_others') ?>;
	var select = $("#<?php echo $uniqueId ?>_select");
	var module = CherryPicker.module.makeContext(<?php echo $module_id ?>);
	//var module = CherryPicker.module.contextModule();
	var submodule = CherryPicker.new("submodule", {
		name: "<?php echo $this->name() ?>",
		urlname: "<?php echo $this->urlname() ?>",
		selectedValues: [<?php echo ($selectedValueAliases) ? "'". implode("', '", $selectedValueAliases) ."'" : '' ?>]
	});
	if (( submodules = module.submodules(submodule.name()) ) && submodules.length)
		module.replaceSubmodule(submodules[0], submodule);
	else
		module.addSubmodule(submodule);


	function getData() {
		var data = [],
			s;

		if (! multimodule_isolated_mode) {
			var has_manufacturer = module.submodules('manufacturer').length;
			var has_category = module.submodules('category').length;
			var modules = CherryPicker.module.modules;
			for (var i = 0; i < modules.length; ++i)
				if (modules[i] != module) {
					var exclude = [];
					if (has_manufacturer && ( manufacturer = modules[i].submodules('manufacturer')[0] ))
						exclude.push(manufacturer);
					if (has_category && ( category = modules[i].submodules('category')[0] ))
						exclude.push(category);
					s = modules[i].toString({
						exclude_submodules: exclude,
						urlencode: true,
						originalState: true
					});
					if (s) data.push(s);
				}
		}

		var exclude = [submodule];
		if (ignore_category && ( category = module.submodules('category') ))
			exclude.push(category[0]);

		s = module.toString({
			exclude_submodules: exclude,
			urlencode: true,
			originalState: true
		});
		if (s) data.push(s);

		s = submodule.toString({urlencode: true});
		if (s) data.push(s);
		//if (s) data += data ? '&' + s : s;
		return data.join('&');
	}

	select.change(function() {
		var $this = $(this);
		submodule.selectedValues($this.val());
<?php

	$update_products = $this->option('onselect_update_products', 0);
	$update_filters = $this->option('onselect_update_filters', 0);
	if ($update_products || $update_filters) {

?>
		//var data = module.toString({
		//	exclude_submodules: [submodule],
		//	originalState: true,
		//	urlencode: true
		//});

		//var s = submodule.toString({urlencode: true});
		//if (s) data += data ? '&' + s : s;

		var data = getData();
		var update_products = <?php echo $update_products ?>;
		var update_filters = <?php echo $update_filters ?>;
		var updater = CherryPicker.updater;
		if (update_filters) {
			updater.updateModule({
				content: module.content(),
				data: 'module_id=' + module.id() + '&' + data,
				//showLoader: false
				loader: {
					relativeTo: this
				}
			});
		}

		if (update_products) {
			updater.updateProducts({
				content: $("#main"),
				url: CherryPicker.location.base + (data ? '&' + data : ''),
				showLoader: true,
				data: data,
				module_id: module.id(),
				triggerUpdateEvent: update_filters
			});
		}
<?php

	}

?>
	});
});
</script>

<?php

*/

?>
