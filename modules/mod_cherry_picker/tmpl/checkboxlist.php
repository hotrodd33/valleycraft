<?php
/**
 * @package     Cherry Picker for HikaShop
 * @subpackage  Checkboxlist Layout
 * @author		Maksym Stefanhuck
 * @copyright   Copyright (C) 2009 - 2014 Galt.md. All rights reserved.
 * @license     GNU General Public License version 2 or later; see COPYING.txt
 */

defined( '_JEXEC' ) or die( 'Restricted access' );

$conf = CPFactory::getConfiguration();
$filterDataModel = CPFactory::getFilterDataModel();
$manufacturers = CPFactory::getManufacturersDataModel();
$totalProductsCount = $filterDataModel->getTotalProductsCount();

$inquiringWithAjax = $conf->get('ajax_request');
if ($inquiringWithAjax)
	$this->loadJavascript = false;


// $showNFilters = false;
// $useSeeMoreAjax = false;
// if ($conf->get('use_seemore')) {
// 	if ($conf->get('use_seemore_ajax')) {
// 		$useSeeMoreAjax = true;
// 	} else {
// 		$showNFilters = true;
// 		$show_before = $conf->get('b4seemore');
// 	}

// 	$seeMoreAnchor = $conf->get('smanchor');
// 	$this->loadSeeMoreJavascript = true;
// }

$hideFilters = $conf->get('hide_filters');
$useSeeMore = ($hideFilters == CP_HIDE_FILTERS_USING_SEEMORE);
$useSeeMoreAjax = $conf->get('use_seemore_ajax');
$showBeforeSeeMore = $conf->get('b4seemore');
$seeMoreAnchor = $conf->get('smanchor');
if ($useSeeMore)
	$this->loadSeeMoreJavascript = true;

$useScroll = ($hideFilters == CP_HIDE_FILTERS_USING_SCROLL);
$scrollHeight = $conf->get('scroll_height');


$show_clearlink = $conf->get('show_clearlink');
$moduleID = $conf->get('module_id');
$useQuickrefine = $conf->get('use_quickrefine');
$quickrefineManufacturers = ($useQuickrefine && $conf->get('quickrefine_manufacturers'));

echo '<div class="cp-filter-checkboxlist" id="cpFilters'. $moduleID .'">';

$this->printFiltersTitle();
$this->printFormStart();

$printPriceForm = false;
// $filterDataModel->showPriceFormForCurrentUser();
if ($conf->get('use_price_search')) {
	if ($conf->get('price_position') == CP_PRICE_TOP) {
		$this->printPriceForm();
	} else {
		$printPriceForm = true;
	}
}

$collapseClass = '';
if ($useCollapse = $conf->get('use_collapse')) {
	$collapseClass = ' cp-collapse';
	$this->loadCollapseJavascript = true;
}

$translate = $conf->get('translate');
$hideParamsWith1Filter = $conf->get('hide_params_with1filter');
$showCount = ($conf->get('filter_count') == PROD_COUNT_SHOW) ? true : false;



// --------------------------------------------------------------------
// Show available FILTERS
// --------------------------------------------------------------------

//echo '<pre style="font-size:13px;">';
//print_r($filtersCollection);

foreach ($filtersCollection as $i => $productType) {
	$filterDataModel->setPTIndex($i);

	if ($conf->get('show_pt_title'))
		echo '<h2 class="cp-group-parent">'. $filterDataModel->currentPTTitle() .'</h2>';

	foreach ($productType as $j => $parameter) {
		$filterDataModel->setParameterIndex($j);

		$parameterMode = $filterDataModel->currentParameterAttribute('mode');
		if ($filterDataModel->currentParameterIsTrackbar()) {
			$this->printTrackbarParameter($parameter);
			continue;
		}

		// skip parameters that end up with 1 filter
		if ($hideParamsWith1Filter && count($parameter['filters']) < 2)
			continue;


		if ($parameterMode == CP_COLOR_PALETTE_PARAMETER) {
			$this->printColorPaletteParameter($parameter);
			continue;
		}

		$parameterHiding = $filterDataModel->currentParameterAttribute('hiding_filters');
		if ( !$this->loadSeeMoreJavascript && $parameterHiding == CP_PARAMETER_HIDE_USING_SEEMORE) {
			$this->loadSeeMoreJavascript = true;
		}

		$parameterSeeMoreSize = $filterDataModel->currentParameterAttribute('see_more_size');
		$parameterShowBeforeSeeMore = ($parameterHiding == CP_PARAMETER_HIDE_USING_SEEMORE && $parameterSeeMoreSize) ?
			$parameterSeeMoreSize : $showBeforeSeeMore;


		$add_seemore_handle = false;
		$appliedFilters = $filterDataModel->currentParameterAppliedFilters();

		echo '<div>';

		$parameterName = $filterDataModel->currentParameterName();
		echo '<input type="hidden" name="'. $parameterName .'" value="'.
			htmlentities($appliedFilters, ENT_QUOTES, "UTF-8") .'" class="hidden-filter" />';

		// header
		// $groupCollapseState = $filterDataModel->currentParameterCollapseState();
		$groupCollapseState = $filterDataModel->currentParameterAttribute('collapse');
		$appliedAttr = ($appliedFilters) ? ' applied="1"' : '';
		echo '<h2 class="cp-chkb-group-header'. $collapseClass .'"'. $appliedAttr .'
			data-name="'. $parameterName .'"';
		if ($useCollapse)
			echo ' data-default="'. $groupCollapseState .'"';
		echo '>';
		if ($useCollapse) {
			echo '<span class="cp-group-header-state">';
			// echo (!$appliedFilters && $groupCollapseState != 2 && $conf->get('default_collapsed')) ? '+' : '-';
			echo (!$appliedFilters && ($groupCollapseState == CP_COLLAPSE_GROUP_YES ||
					$conf->get('default_collapsed') && $groupCollapseState == CP_COLLAPSE_GROUP_GLOBAL)) ? '[+]' : '[-]';
			echo '</span>';
		}
		$parameterTitle = $filterDataModel->currentParameterTitle();
		if ($translate)
			$parameterTitle = JText::_($parameterTitle);
		echo '<span class="cp-chkb-group-title">'. $parameterTitle .'</span>';
		echo '</h2>';

		echo '<div>';

		/* These dedicated class names will be used for filters quickrefine feature,
			so they must be in place */
		// $quickrefineParameter = ($useQuickrefine && $filterDataModel->currentParameterShowQuickrefine());
		$quickrefineParameter = ($useQuickrefine && $filterDataModel->currentParameterAttribute('show_quickrefine'));
		if ($quickrefineParameter) {
			$qrFilterClass = ' class="cp-qr-filter"';
			$qrFilterParentClass = ' class="cp-qr-filter-parent"';
			$this->printQuickrefineFieldForGroup($parameterName, $appliedFilters);
		} else {
			$qrFilterClass = '';
			$qrFilterParentClass = '';
		}

		$parameterUseScroll = ($parameterHiding == CP_PARAMETER_HIDE_USING_SCROLL
			|| $useScroll && $parameterHiding == CP_PARAMETER_HIDING_GLOBAL);

		echo '<div id="cp'. $moduleID .'_group_'. $parameterName .'" class="cp-chkb-filter-group">';
		if ($parameterUseScroll) {
			$height = ($parameterHiding == CP_PARAMETER_HIDE_USING_SCROLL
				&& ($v = $filterDataModel->currentParameterAttribute('scroll_height'))) ? $v : $scrollHeight;
			echo '<div class="cp-chkb-scroll-box" style="max-height:'. $height .'px">';
		}
		echo '<div class="cp-chkb-padding-cont"><ul class="cp-chkb-list">';

		$units = $filterDataModel->currentParameterUnits();


		// list filters

		if ($show_clearlink && $appliedFilters) {
			echo '<li><a href="'. $parameter['xurl'] .'" class="cp-clearlink">'. $conf->get('clear') .'</a></li>';
		}
		foreach ($parameter['filters'] as $k => $filter) {
			// regular See More.. (without Ajax)
			// if ($showNFilters && $k == $show_before && $parameterUseSeeMore) {
			// 	echo '</ul>';
			// 	if ($seeMoreAnchor == CP_ANCHOR_TOP) {
			// 		$this->printSeeMore($parameterName, 'filter');
			// 	} else {
			// 		$add_seemore_handle = true;
			// 	}
			// 	echo '<ul class="cp-chkb-list hid">';
			// }

			if (!$useSeeMoreAjax && ($parameterHiding == CP_PARAMETER_HIDE_USING_SEEMORE
				|| $useSeeMore && $parameterHiding == CP_PARAMETER_HIDING_GLOBAL)
				&& $k == $parameterShowBeforeSeeMore) {
				echo '</ul>';
				if ($seeMoreAnchor == CP_ANCHOR_TOP) {
					$this->printSeeMore($parameterName, 'filter');
				} else {
					$add_seemore_handle = true;
				}
				echo '<ul class="cp-chkb-list hid">';
			}


			$filterName = ($translate) ? JText::_($filter['name']) : $filter['name'];
			if ($units) $filterName .= $units;

			$checked = ($filter['applied']) ? ' checked' : '';

			$dataFilter = '';
			if ($quickrefineParameter) {
				$dataFilter = ' data-filter="'. htmlentities($filterName, ENT_QUOTES, "UTF-8") .'"';
			}

			echo '<li'. $qrFilterParentClass .'><input id="cp'. $moduleID .'_inpt_'. $parameterName .'_'. $k .'" type="checkbox" value="'.
				htmlentities($filter['name'], ENT_QUOTES, "UTF-8") .'" class="cp-filter-input" data-groupname="'.
				$parameterName .'"'. $checked .' />';
			echo '<label for="cp'. $moduleID .'_inpt_'. $parameterName .'_'. $k .'" class="cp-filter-label">'.
				'<span'. $qrFilterClass . $dataFilter .'>'. $filterName .'</span>';
			if ($showCount)
				echo '<span class="cp-filter-count">('. $filter['count'] .')</span>';
			echo '</label>';
			echo '</li>';
		}


		echo '</ul>';

		// regular See More.. process
		if ($add_seemore_handle)
			$this->printSeeMore($parameterName, 'filter');
		// Ajax See More.. process
		// if ($useSeeMoreAjax && $parameterUseSeeMore) {
		// 	if ($seeMoreAnchor == CP_ANCHOR_TOP) {
		// 		$this->printSeeMore($parameterName, 'filter');
		// 		echo '<ul class="cp-chkb-list hid"></ul>';
		// 	} else {
		// 		echo '<ul class="cp-chkb-list hid"></ul>';
		// 		$this->printSeeMore($parameterName, 'filter');
		// 	}
		// }
		if ($useSeeMoreAjax && ($parameterHiding == CP_PARAMETER_HIDE_USING_SEEMORE ||
			$useSeeMore && $parameterHiding == CP_PARAMETER_HIDING_GLOBAL)) {
			if ($seeMoreAnchor == CP_ANCHOR_TOP) {
				$this->printSeeMore($parameterName, 'filter');
				echo '<ul class="cp-chkb-list hid"></ul>';
			} else {
				echo '<ul class="cp-chkb-list hid"></ul>';
				$this->printSeeMore($parameterName, 'filter');
			}
		}

		if ($parameterUseScroll)
			echo '</div>';

		echo '</div></div></div></div>';
	}
	$itemid = JRequest::getVar('Itemid', ''); 
$url='/~vc2admin/index.php?option=com_hikashop&view=category&layout=listing&Itemid='. $itemid;
echo '<a href="' . $url . '">Reset all filters</a>';
}



// --------------------------------------------------------------------
// Show available MANUFACTURERS
// --------------------------------------------------------------------

$manufacturersCollection = $manufacturers->getCollection();
if ($manufacturersCollection &&
	!($hideParamsWith1Filter && count($manufacturersCollection['mfs']) < 2))
{
	//echo '<pre>';
	//print_r($manufacturersCollection);
	//echo '</pre>';

	echo '<div>';

	//$appliedManufacturersSlug = $manufacturers->currentMCAppliedMFsSlug();
	$appliedManufacturers = $manufacturers->appliedNames();
	$appliedManufacturersAliases = $manufacturers->appliedAliases();
	$manufacturerParameterName = $manufacturers->urlParameterName();
	//$appliedManufacturers = $manufacturers->currentMCAppliedMFsNames();
	$add_seemore_handle = false;

	//$manufacturerCategorySlug = $manufacturers->currentManufacturerCategorySlug();
	//$manufacturerCategoryName = $manufacturers->currentManufacturerCategoryName();


	echo '<input type="hidden" name="'. $manufacturerParameterName .'" value="'.
		htmlentities(implode('|', $appliedManufacturersAliases),
			ENT_QUOTES,
			"UTF-8") .'" class="hidden-filter" />';

	// header
	$appliedAttr = ($appliedManufacturers) ? ' applied="1"' : '';
	echo '<h2 class="cp-chkb-group-header'. $collapseClass .'"'. $appliedAttr .'
		data-name="'. $manufacturerParameterName .'">';
	if ($useCollapse) {
		echo '<span class="cp-group-header-state">';
		echo ($conf->get('default_collapsed') && !$appliedManufacturers) ? '[+]' : '[-]';
		echo '</span>';
	}
	echo '<span class="cp-chkb-group-title">'. $conf->get('mf_title') .'</span>';
	echo '</h2>';

	echo '<div>';

	/* These dedicated class names will be used for filters quickrefine feature,
		so they must be in place */
	if ($quickrefineManufacturers) {
		$qrFilterClass = ' class="cp-qr-filter"';
		$qrFilterParentClass = ' class="cp-qr-filter-parent"';
		$this->printQuickrefineFieldForGroup($manufacturerParameterName, implode('|', $appliedManufacturers));
	} else {
		$qrFilterClass = '';
		$qrFilterParentClass = '';
	}


	echo '<div id="cp'. $moduleID .'_group_'. $manufacturerParameterName .'" class="cp-chkb-filter-group">';
	if ($useScroll) {
		echo '<div class="cp-chkb-scroll-box" style="max-height:'. $scrollHeight .'px">';
	}

	echo '<div class="cp-chkb-padding-cont"><ul class="cp-chkb-list">';

	// list manufacturers as filters
	if ($show_clearlink && $appliedManufacturers) {
		echo '<li><a href="'. $manufacturersCollection['xurl'] .'" class="cp-clearlink">'.
			$conf->get('clear') .'</a></li>';
	}

	foreach ($manufacturersCollection['mfs'] as $k => $mf) {
		// regular See More.. (without Ajax)
		// if ($showNFilters && $k == $show_before) {
		// 	echo '</ul>';
		// 	if ($seeMoreAnchor == CP_ANCHOR_TOP) {
		// 		$this->printSeeMore($manufacturerParameterName, 'manufacturer');
		// 	} else {
		// 		$add_seemore_handle = true;
		// 	}
		// 	echo '<ul class="cp-chkb-list hid">';
		// }

		if ($useSeeMore && !$useSeeMoreAjax && $k == $showBeforeSeeMore) {
			echo '</ul>';
			if ($seeMoreAnchor == CP_ANCHOR_TOP) {
				$this->printSeeMore($manufacturerParameterName, 'manufacturer');
			} else {
				$add_seemore_handle = true;
			}
			echo '<ul class="cp-chkb-list hid">';
		}

		$checked = ($mf['applied']) ? ' checked' : '';

		$dataFilter = '';
		if ($quickrefineManufacturers) {
			$dataFilter = ' data-filter="'. htmlentities($mf['name'], ENT_QUOTES, "UTF-8") .'"';
		}

		echo '<li'. $qrFilterParentClass .'><input id="cp'. $moduleID .'_inpt_'. $manufacturerParameterName .'_'. $k .
			'" type="checkbox" value="'. htmlentities($mf['alias'], ENT_QUOTES, "UTF-8") .'"'. $dataFilter .
			' class="cp-filter-input" data-groupname="'. $manufacturerParameterName .'"'. $checked .' />';
		echo '<label for="cp'. $moduleID .'_inpt_'. $manufacturerParameterName .'_'. $k .
			'" class="cp-filter-label">'.
			'<span'. $qrFilterClass . $dataFilter .'>'. $mf['name'] .'</span>';
		if ($showCount)
			echo '<span class="cp-filter-count">('. $mf['count'] .')</span>';
		echo '</label>';
		echo '</li>';
	}

	echo '</ul>';

	// regular See More.. process
	if ($add_seemore_handle)
		$this->printSeeMore($manufacturerParameterName, 'manufacturer');
	// Ajax See More.. process
	if ($useSeeMore && $useSeeMoreAjax) {
		if ($seeMoreAnchor == CP_ANCHOR_TOP) {
			$this->printSeeMore($manufacturerParameterName, 'manufacturer');
			echo '<ul class="cp-chkb-list hid"></ul>';
		} else {
			echo '<ul class="cp-chkb-list hid"></ul>';
			$this->printSeeMore($manufacturerParameterName, 'manufacturer');
		}
	}

	if ($useScroll)
		echo '</div>';

	echo '</div></div></div>';

	echo '</div>';
}




if ($totalProductsCount && !($conf->get('enable_dynamic_update') && $conf->get('update_each_step'))) {
	echo '<div><button type="submit" class="cp-apply-filters">'.
		$conf->get('apply_filters') .'</button></div>';
}

// if Price form is set to display at Bottom
if ($printPriceForm)
	$this->printPriceForm();

if ($conf->get('show_total_products'))
	$this->printTotalProducts();
echo '</form>';

echo '<div class="cp-checkboxlist-liveresult hid"><div class="cp-dr-arrow cp-dr-arrow-outer-left"></div>'.
	'<div class="cp-dr-arrow cp-dr-arrow-inner-left"></div>'.
	'<div class="cp-dr-cont-outer"><div class="cp-dr-cont-inner">'.
	'<span>'. $conf->get('results') .'</span> <span class="cp-dr-resvalue"></span> '.
	'<span class="cp-dr-go">'. $conf->get('show_results') .'</span></div></div></div>';


echo '</div>';

// echo '<div id="cpDynamicResults" class="hid"><div id="cp-dr-arrow-outer" class="cp-dr-arrow cp-left"></div>'.
// 	'<div id="cp-dr-arrow-inner" class="cp-dr-arrow cp-left"></div>'.
// 	'<div class="cp-dr-cont-outer"><div class="cp-dr-cont-inner">'.
// 	'<span>'. $conf->get('results') .'</span> <span class="cp-dr-resvalue"></span> '.
// 	'<span class="cp-dr-go" onclick="cpSubmitFilters()">'. $conf->get('show_results') .'</span></div></div></div>';

if (! $inquiringWithAjax) {
	// load according CSS file
	$doc = JFactory::getDocument();
	$doc->addStyleSheet($conf->get('module_url') .'static/css/checkboxlist.css');

	$this->loadCheckboxListJavascript = true;
}

?>
