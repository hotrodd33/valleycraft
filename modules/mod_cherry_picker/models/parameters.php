<?php
/**
 * @package     Cherry Picker for HikaShop
 * @subpackage  Parameters
 * @author		Maksym Stefanhuck
 * @copyright   Copyright (C) 2009 - 2014 Galt.md. All rights reserved.
 * @license     GNU General Public License version 2 or later; see COPYING.txt
 */

defined('_JEXEC') or die('Restricted access');

require_once(dirname(__FILE__) .'/../helpers/fsschema.php');
require_once(dirname(__FILE__) ."/../defines.php");

class CPParameter {
	private $_vars = array();
	private $_cherry_picker_attributes;

	function __construct($vars) {
		$this->_vars = $vars;
	}


	public function ptid() {
		return $this->_vars['product_type_id'];
	}

	public function name() {
		return $this->_vars['parameter_name'];
	}

	public function label() {
		return $this->_vars['parameter_label'];
	}

	public function multiAssigned() {
		return ($this->_vars['parameter_type'] == "V" ? true : false);
	}

	public function cherryPickerAttribute($attribute) {
		if (! $this->_cherry_picker_attributes) {
			$this->_cherry_picker_attributes = new JRegistry;
			$this->_cherry_picker_attributes->loadString($this->_vars['cherry_picker_attribs']);
		}
		return $this->_cherry_picker_attributes->get($attribute);
	}




	/*
	* Return type Array
	*/
	public function selectedValues() {
		if (isset($this->_vars['selected_values']))
			return $this->_vars['selected_values'];

		$string = JRequest::getVar($this->name(), '');
		$this->_vars['selected_values'] = $string ?
			explode('|', $string) : array();
		return $this->_vars['selected_values'];
	}


	/*
	* Return type String
	*/
	public function sqlWhere($ignore_mode = false) {
		$selectedValues = $this->selectedValues();
		if (! $selectedValues)
			return '';

		$db = JFactory::getDBO();
		$column = "pt". $this->ptid() .".`". $this->name() ."`";
		$mode = $this->cherryPickerAttribute('mode');
		$s = '';

		if ($mode == CP_TRACKBAR_TWO_KNOBS && !$ignore_mode) {
			$min = $selectedValues[0];
			$max = isset($selectedValues[1]) ? $selectedValues[1] : null;
			$s = $column;
			if ($min && !$max) {
				$s .= ">=". $min;
			} else if (!$min && $max) {
				$s .= "<=". $max;
			} else if ($min == $max) {
				$s .= "=". $min;
			} else {
				$s .= " BETWEEN ". $min ." AND ". $max;
			}
		} else if ($mode == CP_TRACKBAR_ONE_KNOB_COMPARE && !$ignore_mode) {
			$s = $column ."<=". $selectedValues[0];
		} else {
			if ($this->multiAssigned()) {
				$where = array();
				foreach ($selectedValues as $value) {
					$where[] = "FIND_IN_SET(". $db->quote($value) .
						", REPLACE(". $column .", ';', ','))";
					// Use this LIKE comparison instead of FIND_IN_SET if there is a need
					// to search among filters that contain comma character.
					// Make sure to do the same in filterData.php at getParameterWhereClause().
					// $where[] = $ptTableAlias .".`". $parameterName . "` LIKE '%". $value ."%'";
				}
				$s = "(". implode(' OR ', $where) .")";
			} else {
				$s = $column ." IN ('". implode("', '", $selectedValues) ."')";
			}
		}

		return $s;
	}


}


class CPParameters {

	private $_list = array();
	private $_list_of_selected = array();
	private $_sql_joins = array();
	private $_sql_wheres = array();

	function __construct($ptids = array()) {
		$db = JFactory::getDBO();
		$q = "SELECT * FROM ". FSSchema::tbl_parameter() ." as ptp".
			" JOIN ". FSSchema::tbl_producttype() ." as pt USING (`product_type_id`)";

		if ($ptids)
			$q .= " WHERE `product_type_id` IN ('". implode("', '", $ptids) ."')";

		$q .= " ORDER BY `product_type_list_order`, ptp.`product_type_id`, `parameter_list_order`";

		$db->setQuery($q);
		$parameters = $db->loadAssocList();
		foreach ($parameters as $parameterData) {
			$parameter = new CPParameter($parameterData);
			$this->_list[] = $parameter;
		}
	}

	public function getList() {
		return $this->_list;
	}

	public function listOfSelected() {
		if ($this->_list_of_selected)
			return $this->_list_of_selected;

		foreach ($this->_list as $parameter)
			if ($parameter->selectedValues())
				$this->_list_of_selected[] = $parameter;

		return $this->_list_of_selected;
	}


	public function sqlJoins($on_table = 'p') {
		if ($this->_sql_joins)
			return $this->_sql_joins;

		$joins = array();
		$ptids = array();
		foreach ($this->listOfSelected() as $parameter)
			if (! in_array($parameter->ptid(), $ptids))
				$ptids[] = $parameter->ptid();

		foreach ($ptids as $ptid)
			$joins[] = "JOIN ". FSSchema::tbl_producttype_id($ptid) ." as pt". $ptid
				." ON ". $on_table .".`product_id`=pt". $ptid .".`product_id`";

		$this->_sql_joins = $joins;
		return $joins;
	}


	public function sqlWheres($ignore_mode = false) {
		if ($this->_sql_wheres)
			return $this->_sql_wheres;

		$wheres = array();
		foreach ($this->listOfSelected() as $parameter)
			$wheres[] = $parameter->sqlWhere($ignore_mode);

		$this->_sql_wheres = $wheres;
		return $wheres;

	}

}
