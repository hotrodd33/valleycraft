<?php
namespace cherrypicker;
defined('_JEXEC') or die;

use \JRequest;
use \JFactory;

//class Environment extends Filters {
class Environment {
	private static $_instance = null;
	public $categoryId;
	public $manufacturerId;
	public $keyword;
	public $Itemid;
	public $virtuemartView;
	public $lang;
	private $baseUrl;
	private $double_encode = true;
	private $is_sef_enabled = null;


	public static function getInstance() {
		if (! self::$_instance) {
			self::$_instance = new Environment();
		}
		return self::$_instance;
	}


	function __construct() {
		//parent::__construct();
		//$config = Factory::getConfig();

		//$display_mode = $config->get('display_mode');
		//if ( !($display_mode && $display_mode == CP_SHOW_SPECIFIC_PTS) ) {
		//	$this->categoryId = (int)JRequest::getVar('virtuemart_category_id', 0);
		//	$this->manufacturerId = (int)JRequest::getVar('virtuemart_manufacturer_id', 0);
		//	$this->keyword = JRequest::getVar('keyword', null);
		//}

		$this->virtuemartView = JRequest::getVar('view', '');
		$this->lang = JRequest::getVar('lang', '');

		$option = JRequest::getVar('option');
		$itemid = JRequest::getVar('Itemid', null);
		if ($option != 'com_hikashop') {
			$app = JFactory::getApplication();
			$vmMenuItems = $app->getMenu()->getItems('component', 'com_hikashop');
			if ($vmMenuItems) {
				$itemid = $vmMenuItems[0]->id;
			}
		}
		$this->Itemid = $itemid;
	}

	public function baseUrl() {
		$ctrl = JRequest::getVar('ctrl', '');
		$url = 'index.php?';
		if ($ctrl == 'category')
			$url .= 'option=com_hikashop&ctrl=category&task=listing';
		else
			$url .= 'option=com_hikashop&ctrl=product&task=listing';
		$url .= '&start=0';


		//if (! $this->baseUrl) {
			//$url = 'index.php?option=com_virtuemart&view=category';
			//if ($this->categoryId)
			//	$url .= "&virtuemart_category_id=". $this->categoryId;
			//else
			//	$url .= "&search=true";

			//if ($add_search)
				//$url .= "&search=true";


			//if ($mid)
				//$url .= "&virtuemart_manufacturer_id=". $mid;
			if ($this->Itemid)
				$url .= "&Itemid=". $this->Itemid;
			if ($this->keyword)
				$url .= "&searchword=". urlencode($this->keyword);
			if ($this->lang)
				$url .= '&lang='. $this->lang;


			$this->baseUrl = $url;
		//}

		return $this->baseUrl;
	}


	private function encodeUrlEntities($string) {
		$sefEnabled = JFactory::getConfig()->get('sef');
		//$encodeUrl = Factory::getConfig()->get('encode_url');

		/*
		 * When Joomla SEF is enabled we need to double-encode special chars.
		 * This way, when Joomla decodes URL, we end up with a properly
		 * encoded one.
		 */
		if ($sefEnabled && $this->double_encode) {
			$string = str_replace('&', '%2526', $string);
			$string = str_replace('+', '%252B', $string);
		} else {
			$string = str_replace('&', '%26', $string);
			$string = str_replace('+', '%2B', $string);
		}

		return $string;
	}


	public function urlEncode($string) {
		//$sefEnabled = JFactory::getConfig()->get('sef');
		$sefEnabled = $this->isSefEnabled();
		//return $this->encodeUrlEntities($string);
		//return urlencode($this->encodeUrlEntities($string));
		//$v = urlencode($this->encodeUrlEntities($string));
		// Use double (tripple if sef enabled) encoding in order to properly
		// encode non-ASCII characters in URI. encodeUrlEntities() would encode
		// only some chars which is not correct.
		// This double encoding is required, because Joomla's JRoute::_() method
		// will decode urls, and in the end we'll have proper urls.
		$v = urlencode($string);
		$v = urlencode($v);
		if ($sefEnabled)
			$v = urlencode($v);
//echo nl2br("v:$v\n");
		return $v;
	}


	public function shouldDoubleEncodeUrlEntities($option = null) {
		if ($option === null) {
			return $this->double_encode;
		} else {
			$this->double_encode = $option;
		}
	}

	private function isSefEnabled() {
		if ($this->is_sef_enabled === null) {
			$this->is_sef_enabled = JFactory::getConfig()->get('sef');
		}
		return $this->is_sef_enabled;
	}

}
?>
