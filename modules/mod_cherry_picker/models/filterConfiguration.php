<?php
/**
 * @package     Cherry Picker for HikaShop
 * @subpackage  Configuration
 * @author		Maksym Stefanhuck
 * @copyright   Copyright (C) 2009 - 2014 Galt.md. All rights reserved.
 * @license     GNU General Public License version 2 or later; see COPYING.txt
 */

defined( '_JEXEC' ) or die( 'Restricted access' );

class CPFilterConfiguration {

	private $_options = array();
	//private $_assistOptions = array();


	public function initializeOptions($options) {
		$this->_options = $options;
	}

	public function get($key, $default = null) {
		if (!isset($this->_options[$key])) {
			return $default;
			//if ($this->_options['enable_debug'] == true) {
				//echo '<pre>';
				//echo 'Undefinded index: '. $key;
				//echo '</pre>';
				//return false;
			//} else {
				//return false;
			//}
		}

		return $this->_options[$key];
	}


	public function set($key, $value) {
		$this->_options[$key] = $value;
	}


	public function getOptionsForModule($mid) {
		$q = "SELECT `params`, `module` FROM `#__modules` WHERE id='$mid'";
		$db = JFactory::getDBO();
		$db->setQuery($q);
		$module = $db->loadAssoc();

		$params = json_decode($module['params'], true);
		$this->initializeOptions($params);

		$this->set('module_id', $mid);
		$this->set('module_name', $module['module']);
	}


	// this is for assistant options in file
//	public function initAssistOptions() {
//		require(CP_ROOT .'assistOptions.php');
//		$this->_assistOptions = $cpAssistOption;
//	}
//
//
//	public function getAssist($key) {
//		return $this->_assistOptions[$key];
//	}
//
//
//	public function setAssist($key, $value) {
//		$this->_assistOptions[$key] = $value;
//	}
//
//
//	public function writeAssistConfigurationToFile() {
//
//		$s = "<?php \n".
//			"defined('_JEXEC') or die('Restricted access');\n\n";
//
//		foreach ($this->_assistOptions as $key => $value) {
//			//$value = JRequest::getVar($key, 0);
//			if (!is_numeric($value)) $value = "'". $value ."'";
//
//			$s .= "\$cpAssistOption['$key'] = ". $value .";\n";
//		}
//
//		//$s .= "\n? >";
//
//		$conffile = CP_ROOT .'assistOptions.php';
//		if (!chmod($conffile, 0644))
//			return 1;
//		if (!$handle = fopen($conffile, "w"))
//			return 1;
//		if (fwrite($handle, $s) === false)
//			return 1;
//		fclose($handle);
//		chmod($conffile, 0444);
//
//		return 0;
//	}

}
?>
