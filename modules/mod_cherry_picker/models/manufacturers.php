<?php
/**
 * @package     Cherry Picker for HikaShop
 * @subpackage  Manufacturers
 * @author		Maksym Stefanhuck
 * @copyright   Copyright (C) 2009 - 2014 Galt.md. All rights reserved.
 * @license     GNU General Public License version 2 or later; see COPYING.txt
 */

defined('_JEXEC') or die('Restricted access');

//require_once(dirname(__FILE__) .'/../helpers/fsschema.php');
require_once(dirname(__FILE__) ."/../defines.php");

//class CPHikaShopManufacturer {
//	private $_vars = array();
//
//	function __construct($vars) {
//	}
//}


class CPHikaShopManufacturers {
	private $_list = array();
	private $_url_parameter_name;
	private $_sql_wheres = array();

	function __construct($url_parameter_name = 'brand') {
		$this->_url_parameter_name = $url_parameter_name;

		$selected = array();
		$selected_str = JRequest::getVar($this->_url_parameter_name, '');
		if ($selected_str) {
			$selected_array = explode('|', $selected_str);
			foreach ($selected_array as $mf)
				if (trim($mf))
					$selected[] = $mf;
		}

		$cid = (int)JRequest::getVar('cid', 0);
		$db = JFactory::getDBO();
		$q = "SELECT `category_name`, `category_alias` FROM `#__hikashop_category`".
			" WHERE `category_id`='$cid' AND `category_type`='manufacturer'";
		$db->setQuery($q);
		$result = $db->loadAssoc();
		if ($result) {
			$selected[] = $result['category_alias'] ? $result['category_alias']
						: $result['category_name'];
		}

		if ($selected) {
			$q = "SELECT `category_id` as id, `category_name` as name,".
				" `category_alias` as alias".
				" FROM `#__hikashop_category`".
				" WHERE"
				." `category_alias` IN ('".  implode("', '", $selected) ."')"
				." OR `category_name` IN ('".  implode("', '", $selected) ."')";
			$db->setQuery($q);
			$manufacturers = $db->loadObjectList();
			$this->_list = $manufacturers;
		//	foreach ($manufacturers as $mfData) {
		//		$manufacturer = new CPHikaShopManufacturer($mfData);
		//		$this->_list[] = $manufacturer;
		//	}
		}
	}


	public function selectedIds() {
		$ids = array();
		foreach ($this->_list as $manufacturer)
			$ids[] = $manufacturer->id;
		return $ids;
	}


	public function sqlWheres() {
		if ($this->_sql_wheres)
			return $this->_sql_wheres;

		$wheres = array();
		if (($ids = $this->selectedIds())) {
			$wheres = array("`product_manufacturer_id` IN (".
				implode(', ', $ids) .")");
			$this->_sql_wheres = $wheres;
		}

		return $wheres;
	}

}
?>
