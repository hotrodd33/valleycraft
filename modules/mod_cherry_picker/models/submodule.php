<?php
namespace cherrypicker;
defined('_JEXEC') or die;

require_once(dirname(__FILE__) .'/../defines.php');
require_once(CP_BASEPATH .'helpers/factory.php');
require_once(CP_BASEPATH .'helpers/sql.php');

ini_set('display_errors', 1);
error_reporting(E_ALL);


interface SubmoduleInterface {
	/*
	* Could be overriden by subclasses to specify their dependent joins
	* Currently, Prices in certain occasions need Manufacturer table to be joined.
	*/
	//public function needsManufacturerJoin();

	public function submoduleName();
	public function url();
	public function sqlJoins(SqlColumn $on_column);
	public function sqlWheres();

}


//interface DisplayInterface {
//	/*
//	* Return a string name of the submodule.
//	* Value is used to differentiate submodules in a pool.
//	*/
//	public function displayName();
//	public function hasDisplay();
//	// public function listOrder();
//}




/*

    +-----------------------------------------------------------------------+
    |                                                                       |
    |               SUBMODULE                                               |
    |                                                                       |
    +-----------------------------------------------------------------------+
          |                |                  |               |
          |                |                  |               |
    +------------+  +--------------+  +---------------+  +--------+
    | Parameters |	| Customfields |  | Manufacturers |  | Prices | ..
    +------------+  +--------------+  +---------------+  +--------+


*/


abstract class Submodule implements SubmoduleInterface {
	//private static $_instance = null;
	private static $_submodules = array();
	//public $module_id;
	//public $module_name;
	//public $module_url;
	//public $config;
	// dependencies should too be gone (moved to SubmoduleView)
	protected $_dependencies = array();
	protected $_activated = true;


//	public static function getInstance() {
//echo nl2br("\n\n\n");
//echo get_called_class();
//	$class= get_called_class();
//			$ins = new $class;
//			self::$_submodules[] = $ins;
////		if (! self::$_instance) {
////			echo 'creating submodule';
////			self::$_instance = new $class;
////			self::$_submodules[] = self::$_instance;
////		}
//var_dump(count(self::$_submodules));
//return $ins;
//		return self::$_instance;
//	}


	final public static function add(Submodule $submodule) {
		if (! in_array($submodule, self::$_submodules))
			self::$_submodules[] = $submodule;
	}

	final public static function submodules() {
		return self::$_submodules;
	}


	function __construct() {
	//	$config = Factory::getConfig();
	//	$this->config = $config;
	//	$this->module_id = $config->get('module_id');
	//	$this->module_name = $config->get('module_name');
	//	$this->module_url = $config->get('module_url');
	}


	abstract public function submoduleName();
	abstract public function url();
	abstract public function sqlJoins(SqlColumn $on_column);
	abstract public function sqlWheres();
	//abstract public function activate();
	//abstract public function deactivate();



	public function activate() {
		$this->_activated = true;
	}

	public function deactivate() {
		$this->_activated = false;
	}

	public function activated() {
		return $this->_activated;
	}




	//public function needsManufacturerJoin() {
	//	return false;
	//}


	//public function useConfig($config) {
		//$this->config = $config;
	//}


	public function setDependencies($dependencies = array()) {
		$this->_dependencies = $dependencies;
	}

	public function addDependency($dependency) {
		if (! in_array($dependency, $this->_dependencies))
			$this->_dependencies[] = $dependency;
	}

	public function dependencies() {
		return $this->_dependencies;
	}

	/*
	 * To be overriden by other submodules to let other submodules know about
	 * dependencies.
	 * For example:
	 * In Virtuemart prices may need manufacturers table join when prices need
	 * to do manufacturer calculation rules.
	 */
	public function needsDependency(Submodule $submodule) {
		return false;
	}

}
