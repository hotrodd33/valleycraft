<?php // Extension params save file for sh404sef
    //
    if (!defined('_JEXEC')) die('Direct Access to this location is not allowed.');
$shConfig = array (
  'extension_id' => '10252',
  'package_id' => '0',
  'name' => 'sh404sef - Default component support plugin',
  'type' => 'plugin',
  'element' => 'sh404sefextplugindefault',
  'folder' => 'sh404sefextplugins',
  'client_id' => '0',
  'enabled' => '0',
  'access' => '1',
  'protected' => '0',
  'manifest_cache' => '{"name":"sh404sef - Default component support plugin","type":"plugin","creationDate":"2016-10-31","author":"Yannick Gaultier","copyright":"(c) Yannick Gaultier - Weeblr llc - 2016","authorEmail":"contact@weeblr.com","authorUrl":"https:\\/\\/weeblr.com","version":"4.8.1.3465","description":"Provide default support for sef urls and meta data","group":"","filename":"sh404sefextplugindefault"}',
  'params' => '{}',
  'custom_data' => '',
  'system_data' => '',
  'checked_out' => '0',
  'checked_out_time' => '0000-00-00 00:00:00',
  'ordering' => '10',
  'state' => '0',
);
