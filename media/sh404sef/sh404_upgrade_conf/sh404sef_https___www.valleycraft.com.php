<?php // Extension params save file for sh404sef
    //
    if (!defined('_JEXEC')) die('Direct Access to this location is not allowed.');
$shConfig = array (
  'package_id' => '0',
  'name' => 'sh404sef - System plugin',
  'type' => 'plugin',
  'element' => 'sh404sef',
  'folder' => 'system',
  'client_id' => '0',
  'enabled' => '0',
  'access' => '1',
  'protected' => '0',
  'manifest_cache' => '{"name":"sh404sef - System plugin","type":"plugin","creationDate":"2016-10-31","author":"Yannick Gaultier","copyright":"(c) Yannick Gaultier - Weeblr llc - 2016","authorEmail":"contact@weeblr.com","authorUrl":"https:\\/\\/weeblr.com","version":"4.8.1.3465","description":"Sh404sef main system plugin","group":"","filename":"sh404sef"}',
  'params' => '{}',
  'custom_data' => '',
  'system_data' => '',
  'checked_out' => '0',
  'checked_out_time' => '0000-00-00 00:00:00',
  'ordering' => '64',
  'state' => '0',
);
