<html>

<head>
<?php require_once 'connect.php';
	  require_once 'functions.php';
	//$link = f_sqlConnect(DB_USER, DB_PASSWORD, DB_NAME);
	?>
    <?php include 'head.php'; ?>        

</head>

<body>
    <div id="wrapper" class="container">
        <?php include 'header.php'; ?>
        <div id="menu" class="row noprint">
            <ul>
                <li><a href="signs-order-form.php">Start New Order</a></li>
                <li><a href="#">Open Existing Order</a></li>
                <li><a href="#">Display All Orders</a></li>
                <li><a href="#">Save Current Order</a></li>
                <li><a href="#" onclick="window.print()">Print Current Order</a></li>
            </ul>
        </div>
        <div id="content">
            <?php //$order_number = get_next_counter('invoice_number'); ?>
            <form action="process.php" id="orderform" method="post">
				<input type="hidden" name="formID" id="formID" value="craft_orders" />
                <div id="orderTop" class="row">
					<div id="orderHeader" class="text-left col-xs-6">
                        <label>FASTSIGNS Center #</label><input class="underline" name="storeNum" id="storeNum" type="text" tabindex=1 /><br />
						<label>Customer Name</label><input class="underline" name="custName" id="custName" type="text" tabindex=2 />
                    </div>
                    <div id="orderNumber" class="text-right col-xs-6">
						<label>Order Date</label><input name="orderDate" class="underline" id="orderDate" type="text" tabindex=1 /><br />
						<label>Order Number</label><input class="underline" selected name="orderNum" id="orderNum" value="<?php echo $order_number; ?>" type="text" /><br />
						<label>Zip Code</label><input class="underline" name="orderZip" id="storeNum" type="text" tabindex=3 />
					</div>
                    
                </div>
                <div id="orderContentHeader" class="row">
                    <div class="col-xs-4">Product Name</div>
                    <div class="col-xs-2 text-right">MSRP</div>
                    <div class="col-xs-2 text-right">FASTSIGNS Price</div>
                    <div class="col-xs-2">Order Qty</div>
                    <div class="col-xs-2">Ext Price</div>
                </div>
                <div id="orderContent1" class="row">
                    <div class="row main">
                        <div class="col-xs-12"><strong>Media Rack (Minimum of 2 for FREE shipping)</strong></div>
                    </div>
                    <div class="row sub">
						<input type="hidden" name="sku1" value="F89284" />
						<input type="hidden" id="prod1" value="Media Rack" />
						<input type="hidden" name="spec1" value="12''W x 50''L" />
                        <div class="col-xs-4 text-right">12''W x 50''L</div>
                        <div id="msrp1" value="149" class="col-xs-2 currency">149</div>
                        <div class="col-xs-2 currency"><input class="text-center input price" name="price1" id="price1" type="text" value="134" /></div>
                        <div class="col-xs-2"><input class="text-center input" name="qty1" id="qty1" type="text" tabindex=4 /></div>
                        <div class="col-xs-2 currency"><input class="text-center readonly sum" name="ttl1" id="ttl1" type="text" /></div>
                    </div>
                </div>
                <div id="orderContent2" class="row">
                    <div class="row main">
                        <div class="col-xs-12"><strong>Louvered Wall Panels - 2 per pack</strong></div>
                    </div>
                    <div class="row sub">
						<input type="hidden" name="prod2" value="Lovered Wall Panels - 2 per pack" />
						<input type="hidden" name="spec2" value="18''W x 24''H" />
                        <div class="col-xs-3 text-right">18''W x 24''H</div>
						<div class="col-xs-1">
							<select name="sku2">
								<option>Color</option>
								<option value="F85256A7">Black</option>
								<option value="F85256WW">White</option>								
							</select>
						</div>
                        <div id="msrp2" class="col-xs-2 currency">57</div>
                        <div class="col-xs-2 currency"><input class="text-center input price" name="price2" id="price2" type="text" value="52" /></div>
                        <div class="col-xs-2"><input class="text-center input" name="qty2" id="qty2" type="text" tabindex=5 /></div>
                        <div class="col-xs-2 currency total"><input class="text-center readonly sum" name="ttl2" id="ttl2" type="text" /></div>
                    </div>
                    <div class="row sub">
						<input type="hidden" name="prod3" value="Lovered Wall Panels - 2 per pack" />
						<input type="hidden" name="spec3" value="24''W x 24''H" />
                        <div class="col-xs-3 text-right">24''W x 24''H</div>
						<div class="col-xs-1">
							<select name="sku3">
								<option>Color</option>
								<option value="F85225A5">Black</option>
								<option value="F85225WW">White</option>								
							</select>
						</div>
                        <div id="msrp3" class="col-xs-2 currency">69</div>
                        <div class="col-xs-2 currency"><input class="text-center input price" name="price3" id="price3" type="text" value="62" /></div>
                        <div class="col-xs-2"><input class="text-center input" name="qty3" id="qty3" type="text" tabindex=6 /></div>
                        <div class="col-xs-2 currency total"><input class="text-center readonly sum" name="ttl3" id="ttl3" type="text" /></div>
                    </div>
                    <div class="row main">
                        <div class="col-xs-12"><strong>Pegboard Wall Panels - 2 per pack</strong></div>
                    </div>
                    <div class="row sub">
						<input type="hidden" id="prod4" value="PegBoard Wall Panels - 2 per pack" />
						<input type="hidden" id="spec4" value="18''W x 24''H" />
                        <div class="col-xs-3 text-right">18''W x 24''H</div>
						<div class="col-xs-1">
							<select id="sku4">
								<option>Color</option>
								<option value="F85258A5">Black</option>
								<option value="F85258WW">White</option>								
							</select>
						</div>							
                        <div id="msrp4" class="col-xs-2 currency">81</div>
                        <div class="col-xs-2 currency"><input class="text-center input price" name="price4" id="price4" type="text" value="73" /></div>
                        <div class="col-xs-2"><input class="text-center input" name="qty4" id="qty4" type="text"  tabindex=7 /></div>
                        <div class="col-xs-2 currency total"><input class="text-center readonly sum" name="ttl4" id="ttl4" type="text" /></div>
                    </div>
                    <div class="row sub">
						<input type="hidden" id="prod5" value="PegBoard Wall Panels - 2 per pack" />
						<input type="hidden" id="spec5" value="18''W x 24''H" />
                        <div class="col-xs-3 text-right">24''W x 24''H</div>
						<div class="col-xs-1">
							<select id="sku5">
								<option>Color</option>
								<option value="F85229A1">Black</option>
								<option value="F85229WW">White</option>								
							</select>
						</div>	
                        <div id="msrp5" class="col-xs-2 currency">94</div>
                        <div class="col-xs-2 currency"><input class="text-center input price" name="price5" id="price5" type="text" value="85" /></div>
                        <div class="col-xs-2"><input class="text-center input" name="qty5" id="qty5" type="text" tabindex=8 /></div>
                        <div class="col-xs-2 currency total"><input class="text-center readonly sum" name="ttl5" id="ttl5" type="text" /></div>
                    </div>
                </div>
                <div id="orderContent3" class="row">
                    <div class="row main">
						<div class="col-xs-12"><strong>A-Frame Sign</strong></div>
                    </div>
                    <div class="row sub">						
						<input type="hidden" id="prod6" value="A-Frame Sign" />
						<input type="hidden" id="spec6" value="14''W x 36''L" />
                        <div class="col-xs-3 text-right" value="Standard">View Area 24''W x 36''H</div>
						<div class="col-xs-1">
							<select id="sku6">
								<option>Color</option>
								<option value="F89288">White</option>
								<option value="F89289">Black</option>
								<option value="F89290">Red</option>			
							</select>							
						</div>
                        <div id="msrp6" class="col-xs-2 currency">289</div>
                        <div class="col-xs-2 currency"><input class="text-center input price" name="price6" id="price6" type="text" value="156" /></div>
                        <div class="col-xs-2"><input class="text-center input" name="qty6" id="qty6" type="text" tabindex=9 /></div>
                        <div class="col-xs-2 currency total"><input class="text-center readonly sum" name="ttl6" id="ttl6" type="text" /></div>
                    </div>
                    <div class="row sub">
						<input type="hidden" id="prod7" value="A-Frame Sign" />
						<input type="hidden" id="spec7" value="28''W x 42''H" />
                        <div class="col-xs-3 text-right" value="Large">View Area 28''W x 42''H</div>
						<div class="col-xs-1">
							<select id="sku7">
								<option>Color</option>								
								<option value="F89291">White</option>
								<option value="F89292">Black</option>
								<option value="F89293">Red</option>								
							</select>							
						</div>
                        <div id="msrp7" class="col-xs-2 currency">299</div>
                        <div class="col-xs-2 currency"><input class="text-center input price" name="price7" id="price7" type="text" value="162" /></div>
                        <div class="col-xs-2"><input class="text-center input" name="qty7" id="qty7" type="text" tabindex=10 /></div>
                        <div class="col-xs-2 currency total"><input class="text-center readonly sum" name="ttl7" id="ttl7" type="text" /></div>
                    </div>
                </div>
                <div id="orderContent4" class="row">
                    <div class="row main">
                        <div class="col-xs-12"><strong>LED Display Mount</strong></div>
                    </div>
					<div class="row sub">
						<input type="hidden" id="sku8" value="F88366" />
						<input type="hidden" id="prod8" value="LED Display Mount" />
						<input type="hidden" id="spec8" value="24''W x 36''H" />
                        <div class="col-xs-4 text-right">Frame 24''W x 36''H</div>
                        <div id="msrp8" class="col-xs-2 currency">369</div>
                        <div class="col-xs-2 currency"><input class="text-center input price" name="price8" id="price8" type="text" value="198" /></div>
                        <div class="col-xs-2"><input class="text-center input" name="qty8" id="qty8" type="text" tabindex=11 /></div>
                        <div class="col-xs-2 currency total"><input class="text-center readonly sum" name="ttl8" id="ttl8" type="text" /></div>
                    </div>
                    <div class="row sub">
						<input type="hidden" id="sku9" value="F88367" />
						<input type="hidden" id="prod9" value="LED Display Mount" />
						<input type="hidden" id="spec9" value="36''W x 48''H" />
                        <div class="col-xs-4 text-right">Frame 36''W x 48''H</div>
                        <div id="msrp9" class="col-xs-2 currency">375</div>
                        <div class="col-xs-2 currency"><input class="text-center input price" name="price9" id="price9" type="text" value="203" /></div>
                        <div class="col-xs-2"><input class="text-center input" name="qty9" id="qty9" type="text" tabindex=12 /></div>
                        <div class="col-xs-2 currency total"><input class="text-center readonly sum" name="ttl9" id="ttl9" type="text" /></div>
                    </div>                    
                </div>
                <div id="orderContent5" class="row">
                    <div class="row main">
                        <div class="col-xs-12"><strong>Printer Table</strong></div>
                    </div>
                    <div class="row sub">
						<input type="hidden" id="sku10" value="F82901A3" />
						<input type="hidden" id="prod10" value="Printer Table" />
						<input type="hidden" id="spec10" value="48''W x 48''D" />
                        <div class="col-xs-4 text-right">Table Top 48''W x 48''D</div>
                        <div id="msrp10" class="col-xs-2 currency">635</div>
                        <div class="col-xs-2 currency"><input class="text-center input price" name="price10" id="price10" type="text" value="572" /></div>
                        <div class="col-xs-2"><input class="text-center input" name="qty10" id="qty10" type="text" tabindex=13 /></div>
                        <div class="col-xs-2 currency total"><input class="text-center readonly sum" name="ttl10" id="ttl10" type="text" /></div>
                    </div>
                    <div class="row sub">
						<input type="hidden" id="sku11" value="F82902A2" />
						<input type="hidden" id="prod11" value="Printer Table" />
						<input type="hidden" id="spec11" value="72''W x 48''D" />
                        <div class="col-xs-4 text-right">Table Top 72''W x 48''D</div>
                        <div id="msrp11" class="col-xs-2 currency">794</div>
                        <div class="col-xs-2 currency"><input class="text-center input price" name="price11" id="price11" type="text" value="715" /></div>
                        <div class="col-xs-2"><input class="text-center input" name="qty11" id="qty11" type="text" tabindex=14 /></div>
                        <div class="col-xs-2 currency total"><input class="text-center readonly sum" name="ttl11" id="ttl11" type="text" /></div>
                    </div>
                </div>
				<div id="totalsContent" class="row">
                    <div class="row" style="border-top: 1px solid #111;">
						<div class="col-xs-8">
							<div id="comments" class="row"><textarea class="comments" name="comments" id="comments1" type="text" rows="3" cols="60">Additional Comments</textarea></div>
						</div>
						<div class="col-xs-4">
							<div class="col-xs-12">
								<label>Total Due:</label><input class="text-center readonly price ttlprice" name="ttlprice" id="ttlprice" type="text" />
							</div>
							<div class="col-xs-12">
								<label>Amt Paid:</label><input class="text-center price ttlprice ttlpaid" name="ttlpaid" id="ttlpaid" type="text" />
							</div>
						</div>						
					</div>                                       
                </div>
				<div id="formSubmit" class="noprint"><input type="submit" class="button" name="Submit" value="Submit" /><input type="button" class="button" value="Print Order" onClick="window.print()"></div>
            </form>
        </div>
        <div id="footer">
			<div id="signature" class="row underline">Customer Signature</div>
        </div>
    </div>
	
</body>

<script src="scripts.js"></script>	


</html>