/* Select all text when input clicked */

$(".input").on("click", function () {
   $(this).select();
});
$(".comments").on("click", function () {
   $(this).select();
});

$(document).ready(function() {
  $(window).keydown(function(event){
    if(event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });
});

/* Calculate Totals */
  


$('.readonly').attr('readonly', 'readonly');
$(document).ready(function() {
    //this calculates values automatically 
    calculateSum();

    $(".input").change("keydown keyup", function() {
        calculateSum();
    });
});



function calculateSum() {
    var sum = 0;
    //iterate through each textboxes and add the values
    $(".sum").each(function() {
        //add only if the value is number
        if (!isNaN(this.value) && this.value.length != 0) {
            sum += parseFloat(this.value);				
        }        
    });
	console.log(sum.toFixed(2));
    $("#ttlprice").val(sum.toFixed(2));
}

$(document).ready(function(){
	var now = new Date();
 
    var day = ("0" + now.getDate()).slice(-2);
    var month = ("0" + (now.getMonth() + 1)).slice(-2);

    var today = now.getFullYear()+"-"+(month)+"-"+(day) ;


   $('#orderDate').val(today);
	$("#qty1").keyup(function(){
		var price1 = parseFloat($("#price1").val());
		var qty1 = parseFloat($("#qty1").val());
		var ttl1 = qty1*price1;	
		if(isNaN(ttl1)) {
				var ttl1 = 0;
		}
		$("#ttl1").val(ttl1.toFixed(2));
	});
	$("#qty2").keyup(function(){
		var price2 = parseFloat($("#price2").val());
		var qty2 = parseFloat($("#qty2").val());
		var ttl2 = qty2*price2;
		if(isNaN(ttl2)) {
				var ttl2 = 0;
				return;
		}
		$("#ttl2").val(ttl2.toFixed(2));
	});
	$("#qty3").keyup(function(){
		var price3 = parseFloat($("#price3").val());
		var qty3 = parseFloat($("#qty3").val());
		var ttl3 = qty3*price3;
		if(isNaN(ttl3)) {
				var ttl3 = 0;
		}
		$("#ttl3").val(ttl3.toFixed(2));
	});
	$("#qty4").keyup(function(){
		var price4 = parseFloat($("#price4").val());
		var qty4 = parseFloat($("#qty4").val());
		var ttl4 = qty4*price4;
		if(isNaN(ttl4)) {
				var ttl4 = 0;
		}
		$("#ttl4").val(ttl4.toFixed(2));
	});
	$("#qty5").keyup(function(){
		var price5 = parseFloat($("#price5").val());
		var qty5 = parseFloat($("#qty5").val());
		var ttl5 = qty5*price5;
		if(isNaN(ttl5)) {
				var ttl5 = 0;
		}
		$("#ttl5").val(ttl5.toFixed(2));
	});
	$("#qty6").keyup(function(){
		var price6 = parseFloat($("#price6").val());
		var qty6 = parseFloat($("#qty6").val());
		var ttl6 = qty6*price6;
		if(isNaN(ttl6)) {
				var ttl6 = 0;
		}
		$("#ttl6").val(ttl6.toFixed(2));
	});
	$("#qty7").keyup(function(){
		var price7 = parseFloat($("#price7").val());
		var qty7 = parseFloat($("#qty7").val());
		var ttl7 = qty7*price7;		
		if(isNaN(ttl7)) {
				var ttl7 = 0;
		}
		$("#ttl7").val(ttl7.toFixed(2));
	});
	$("#qty8").keyup(function(){
		var price8 = parseFloat($("#price8").val());
		var qty8 = parseFloat($("#qty8").val());
		var ttl8 = qty8*price8;		
		if(isNaN(ttl8)) {
				var ttl8 = 0;
		}
		$("#ttl8").val(ttl8.toFixed(2));
	});
	$("#qty9").keyup(function(){
		var price9 = parseFloat($("#price9").val());
		var qty9 = parseFloat($("#qty9").val());
		var ttl9 = qty9*price9;
		if(isNaN(ttl9)) {
				var ttl9 = 0;
		}
		$("#ttl9").val(ttl9.toFixed(2));
	});
	$("#qty10").keyup(function(){
		var price10 = parseFloat($("#price10").val());
		var qty10 = parseFloat($("#qty10").val());
		var ttl10 = qty10*price10;
		if(isNaN(ttl10)) {
				var ttl10 = 0;
		}
		$("#ttl10").val(ttl10.toFixed(2));
	});
	$("#qty11").keyup(function(){
		var price11 = parseFloat($("#price11").val());
		var qty11 = parseFloat($("#qty11").val());
		var ttl11 = qty11*price11;
		if(isNaN(ttl11)) {
				var ttl11 = 0;
		}		
		$("#ttl11").val(ttl11.toFixed(2));
	});	
});